import type { Config } from 'tailwindcss';

export default {
  content: ['./src/**/*.{html,js,ts}'],
  theme: {
    extend: {
      colors: {
        background: '#f3f6fe',
        'blue-light': '#0086cd',
        'blue-dark': '#00679e',
        'red-light': '#e51b33',
        'red-dark': '#980012',
        'light-text': '#f9f9f9',
        'dark-text': '#505050',
        border: '#B8CED9',

        'garbageCollection-restmuell': '#545454',
        'garbageCollection-papier': '#4D7EB5',
        'garbageCollection-gelber_sack': '#FFC32C',
        'garbageCollection-biomuell': '#68533A',
      },
      fontFamily: {
        sans: ['Roboto', 'sans-serif'],
      },
    },
  },
  plugins: [],
} satisfies Config;
