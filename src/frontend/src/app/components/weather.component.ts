import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { distinct, map, switchMap, timer } from 'rxjs';
import {
  selectWeatherForecasts,
  selectWeatherReport,
} from '../stores/weather/weather.selectors';
import { weatherIcon } from '../weather';

@Component({
  selector: 'app-wetter',
  template: `
    <div *ngIf="report$ | async; let report">
      <div class="mx-12 flex flex-row items-center justify-around">
        <img
          class="h-20"
          [src]="
            'assets/images/weather/' +
            iconToImage(report.icon, report.timestamp)
          "
        />
        <div class="ml-4 flex items-start text-7xl font-bold text-dark-text">
          {{ report.temperature | number : '1.0-0' }}
          <span class="text-4xl">°C</span>
        </div>
      </div>
      <div class="text-center text-2xl text-gray-400">
        {{ 'weather.' + report.icon | translate }}
      </div>
    </div>

    <div
      *ngIf="forecasts$ | async; let forecasts"
      class="flex flex-row flex-nowrap font-semibold text-gray-400"
    >
      <div
        *ngFor="let forecast of forecasts"
        class="flex flex-col items-center border-r-2 border-gray-200 px-6 last:border-none"
      >
        <div>{{ forecast.timestamp | date : 'HH:mm' }}</div>
        <div>
          <img
            class="mt-2 w-12"
            [src]="
              'assets/images/weather/' +
              iconToImage(forecast.icon, forecast.timestamp)
            "
          />
        </div>
        <div class="text-xl">
          {{ forecast.temperature | number : '1.0-0' }} °C
        </div>
      </div>
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex flex-row flex-grow justify-around items-center;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WeatherComponent {
  date$ = timer(0, 1000).pipe(
    map((_) => new Date()),
    distinct((_) => {
      const roundedDate = new Date(_);
      roundedDate.setMinutes(0, 0, 0);
      return roundedDate.getTime();
    })
  );

  report$ = this.store.select(selectWeatherReport);

  forecastHours = 5;

  forecasts$ = this.date$.pipe(
    switchMap((date) =>
      this.store.select(selectWeatherForecasts(date, this.forecastHours))
    )
  );

  constructor(private store: Store) {}

  iconToImage(icon: string | undefined, timestamp: Date): string {
    const hours = timestamp.getHours();
    const isNight = hours >= 18 || hours <= 8;
    return weatherIcon(icon as string, isNight);
  }
}
