import { Component, Renderer2 } from '@angular/core';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { enable } from '../stores/screen-saver/screen-saver.actions';
import { selectEnabled } from '../stores/screen-saver/screen-saver.selectors';

@Component({
  selector: 'app-bedienung',
  template: `
    <div class="flex flex-row justify-between">
      <button
        (click)="englisch()"
        class="button w-[5.563rem] py-1 px-3"
        trackFeature
        [feature]="'sprache-wechsel'"
      >
        {{ 'sprachWechselButton' | translate }}
      </button>

      <button
        (click)="arabisch()"
        class="button w-[5.563rem] px-3"
        trackFeature
        [feature]="'sprache-wechsel'"
      >
        {{ 'sprachWechselButton2' | translate }}
      </button>

      <button
        class="button p1 flex items-center px-1"
        (click)="changeFontSize('increase')"
        trackFeature
        [feature]="'zoom-plus'"
      >
        <i class="mi filled text-3xl text-blue-light">zoom_in</i>
      </button>

      <button
        class="button p1 flex items-center px-1"
        (click)="changeFontSize('decrease')"
        trackFeature
        [feature]="'zoom-minus'"
      >
        <i class="mi filled text-3xl text-blue-light">zoom_out</i>
      </button>

      <button
        class="button p1 flex items-center px-1"
        (click)="startScreensaver()"
        trackFeature
        [feature]="'screensaver'"
      >
        <i class="mi filled text-3xl text-blue-light">info_outline</i>
      </button>
      <div *ngIf="screensaverActive$ | async">
        {{ resetFontSize() }}
      </div>
    </div>
  `,
  styles: [
    `
      .button {
        @apply bg-white shadow-md border-2 border-blue-light rounded-lg text-blue-light font-medium text-[1-25rem];
      }
    `,
  ],
})
export class ServiceComponent {
  screensaverActive$ = this.store.select(selectEnabled);
  constructor(
    private translate: TranslateService,
    private renderer: Renderer2,
    private store: Store
  ) {
    translate.setDefaultLang('de');
    (this.translate.currentLang = 'de'),
      this.renderer.removeClass(document.documentElement, 'rtl');
  }

  private currentZoom = 100;

  englisch() {
    const currentLang = this.translate.currentLang;
    if (currentLang === 'de' || currentLang === 'ar') {
      this.translate.use('en'),
        this.renderer.removeClass(document.documentElement, 'rtl');
    } else {
      this.translate.use('de');
    }
  }
  arabisch() {
    const currentLang = this.translate.currentLang;
    if (currentLang === 'de' || currentLang === 'en') {
      this.translate.use('ar'),
        this.renderer.addClass(document.documentElement, 'rtl');
    } else {
      this.translate.use('de'),
        this.renderer.removeClass(document.documentElement, 'rtl');
    }
  }
  zoomIn(): void {
    this.currentZoom += 10;
    this.updateZoom();
  }

  zoomOut(): void {
    if (this.currentZoom > 100) {
      this.currentZoom -= 10;
      this.updateZoom();
    }
  }

  private updateZoom(): void {
    const transformValue = `scale(${this.currentZoom / 100})`;
    document.body.style.transform = transformValue;
    document.body.style.transformOrigin = '0 0';
  }

  fontSize: string = 'medium-font';
  changeFontSize(action: string) {
    if (action === 'increase') {
      switch (this.fontSize) {
        case 'medium-font':
          this.fontSize = 'large-font';
          break;
        case 'large-font':
          this.fontSize = 'xlarge-font';
          break;
        case 'xlarge-font':
          this.fontSize = 'xxlarge-font';
          break;
        case 'xxlarge-font':
          this.fontSize = 'xxxlarge-font';
          break;
        case 'xxxlarge-font':
          this.fontSize = 'xxxxlarge-font';
          break;
        default:
          break;
      }
    } else if (action === 'decrease') {
      switch (this.fontSize) {
        case 'xxxxlarge-font':
          this.fontSize = 'xxxlarge-font';
          break;
        case 'xxxlarge-font':
          this.fontSize = 'xxlarge-font';
          break;
        case 'xxlarge-font':
          this.fontSize = 'xlarge-font';
          break;
        case 'xlarge-font':
          this.fontSize = 'large-font';
          break;
        case 'large-font':
          this.fontSize = 'medium-font';
          break;
        default:
          break;
      }
    }

    this.applyFontSize();
  }

  applyFontSize() {
    [
      'medium-font',
      'large-font',
      'xlarge-font',
      'xxlarge-font',
      'xxxlarge-font',
      'xxxxlarge-font',
    ].forEach((className) => {
      this.renderer.removeClass(document.documentElement, className);
    });

    this.renderer.addClass(document.documentElement, this.fontSize);
  }

  resetFontSize() {
    [
      'medium-font',
      'large-font',
      'xlarge-font',
      'xxlarge-font',
      'xxxlarge-font',
      'xxxxlarge-font',
    ].forEach((className) => {
      this.renderer.removeClass(document.documentElement, className);
    });
    this.fontSize = 'medium-font';
  }

  startScreensaver() {
    this.store.dispatch(enable());
  }
}
