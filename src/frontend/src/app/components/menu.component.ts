import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  selectBoardIcon,
  selectMenuItemActive,
  selectMonitorName,
} from '../stores/monitor/monitor.selectors';
import * as screenSaverSelectors from '../stores/screen-saver/screen-saver.selectors';
import * as screenSaverActions from '../stores/screen-saver/screen-saver.actions';

@Component({
  selector: 'app-menue',
  template: `
    <div class="flex max-h-80 flex-col justify-between gap-4">
      <div class="flex justify-center">
        <img
          [src]="boardLogo$ | async"
          class="h-[16rem] w-[16rem] object-contain"
        />
      </div>

      <div class="flex justify-center">
        <span
          class="inline-flex items-center rounded-lg bg-red-light px-3 py-1 font-bold text-white shadow-md"
        >
          {{ name$ | async }}
        </span>
      </div>
    </div>

    <div class="mt-2 flex flex-grow flex-col justify-center">
      <div class="relative left-[-2.188rem] flex flex-col gap-5">
        <li
          *ngIf="selectOurHauseActive$ | async"
          [class.hand]="showHandOurHouse$ | async"
          routerLinkActive="active"
          (click)="disabledScreenSaver()"
          trackNavigation
          [page]="'unser-haus'"
        >
          <img
            *ngIf="showHandOurHouse$ | async"
            class="absolute -right-6 top-4"
            style="width: 6rem"
            src="../assets/images/screensaver/finger-icon.svg"
            alt="Hand"
          />
          <a
            class="flex flex-row items-center pl-14"
            routerLink="/seiten/unser-haus"
          >
            <i class="mi filled text-3xl text-light-text">{{
              'icon-haus' | translate
            }}</i>
            <p>{{ 'haus' | translate }}</p>
          </a>
        </li>

        <li
          *ngIf="selectPublicTransportActive$ | async"
          [class.hand]="showHandPublicTransport$ | async"
          routerLinkActive="active"
          (click)="disabledScreenSaver()"
          trackNavigation
          [page]="'oepnv'"
        >
          <img
            *ngIf="showHandPublicTransport$ | async"
            class="absolute -right-6 top-4"
            style="width: 6rem"
            src="../assets/images/screensaver/finger-icon.svg"
            alt="Hand"
          />
          <a class="flex flex-row items-center pl-14 pr-4" routerLink="oepnv">
            <i class="mi filled text-3xl text-light-text">{{
              'icon-oepnv' | translate
            }}</i>
            <p>{{ 'oepnv' | translate }}</p>
          </a>
        </li>

        <li
          *ngIf="selectNewsActive$ | async"
          [class.hand]="showHandNews$ | async"
          routerLinkActive="active"
          (click)="disabledScreenSaver()"
          trackNavigation
          [page]="'nachrichten'"
        >
          <img
            *ngIf="showHandNews$ | async"
            class="absolute -right-6 top-4"
            style="width: 6rem"
            src="../assets/images/screensaver/finger-icon.svg"
            alt="Hand"
          />
          <a class="flex flex-row items-center pl-14" routerLink="nachrichten">
            <i class="mi filled text-3xl text-light-text">{{
              'icon-nachrichten' | translate
            }}</i>
            <p>{{ 'nachrichten' | translate }}</p>
          </a>
        </li>

        <li
          *ngIf="selectCultureActive$ | async"
          [class.hand]="showHandCulture$ | async"
          routerLinkActive="active"
          (click)="disabledScreenSaver()"
          trackNavigation
          [page]="'kultur'"
        >
          <img
            *ngIf="showHandCulture$ | async"
            class="absolute -right-6 top-4"
            style="width: 6rem"
            src="../assets/images/screensaver/finger-icon.svg"
            alt="Hand"
          />
          <a class="flex flex-row items-center pl-14" routerLink="kultur">
            <i class="mi filled text-3xl text-light-text">{{
              'icon-kultur' | translate
            }}</i>
            <p>{{ 'kultur' | translate }}</p>
          </a>
        </li>
      </div>
    </div>
  `,
  styles: [
    `
      li {
        @apply list-none ease-in-out duration-300 bg-blue-light border-blue-dark border-2 rounded-r-lg shadow-md hover:cursor-pointer text-white font-bold text-[1.375rem];
      }
      a {
        @apply py-2;
      }
      p {
        @apply pl-3;
      }
      .active {
        @apply border-red-dark bg-red-light;
      }
      img[src$='.svg'] {
        width: 1.5rem;
      }
      .hand {
        @apply border-red-dark bg-red-light relative;
      }
      :host {
        @apply flex flex-grow h-full flex-col;
      }
    `,
  ],
})
export class MenuComponent {
  name$ = this.store.select(selectMonitorName);
  boardLogo$ = this.store.select(selectBoardIcon);
  showHandOurHouse$ = this.store.select(
    screenSaverSelectors.selectIsPageVisible('our-house')
  );
  showHandPublicTransport$ = this.store.select(
    screenSaverSelectors.selectIsPageVisible('public-transport')
  );
  showHandNews$ = this.store.select(
    screenSaverSelectors.selectIsPageVisible('news')
  );
  showHandCulture$ = this.store.select(
    screenSaverSelectors.selectIsPageVisible('culture')
  );
  selectOurHauseActive$ = this.store.select(selectMenuItemActive('our-house'));
  selectPublicTransportActive$ = this.store.select(
    selectMenuItemActive('public-transport')
  );
  selectNewsActive$ = this.store.select(selectMenuItemActive('news'));
  selectCultureActive$ = this.store.select(selectMenuItemActive('culture'));

  disabledScreenSaver() {
    this.store.dispatch(screenSaverActions.disable());
  }

  constructor(private store: Store) {}
}
