import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';

@Component({
  selector: 'app-progress-bar',
  template: `
    <div class="w-full h-3 bg-gray-200 rounded mb-12 mt-4">
      <div
        class="h-full bg-blue-light rounded opacity-30"
        [style.width.%]="progress"
      ></div>
    </div>
  `,
})
export class ProgressBarComponent implements AfterViewInit {
  @Input() set duration(value: number) {
    this._duration = value;
  }

  _duration: number = 15000;

  animationStartTime!: number;
  progress = 0;

  ngAfterViewInit(): void {
    this.reset();
  }

  public reset() {
    this.progress = 0;
    this.animationStartTime = performance.now();
    this.animate();
  }

  animate() {
    const now = performance.now();
    const elapsedTime = now - this.animationStartTime;
    this.progress = Math.min((elapsedTime / this._duration) * 100, 100);

    if (elapsedTime < this._duration) {
      requestAnimationFrame(() => this.animate());
    }
  }
}
