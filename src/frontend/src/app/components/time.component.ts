import { Component, OnInit } from '@angular/core';
import { map, timer } from 'rxjs';

@Component({
  selector: 'app-uhrzeit',
  template: `
    <div
      class="flex flex-col content-center items-center font-sans text-dark-text"
    >
      <p class="font-bold text-[5.131rem] leading-[1]  border-b-4">
        {{ currentDateTime | async | date : 'HH:mm' }}
      </p>
      <p class="text-[1.263rem] pt-1">
        {{ currentDateTime | async | date : 'EEEE, dd.MM.yyyy' }}
      </p>
    </div>
  `,
  styles: [],
})
export class TimeComponent implements OnInit {
  public currentDateTime = timer(0, 1000).pipe(map(() => new Date()));

  ngOnInit(): void {}
}
