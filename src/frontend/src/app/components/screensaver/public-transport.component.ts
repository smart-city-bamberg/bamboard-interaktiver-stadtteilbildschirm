import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { disable } from 'src/app/stores/screen-saver/screen-saver.actions';

@Component({
  selector: 'app-screensaver-public-transport',
  template: `
    <div
      class="flex h-full w-full flex-col justify-between text-dark-text"
      (click)="closeScreensaver()"
      [routerLink]="'../oepnv'"
    >
      <div
        class="flex flex-row items-center justify-center gap-12"
        [routerLink]="'/nachrichten'"
      >
        <div
          class="flex h-[12rem] w-[28rem] items-center justify-center rounded-3xl bg-[#E5F3FA]"
        >
          <i class="mi filled text-[9rem] text-blue-dark">directions_bus</i>
        </div>
        <div class="flex flex-grow flex-col justify-center space-y-6">
          <h1 class="rightToLeft text-5xl font-bold text-blue-light">
            {{ 'ScreenSaverOepnvH1' | translate }}
          </h1>
          <span class="rightToLeft text-3xl">
            {{ 'ScreenSaverOepnvInfo' | translate }}</span
          >
        </div>
      </div>

      <div class="mt-auto flex flex-row items-center justify-between">
        <div class="flex flex-col justify-center space-y-8">
          <span class="flex flex-row items-center gap-4 text-3xl">
            <i class="mi outlined text-5xl text-red-light">check_circle</i>
            {{ 'ScreenSaverOepnvP1' | translate }}
          </span>
          <span class="flex flex-row items-center gap-4 text-3xl">
            <i class="mi outlined text-5xl text-red-light">check_circle</i>
            {{ 'ScreenSaverOepnvP2' | translate }}
          </span>
          <span class="flex flex-row items-center gap-4 text-3xl">
            <i class="mi outlined text-5xl text-red-light">check_circle</i>
            {{ 'ScreenSaverOepnvP3' | translate }}
          </span>
        </div>
        <div class="flex-col justify-center space-y-6">
          <img
            class=""
            src="../assets/images/screensaver/public-transport-preview.svg"
            alt="House"
          />
          <button class="button">
            {{ 'screenSaverStartButton' | translate }}
          </button>
        </div>
      </div>
    </div>
  `,
  styles: [
    `
      /* :host {
        @apply flex;
      } */
      .button {
        @apply flex flex-row w-full items-center justify-center bg-blue-light border-blue-dark border-2 rounded-lg shadow-md text-white font-bold text-[1.1rem] p-2 gap-2;
      }
    `,
  ],
})
export class PublicTransportComponent implements OnInit {
  progress: number = 0;
  duration: number = 3000;
  interval: number = 10;

  ngOnInit() {
    const increment = (this.interval / this.duration) * 100;

    const animateProgressBar = setInterval(() => {
      this.progress += increment;

      if (this.progress >= 100) {
        clearInterval(animateProgressBar);
      }
    }, this.interval);
  }

  constructor(private store: Store) {}

  closeScreensaver() {
    this.store.dispatch(disable());
  }
}
