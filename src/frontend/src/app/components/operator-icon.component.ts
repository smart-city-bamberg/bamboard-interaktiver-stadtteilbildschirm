import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { selectBetreiberIcon } from '../stores/monitor/monitor.selectors';

@Component({
  selector: 'app-betreiber-icon',
  template: `
    <div class="flex justify-center">
      <img [src]="betreiberIcon$ | async" class="object-contain mt-1" />
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex;
      }
    `,
  ],
})
export class OperatorIconComponent {
  betreiberIcon$ = this.store.select(selectBetreiberIcon);

  constructor(private store: Store) {}
}
