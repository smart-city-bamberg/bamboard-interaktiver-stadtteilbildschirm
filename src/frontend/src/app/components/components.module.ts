import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WeatherComponent } from './weather.component';
import { TimeComponent } from './time.component';
import { MenuComponent } from './menu.component';
import { OperatorIconComponent } from './operator-icon.component';
import { ServiceComponent } from './service.component';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { RichTextComponent } from './rich-text/rich-text.component';
import { ParagraphComponent } from './rich-text/paragraph.component';
import { CodeComponent } from './rich-text/code.component';
import { HeadingComponent } from './rich-text/heading.component';
import { BlockQuoteComponent } from './rich-text/block-quote.component';
import { UnorderedListComponent } from './rich-text/unordered-list.component';
import { ListItemComponent } from './rich-text/list-item.component';
import { ListItemContentComponent } from './rich-text/list-item-content.component';
import { OrderedListComponent } from './rich-text/ordered-list.component';
import { OurHouseComponent } from './screensaver/our-house.component';
import { PublicTransportComponent } from './screensaver/public-transport.component';
import { NewsComponent } from './screensaver/news.component';
import { CultureComponent } from './screensaver/culture.component';
import { ProgressBarComponent } from './progress-bar.component';
import { WarningTickerComponent } from './warning-ticker.component';
import { AnalyticsModule } from '../analytics/analytics.module';
import { LinkComponent } from './rich-text/link.component';
import { QRCodeModule } from 'angularx-qrcode';

@NgModule({
  declarations: [
    WeatherComponent,
    TimeComponent,
    MenuComponent,
    OperatorIconComponent,
    ServiceComponent,
    RichTextComponent,
    ParagraphComponent,
    CodeComponent,
    LinkComponent,
    HeadingComponent,
    BlockQuoteComponent,
    UnorderedListComponent,
    ListItemComponent,
    ListItemContentComponent,
    OrderedListComponent,
    OurHouseComponent,
    PublicTransportComponent,
    NewsComponent,
    CultureComponent,
    ProgressBarComponent,
    WarningTickerComponent,
  ],
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    AnalyticsModule,
    QRCodeModule,
  ],
  exports: [
    WeatherComponent,
    TimeComponent,
    MenuComponent,
    OperatorIconComponent,
    ServiceComponent,
    RichTextComponent,
    ParagraphComponent,
    CodeComponent,
    LinkComponent,
    HeadingComponent,
    BlockQuoteComponent,
    UnorderedListComponent,
    OurHouseComponent,
    PublicTransportComponent,
    NewsComponent,
    CultureComponent,
    ProgressBarComponent,
    WarningTickerComponent,
  ],
})
export class ComponentsModule {}
