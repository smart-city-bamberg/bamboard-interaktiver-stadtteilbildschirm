import { Component } from '@angular/core';
import { selectWarnings } from '../stores/weather/weather.selectors';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-warning-ticker',
  template: `
    <div
      class="flex h-11 flex-grow flex-row items-center rounded-l-lg bg-[#E51B33] pl-4 text-white"
    >
      <p class="text-3xl">Warnung!</p>
    </div>
    <div class="ticker-wrap ml-44 h-11">
      <div class="ticker mx-8">
        <div
          class="ticker-item"
          *ngFor="let warning of warnings$ | async; let last = last"
        >
          <div class="flex flex-row">
            <p>{{ warning.timestamp | date : 'dd.MM.yy, HH:mm' }}</p>
            <p class="ml-4">{{ warning.message }}</p>
            <ng-container *ngIf="!last">
              <p class="ml-4">|</p>
            </ng-container>
          </div>
        </div>
      </div>
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex flex-row flex-grow items-center;
      }
      /* * {
        box-sizing: border-box;
      } */
      $duration: 30s;

      @-webkit-keyframes ticker {
        0% {
          -webkit-transform: translate3d(0, 0, 0);
          transform: translate3d(0, 0, 0);
          visibility: visible;
        }

        100% {
          -webkit-transform: translate3d(-100%, 0, 0);
          transform: translate3d(-100%, 0, 0);
        }
      }

      @keyframes ticker {
        0% {
          -webkit-transform: translate3d(0, 0, 0);
          transform: translate3d(0, 0, 0);
          visibility: visible;
        }

        100% {
          -webkit-transform: translate3d(-100%, 0, 0);
          transform: translate3d(-100%, 0, 0);
        }
      }

      .ticker-wrap {
        @apply fixed w-full overflow-hidden bg-[#E51B33] box-content pl-[100%];

        .ticker {
          @apply inline-block whitespace-nowrap box-content my-1 pr-[100%];

          -webkit-animation-iteration-count: infinite;
          animation-iteration-count: infinite;
          -webkit-animation-timing-function: linear;
          animation-timing-function: linear;
          -webkit-animation-name: ticker;
          animation-name: ticker;
          -webkit-animation-duration: $duration;
          animation-duration: $duration;

          &-item {
            @apply inline-block text-3xl text-white mr-4;
          }
        }
      }
    `,
  ],
})
export class WarningTickerComponent {
  warnings$ = this.store.select(selectWarnings);

  constructor(private store: Store) {}
}
