import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-rich-text',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <ng-container *ngFor="let paragraph of content" [ngSwitch]="paragraph.type">
      <app-richtext-paragraph
        *ngSwitchCase="'paragraph'"
        [content]="paragraph"
      />
      <app-richtext-code *ngSwitchCase="'code'" [content]="paragraph" />
      <app-richtext-heading *ngSwitchCase="'heading'" [content]="paragraph" />
      <app-richtext-block-quote
        *ngSwitchCase="'blockquote'"
        [content]="paragraph"
      />
      <app-richtext-unordered-list
        *ngSwitchCase="'unordered-list'"
        [content]="paragraph"
      />

      <app-richtext-ordered-list
        *ngSwitchCase="'ordered-list'"
        [content]="paragraph"
      />

      <app-richtext-list-item-content
        *ngSwitchCase="'list-item-content'"
        [content]="paragraph"
      />

      <app-richtext-link *ngSwitchCase="'link'" [content]="paragraph" />

      <hr *ngSwitchCase="'divider'" />

      <div *ngSwitchDefault class="h-96 overflow-auto">
        ⚠️ Unbekannter Inhaltstyp:
        <pre> {{ paragraph | json }} </pre>
      </div>
    </ng-container>
  `,
  styles: [],
})
export class RichTextComponent {
  @Input()
  content?: any;
}
