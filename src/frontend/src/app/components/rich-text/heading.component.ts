import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-richtext-heading',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <ng-container [ngSwitch]="content.level">
      <h1 class="my-4 font-bold text-4xl" *ngSwitchCase="1">
        {{ content.children[0].text }}
      </h1>
      <h2 class="my-4 font-bold text-3xl" *ngSwitchCase="2">
        {{ content.children[0].text }}
      </h2>
      <h3 class="my-4 font-bold text-2xl" *ngSwitchCase="3">
        {{ content.children[0].text }}
      </h3>
      <h4 class="my-4 font-bold text-xl" *ngSwitchCase="4">
        {{ content.children[0].text }}
      </h4>
      <h5 class="my-4 font-bold text-lg" *ngSwitchCase="5">
        {{ content.children[0].text }}
      </h5>
      <h6 class="my-4 font-bold text-sm" *ngSwitchCase="6">
        {{ content.children[0].text }}
      </h6>
    </ng-container>
  `,
  styles: [],
})
export class HeadingComponent {
  @Input()
  content: any;
}
