import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-richtext-unordered-list',
  template: `
    <ul
      class="list-disc list-outside ml-8"
      *ngFor="let child of content.children"
    >
      <li [ngSwitch]="child.type">
        <app-richtext-list-item *ngSwitchCase="'list-item'" [content]="child" />

        <div *ngSwitchDefault class="h-96 overflow-auto">
          ⚠️ Unbekannter Inhaltstyp:
          <pre> {{ child | json }} </pre>
        </div>
      </li>
    </ul>
  `,
  styles: [],
})
export class UnorderedListComponent {
  @Input()
  content: any;
}
