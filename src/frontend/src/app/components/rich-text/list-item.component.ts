import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-richtext-list-item',
  template: ` <app-rich-text [content]="content.children" />`,
  styles: [],
})
export class ListItemComponent {
  @Input()
  content: any;
}
