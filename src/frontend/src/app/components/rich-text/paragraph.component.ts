import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-richtext-paragraph',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <p class="my-2 whitespace-pre-line">
      <ng-container
        *ngFor="let child of content.children"
        [ngSwitch]="child.type"
      >
        <app-richtext-link *ngSwitchCase="'link'" [content]="child" />
        <span
          *ngSwitchDefault
          [class.font-bold]="child.bold"
          [class.underline]="child.underline"
          [class.italic]="child.italic"
          [class.line-through]="child.strikethrough"
          [class.code]="child.code"
          [class.align-sub]="child.subscript"
          [class.align-super]="child.superscript"
          [innerHtml]="toHtmlEntities(child.text)"
        ></span>
      </ng-container>
    </p>
  `,
  styles: [
    `
      :host {
        & .code {
          @apply bg-gray-300 p-1;
        }
      }
    `,
  ],
})
export class ParagraphComponent {
  @Input()
  content: any;

  toHtmlEntities(text: string) {
    if (text?.trim() !== '') {
      return text;
    }

    return '&nbsp;';
  }
}
