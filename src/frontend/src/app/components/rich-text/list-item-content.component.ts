import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-richtext-list-item-content',
  template: ` <span>{{ content.children[0].text }}</span> `,
  styles: [],
})
export class ListItemContentComponent {
  @Input()
  content: any;
}
