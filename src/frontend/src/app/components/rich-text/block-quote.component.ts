import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-richtext-block-quote',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <app-rich-text [content]="content.children" class="block m-1 border-solid border-l-[3px] border-gray-300" />
  `,
  styles: [
    `
      :host {
        @apply block py-2;
      }
    `,
  ],
})
export class BlockQuoteComponent {
  @Input()
  content: any;
}
