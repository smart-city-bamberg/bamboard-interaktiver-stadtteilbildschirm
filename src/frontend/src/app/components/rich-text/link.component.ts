import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-richtext-link',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <span class="m-0 p-0">{{ content.children[0].text }}</span>
    <qrcode
      [qrdata]="content.href"
      [allowEmptyString]="true"
      [ariaLabel]="'QR Code zum Link ' + content.href"
      [cssClass]="'center'"
      [elementType]="'canvas'"
      [errorCorrectionLevel]="'M'"
      [margin]="0"
      [scale]="1"
      [width]="200"
    ></qrcode>
  `,
  styles: [
    `
      :host {
        @apply flex flex-col gap-2;
      }
    `,
  ],
})
export class LinkComponent {
  @Input()
  content: any;
}
