import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'app-richtext-code',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <pre class="bg-gray-300 p-1">{{ content.children[0].text }}</pre>
  `,
  styles: [],
})
export class CodeComponent {
  @Input()
  content: any;
}
