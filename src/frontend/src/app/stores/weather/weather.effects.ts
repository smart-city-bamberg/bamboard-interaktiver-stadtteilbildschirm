import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  catchError,
  map,
  concatMap,
  distinct,
  withLatestFrom,
  filter,
} from 'rxjs/operators';
import { of, timer } from 'rxjs';
import * as WeatherActions from './weather.actions';
import { LastWeatherReportGQL, WarningsGQL } from 'src/graphql/generated';
import { environment } from 'src/environments/environment';
import { selectSelectedMonitor } from '../monitor/monitor.selectors';
import { Store } from '@ngrx/store';

@Injectable()
export class WeatherEffects {
  loadWeatherHourly$ = createEffect(() =>
    timer(0, 1000).pipe(
      map((_) => new Date()),
      distinct((_) => {
        const roundedDate = new Date(_);
        roundedDate.setMinutes(0, 0, 0);
        return roundedDate.getTime();
      }),
      map((_) => WeatherActions.loadWeathers())
    )
  );

  loadWeathers$ = createEffect(() =>
    this.actions$.pipe(
      ofType(WeatherActions.loadWeathers),
      withLatestFrom(this.store.select(selectSelectedMonitor)),
      concatMap(([action, monitorId]) =>
        this.lastWeatherReport
          .fetch({
            identity: monitorId as string,
          })
          .pipe(
            map((data) =>
              WeatherActions.loadWeathersSuccess({
                reports:
                  data.data.monitor?.group?.weatherLocation?.reports ?? [],
              })
            ),
            catchError((error) =>
              of(WeatherActions.loadWeathersFailure({ error }))
            )
          )
      )
    )
  );

  loadWarningsInterval$ = createEffect(() =>
    timer(0, 30000).pipe(map((_) => WeatherActions.loadWarnings()))
  );

  loadWarnings$ = createEffect(() =>
    this.actions$.pipe(
      ofType(WeatherActions.loadWarnings),
      withLatestFrom(this.store.select(selectSelectedMonitor)),
      filter(([action, monitor]) => monitor !== undefined),
      concatMap(([action, monitorId]) =>
        this.warningsQuery
          .fetch({
            identity: monitorId as string,
          })
          .pipe(
            map((response) =>
              WeatherActions.loadWarningsSuccess({
                warnings: response.data.monitor?.group?.warnings ?? [],
              })
            ),
            catchError((error) =>
              of(WeatherActions.loadWarningsFailure({ error }))
            )
          )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store,
    private lastWeatherReport: LastWeatherReportGQL,
    private warningsQuery: WarningsGQL
  ) {}
}
