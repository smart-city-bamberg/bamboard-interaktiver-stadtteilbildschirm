import { createAction, props } from '@ngrx/store';
import { Warning, WeatherReport } from 'src/graphql/generated';

export const loadWeathers = createAction('[Weather] Load Weathers');

export const loadWeathersSuccess = createAction(
  '[Weather] Load Weathers Success',
  props<{ reports: WeatherReport[] }>()
);

export const loadWeathersFailure = createAction(
  '[Weather] Load Weathers Failure',
  props<{ error: any }>()
);

export const loadWarnings = createAction('[Weather] Load Warnings');

export const loadWarningsSuccess = createAction(
  '[Weather] Load Warnings Success',
  props<{ warnings: Warning[] }>()
);

export const loadWarningsFailure = createAction(
  '[Weather] Load Warnings Failure',
  props<{ error: any }>()
);
