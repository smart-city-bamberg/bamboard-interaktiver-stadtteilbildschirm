import { createReducer, on } from '@ngrx/store';
import * as WeatherActions from './weather.actions';

export const weatherFeatureKey = 'weather';

export interface State {
  report?: {
    temperature: number;
    icon: string;
    timestamp: Date;
    forecasts: {
      temperature: number;
      icon: string;
      timestamp: Date;
    }[];
  };
  warnings: {
    id: string;
    message: string;
    timestamp: Date;
    type: 'alert' | 'clear';
  }[];
}

export const initialState: State = {
  warnings: [],
};

export const reducer = createReducer(
  initialState,

  on(WeatherActions.loadWeathers, (state) => state),
  on(WeatherActions.loadWeathersSuccess, (state, action) => ({
    ...state,
    report: action.reports[0]
      ? {
          timestamp: new Date(action.reports[0].timestamp),
          icon: action.reports[0].icon as string,
          temperature: action.reports[0].temperature as number,
          forecasts: (action.reports[0].forecasts ?? []).map((foreacast) => ({
            timestamp: new Date(foreacast.timestamp),
            icon: foreacast.icon as string,
            temperature: foreacast.temperature as number,
          })),
        }
      : undefined,
  })),
  on(WeatherActions.loadWeathersFailure, (state, action) => state),
  on(WeatherActions.loadWarningsSuccess, (state, action) => ({
    ...state,
    warnings: action.warnings.map((warning) => ({
      id: warning.id,
      message: warning.message as string,
      timestamp: new Date(warning.timestamp),
      type: warning.type as 'alert' | 'clear',
    })),
  })),
  on(WeatherActions.loadWarningsFailure, (state, action) => state)
);
