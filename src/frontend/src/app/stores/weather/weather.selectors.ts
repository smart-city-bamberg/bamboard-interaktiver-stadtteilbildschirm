import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromWeather from './weather.reducer';

export const selectWeatherState = createFeatureSelector<fromWeather.State>(
  fromWeather.weatherFeatureKey
);

export const selectWeatherReport = createSelector(
  selectWeatherState,
  (state) => state.report
);

const millisecondsPerHour = 3600 * 1000;

export const selectWeatherForecasts = (date: Date, amount: number = 5) =>
  createSelector(selectWeatherState, (state) =>
    state.report?.forecasts
      ?.filter(
        (forecast) =>
          forecast.timestamp.getTime() > date.getTime() + millisecondsPerHour
      )
      ?.slice(0, amount)
      .map((forecast) => ({
        temperature: forecast.temperature,
        icon: forecast.icon,
        timestamp: forecast.timestamp,
      }))
  );

export const selectWarnings = createSelector(
  selectWeatherState,
  (state) => state.warnings
);

export const selectShowWarning = createSelector(
  selectWarnings,
  (warnings) => warnings.filter((warning) => warning.message !== '').length > 0
);
