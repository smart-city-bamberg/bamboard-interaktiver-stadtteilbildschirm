import { isDevMode } from '@angular/core';
import { routerReducer, RouterState } from '@ngrx/router-store';
import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer,
} from '@ngrx/store';
import * as fromWeather from './weather/weather.reducer';
import * as fromOurHouse from './our-house/our-house.reducer';
import * as fromMonitor from './monitor/monitor.reducer';
import * as fromNews from './news/news.reducer';
import * as fromEvents from './events/events.reducer';
import * as fromPublicTransport from './public-transport/public-transport.reducer';
import * as fromAuth from './auth/auth.reducer';
import * as fromScreenSaver from './screen-saver/screen-saver.reducer';

export interface State {
  router: RouterState;  [fromWeather.weatherFeatureKey]: fromWeather.State;

[fromOurHouse.ourHouseFeatureKey]: fromOurHouse.State;
[fromMonitor.monitorFeatureKey]: fromMonitor.State;
[fromNews.newsFeatureKey]: fromNews.State;
[fromEvents.eventsFeatureKey]: fromEvents.State;
[fromPublicTransport.publicTransportFeatureKey]: fromPublicTransport.State;
[fromAuth.authFeatureKey]: fromAuth.State;
[fromScreenSaver.screenSaverFeatureKey]: fromScreenSaver.State;
}

export const reducers: ActionReducerMap<State> = {
  router: routerReducer,
  [fromWeather.weatherFeatureKey]: fromWeather.reducer,
  [fromOurHouse.ourHouseFeatureKey]: fromOurHouse.reducer,
  [fromMonitor.monitorFeatureKey]: fromMonitor.reducer,
  [fromNews.newsFeatureKey]: fromNews.reducer,
  [fromEvents.eventsFeatureKey]: fromEvents.reducer,
  [fromPublicTransport.publicTransportFeatureKey]: fromPublicTransport.reducer,
  [fromAuth.authFeatureKey]: fromAuth.reducer,
  [fromScreenSaver.screenSaverFeatureKey]: fromScreenSaver.reducer,
};

export const metaReducers: MetaReducer<State>[] = isDevMode() ? [] : [];
