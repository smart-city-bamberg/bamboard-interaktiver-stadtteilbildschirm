import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromMonitor from './monitor.reducer';

export const selectMonitorState = createFeatureSelector<fromMonitor.State>(
  fromMonitor.monitorFeatureKey
);

export const selectMonitorName = createSelector(
  selectMonitorState,
  (state) => state.monitor?.name
);

export const selectMonitor = createSelector(
  selectMonitorState,
  (state) => state.monitor
);

export const selectMonitorOrganization = createSelector(
  selectMonitorState,
  (state) => state.monitor?.organization
);

export const selectBetreiberIcon = createSelector(
  selectMonitorState,
  (state) => state.monitor?.operatorLogo?.url
);

export const selectBoardIcon = createSelector(
  selectMonitorState,
  (state) => state.monitor?.boardLogo?.url
);

export const selectMonitors = createSelector(
  selectMonitorState,
  (state) => state.monitors
);

export const selectSelectedMonitor = createSelector(
  selectMonitorState,
  (state) => state.selectedMonitor
);

export const selectMenuItemActive = (menuName: string) =>
  createSelector(selectMonitorState, (state) => state.menu.includes(menuName));
