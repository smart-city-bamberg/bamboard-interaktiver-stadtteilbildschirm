import { Action, createReducer, on } from '@ngrx/store';
import { MonitorQuery, MonitorsQuery } from 'src/graphql/generated';
import * as MonitorActions from './monitor.actions';
import { environment } from 'src/environments/environment';

export const monitorFeatureKey = 'monitor';

export interface State {
  selectedMonitor?: string;
  monitor?: MonitorQuery['monitor'];
  monitors?: MonitorsQuery['monitors'];
  publicTransportActive: boolean;
  ourHauseActive: boolean;
  newsActive: boolean;
  cultureActive: boolean;
  menu: string[];
}

export const initialState: State = {
  publicTransportActive: false,
  ourHauseActive: true,
  newsActive: true,
  cultureActive: true,
  menu: environment.menu,
};

export const reducer = createReducer(
  initialState,

  on(MonitorActions.loadMonitorSuccess, (state, action) => ({
    ...state,
    ...action.data,
  })),
  on(MonitorActions.loadMonitorFailure, (state, action) => state),
  on(MonitorActions.loadMonitorsSuccess, (state, action) => ({
    ...state,
    ...action.data,
  })),
  on(MonitorActions.selectMonitor, (state, action) => ({
    ...state,
    selectedMonitor: action.monitorId,
  })),
  on(MonitorActions.clearMonitors, (state, action) => ({
    ...state,
    monitor: undefined,
    monitors: undefined,
    selectedMonitor: undefined,
  })),
  on(MonitorActions.loadSelectedMonitorSuccess, (state, action) => ({
    ...state,
    selectedMonitor: action.monitorId,
  }))
);
