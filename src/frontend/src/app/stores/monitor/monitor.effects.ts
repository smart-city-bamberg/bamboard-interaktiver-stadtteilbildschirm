import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  catchError,
  map,
  concatMap,
  tap,
  debounceTime,
  withLatestFrom,
  switchMap,
  filter,
} from 'rxjs/operators';
import { of, fromEvent, merge, timer } from 'rxjs';
import * as MonitorActions from './monitor.actions';
import { MonitorGQL, MonitorsGQL } from 'src/graphql/generated';
import { Router } from '@angular/router';
import { loadMonitor } from './monitor.actions';
import { loadOurHouse } from '../our-house/our-house.actions';
import { loadNews } from '../news/news.actions';
import { loadPublicTransport } from '../public-transport/public-transport.actions';
import { selectSelectedMonitor } from './monitor.selectors';
import { Store } from '@ngrx/store';
import * as authActions from '../auth/auth.actions';
import { loadWeathers } from '../weather/weather.actions';
import { loadEvents } from '../events/events.actions';
import { getRouterSelectors, routerNavigatedAction } from '@ngrx/router-store';
import * as screenSaverActions from '../screen-saver/screen-saver.actions';
import { environment } from 'src/environments/environment';

const { selectCurrentRoute } = getRouterSelectors();

@Injectable()
export class MonitorEffects {
  loadMonitor$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MonitorActions.loadMonitor),
      withLatestFrom(this.store.select(selectSelectedMonitor)),
      concatMap(([action, monitorId]) =>
        this.monitorQuery
          .fetch({
            identity: monitorId as string,
          })
          .pipe(
            map((result) =>
              MonitorActions.loadMonitorSuccess({ data: result.data })
            ),
            catchError((error) =>
              of(MonitorActions.loadMonitorFailure({ error }))
            )
          )
      )
    )
  );

  loadMonitors$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MonitorActions.loadMonitors),
      concatMap(() =>
        this.monitorsQuery.fetch().pipe(
          map((result) =>
            MonitorActions.loadMonitorsSuccess({ data: result.data })
          ),
          catchError((error) =>
            of(MonitorActions.loadMonitorsFailure({ error }))
          )
        )
      )
    )
  );

  loadMonitorsWhenLoginSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(authActions.loginSuccess),
      withLatestFrom(this.store.select(selectCurrentRoute)),
      filter(
        ([action, route]) => route.routeConfig.path == 'bildschirmauswahl'
      ),
      map(([action, route]) => MonitorActions.loadMonitors())
    )
  );

  loadMonitorWhenLoginSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(authActions.loginSuccess),
      withLatestFrom(this.store.select(selectCurrentRoute)),
      filter(
        ([action, route]) => route.routeConfig.path != 'bildschirmauswahl'
      ),
      map(([action, route]) => MonitorActions.loadSelectedMonitor())
    )
  );

  loadSelectedMonitor$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MonitorActions.loadSelectedMonitor),
      map((action) => localStorage.getItem('monitor')),
      map((monitorId) =>
        monitorId
          ? MonitorActions.loadSelectedMonitorSuccess({ monitorId })
          : MonitorActions.loadSelectedMonitorFailure()
      )
    )
  );

  clearMonitorsWhenLogoutSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(authActions.logoutSuccess),
      map((action) => MonitorActions.clearMonitors())
    )
  );

  idleTimer$ = createEffect(() =>
    this.actions$.pipe(
      ofType(routerNavigatedAction),
      filter((action) => action.payload.routerState.url.includes('/seiten')),
      switchMap(() =>
        merge(
          fromEvent(document, 'touchstart'),
          fromEvent(document, 'mousemove'),
          of(0)
        ).pipe(
          debounceTime(environment.screenSaver.idleTimeout),
          map(() => screenSaverActions.enable())
        )
      )
    )
  );

  polling$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        MonitorActions.loadSelectedMonitorSuccess,
        MonitorActions.selectMonitorSuccess
      ),
      switchMap(() =>
        timer(0, environment.monitor.pollingInterval).pipe(
          concatMap(() => [
            loadMonitor(),
            loadWeathers(),
            loadOurHouse(),
            loadNews(),
            loadPublicTransport(),
            loadEvents(),
          ])
        )
      )
    )
  );

  reditectToHomeWhenMonitorSelected$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(MonitorActions.selectMonitorSuccess),
        tap(() => open('/seiten/unser-haus', '_blank'))
      ),
    {
      dispatch: false,
    }
  );

  reditectToHomeWhenLoginFailure$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(authActions.loginFailure),
        withLatestFrom(this.store.select(selectCurrentRoute)),
        filter(
          ([action, route]) => route.routeConfig.path != 'bildschirmauswahl'
        ),
        tap(() => this.router.navigate(['/']))
      ),
    {
      dispatch: false,
    }
  );

  saveSelectedMonitorWhenSelected$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MonitorActions.selectMonitor),
      tap((action) => {
        localStorage.setItem('monitor', action.monitorId);
      }),
      map((action) => MonitorActions.selectMonitorSuccess({ ...action }))
    )
  );

  reditectToHomeWhenReloaded$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(MonitorActions.loadSelectedMonitorSuccess),
        withLatestFrom(this.store.select(selectCurrentRoute)),
        filter(([action, route]) => route.routeConfig.path == ''),
        tap(() => this.router.navigate(['/seiten/unser-haus']))
      ),
    {
      dispatch: false,
    }
  );

  constructor(
    private actions$: Actions,
    private monitorQuery: MonitorGQL,
    private monitorsQuery: MonitorsGQL,
    private store: Store,
    private router: Router
  ) {}
}
