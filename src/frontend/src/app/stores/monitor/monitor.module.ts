import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import * as fromMonitor from './monitor.reducer';
import { EffectsModule } from '@ngrx/effects';
import { MonitorEffects } from './monitor.effects';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(fromMonitor.monitorFeatureKey, fromMonitor.reducer),
    EffectsModule.forFeature([MonitorEffects])
  ]
})
export class MonitorModule { }
