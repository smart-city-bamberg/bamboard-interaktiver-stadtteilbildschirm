import { createAction, props } from '@ngrx/store';
import { MonitorQuery, MonitorsQuery } from 'src/graphql/generated';

export const loadMonitor = createAction('[Monitor] Load Monitor');

export const loadMonitorSuccess = createAction(
  '[Monitor] Load Monitor Success',
  props<{ data: MonitorQuery }>()
);

export const loadMonitorFailure = createAction(
  '[Monitor] Load Monitor Failure',
  props<{ error: any }>()
);

export const loadMonitors = createAction('[Monitor] Load Monitors');

export const loadMonitorsSuccess = createAction(
  '[Monitor] Load Monitors Success',
  props<{ data: MonitorsQuery }>()
);

export const loadMonitorsFailure = createAction(
  '[Monitor] Load Monitors Failure',
  props<{ error: any }>()
);

export const selectMonitor = createAction(
  '[Monitor] Select Monitor',
  props<{ monitorId: string }>()
);

export const selectMonitorSuccess = createAction(
  '[Monitor] Select Monitor Success',
  props<{ monitorId: string }>()
);

export const loadSelectedMonitor = createAction(
  '[Monitor] Load Selected Monitor'
);

export const loadSelectedMonitorSuccess = createAction(
  '[Monitor] Load Selected Monitor Success',
  props<{ monitorId: string }>()
);

export const loadSelectedMonitorFailure = createAction(
  '[Monitor] Load Selected Monitor Failure'
);

export const clearMonitors = createAction('[Monitor] Clear Monitors');
