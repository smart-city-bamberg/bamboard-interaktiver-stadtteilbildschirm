import { createAction, props } from '@ngrx/store';
import { OurHouseQuery } from 'src/graphql/generated';

export const loadOurHouse = createAction('[OurHouse] Load OurHouse');

export const loadOurHouseSuccess = createAction(
  '[OurHouse] Load OurHouse Success',
  props<{ data: OurHouseQuery }>()
);

export const loadOurHouseFailure = createAction(
  '[OurHouse] Load OurHouse Failure',
  props<{ error: any }>()
);

export const openDocument = createAction(
  '[OurHouse] Open Document',
  props<{ documentUrl: string }>()
);
export const closeDocument = createAction('[OurHouse] Close Document');
