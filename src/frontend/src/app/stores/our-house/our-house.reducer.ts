import { Action, createReducer, on } from '@ngrx/store';
import { Document, OurHouseQuery } from 'src/graphql/generated';
import * as OurHouseActions from './our-house.actions';

export const ourHouseFeatureKey = 'ourHouse';

export interface State {
  monitor?: OurHouseQuery['monitor'];
  showDocument?: boolean;
  documentUrl?: string;
}

export const initialState: State = {
  showDocument: false,
  documentUrl: '',
};

export const reducer = createReducer(
  initialState,
  on(OurHouseActions.loadOurHouse, (state) => state),
  on(OurHouseActions.loadOurHouseSuccess, (state, action) => ({
    ...action.data,
  })),
  on(OurHouseActions.loadOurHouseFailure, (state, action) => state),
  on(OurHouseActions.openDocument, (state, action) => ({
    ...state,
    showDocument: true,
    documentUrl: action.documentUrl,
  })),
  on(OurHouseActions.closeDocument, (state, action) => ({
    ...state,
    showDocument: false,
    documentUrl: '',
  }))
);
