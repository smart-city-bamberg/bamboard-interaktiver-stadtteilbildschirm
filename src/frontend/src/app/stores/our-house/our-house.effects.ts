import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, concatMap, withLatestFrom } from 'rxjs/operators';
import { of } from 'rxjs';
import * as OurHouseActions from './our-house.actions';
import { OurHouseGQL } from 'src/graphql/generated';
import { environment } from 'src/environments/environment';
import { selectSelectedMonitor } from '../monitor/monitor.selectors';
import { Store } from '@ngrx/store';

@Injectable()
export class OurHouseEffects {
  loadOurHouses$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(OurHouseActions.loadOurHouse),
      withLatestFrom(this.store.select(selectSelectedMonitor)),
      concatMap(([action, monitorId]) =>
        this.ourHouseQuery
          .fetch({
            identity: monitorId as string,
            timestamp: new Date(),
          })
          .pipe(
            map((result) =>
              OurHouseActions.loadOurHouseSuccess({ data: result.data })
            ),
            catchError((error) =>
              of(OurHouseActions.loadOurHouseFailure({ error }))
            )
          )
      )
    );
  });

  constructor(
    private actions$: Actions,
    private store: Store,
    private ourHouseQuery: OurHouseGQL
  ) {}
}
