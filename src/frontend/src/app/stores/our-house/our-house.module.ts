import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import * as fromOurHouse from './our-house.reducer';
import { EffectsModule } from '@ngrx/effects';
import { OurHouseEffects } from './our-house.effects';

@NgModule({
  declarations: [],
  imports: [CommonModule, StoreModule.forFeature(fromOurHouse.ourHouseFeatureKey, fromOurHouse.reducer), EffectsModule.forFeature([OurHouseEffects])],
})
export class OurHouseModule {}
