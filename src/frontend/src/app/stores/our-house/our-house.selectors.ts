import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromOurHouse from './our-house.reducer';

export const selectOurHouseState = createFeatureSelector<fromOurHouse.State>(
  fromOurHouse.ourHouseFeatureKey
);

export const selectDocuments = createSelector(
  selectOurHouseState,
  (state) => state.monitor?.group?.documents
);

export const selectAnnouncements = createSelector(
  selectOurHouseState,
  (state) => state.monitor?.group?.announcements
);

export const selectContact = createSelector(
  selectOurHouseState,
  (state) =>
    state.monitor?.group?.contact?.content?.document as {
      type: 'paragraph';
      children: {
        text: string;
        bold?: boolean;
        underline?: boolean;
        italic?: boolean;
      }[];
    }[]
);

const toDate = (
  x:
    | {
        timestamp?: any;
        type?: string | null | undefined;
      }[]
    | null
    | undefined
) => x?.map((_) => new Date(_.timestamp as string))?.[0];

export const selectGarbageCollection = createSelector(
  selectOurHouseState,
  (state) => ({
    biomeull: toDate(state.monitor?.biomeull),
    gelber_sack: toDate(state.monitor?.gelber_sack),
    papier: toDate(state.monitor?.papier),
    restmuell: toDate(state.monitor?.restmuell),
  })
);

export const selectGarbageCollectionAssignment = createSelector(
  selectOurHouseState,
  (state) => state.monitor?.garbageCollectionAssignment
);

export const selectshowDocument = createSelector(
  selectOurHouseState,
  (state) => state.showDocument
);

export const selectDocumentUrl = createSelector(
  selectOurHouseState,
  (state) => state.documentUrl
);

export const selectNoDocuments = createSelector(
  selectOurHouseState,
  (state) => state.monitor?.group?.documents?.length === 0
);

export const selectNoAnnouncements = createSelector(
  selectOurHouseState,
  (state) => state.monitor?.group?.announcements?.length === 0
);
