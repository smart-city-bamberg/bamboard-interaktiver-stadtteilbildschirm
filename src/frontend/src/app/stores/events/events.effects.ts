import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  catchError,
  map,
  concatMap,
  distinctUntilChanged,
  filter,
  switchMap,
  withLatestFrom,
} from 'rxjs/operators';
import { of, timer } from 'rxjs';
import * as EventsActions from './events.actions';
import { EventsGQL } from 'src/graphql/generated';
import * as moment from 'moment';
import { routerNavigatedAction } from '@ngrx/router-store';
import { selectSelectedMonitor } from '../monitor/monitor.selectors';
import { Store } from '@ngrx/store';
import { Monitor } from './events.types';
import { environment } from 'src/environments/environment';

const maximumDays = environment.events.maximumDays ?? 15;

@Injectable()
export class EventsEffects {
  loadEvents$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(EventsActions.loadEvents),
      withLatestFrom(this.store.select(selectSelectedMonitor)),
      concatMap(([action, monitorId]) =>
        this.eventsQuery
          .fetch({
            identity: monitorId as string,
            dateFrom: moment(new Date()).format('YYYY-MM-DD'),
            dateTo: moment(new Date())
              .add(maximumDays, 'days')
              .format('YYYY-MM-DD'),
          })
          .pipe(
            map((response) =>
              EventsActions.loadEventsSuccess({
                monitor: response.data.monitor as Monitor,
              })
            ),
            catchError((error) =>
              of(EventsActions.loadEventsFailure({ error }))
            )
          )
      )
    );
  });

  loadEventsAfterDayChanged$ = createEffect(() => {
    return timer(0, 1000).pipe(
      map(() => new Date()),
      distinctUntilChanged(
        (previous, current) =>
          moment(previous).format('YYYY-MM-DD') ===
          moment(current).format('YYYY-MM-DD')
      ),
      map(() => EventsActions.loadEvents())
    );
  });

  resetCategoriesFilter$ = createEffect(() =>
    this.actions$.pipe(
      ofType(routerNavigatedAction),
      filter((action) =>
        action.payload.routerState.url.includes('/seiten/kultur')
      ),
      switchMap(() => {
        return [
          EventsActions.resetCategoriesFilter(),
          EventsActions.loadEvents(),
        ];
      })
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store,
    private eventsQuery: EventsGQL
  ) {}
}
