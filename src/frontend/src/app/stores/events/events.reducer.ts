import { Action, createReducer, on } from '@ngrx/store';
import * as EventsActions from './events.actions';
import { EventsQuery } from 'src/graphql/generated';
import * as moment from 'moment';
import { Monitor } from './events.types';
import { environment } from 'src/environments/environment';

const maximumDays = environment.events.maximumDays ?? 15;
export const eventsFeatureKey = 'events';

export interface State {
  monitor?: Monitor;
  selectedCategories: string[];
  dayPages: {
    [index: string]: number;
  };
  page: number;
  showEventPost: boolean;
}

export const initialState: State = {
  selectedCategories: [],
  dayPages: {},
  page: 0,
  showEventPost: false,
};

export const reducer = createReducer(
  initialState,

  on(EventsActions.loadEvents, (state) => state),
  on(EventsActions.loadEventsSuccess, (state, action) => ({
    ...state,
    monitor: action.monitor,
  })),
  on(EventsActions.loadEventsFailure, (state, action) => state),
  on(EventsActions.toggleCategory, (state, action) => ({
    ...state,
    dayPages: {},
    selectedCategories: state.selectedCategories.includes(action.category)
      ? state.selectedCategories.filter((_) => _ !== action.category)
      : [...state.selectedCategories, action.category],
  })),
  on(EventsActions.nextDayPage, (state, action) => {
    const day = moment(action.day).format('YYYY-MM-DD');

    return {
      ...state,
      dayPages: {
        ...state.dayPages,
        [day]: (state.dayPages[day] ?? 0) + 1,
      },
    };
  }),
  on(EventsActions.previousDayPage, (state, action) => {
    const day = moment(action.day).format('YYYY-MM-DD');

    return {
      ...state,
      dayPages: {
        ...state.dayPages,
        [day]: Math.max(0, (state.dayPages[day] ?? 0) - 1),
      },
    };
  }),

  on(EventsActions.next3DayPage, (state, action) => ({
    ...state,
    page: Math.min(state.page + 1, maximumDays),
  })),
  on(EventsActions.previous3DayPage, (state, action) => ({
    ...state,
    page: Math.max(state.page - 1, 0),
  })),
  on(EventsActions.resetCategoriesFilter, (state, action) => {
    return {
      ...state,
      selectedCategories: [],
      page: 0,
      dayPages: {},
    };
  }),
  on(EventsActions.openEventPost, (state, action) => ({
    ...state,
    showEventPost: true,
  })),
  on(EventsActions.closeEventPost, (state, action) => ({
    ...state,
    showEventPost: false,
  }))
);
