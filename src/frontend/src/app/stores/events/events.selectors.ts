import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromEvents from './events.reducer';
import * as moment from 'moment';
import { Event } from './events.types';
import { environment } from 'src/environments/environment';

const daysPerPage = environment.events.daysPerPage ?? 3;
const newsPerPage = environment.events.newsPerPage ?? 3;
const scrollPerPage = environment.events.scrollPerPage ?? 1;
const maximumDays = environment.events.maximumDays ?? 15;

export const selectEventsState = createFeatureSelector<fromEvents.State>(
  fromEvents.eventsFeatureKey
);

export const selectMonitor = createSelector(
  selectEventsState,
  (state) => state.monitor
);

export const selectDefaultEventImage = createSelector(
  selectEventsState,
  (state) => state.monitor?.group?.defaultEventImage?.url
);

export const selectEvents = createSelector(
  selectEventsState,
  selectDefaultEventImage,
  (state, defaultImage) => {
    const result = [...(state.monitor?.group.events ?? [])];

    if (state.monitor?.group?.eventProviders) {
      result.push(
        ...state.monitor?.group?.eventProviders.flatMap(
          (provider) => provider.events
        )
      );
    }

    return result
      .map((event) => ({
        ...event,
        image: event.image ?? {
          url: defaultImage ?? '/assets/images/default_news.png',
        },
      }))
      .reduce(
        (sum, next) =>
          sum.some((report) => report.id === next.id) ? sum : [...sum, next],
        new Array<Event>()
      )
      .sort((a, b) =>
        `${a.date} ${a.title}`.localeCompare(`${b.date} ${b.title}`)
      );
  }
);

export const selectCategories = createSelector(selectEvents, (events) =>
  events
    ?.map((event) => event.category as string)
    .reduce(
      (previous, next) =>
        previous.includes(next) || !next ? previous : [...previous, next],
      new Array<string>()
    )
);

export const selectSelectedCategories = createSelector(
  selectEventsState,
  (state) => state.selectedCategories
);

export const selectCategoryFilters = createSelector(
  selectCategories,
  selectSelectedCategories,
  (categories, selected) =>
    categories?.map((category) => ({
      name: category,
      selected: selected.includes(category),
    }))
);

export const selectDayPages = createSelector(
  selectEventsState,
  (state) => state.dayPages
);

export const selectDayPage = (day: Date) =>
  createSelector(
    selectDayPages,
    (pages) => pages[moment(day).format('YYYY-MM-DD')] ?? 0
  );

export const selectDaysPage = createSelector(
  selectEventsState,
  (state) => state.page
);

export const selectDaysPageUpDisable = createSelector(
  selectDaysPage,
  (daysPage) => daysPage === 0
);

export const selectDaysPageDownDisable = (from: Date) =>
  createSelector(
    selectDaysPage,
    selectEventsByDaySince(from),
    (daysPage, events) =>
      daysPage * scrollPerPage + daysPerPage >= events.length
  );

export const selectShowEventPost = createSelector(
  selectEventsState,
  (state) => state.showEventPost
);

export const selectFilteredEvents = createSelector(
  selectEvents,
  selectSelectedCategories,
  (events, selected) =>
    events.filter((event) =>
      !selected.length ? true : selected.includes(event.category as string)
    )
);

export const selectEventsByDay = createSelector(
  selectFilteredEvents,
  (events) =>
    events.reduce((result, next) => {
      const day = moment(next.date).format('YYYY-MM-DD');
      const dayEvents = result[day];

      return {
        ...result,
        [day]: [...(dayEvents ?? []), next],
      };
    }, {} as { [index: string]: Event[] })
);

export const selectEventsByDaySince = (from: Date) =>
  createSelector(selectEventsByDay, (events) => {
    const startDate = moment(from).toDate();
    const days = Array(maximumDays)
      .fill(0)
      .map((value, index) =>
        moment(
          new Date(startDate.getTime() + index * 24 * 60 * 60 * 1000)
        ).format('YYYY-MM-DD')
      );

    const map = days.reduce(
      (result, day) => ({
        ...result,
        [day]: events[day] ?? [],
      }),
      {} as { [index: string]: Event[] }
    );

    return Object.entries(map)
      .filter((entry) =>
        environment.events.hideEmptyDays ? entry[1].length > 0 : true
      )
      .sort((a, b) => a[0].localeCompare(b[0]));
  });

export const selectEventsByDaySincePage = (from: Date) =>
  createSelector(
    selectEventsByDaySince(from),
    selectDaysPage,
    selectDayPages,
    (events, daysPage, dayPages) =>
      events
        .map((entry, index) => {
          const dayPageIndex = dayPages[entry[0]] ?? 0;
          const eventSlice = entry[1].slice(
            dayPageIndex * newsPerPage,
            dayPageIndex * newsPerPage + newsPerPage
          );

          return {
            timeStamp: new Date(entry[0]),
            events: eventSlice,
            hasNext: dayPageIndex * newsPerPage < entry[1].length - newsPerPage,
            hasPrevious: dayPageIndex !== 0,
          };
        })
        .slice(daysPage * scrollPerPage, daysPage * scrollPerPage + daysPerPage)
  );
