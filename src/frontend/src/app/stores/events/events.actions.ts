import { createAction, props } from '@ngrx/store';
import { Monitor } from './events.types';

export const loadEvents = createAction('[Events] Load Events');

export const loadEventsSuccess = createAction(
  '[Events] Load Events Success',
  props<{ monitor: Monitor }>()
);

export const loadEventsFailure = createAction(
  '[Events] Load Events Failure',
  props<{ error: any }>()
);

export const toggleCategory = createAction(
  '[Events] Toggle Category',
  props<{ category: string }>()
);

export const nextDayPage = createAction(
  '[Events] Next Day Page',
  props<{ day: Date }>()
);

export const previousDayPage = createAction(
  '[Events] Previous Day Page',
  props<{ day: Date }>()
);

export const next3DayPage = createAction('[Events] Next 3 Day Page');

export const previous3DayPage = createAction('[Events] Previous 3 Day Page');

export const resetCategoriesFilter = createAction(
  '[Events] Reset Categories Filter'
);

export const openEventPost = createAction('[Events] Open Event Post');
export const closeEventPost = createAction('[Events] Close Event Post');
