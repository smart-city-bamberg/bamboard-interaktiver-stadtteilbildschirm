export interface Event {
  id: string;
  title: string;
  category: string;
  date: string;
  time: string;
  location: string;
  link: string;
  image: {
    id: string;
    url: string;
  };
  eventProvider: {
    name: string;
    image: {
      url: string;
    };
  };
}

export interface EventProvider {
  events: Event[];
}

export interface Monitor {
  group: {
    defaultEventImage: {
      url: string;
    };
    eventProviders: EventProvider[];
    events: Event[];
  };
}
