export interface NewsReport {
  id: string;
  timestamp: string;
  title: string;
  content: {
    document: string;
  };
  previewContent: string;
  category: string;
  image: {
    url: string;
  };
  link: string;
  newsProvider: {
    name: string;
    image: {
      url: string;
    };
  };
}

export interface NewsProvider {
  newsReportsCount: number;
  newsReports: NewsReport[];
}

export interface Monitor {
  group: {
    defaultNewsImage: {
      url: string;
    };
    newsProviders: NewsProvider[];
    newsReportsCount: number;
    newsReports: NewsReport[];
  };
}
