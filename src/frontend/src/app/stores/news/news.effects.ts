import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  catchError,
  map,
  concatMap,
  withLatestFrom,
  filter,
  switchMap,
} from 'rxjs/operators';
import { of } from 'rxjs';
import * as NewsActions from './news.actions';
import { NewsReportsGQL } from 'src/graphql/generated';
import { Store } from '@ngrx/store';
import { selectPage } from './news.selectors';
import { selectSelectedMonitor } from '../monitor/monitor.selectors';
import { routerNavigatedAction } from '@ngrx/router-store';
import { Monitor } from './news.types';

const pageSize = 8;

@Injectable()
export class NewsEffects {
  loadNews$ = createEffect(() =>
    this.actions$.pipe(
      ofType(NewsActions.loadNews),
      withLatestFrom(
        this.store.select(selectPage),
        this.store.select(selectSelectedMonitor)
      ),
      concatMap(([action, page, monitorId]) =>
        this.newsReportsQuery
          .fetch({
            identity: monitorId as string,
          })
          .pipe(
            map((response) =>
              NewsActions.loadNewsSuccess({
                monitor: response.data.monitor as Monitor,
              })
            ),
            catchError((error) => of(NewsActions.loadNewsFailure({ error })))
          )
      )
    )
  );

  resetCategoriesFilter$ = createEffect(() =>
    this.actions$.pipe(
      ofType(routerNavigatedAction),
      filter((action) =>
        action.payload.routerState.url.includes('/seiten/nachrichten')
      ),
      switchMap(() => {
        return [NewsActions.resetPage(), NewsActions.loadNews()];
      })
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store,
    private newsReportsQuery: NewsReportsGQL
  ) {}
}
