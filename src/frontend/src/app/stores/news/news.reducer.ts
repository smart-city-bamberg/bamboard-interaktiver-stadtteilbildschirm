import { createReducer, on } from '@ngrx/store';
import * as NewsActions from './news.actions';
import { Monitor } from './news.types';

export const newsFeatureKey = 'news';

export interface State {
  monitor?: Monitor;
  page: number;
  showNewsPost: boolean;
}

export const initialState: State = {
  page: 0,
  showNewsPost: false,
};

export const reducer = createReducer(
  initialState,

  on(NewsActions.loadNews, (state) => state),
  on(NewsActions.loadNewsSuccess, (state, action) => ({
    ...state,
    monitor: action.monitor,
  })),
  on(NewsActions.loadNewsFailure, (state, action) => state),

  on(NewsActions.nextPage, (state, action) => ({
    ...state,
    page: state.page + 1,
  })),
  on(NewsActions.previousPage, (state, action) => ({
    ...state,
    page: Math.max(0, state.page - 1),
  })),
  on(NewsActions.resetPage, (state, action) => ({
    ...state,
    page: 0,
  })),
  on(NewsActions.openNewsPost, (state, action) => ({
    ...state,
    showNewsPost: true,
  })),
  on(NewsActions.closeNewsPost, (state, action) => ({
    ...state,
    showNewsPost: false,
  }))
);
