import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromNews from './news.reducer';
import { NewsReport } from './news.types';

const pageSize = 8;

export const selectNewsState = createFeatureSelector<fromNews.State>(
  fromNews.newsFeatureKey
);

export const selectDefaultNewsImage = createSelector(
  selectNewsState,
  (state) => state.monitor?.group?.defaultNewsImage?.url
);

export const selectPage = createSelector(
  selectNewsState,
  (state) => state.page
);

export const selectNewsReports = createSelector(
  selectNewsState,
  selectDefaultNewsImage,
  selectPage,
  (state, defaultImage, page) => {
    const result = [...(state.monitor?.group.newsReports ?? [])];

    if (state.monitor?.group?.newsProviders) {
      result.push(
        ...state.monitor?.group?.newsProviders
          .map((provider) =>
            provider.newsReports.map((report) => ({
              ...report,
              image: report.image ?? {
                url: defaultImage ?? '/assets/images/default_news.png',
              },
            }))
          )
          .flat()
      );
    }

    return result
      .reduce(
        (sum, next) =>
          sum.some((report) => report.id === next.id) ? sum : [...sum, next],
        new Array<NewsReport>()
      )
      .sort(
        (a, b) =>
          new Date(b.timestamp).getTime() - new Date(a.timestamp).getTime()
      )
      .slice(page * pageSize, (page + 1) * pageSize);
  }
);

export const selectBigNewsReports = createSelector(selectNewsReports, (state) =>
  state?.slice(0, 3)
);

export const selectSmallNewsReports = createSelector(
  selectNewsReports,
  (state) => state?.slice(3, pageSize)
);

export const selectPageCount = createSelector(selectNewsReports, (reports) =>
  Math.ceil(reports.length / pageSize)
);

export const selectHasNextPage = createSelector(
  selectPageCount,
  selectPage,
  (pageCount, page) => page + 1 < pageCount
);

export const selectHasPreviousPage = createSelector(
  selectPage,
  (page) => page > 0
);

export const selectShowNewsPost = createSelector(
  selectNewsState,
  (state) => state.showNewsPost
);
