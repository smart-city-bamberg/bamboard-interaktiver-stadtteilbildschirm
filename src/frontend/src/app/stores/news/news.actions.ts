import { createAction, props } from '@ngrx/store';
import { Monitor } from './news.types';

export const loadNews = createAction('[News] Load News');

export const loadNewsSuccess = createAction(
  '[News] Load News Success',
  props<{ monitor: Monitor }>()
);

export const loadNewsFailure = createAction(
  '[News] Load News Failure',
  props<{ error: any }>()
);

export const nextPage = createAction('[News] Next Page');
export const previousPage = createAction('[News] Previous Page');
export const resetPage = createAction('[News] Reset Page');

export const openNewsPost = createAction('[News] Open News Post');
export const closeNewsPost = createAction('[News] Close News Post');
