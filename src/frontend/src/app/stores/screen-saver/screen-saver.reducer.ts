import { createReducer, on } from '@ngrx/store';
import * as ScreenSaverActions from './screen-saver.actions';
import { environment } from 'src/environments/environment';

export const screenSaverFeatureKey = 'screenSaver';

export interface State {
  isActive: boolean;
  pageIndex: number;
}

export const initialState: State = {
  isActive: false,
  pageIndex: 0,
};

const pageCount = environment.menu.length;

export const reducer = createReducer(
  initialState,

  on(ScreenSaverActions.enable, (state) => ({
    ...state,
    isActive: true,
    pageIndex: 0,
  })),
  on(ScreenSaverActions.disable, (state) => ({
    ...state,
    isActive: false,
  })),
  on(ScreenSaverActions.nextPage, (state) => ({
    ...state,
    pageIndex: state.pageIndex < pageCount - 1 ? state.pageIndex + 1 : 0,
  }))
);
