import { createAction, props } from '@ngrx/store';

export const enable = createAction('[ScreenSaver] Enable');
export const enabled = createAction('[ScreenSaver] Enabled');

export const disable = createAction('[ScreenSaver] Disable');
export const disabled = createAction('[ScreenSaver] Disabled');

export const nextPage = createAction('[ScreenSaver] Next Page');
