import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromScreenSaver from './screen-saver.reducer';
import { environment } from 'src/environments/environment';

export const selectState = createFeatureSelector<fromScreenSaver.State>(
  fromScreenSaver.screenSaverFeatureKey
);

export const selectEnabled = createSelector(
  selectState,
  (state) => state.isActive
);

export const selectPageIndex = createSelector(
  selectState,
  (state) => state.pageIndex
);

export const selectActivePage = createSelector(
  selectState,
  (state) => environment.menu[state.pageIndex]
);

export const selectIsPageVisible = (page: string) =>
  createSelector(
    selectEnabled,
    selectPageIndex,
    (enabled, pageIndex) =>
      enabled && pageIndex === environment.menu.indexOf(page)
  );
