import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { delay, map, switchMap, tap } from 'rxjs/operators';
import { of, interval } from 'rxjs';
import * as ScreenSaverActions from './screen-saver.actions';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable()
export class ScreenSaverEffects {
  enableScreenSaver$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScreenSaverActions.enable),
      tap((action) => {
        this.router.navigate(['/seiten/screensaver']);
      }),
      map(() => ScreenSaverActions.enabled()),
      delay(environment.screenSaver.entireDuration),
      map(() => ScreenSaverActions.disable()),
      tap((action) => {
        this.router.navigate(['/seiten/unser-haus']);
      })
    );
  });

  nextPageInterval$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ScreenSaverActions.enable, ScreenSaverActions.disable),
      switchMap((action) => {
        if (action.type == ScreenSaverActions.enable.type) {
          return interval(environment.screenSaver.pageDuration).pipe(
            map((time) => ScreenSaverActions.nextPage())
          );
        }

        return of();
      })
    );
  });

  constructor(private actions$: Actions, private router: Router) {}
}
