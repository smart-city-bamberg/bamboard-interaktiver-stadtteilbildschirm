import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import * as fromScreenSaver from './screen-saver.reducer';
import { EffectsModule } from '@ngrx/effects';
import { ScreenSaverEffects } from './screen-saver.effects';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(fromScreenSaver.screenSaverFeatureKey, fromScreenSaver.reducer),
    EffectsModule.forFeature([ScreenSaverEffects])
  ]
})
export class ScreenSaverModule { }
