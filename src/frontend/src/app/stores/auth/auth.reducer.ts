import { Action, createReducer, on } from '@ngrx/store';
import * as AuthActions from './auth.actions';

export const authFeatureKey = 'auth';

export interface State {
  username?: string;
  password?: string;
  loginSuccess?: boolean;
}

export const initialState: State = {};

export const reducer = createReducer(
  initialState,
  on(AuthActions.login, (state, action) => ({
    ...state,
    loginSuccess: undefined,
  })),
  on(AuthActions.loginSuccess, (state, action) => ({
    ...state,
    loginSuccess: true,
  })),
  on(AuthActions.loginFailure, (state, action) => ({
    ...state,
    loginSuccess: false,
  })),
  on(AuthActions.logoutSuccess, (state, action) => ({
    ...state,
    loginSuccess: undefined,
  })),
  on(AuthActions.updateUsername, (state, action) => ({
    ...state,
    username: action.username,
  })),
  on(AuthActions.updatePassword, (state, action) => ({
    ...state,
    password: action.password,
  })),
  on(AuthActions.loadCredentialsSuccess, (state, action) => ({
    ...state,
    ...action,
  }))
);
