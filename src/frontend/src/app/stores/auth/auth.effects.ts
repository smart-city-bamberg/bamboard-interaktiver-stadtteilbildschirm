import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import {
  catchError,
  map,
  concatMap,
  withLatestFrom,
  tap,
  filter,
} from 'rxjs/operators';
import { of } from 'rxjs';
import * as AuthActions from './auth.actions';
import {
  AuthenticateUserWithPasswordGQL,
  LogoutGQL,
} from 'src/graphql/generated';
import { Store } from '@ngrx/store';
import { selectPassword, selectUsername } from './auth.selectors';
import { getRouterSelectors } from '@ngrx/router-store';

const { selectCurrentRoute } = getRouterSelectors();

@Injectable()
export class AuthEffects {
  login$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(AuthActions.login),
      withLatestFrom(
        this.store.select(selectUsername),
        this.store.select(selectPassword)
      ),
      concatMap(([action, username, password]) =>
        this.loginMutation
          .mutate({
            email: username as string,
            password: password as string,
          })
          .pipe(
            map((response) => {
              if (!response.data?.authenticateUserWithPassword) {
                return AuthActions.loginFailure({ error: response.errors });
              }

              if ('message' in response.data?.authenticateUserWithPassword) {
                return AuthActions.loginFailure({
                  error: response.data?.authenticateUserWithPassword?.message,
                });
              }

              return AuthActions.loginSuccess({
                data: response.data.authenticateUserWithPassword,
              });
            }),
            catchError((error) => of(AuthActions.loginFailure({ error })))
          )
      )
    );
  });

  logout$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.logout),
      concatMap((action) =>
        this.logoutMutation.mutate().pipe(
          map((data) => AuthActions.logoutSuccess({ data })),
          catchError((error) => of(AuthActions.logoutFailure({ error })))
        )
      )
    )
  );

  saveCredentialsAfterLoginSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.loginSuccess),
      withLatestFrom(this.store.select(selectCurrentRoute)),
      filter(
        ([action, route]) => route.routeConfig.path == 'bildschirmauswahl'
      ),
      withLatestFrom(
        this.store.select(selectUsername),
        this.store.select(selectPassword)
      ),
      tap(([action, username, password]) => {
        localStorage.setItem('username', username as string);
        localStorage.setItem('password', password as string);
      }),
      map(() => AuthActions.credentialsSaved())
    )
  );

  loadCredentials$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.loadCredentials),
      map(() =>
        AuthActions.loadCredentialsSuccess({
          username: localStorage.getItem('username') as string,
          password: localStorage.getItem('password') as string,
        })
      )
    )
  );

  deleteCredentialsAfterLogout$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.logoutSuccess),
      tap(() => {
        localStorage.removeItem('username');
        localStorage.removeItem('password');
      }),
      map(() => AuthActions.credentialsDeleted())
    )
  );

  loginAfterLoadCredentialsSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(AuthActions.loadCredentialsSuccess),
      map(() => AuthActions.login())
    )
  );

  loadCredentialsAfterAppStart$ = createEffect(() =>
    of(0).pipe(map(() => AuthActions.loadCredentials()))
  );

  constructor(
    private actions$: Actions,
    private store: Store,
    private loginMutation: AuthenticateUserWithPasswordGQL,
    private logoutMutation: LogoutGQL
  ) {}
}
