import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromAuth from './auth.reducer';

export const selectAuthState = createFeatureSelector<fromAuth.State>(
  fromAuth.authFeatureKey
);

export const selectUsername = createSelector(
  selectAuthState,
  (state) => state.username
);

export const selectPassword = createSelector(
  selectAuthState,
  (state) => state.password
);

export const selectLoginSuccess = createSelector(
  selectAuthState,
  (state) => state.loginSuccess === true
);

export const selectLoginError = createSelector(
  selectAuthState,
  (state) => state.loginSuccess === false
);
