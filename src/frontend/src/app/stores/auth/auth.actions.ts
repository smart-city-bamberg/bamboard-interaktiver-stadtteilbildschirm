import { createAction, props } from '@ngrx/store';
import { AuthenticateUserWithPasswordMutation } from 'src/graphql/generated';

export const updateUsername = createAction(
  '[Auth] Update Username',
  props<{ username: string }>()
);

export const updatePassword = createAction(
  '[Auth] Update Password',
  props<{ password: string }>()
);

export const loadCredentials = createAction('[Auth] Load Credentials');

export const loadCredentialsSuccess = createAction(
  '[Auth] Load Credentials Success',
  props<{ username: string; password: string }>()
);

export const login = createAction('[Auth] Login');

export const loginSuccess = createAction(
  '[Auth] Login Success',
  props<{
    data: AuthenticateUserWithPasswordMutation['authenticateUserWithPassword'];
  }>()
);

export const loginFailure = createAction(
  '[Auth] Login Failure',
  props<{ error: any }>()
);

export const logout = createAction('[Auth] Logout');

export const logoutSuccess = createAction(
  '[Auth] Logout Success',
  props<{ data: any }>()
);

export const logoutFailure = createAction(
  '[Auth] Logout Failure',
  props<{ error: any }>()
);

export const credentialsSaved = createAction('[Auth] Credentials Saved');

export const credentialsDeleted = createAction('[Auth] Credentials Deleted');
