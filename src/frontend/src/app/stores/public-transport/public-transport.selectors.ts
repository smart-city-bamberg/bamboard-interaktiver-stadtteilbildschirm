import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromPublicTransport from './public-transport.reducer';

export const selectPublicTransportState =
  createFeatureSelector<fromPublicTransport.State>(
    fromPublicTransport.publicTransportFeatureKey
  );

export const selectPublicTransport = createSelector(
  selectPublicTransportState,
  (state) => state.publicTransport
);

export const selectPublicTransportLink = createSelector(
  selectPublicTransportState,
  (state) => state?.publicTransport?.publicTransportDepartureLink
);

export const selectPublicTransportHeadline = createSelector(
  selectPublicTransportState,
  (state) => state?.publicTransport?.publicTransportSettings?.headline
);

export const selectPublicTransportContent = createSelector(
  selectPublicTransportState,
  (state) => state?.publicTransport?.publicTransportSettings?.content?.document
);

export const selectPublicTransportImage = createSelector(
  selectPublicTransportState,
  (state) => state?.publicTransport?.publicTransportSettings?.image
);

export const selectPublicTransportServiceHeadline = createSelector(
  selectPublicTransportState,
  (state) => state?.publicTransport?.publicTransportSettings?.service
);

export const selectPublicTransportServiceContent = createSelector(
  selectPublicTransportState,
  (state) =>
    state?.publicTransport?.publicTransportSettings?.servicedata?.document
);
