import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, concatMap, withLatestFrom } from 'rxjs/operators';
import { Observable, EMPTY, of } from 'rxjs';
import * as PublicTransportActions from './public-transport.actions';
import { PublicTransportGQL } from 'src/graphql/generated';
import { environment } from 'src/environments/environment';
import { selectSelectedMonitor } from '../monitor/monitor.selectors';
import { Store } from '@ngrx/store';

@Injectable()
export class PublicTransportEffects {
  loadPublicTransports$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PublicTransportActions.loadPublicTransport),
      withLatestFrom(this.store.select(selectSelectedMonitor)),
      concatMap(([action, monitorId]) =>
        this.publicTransportQuery
          .fetch({
            identity: monitorId as string,
          })
          .pipe(
            map((response) =>
              PublicTransportActions.loadPublicTransportSuccess({
                data: response.data.monitor,
              })
            ),
            catchError((error) =>
              of(PublicTransportActions.loadPublicTransportFailure({ error }))
            )
          )
      )
    );
  });

  constructor(
    private actions$: Actions,
    private store: Store,
    private publicTransportQuery: PublicTransportGQL
  ) {}
}
