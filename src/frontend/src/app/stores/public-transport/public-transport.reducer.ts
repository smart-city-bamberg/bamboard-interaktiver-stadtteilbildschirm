import { Action, createReducer, on } from '@ngrx/store';
import * as PublicTransportActions from './public-transport.actions';
import { PublicTransportQuery } from 'src/graphql/generated';

export const publicTransportFeatureKey = 'publicTransport';

export interface State {
  publicTransport?: PublicTransportQuery['monitor'];
}

export const initialState: State = {};

export const reducer = createReducer(
  initialState,

  on(PublicTransportActions.loadPublicTransport, (state) => state),
  on(PublicTransportActions.loadPublicTransportSuccess, (state, action) => ({
    ...state,
    publicTransport: action.data,
  })),
  on(
    PublicTransportActions.loadPublicTransportFailure,
    (state, action) => state
  )
);
