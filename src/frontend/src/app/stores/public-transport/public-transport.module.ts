import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import * as fromPublicTransport from './public-transport.reducer';
import { EffectsModule } from '@ngrx/effects';
import { PublicTransportEffects } from './public-transport.effects';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forFeature(fromPublicTransport.publicTransportFeatureKey, fromPublicTransport.reducer),
    EffectsModule.forFeature([PublicTransportEffects])
  ]
})
export class PublicTransportModule { }
