import { createAction, props } from '@ngrx/store';
import { PublicTransportQuery } from 'src/graphql/generated';

export const loadPublicTransport = createAction(
  '[PublicTransport] Load PublicTransport'
);

export const loadPublicTransportSuccess = createAction(
  '[PublicTransport] Load PublicTransport Success',
  props<{ data: PublicTransportQuery['monitor'] }>()
);

export const loadPublicTransportFailure = createAction(
  '[PublicTransport] Load PublicTransport Failure',
  props<{ error: any }>()
);
