import { Injectable, OnDestroy, isDevMode } from '@angular/core';
import { Store } from '@ngrx/store';
import { Context, SpanOptions, Tracer } from '@opentelemetry/api';
import { ZoneContextManager } from '@opentelemetry/context-zone';
import { OTLPTraceExporter } from '@opentelemetry/exporter-trace-otlp-http';
import {
  BatchSpanProcessor,
  ConsoleSpanExporter,
  SimpleSpanProcessor,
  WebTracerProvider,
} from '@opentelemetry/sdk-trace-web';
import { environment } from 'src/environments/environment';
import { selectMonitor } from '../stores/monitor/monitor.selectors';
import { BehaviorSubject, Subscription } from 'rxjs';
import { MonitorQuery } from 'src/graphql/generated';

@Injectable({
  providedIn: 'root',
})
export class OpenTelemetryServiceService implements OnDestroy {
  tracer: Tracer;

  private provider: WebTracerProvider;

  monitorSubscription: Subscription;
  monitor$ = new BehaviorSubject<MonitorQuery['monitor'] | undefined>(
    undefined
  );

  constructor(private store: Store) {
    this.provider = new WebTracerProvider();

    if (isDevMode()) {
      this.provider.addSpanProcessor(
        new SimpleSpanProcessor(new ConsoleSpanExporter())
      );
    }

    this.provider.addSpanProcessor(
      new BatchSpanProcessor(
        new OTLPTraceExporter({
          url: environment.openTelemetryUrl,
        })
      )
    );

    this.provider.register({ contextManager: new ZoneContextManager() });
    this.tracer = this.provider.getTracer('default-tracer-id');

    this.monitorSubscription = this.store
      .select(selectMonitor)
      .subscribe(this.monitor$);
  }

  setBasicElementSpan(
    elementId: string,
    elementTag: string,
    spanName: string,
    options?: SpanOptions,
    context?: Context
  ) {
    const span = this.tracer.startSpan(spanName, options, context);
    const monitor = this.monitor$.value;

    if (!monitor) {
      return;
    }

    span.setAttributes({
      elementId,
      elementTag,
      monitorId: monitor.id,
      monitorName: monitor.name as string,
      organizationId: monitor.organization?.id as string,
      organizationName: monitor.organization?.name as string,
    });
    span.end();
  }

  trackPageViewDuration(
    page: string,
    duration: number,
    options?: SpanOptions,
    context?: Context
  ) {
    const monitor = this.monitor$.value;

    if (!monitor) {
      return;
    }

    this.tracer
      .startSpan('pageView', options, context)
      .setAttributes({
        page,
        duration,
        monitorId: monitor.id,
        monitorName: monitor.name as string,
        organizationId: monitor.organization?.id as string,
        organizationName: monitor.organization?.name as string,
      })
      .end();
  }

  trackNavigation(page: string, options?: SpanOptions, context?: Context) {
    const monitor = this.monitor$.value;

    if (!monitor) {
      return;
    }

    this.tracer
      .startSpan('navigation', options, context)
      .setAttributes({
        page,
        monitorId: monitor.id,
        monitorName: monitor.name as string,
        organizationId: monitor.organization?.id as string,
        organizationName: monitor.organization?.name as string,
      })
      .end();
  }

  trackFeature(feature: string, options?: SpanOptions, context?: Context) {
    const monitor = this.monitor$.value;

    if (!monitor) {
      return;
    }

    this.tracer
      .startSpan('feature', options, context)
      .setAttributes({
        page: feature,
        monitorId: monitor.id,
        monitorName: monitor.name as string,
        organizationId: monitor.organization?.id as string,
        organizationName: monitor.organization?.name as string,
      })
      .end();
  }

  trackClick(
    x: number,
    y: number,
    width: number,
    height: number,
    options?: SpanOptions,
    context?: Context
  ) {
    const monitor = this.monitor$.value;

    if (!monitor) {
      return;
    }

    this.tracer
      .startSpan('click', options, context)
      .setAttributes({
        locationX: x,
        locationY: y,
        screenWidth: width,
        screenHeight: height,
        monitorId: monitor.id,
        monitorName: monitor.name as string,
        organizationId: monitor.organization?.id as string,
        organizationName: monitor.organization?.name as string,
      })
      .end();
  }

  ngOnDestroy() {
    this.provider.shutdown();
    this.monitorSubscription.unsubscribe();
  }
}
