import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { Router } from '@angular/router';
import { OpenTelemetryServiceService } from './open-telemetry-service.service';
import {
  routerNavigatedAction,
  routerNavigationAction,
} from '@ngrx/router-store';
import { Store } from '@ngrx/store';
import { selectMonitor } from '../stores/monitor/monitor.selectors';

@Injectable()
export class TracingEffects {
  tracePageViewDuration$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(routerNavigatedAction),
        map((action) => ({
          action,
          start: Date.now(),
        })),
        switchMap(({ action, start }) =>
          this.actions$.pipe(
            ofType(routerNavigationAction),
            withLatestFrom(this.store.select(selectMonitor)),
            tap(([action, monitor]) =>
              this.telemetryService.trackPageViewDuration(
                action.payload.routerState.url,
                Date.now() - start,
                monitor as any
              )
            )
          )
        )
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private router: Router,
    private telemetryService: OpenTelemetryServiceService,
    private store: Store
  ) {}
}
