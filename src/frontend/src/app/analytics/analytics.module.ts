import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OpenTelemetryServiceService } from './open-telemetry-service.service';
import { ClickTracingDirective } from './directives/click-tracking.directive';
import { NavigationTrackingDirective } from './directives/navigation-tracking.directive';
import { FeatureTrackingDirective } from './directives/feature-tracking.directive';

@NgModule({
  declarations: [
    ClickTracingDirective,
    NavigationTrackingDirective,
    FeatureTrackingDirective,
  ],
  imports: [CommonModule],
  providers: [OpenTelemetryServiceService],
  exports: [
    ClickTracingDirective,
    NavigationTrackingDirective,
    FeatureTrackingDirective,
  ],
})
export class AnalyticsModule {}
