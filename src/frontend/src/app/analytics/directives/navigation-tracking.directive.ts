import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { OpenTelemetryServiceService } from '../open-telemetry-service.service';

@Directive({
  selector: '*[trackNavigation]',
})
export class NavigationTrackingDirective {
  @Input()
  page: string = '';

  @HostListener('click')
  clickListener() {
    this.telemetryService.trackNavigation(this.page);
  }

  constructor(private telemetryService: OpenTelemetryServiceService) {}
}
