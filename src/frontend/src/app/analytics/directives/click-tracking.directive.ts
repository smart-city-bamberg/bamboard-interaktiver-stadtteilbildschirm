import { Directive, ElementRef, HostListener } from '@angular/core';
import { OpenTelemetryServiceService } from '../open-telemetry-service.service';

@Directive({
  selector: '[analyticsClickTracking]',
})
export class ClickTracingDirective {
  @HostListener('click', ['$event'])
  clickListener($event: any) {
    const x = $event.x;
    const y = $event.y;
    const view = $event.view;
    const width = view.innerWidth;
    const height = view.innerHeight;

    this.telemetryService.trackClick(x, y, width, height);
  }

  constructor(private telemetryService: OpenTelemetryServiceService) {}
}
