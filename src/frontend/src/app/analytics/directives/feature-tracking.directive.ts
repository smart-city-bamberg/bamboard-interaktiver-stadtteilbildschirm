import { Directive, ElementRef, HostListener, Input } from '@angular/core';
import { OpenTelemetryServiceService } from '../open-telemetry-service.service';

@Directive({
  selector: '*[trackFeature]',
})
export class FeatureTrackingDirective {
  @Input()
  feature: string = '';

  @HostListener('click')
  clickListener() {
    this.telemetryService.trackFeature(this.feature);
  }

  constructor(private telemetryService: OpenTelemetryServiceService) {}
}
