import { AfterViewInit, Component, OnDestroy, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import * as screenSaverSelectors from '../stores/screen-saver/screen-saver.selectors';
import { disable } from '../stores/screen-saver/screen-saver.actions';
import { Subscription } from 'rxjs';
import { ProgressBarComponent } from '../components/progress-bar.component';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-screensaver',
  template: `
    <div class="flex-grow px-16 pb-16" (click)="closeScreensaver()">
      <app-progress-bar #progressBar [duration]="progressBarDuration" />
      <div class="contents" [ngSwitch]="activePage$ | async">
        <app-screensaver-our-house *ngSwitchCase="'our-house'" />
        <app-screensaver-public-transport *ngSwitchCase="'public-transport'" />
        <app-screensaver-news *ngSwitchCase="'news'" />
        <app-screensaver-culture *ngSwitchCase="'culture'" />
      </div>
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex;
      }
    `,
  ],
})
export class ScreensaverComponent implements AfterViewInit, OnDestroy {
  activePage$ = this.store.select(screenSaverSelectors.selectActivePage);
  progressBarDuration = environment.screenSaver.pageDuration;

  @ViewChild('progressBar')
  progressBar?: ProgressBarComponent;
  resetSubscription?: Subscription;

  constructor(private store: Store) {}

  closeScreensaver() {
    this.store.dispatch(disable());
  }

  ngAfterViewInit(): void {
    this.resetSubscription = this.activePage$.subscribe((_) => {
      this.progressBar?.reset();
    });
  }

  ngOnDestroy(): void {
    this.resetSubscription?.unsubscribe();
  }
}
