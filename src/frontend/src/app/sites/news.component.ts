import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  selectBigNewsReports,
  selectHasNextPage,
  selectHasPreviousPage,
  selectPage,
  selectPageCount,
  selectShowNewsPost,
  selectSmallNewsReports,
} from '../stores/news/news.selectors';
import { map } from 'rxjs';
import {
  closeNewsPost,
  nextPage,
  openNewsPost,
  previousPage,
} from '../stores/news/news.actions';

@Component({
  selector: 'app-nachrichten',
  template: `
    <div>
      <div class="ueberschrift flex flex-row items-center">
        <i class="mi filled text-3xl text-light-text">newspaper</i>
        <p class="pl-2">{{ 'nachrichten-bamberg' | translate }}</p>
      </div>
      <div class="mt-6 grid grid-cols-3 gap-4">
        <div
          *ngFor="let section of bigNews$ | async"
          (click)="openNewsPost(section)"
          trackFeature
          [feature]="'nachricht-auf'"
        >
          <div
            class="flex h-[20.75rem] w-full overflow-hidden rounded-xl border border-dark-text bg-cover bg-center text-white"
            [style.background-image]="'url(' + section.image.url + ')'"
          >
            <div class="flex w-full flex-col justify-between">
              <span
                class="m-3 ml-auto rounded-[0.2rem] bg-red-light px-1 font-bold"
                >{{ section.category }}</span
              >
              <div class="bg-black bg-opacity-50 px-4 py-2">
                <p class="text-[0.938rem]">
                  {{ section.timestamp | date : 'dd.MM.yyyy' }}
                </p>
                <p
                  class="mt-1 truncate font-bold"
                  [innerHTML]="section.title"
                ></p>
                <div class="">
                  <p
                    class="max-h-20 overflow-hidden text-[0.938rem]"
                    style="display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 3;"
                    [innerHTML]="section.previewContent"
                  ></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="mt-6 grid grid-cols-5 gap-3">
        <div
          *ngFor="let section of smallNews$ | async"
          (click)="openNewsPost(section)"
          trackFeature
          [feature]="'nachricht-auf'"
          class="h-[17.668rem] w-full overflow-hidden rounded-xl border border-dark-text"
        >
          <img
            class="h-[11.653rem] w-full border-b object-cover"
            [src]="section.image.url"
            alt="smallPicture"
          />
          <div class="mx-3 mt-2 flex flex-col justify-center">
            <div class="flex flex-row items-center text-[0.625rem]">
              <p>{{ section.timestamp | date : 'dd.MM.yyyy' }}</p>
              <p
                class="mx-1 inline-block h-[0.15rem] w-[0.15rem] rounded-full bg-gray-500"
              ></p>
              <p class="font-bold text-red-light">
                {{ section.category }}
              </p>
            </div>
            <p
              class="my-1 max-h-[3.9rem] overflow-hidden font-bold leading-[1.3em]"
              style="display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 3; line-clamp: 3;"
              [innerHTML]="section.title"
            ></p>
          </div>
        </div>
      </div>
    </div>

    <div
      *ngIf="showNewsPost$ | async"
      class="fixed inset-0 z-10 h-screen w-screen backdrop-blur-[10px]"
      (click)="closeNewsPost()"
      trackFeature
      [feature]="'nachricht-zu'"
    >
      <button class="button fixed bottom-[1.5%] right-[1%] z-10">
        Schließen
      </button>
    </div>
    <div
      *ngIf="showNewsPost$ | async"
      class="fixed top-1/2 left-1/2 z-20 flex h-[85%] w-[60%] -translate-x-1/2 -translate-y-1/2 transform flex-col rounded-lg border-2 border-[#525659] bg-[#FEFEFF] p-8"
    >
      <div class="flex flex-row items-center justify-between">
        <div>
          <div class="flex flex-row items-center text-[0.938rem]">
            <p>{{ popupNews.timestamp | date : 'dd.MM.yyyy' }}</p>
            <p
              class="mx-3 inline-block h-[0.15rem] w-[0.15rem] rounded-full bg-gray-500"
            ></p>
            <p class="font-bold text-red-light">
              {{ popupNews.category?.name }}
            </p>
          </div>
          <p
            class="my-1 max-h-[7.4rem] overflow-hidden text-[2.25rem] font-bold leading-[1.1em]"
            style="display: -webkit-box; -webkit-box-orient: vertical; -webkit-line-clamp: 3; line-clamp: 3;"
            [innerHTML]="popupNews.title"
          ></p>
        </div>
      </div>
      <div class="mt-4 flex flex-grow flex-row justify-between">
        <div class="flex w-[35%] flex-col items-center">
          <img
            *ngIf="popupNews.image"
            class="w-full object-cover"
            [src]="popupNews.image.url"
            alt="Bild"
          />
          <div
            *ngIf="popupNews.link"
            class="mt-10 overflow-hidden rounded-xl border-4 border-[#0086CD]"
          >
            <qrcode
              [qrdata]="'' + popupNews.link + ''"
              [allowEmptyString]="true"
              [ariaLabel]="'QR Code zur Nachrichtenseite'"
              [cssClass]="'center'"
              [colorDark]="'#0086CD'"
              [colorLight]="'#ffffffff'"
              [elementType]="'canvas'"
              [errorCorrectionLevel]="'M'"
              [margin]="2"
              [scale]="1"
              [title]="popupNews.link"
              [width]="200"
            ></qrcode>
          </div>
          <p *ngIf="popupNews.link" class="button mt-4">
            {{ hostname(popupNews.link) }}
          </p>
        </div>
        <div class="flex w-[60%] flex-col">
          <div class="h-[0] flex-grow overflow-y-auto whitespace-pre-wrap">
            <app-rich-text [content]="popupNews.content?.document" />
          </div>
        </div>
      </div>
    </div>

    <div class="flex flex-row py-3">
      <button
        class="button"
        (click)="previousPage()"
        [disabled]="!(hasPreviousPage$ | async)"
        trackFeature
        [feature]="'nachricht-seite-davor'"
      >
        <i class="mi filled rotate-180 text-3xl text-light-text">forward</i>
        <p>{{ 'neuere-artikel' | translate }}</p>
      </button>
      <div
        class="border-gray mx-4 flex flex-grow items-center justify-center border-y text-[0.938rem]"
      >
        <p>Seite {{ page$ | async }} | {{ pageCount$ | async }}</p>
      </div>
      <button
        class="button"
        (click)="nextPage()"
        [disabled]="!(hasNextPage$ | async)"
        trackFeature
        [feature]="'nachricht-seite-danach'"
      >
        <p>{{ 'aeltere-artikel' | translate }}</p>
        <i class="mi filled text-3xl text-light-text">forward</i>
      </button>
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex flex-col h-full w-full text-dark-text flex-grow place-content-between;
      }
      .ueberschrift {
        @apply rounded bg-[#737373] text-white font-bold text-[1.575rem] p-0 pl-2;
      }
      .button {
        @apply flex flex-row items-center bg-blue-light disabled:bg-[#737373] border-blue-dark border-2 rounded-lg shadow-md text-white font-bold text-[1.1rem] p-2 gap-2;
      }
    `,
  ],
})
export class NewsComponent {
  bigNews$ = this.store.select(selectBigNewsReports);
  smallNews$ = this.store.select(selectSmallNewsReports);
  pageCount$ = this.store.select(selectPageCount);
  page$ = this.store.select(selectPage).pipe(map((page) => page + 1));
  hasNextPage$ = this.store.select(selectHasNextPage);
  hasPreviousPage$ = this.store.select(selectHasPreviousPage);
  showNewsPost$ = this.store.select(selectShowNewsPost);

  popupNews: any;

  constructor(private store: Store) {}

  previousPage() {
    this.store.dispatch(previousPage());
  }
  nextPage() {
    this.store.dispatch(nextPage());
  }

  // openDocument(section: any) {
  //   this.showUrl = true;
  //   this.popupNews = section;
  // }
  // closeDocument() {
  //   this.showUrl = false;
  //   this.popupNews = '';
  // }

  openNewsPost(section: any) {
    this.store.dispatch(openNewsPost());
    this.popupNews = section;
  }

  closeNewsPost() {
    this.store.dispatch(closeNewsPost());
  }

  hostname(link?: string): string {
    if (!link) {
      return '';
    }

    const url = new URL(link);
    return url.hostname;
  }
}
