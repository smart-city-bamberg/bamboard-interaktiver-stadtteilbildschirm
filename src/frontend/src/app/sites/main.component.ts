import { Component } from '@angular/core';
import { selectShowWarning } from '../stores/weather/weather.selectors';
import { Store, select } from '@ngrx/store';

@Component({
  template: `
    <div class="flex h-screen w-screen flex-row" analyticsClickTracking>
      <div class="m-2 flex min-w-[20.375rem] flex-col py-2 pl-2">
        <div class="card flex-grow">
          <app-menue></app-menue>
        </div>
        <div class="mt-4 flex h-[2.813rem] flex-col justify-center">
          <app-bedienung></app-bedienung>
        </div>
      </div>

      <div class="m-2 flex flex-grow flex-col py-2 pr-2">
        <div class="mb-2 flex h-[11.688rem] flex-row justify-between">
          <div class="card flex w-[19.625rem] flex-col justify-center">
            <app-uhrzeit></app-uhrzeit>
          </div>
          <div
            class="card mx-4 flex flex-grow overflow-auto"
            style="contain: inline-size"
          >
            <app-wetter></app-wetter>
          </div>
          <div class="card flex w-[17.188rem] justify-center">
            <app-betreiber-icon></app-betreiber-icon>
          </div>
        </div>
        <div class="my-2 flex h-10 flex-row" *ngIf="showWarnings$ | async">
          <app-warning-ticker></app-warning-ticker>
        </div>
        <div class="card mt-2 flex-grow overflow-auto">
          <router-outlet></router-outlet>
        </div>
      </div>
    </div>
  `,
  styles: [
    `
      .card {
        @apply bg-white border border-border rounded-lg shadow-md p-4;
      }
    `,
  ],
})
export class MainComponent {
  showWarnings$ = this.store.select(selectShowWarning);

  constructor(private store: Store) {}
}
