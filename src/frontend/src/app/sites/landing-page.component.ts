import { Component } from '@angular/core';

@Component({
  template: `
    <div class="text-2xl">
      ⚠️ Dieser Bildschirm wurde noch nicht eingerichtet.
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex h-screen w-screen items-center justify-center;
      }
    `,
  ],
})
export class LandingPageComponent {}
