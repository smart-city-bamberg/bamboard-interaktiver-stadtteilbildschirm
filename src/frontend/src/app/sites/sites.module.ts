import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OurHouseComponent } from './our-house.component';
import { PublicTransportComponent } from './public-transport.component';
import { NewsComponent } from './news.component';
import { CultureComponent } from './culture.component';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from '../components/components.module';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { QRCodeModule } from 'angularx-qrcode';
import { RouterModule } from '@angular/router';
import { LandingPageComponent } from './landing-page.component';
import { MainComponent } from './main.component';
import { MonitorSelectorComponent } from './monitor-selector.component';
import { ScreensaverComponent } from './screensaver.component';
import { ProgressBarComponent } from '../components/progress-bar.component';
import { AnalyticsModule } from '../analytics/analytics.module';

@NgModule({
  declarations: [
    OurHouseComponent,
    PublicTransportComponent,
    NewsComponent,
    CultureComponent,
    LandingPageComponent,
    MainComponent,
    MonitorSelectorComponent,
    ScreensaverComponent,
  ],
  imports: [
    CommonModule,
    TranslateModule,
    ComponentsModule,
    NgxExtendedPdfViewerModule,
    QRCodeModule,
    RouterModule,
    AnalyticsModule,
  ],
  exports: [
    OurHouseComponent,
    PublicTransportComponent,
    NewsComponent,
    CultureComponent,
    LandingPageComponent,
    MainComponent,
    MonitorSelectorComponent,
    ScreensaverComponent,
  ],
})
export class SitesModule {}
