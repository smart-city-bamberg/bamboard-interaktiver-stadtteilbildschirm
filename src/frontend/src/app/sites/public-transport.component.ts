import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  selectPublicTransport,
  selectPublicTransportContent,
  selectPublicTransportHeadline,
  selectPublicTransportImage,
  selectPublicTransportLink,
  selectPublicTransportServiceContent,
  selectPublicTransportServiceHeadline,
} from '../stores/public-transport/public-transport.selectors';
import { DomSanitizer } from '@angular/platform-browser';
import { map } from 'rxjs';

@Component({
  selector: 'app-oepnv',
  template: `
    <div class="flex h-full w-full flex-row text-dark-text">
      <div class="flex flex-grow flex-col">
        <div class="ueberschrift flex flex-row items-center">
          <i class="mi filled text-3xl text-light-text">directions_bus</i>
          <p class="pl-2">
            {{ 'oeffentlicher-nahverkehr' | translate }}
          </p>
        </div>
        <div class="mt-4 flex-grow">
          <iframe
            class="h-full w-full rounded hue-rotate-[50deg] filter"
            [src]="publicTransportLink$ | async"
          ></iframe>
        </div>
      </div>

      <div class="flex w-[29.563rem] flex-col pl-8">
        <div class="ueberschrift flex flex-row items-center">
          <i class="mi filled text-3xl text-light-text">account_box</i>
          <p class="pl-2">{{ publicTransportHeadline$ | async }}</p>
        </div>
        <div class="mt-4 flex flex-row justify-between">
          <div class="flex w-full flex-col content-center items-center">
            <img [src]="publicTransportImageUrl$ | async" alt="qr-code" />
          </div>
        </div>
        <div class="my-8 h-[0] flex-grow overflow-y-auto">
          <app-rich-text [content]="publicTransportContent$ | async" />
        </div>
        <div class="ueberschrift flex flex-row items-center">
          <i class="mi filled text-3xl text-light-text">account_box</i>
          <p class="pl-2">
            {{ publicTransportServiceHeadline$ | async }}
          </p>
        </div>
        <div class="mt-1 mb-[0.1rem] max-h-24 overflow-y-auto leading-5">
          <app-rich-text [content]="publicTransportServiceContent$ | async" />
        </div>
      </div>
    </div>
  `,
  styles: [
    `
      .ueberschrift {
        @apply rounded bg-[#737373] text-white font-bold text-[1.575rem] p-0 pl-2;
      }
      img[src$='.svg'] {
        width: 1.663rem;
      }
    `,
  ],
})
export class PublicTransportComponent {
  publicTransport$ = this.store.select(selectPublicTransport);
  publicTransportHeadline$ = this.store.select(selectPublicTransportHeadline);
  publicTransportContent$ = this.store.select(selectPublicTransportContent);
  publicTransportServiceHeadline$ = this.store.select(
    selectPublicTransportServiceHeadline
  );
  publicTransportServiceContent$ = this.store.select(
    selectPublicTransportServiceContent
  );

  publicTransportLink$ = this.store
    .select(selectPublicTransportLink)
    .pipe(
      map((link) => this.sanitizer.bypassSecurityTrustResourceUrl(link || ''))
    );

  publicTransportImageUrl$ = this.store
    .select(selectPublicTransportImage)
    .pipe(
      map((image) =>
        this.sanitizer.bypassSecurityTrustResourceUrl(image?.url || '')
      )
    );

  constructor(private store: Store, private sanitizer: DomSanitizer) {}
}
