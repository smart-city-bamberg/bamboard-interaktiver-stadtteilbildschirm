import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { selectMonitors } from '../stores/monitor/monitor.selectors';
import { selectMonitor } from '../stores/monitor/monitor.actions';
import * as authActions from '../stores/auth/auth.actions';
import {
  selectLoginError,
  selectLoginSuccess,
  selectUsername,
} from '../stores/auth/auth.selectors';

@Component({
  template: `
    <div
      class="flex flex-col rounded-lg shadow-md border-gray-400 border-2 my-2 p-4"
    >
      <div *ngIf="!(loginSuccess$ | async)">
        <div class="flex flex-row py-2">
          <span class="w-32">Benutzer</span>
          <span class="w-96"
            ><input
              type="text"
              class="p-1 w-full"
              (input)="updateUsername($event)"
            />
          </span>
        </div>
        <div class="flex flex-row py-2">
          <span class="w-32">Passwort</span>
          <span class="w-96"
            ><input
              type="password"
              class="p-1 w-full"
              (input)="updatePassword($event)"
            />
          </span>
        </div>
        <div>
          <button
            *ngIf="!(loginSuccess$ | async)"
            class="p-2 px-4 bg-blue-600 text-white mr-2 rounded-lg"
            (click)="login()"
          >
            Anmelden
          </button>
        </div>
      </div>
      <div class="flex flex-col py-2 items-start">
        <span class="my-2">{{ username$ | async }}</span>
        <button
          *ngIf="loginSuccess$ | async"
          class="p-2 px-4 bg-blue-600 text-white mr-2 rounded-lg"
          (click)="logout()"
        >
          Abmelden
        </button>
      </div>
      <div *ngIf="loginSuccess$ | async">✅ Login erfolgreich</div>
      <div *ngIf="loginError$ | async">❌ Login nicht erfolgreich</div>
    </div>

    <div class="flex flex-col">
      <div
        *ngFor="let monitor of monitors$ | async"
        class="flex flex-row rounded-lg shadow-md border-gray-400 border-2 my-2 p-2 justify-start cursor-pointer hover:bg-slate-200"
        (click)="selectMonitor(monitor.id)"
      >
        <img [src]="monitor.boardLogo?.url" class="object-contain w-16 mr-4" />
        <div class="">
          <div class="text-2xl">{{ monitor.name }}</div>
        </div>
      </div>
    </div>
  `,
  styles: [
    `
      :host {
        @apply flex flex-col p-8 h-screen w-screen;
      }
    `,
  ],
})
export class MonitorSelectorComponent {
  monitors$ = this.store.select(selectMonitors);
  loginSuccess$ = this.store.select(selectLoginSuccess);
  loginError$ = this.store.select(selectLoginError);
  username$ = this.store.select(selectUsername);

  constructor(private store: Store) {}

  selectMonitor(monitorId: string) {
    this.store.dispatch(selectMonitor({ monitorId }));
  }

  updateUsername(username: any) {
    this.store.dispatch(
      authActions.updateUsername({ username: username.target.value })
    );
  }

  updatePassword(password: any) {
    this.store.dispatch(
      authActions.updatePassword({ password: password.target.value })
    );
  }

  login() {
    this.store.dispatch(authActions.login());
  }

  logout() {
    this.store.dispatch(authActions.logout());
  }
}
