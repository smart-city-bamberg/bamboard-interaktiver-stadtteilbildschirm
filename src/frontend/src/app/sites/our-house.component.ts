import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as moment from 'moment';
import { pdfDefaultOptions } from 'ngx-extended-pdf-viewer';
import {
  selectAnnouncements,
  selectContact,
  selectDocumentUrl,
  selectDocuments,
  selectGarbageCollection,
  selectGarbageCollectionAssignment,
  selectNoAnnouncements,
  selectNoDocuments,
  selectshowDocument,
} from '../stores/our-house/our-house.selectors';
import {
  DefaultLangChangeEvent,
  LangChangeEvent,
  TranslateService,
} from '@ngx-translate/core';
import { combineLatest, filter, map, merge, of } from 'rxjs';
import {
  closeDocument,
  openDocument,
} from '../stores/our-house/our-house.actions';

@Component({
  selector: 'app-unser-haus',
  template: `
    <div class="flex h-full w-full flex-row text-dark-text">
      <div class="flex w-2/3 flex-col overflow-y-auto">
        <div class="ueberschrift flex flex-row items-center">
          <i class="mi filled text-3xl text-light-text">mail</i>
          <p class="pl-2">{{ 'dokumente' | translate }}</p>
        </div>
        <div class="mt-4 flex-grow">
          <div class="flex flex-row flex-wrap gap-2">
            <p
              class="flex flex-grow justify-center text-xl"
              *ngIf="noDocuments$ | async"
            >
              Keine Dokumente vorhanden
            </p>
            <button
              class="button"
              *ngFor="let document of documents$ | async"
              (click)="openDocument(document.content?.url)"
              trackFeature
              [feature]="'dokument-auf'"
            >
              <i class="mi filled text-xl text-light-text">description</i>
              <p>{{ document.name }}</p>
            </button>
          </div>

          <div
            *ngIf="showPdf$ | async"
            class="fixed inset-0 z-10 h-screen w-screen backdrop-blur-[10px]"
            (click)="closeDocument()"
            trackFeature
            [feature]="'dokument-zu'"
          >
            <button class="button fixed bottom-[1.5%] right-[1%] z-10">
              Schließen
            </button>
          </div>
          <div
            *ngIf="showPdf$ | async"
            class="fixed left-1/2 top-1/2 z-20 h-[97%] w-[83%] -translate-x-1/2 -translate-y-1/2 transform rounded-lg border-2 border-[#525659] bg-[#e8e8eb] p-[0.2rem]"
          >
            <ngx-extended-pdf-viewer
              [src]="documentUrl"
              [textLayer]="true"
              [showToolbar]="false"
              [showZoomButtons]="true"
              [showHandToolButton]="false"
              [showPresentationModeButton]="false"
              [showDownloadButton]="false"
              [showBorders]="false"
              [minZoom]="0.7"
              spread="odd"
            >
            </ngx-extended-pdf-viewer>
          </div>

          <div class="accordion mt-6">
            <div class="ueberschrift mb-4 flex flex-row items-center">
              <i class="mi filled text-4xl text-light-text">campaign</i>
              <p class="pl-2">{{ 'mitteilungen' | translate }}</p>
            </div>
            <p
              class="flex flex-grow justify-center text-xl"
              *ngIf="noAnnouncements$ | async"
            >
              Keine Mitteilungen vorhanden
            </p>
            <div
              class="accordion-item border-x border-t border-dark-text bg-[#ececed] first:rounded-t last:rounded-b last:border-b"
              *ngFor="let section of announcements$ | async"
            >
              <div
                class="accordion-header items-center justify-between p-2 text-xl font-medium text-dark-text"
                (click)="toggleSection(section.id)"
                trackFeature
                [feature]="'aushang'"
              >
                <div class="flex flex-row items-center">
                  <i class="mi mr-2 text-[2rem]">{{
                    expanded === section.id ? 'expand_less' : 'expand_more'
                  }}</i>
                  <span class="" style="overflow-wrap: anywhere;">{{
                    section.title
                  }}</span>
                </div>
                <div
                  class="ml-2 flex min-w-[12rem] flex-row items-center justify-end"
                >
                  <div class="mr-4 min-w-10">
                    <img
                      *ngIf="isNew(section.timestamp)"
                      class=""
                      src="../assets/images/haus/neu.svg"
                      alt="Neu"
                    />
                  </div>
                  <i class="mi filled mr-2 text-3xl text-dark-text"
                    >mail_outline</i
                  >
                  <span>
                    {{ section.timestamp | date : 'dd.MM.yyyy' }}
                  </span>
                </div>
              </div>
              <div
                class="accordion-content bg-white"
                [ngClass]="{ expanded: expanded === section.id }"
              >
                <app-rich-text
                  [content]="section.content?.document"
                  class="block p-5 text-xl"
                />
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="flex w-1/3 flex-col pl-8">
        <div class="ueberschrift flex flex-row items-center">
          <i class="mi filled text-3xl text-light-text">account_box</i>
          <p class="pl-2">{{ 'kontakt' | translate }}</p>
        </div>
        <div class="my-6 h-[0] flex-grow overflow-y-auto text-[0.938rem]">
          <app-rich-text [content]="contact$ | async" />
        </div>
        <div *ngIf="garbageCollectionAssignment$ | async">
          <div class="ueberschrift mb-2 flex flex-row items-center">
            <i class="mi filled text-3xl text-light-text">recycling</i>
            <p class="pl-2">{{ 'muellentsorgung' | translate }}</p>
          </div>
          <div
            class="mt-3 grid grid-cols-2 gap-3"
            *ngIf="garbageCollection$ | async; let garbageCollection"
          >
            <div class="muell-card right bg-garbageCollection-restmuell/20">
              <div class="trash-can-background bg-garbageCollection-restmuell">
                <i class="mi filled text-xl text-light-text">delete</i>
              </div>

              <p class="rightToLeft">
                <span class="font-bold">{{ 'restmuell' | translate }}</span>
                <br />
                {{ getRemainingDaysFormatted(garbageCollection.restmuell) }}
              </p>
            </div>
            <div class="muell-card right bg-garbageCollection-papier/20">
              <div class="trash-can-background bg-garbageCollection-papier">
                <i class="mi filled text-xl text-light-text">delete</i>
              </div>
              <p class="rightToLeft">
                <span class="font-bold">{{ 'altpapier' | translate }}</span>
                <br />
                {{ getRemainingDaysFormatted(garbageCollection.papier) }}
              </p>
            </div>
            <div class="muell-card right bg-garbageCollection-gelber_sack/20">
              <div
                class="trash-can-background bg-garbageCollection-gelber_sack"
              >
                <i class="mi filled text-xl text-light-text">delete</i>
              </div>
              <p class="rightToLeft">
                <span class="font-bold">{{
                  'verpackungsmuell' | translate
                }}</span>
                <br />
                {{ getRemainingDaysFormatted(garbageCollection.gelber_sack) }}
              </p>
            </div>
            <div class="muell-card right bg-garbageCollection-biomuell/20">
              <div class="trash-can-background bg-garbageCollection-biomuell">
                <i class="mi filled text-xl text-light-text">delete</i>
              </div>
              <p class="rightToLeft">
                <span class="font-bold">{{ 'biomuell' | translate }}</span>
                <br />
                {{ getRemainingDaysFormatted(garbageCollection.biomeull) }}
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  `,
  styles: [
    `
      .ueberschrift {
        @apply rounded bg-[#737373] text-white font-bold text-[1.575rem] p-0 pl-2;
      }
      .muell-card {
        @apply leading-5 flex flex-row content-center items-center rounded flex-1 text-[0.938rem];
      }
      .button {
        @apply flex flex-row items-center bg-blue-light border-blue-dark border-2 rounded-lg shadow-md text-white font-bold text-[1.1rem] p-2 gap-2;
      }
      .accordion {
        @apply flex flex-col;
      }
      .accordion-header {
        @apply flex cursor-pointer py-4 pr-4 pl-2;
      }
      .accordion-content {
        @apply max-h-0 overflow-hidden transition-[max-height] duration-[0.2s] ease-[ease-out] border-t-2;
      }
      .accordion-content.expanded {
        @apply max-h-[14rem] overflow-y-auto;
        transition: max-height 0.2s ease-in;
      }
      .trash-can-background {
        @apply mx-2 flex h-[2rem] w-[2rem] items-center justify-center rounded-full;
      }
    `,
  ],
})
export class OurHouseComponent implements OnInit {
  contact$ = this.store.select(selectContact);
  documents$ = this.store.select(selectDocuments);
  noDocuments$ = this.store.select(selectNoDocuments);
  announcements$ = combineLatest([
    this.store
      .select(selectAnnouncements)
      .pipe(filter((announcements) => announcements != undefined)),
    merge(
      this.translateService.onDefaultLangChange,
      this.translateService.onLangChange,
      of({ lang: this.translateService.currentLang } as LangChangeEvent)
    ),
  ]).pipe(
    map(([announcements, languageChange]) =>
      announcements?.map((announcement) => ({
        id: announcement.id,
        timestamp: announcement.timestamp,
        title: this.buildAnnouncementTitle(languageChange, announcement),
        content: this.buildAnnouncementContent(languageChange, announcement),
      }))
    )
  );
  noAnnouncements$ = this.store.select(selectNoAnnouncements);

  garbageCollection$ = this.store.select(selectGarbageCollection);
  garbageCollectionAssignment$ = this.store.select(
    selectGarbageCollectionAssignment
  );
  showPdf$ = this.store.select(selectshowDocument);
  documentUrl$ = this.store.select(selectDocumentUrl);
  documentUrl = '';

  expanded?: string;
  currentDate = moment().startOf('day');

  private buildAnnouncementContent(
    languageChange: DefaultLangChangeEvent | LangChangeEvent,
    announcement: {
      content?: { document: any } | null | undefined;
      content_english?: { document: any } | null | undefined;
      content_arabic?: { document: any } | null | undefined;
    }
  ): { document: any } | null | undefined {
    if (
      languageChange.lang == 'en' &&
      announcement.content_english?.document[0].children[0]?.text != ''
    )
      return announcement.content_english;

    if (
      languageChange.lang == 'ar' &&
      announcement.content_arabic?.document[0].children[0]?.text != ''
    )
      return announcement.content_arabic;

    return announcement.content;
  }

  private buildAnnouncementTitle(
    languageChange: DefaultLangChangeEvent | LangChangeEvent,
    announcement: {
      title?: string | null | undefined;
      title_english?: string | null | undefined;
      title_arabic?: string | null | undefined;
    }
  ): string | null | undefined {
    if (languageChange.lang == 'en' && announcement.title_english != '')
      return announcement.title_english;

    if (languageChange.lang == 'ar' && announcement.title_arabic != '')
      return announcement.title_arabic;

    return announcement.title;
  }

  toggleSection(sectionid: string) {
    if (this.expanded === sectionid) {
      this.expanded = undefined;
      return;
    }
    this.expanded = sectionid;
  }

  isNew(newsDate: string) {
    var newsDateObj = new Date(newsDate);
    var cmsDate = moment(newsDateObj).startOf('day');
    var diffDays = this.currentDate.diff(cmsDate, 'days');
    if (diffDays < 7) {
      return true;
    }
    return false;
  }

  getRemainingDaysFormatted(timestamp?: Date): string {
    var cmsDate = moment(timestamp).startOf('day');
    var diffDays = cmsDate.diff(this.currentDate, 'days');

    if (!timestamp) {
      return this.translateService.instant('Kein Abholtermin');
    }

    if (diffDays === 0) {
      return this.translateService.instant('Heute');
    }

    if (diffDays === 1) {
      return this.translateService.instant('Morgen');
    }

    return (
      this.translateService.instant('in') +
      diffDays +
      this.translateService.instant('Tagen')
    );
  }

  constructor(
    private store: Store,
    private translateService: TranslateService
  ) {}

  openDocument(documentUrl: any) {
    this.store.dispatch(openDocument({ documentUrl }));
    this.documentUrl = documentUrl;
  }

  closeDocument() {
    this.store.dispatch(closeDocument());
  }

  ngOnInit() {
    pdfDefaultOptions.externalLinkTarget = 0;
    pdfDefaultOptions.externalLinkRel = 'noopener';
  }
}
