import { ChangeDetectionStrategy, Component } from '@angular/core';
import { Store } from '@ngrx/store';
import * as moment from 'moment';
import 'moment/locale/de';
import { timer } from 'rxjs';
import { distinctUntilChanged, map, switchMap, tap } from 'rxjs/operators';
import {
  selectCategoryFilters,
  selectDaysPageDownDisable,
  selectDaysPageUpDisable,
  selectEventsByDaySincePage,
  selectShowEventPost,
} from '../stores/events/events.selectors';
import * as eventsActions from '../stores/events/events.actions';
import { closeEventPost, openEventPost } from '../stores/events/events.actions';

@Component({
  selector: 'app-kultur',
  template: `
    <div
      class="flex h-full w-full flex-grow flex-row overflow-x-hidden text-dark-text"
    >
      <div class="flex w-full flex-col">
        <div class="ueberschrift flex flex-row items-center justify-between">
          <div class="flex flex-row items-center">
            <i class="mi filled text-3xl text-light-text">event</i>
            <p class="pl-2">{{ 'veranstaltungskalender' | translate }}</p>
          </div>
          <p class="mr-2 text-base">
            {{ 'VeranstaltungsInfosAnbieter' | translate }}
          </p>
        </div>
        <div class="mt-4">
          <div class="flex flex-row flex-wrap gap-2">
            <button
              *ngFor="let section of categorys$ | async"
              class="buttonMenue"
              [class.buttonSelected]="section.selected"
              (click)="toggleCategorySelected(section.name)"
              trackFeature
              [feature]="'kultur-kategorie'"
            >
              <p class="flex-nowrap">{{ section.name }}</p>
            </button>
          </div>
        </div>

        <div class="my-3 flex flex-row">
          <button
            class="flex flex-grow justify-center p-2 disabled:opacity-10"
            (click)="previous3DayPage()"
            [disabled]="daysPageUpDisable$ | async"
            trackFeature
            [feature]="'kultur-tag-seite-davor'"
          >
            <i
              class="mi -mr-8 h-5 rotate-90 scale-x-50 text-7xl text-blue-light"
              >arrow_back_ios_new</i
            >
          </button>
        </div>

        <div class="flex flex-grow flex-col space-y-6">
          <div
            *ngFor="let day of filteredDays$ | async"
            class="flex h-1/3 flex-row gap-4"
          >
            <div
              class="flex h-full min-w-[7.25rem] flex-col items-center justify-center rounded-md border-2 border-[#c0c0c0]"
            >
              <p
                class="rounded-lg bg-blue-light px-4 text-[1.25rem] text-white"
              >
                {{ getRemainingDaysFormatted(day.timeStamp) }}
              </p>
              <p
                class="text-extrabold mb-[-0.625rem] mt-[0.625rem] text-[2.5rem]"
              >
                {{ day.timeStamp | date : 'dd' }}
              </p>
              <p class="text-extrabold text-[1.875rem]">
                {{ day.timeStamp | date : 'MMM' }}
              </p>
            </div>
            <button
              class="w-6 disabled:opacity-10"
              (click)="previousDayPage(day.timeStamp)"
              [disabled]="!day.hasPrevious"
              trackFeature
              [feature]="'kultur-tag-seite-davor'"
            >
              <i class="mi -ml-6 scale-x-50 text-7xl text-blue-light"
                >arrow_back_ios_new</i
              >
            </button>
            <div
              class="flex w-full flex-grow items-center justify-center text-3xl text-dark-text"
              *ngIf="day.events.length === 0; grid"
            >
              Keine Veranstaltungen an diesem Tag.
            </div>
            <div
              *ngIf="day.events.length !== 0"
              #grid
              class="grid w-full grid-cols-3 gap-4"
            >
              <div
                *ngFor="let event of day.events"
                (click)="openDocument(event)"
                trackFeature
                [feature]="'kultur-auf'"
              >
                <div
                  class="flex h-full w-full overflow-hidden rounded-xl border border-dark-text bg-cover bg-center text-white"
                  [style.background-image]="'url(' + event.image.url + ')'"
                >
                  <div class="flex w-full flex-col justify-between">
                    <span
                      class="m-2 mr-auto rounded-[0.2rem] bg-white px-1 font-bold text-dark-text"
                      >{{ event.category }}</span
                    >
                    <div class="bg-black bg-opacity-50 px-3 py-1">
                      <p class="mt-1 truncate font-bold">{{ event.title }}</p>
                      <p
                        class="flex flex-row items-center gap-[0.5rem] truncate text-[0.938rem]"
                      >
                        <i class="mi filled text-lg text-light-text"
                          >location_on</i
                        >
                        <span *ngIf="!event.location">Keine Angabe</span>
                        {{ event.location }}
                      </p>
                      <p
                        class="flex flex-row items-center gap-[0.5rem] text-[0.938rem]"
                      >
                        <i class="mi filled text-lg text-light-text"
                          >schedule</i
                        >
                        <span *ngIf="event.time === ''">Keine Angabe</span>
                        {{ event.time }}
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <button
              (click)="nextDayPage(day.timeStamp)"
              [disabled]="!day.hasNext"
              class="w-6 disabled:opacity-10"
              trackFeature
              [feature]="'kultur-seite-danach'"
            >
              <i class="mi -ml-6 rotate-180 scale-x-50 text-7xl text-blue-light"
                >arrow_back_ios_new</i
              >
            </button>

            <div
              *ngIf="showUrl"
              class="fixed inset-0 z-10 h-screen w-screen backdrop-blur-[10px]"
              (click)="closeDocument()"
              trackFeature
              [feature]="'kultur-zu'"
            >
              <button class="button fixed bottom-[1.5%] right-[1%] z-10">
                Schließen
              </button>
            </div>
            <div
              *ngIf="showUrl"
              class="fixed left-1/2 top-1/2 z-20 h-[36.625rem] w-[35.625rem] -translate-x-1/2 -translate-y-1/2 transform rounded-lg border-2 border-[#525659] bg-[#FEFEFF] p-6"
            >
              <div class="flex flex-col items-center justify-between">
                <div>
                  <p class="my-1 text-[2.25rem] font-bold">
                    Mehr Informationen unter
                  </p>
                </div>
                <div
                  class="mt-4 overflow-hidden rounded-xl border-4 border-[#0086CD]"
                >
                  <qrcode
                    [qrdata]="'' + popupNews.link + ''"
                    [allowEmptyString]="true"
                    [ariaLabel]="'QR Code image with the following content...'"
                    [cssClass]="'center'"
                    [colorDark]="'#0086CD'"
                    [colorLight]="'#ffffffff'"
                    [elementType]="'canvas'"
                    [errorCorrectionLevel]="'M'"
                    [margin]="2"
                    [scale]="1"
                    [title]="'A custom title attribute'"
                    [width]="300"
                  ></qrcode>
                </div>
                <p class="button w-min-[40rem] mt-4 text-center">
                  {{ hostname(popupNews.link) }}
                </p>
                <img
                  *ngIf="popupNews.dataProvider"
                  class="mt-5 max-h-16"
                  [src]="popupNews.dataProvider.image.url"
                  alt="logo_kuba"
                />
              </div>
            </div>
          </div>
        </div>

        <div class="my-2 flex flex-row">
          <button
            class="flex h-10 flex-grow justify-center p-2 disabled:opacity-10"
            (click)="next3DayPage()"
            [disabled]="daysPageDownDisable$ | async"
            trackFeature
            [feature]="'kultur-tag-seite-danach'"
          >
            <i
              class="mi -mt-4 w-16 -rotate-90 scale-x-50 text-7xl text-blue-light"
              >arrow_back_ios_new</i
            >
          </button>
        </div>
      </div>
    </div>
  `,
  styles: [
    `
      .ueberschrift {
        @apply rounded bg-[#737373] text-white font-bold text-[1.575rem] p-0 pl-2;
      }
      .buttonMenue {
        @apply flex flex-row items-center border-blue-light border-2 rounded-lg shadow-md text-blue-light text-[1.25rem] py-2 px-6 gap-2 ease-in-out duration-300;
      }
      .button {
        @apply flex flex-row items-center bg-blue-light border-blue-dark border-2 rounded-lg shadow-md text-white font-bold text-[1.1rem] p-2 gap-2;
      }
      .buttonSelected {
        @apply bg-blue-light text-white;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CultureComponent {
  day$ = timer(0, 1000)
    .pipe(map(() => new Date()))
    .pipe(
      distinctUntilChanged(
        (previous, current) =>
          moment(previous).format('YYYY-MM-DD') ===
          moment(current).format('YYYY-MM-DD')
      )
    );

  categorys$ = this.store.select(selectCategoryFilters);
  daysPageUpDisable$ = this.store.select(selectDaysPageUpDisable);
  daysPageDownDisable$ = this.day$.pipe(
    switchMap((day) =>
      this.store.select(selectDaysPageDownDisable(moment(day).toDate()))
    )
  );
  showEventPost$ = this.store.select(selectShowEventPost);

  filteredDays$ = this.day$.pipe(
    switchMap((day) =>
      this.store.select(selectEventsByDaySincePage(moment(day).toDate()))
    )
  );

  currentDate = moment().startOf('day');
  showUrl = false;
  popupNews: any;

  constructor(private store: Store) {}

  getRemainingDaysFormatted(timestamp: Date): string {
    var cmsDate = moment(timestamp).startOf('day');
    var diffDays = cmsDate.diff(this.currentDate, 'days');

    if (diffDays === 0) {
      return 'Heute';
    }

    if (diffDays === 1) {
      return 'Morgen';
    }

    return moment(timestamp).locale('de').format('ddd');
  }

  openDocument(section: any) {
    this.showUrl = true;
    this.popupNews = section;
  }
  closeDocument() {
    this.showUrl = false;
    this.popupNews = '';
  }

  openEventPost(section: any) {
    this.store.dispatch(openEventPost());
    this.popupNews = section;
  }
  closeEventPost() {
    this.store.dispatch(closeEventPost());
  }

  toggleCategorySelected(category: string) {
    this.store.dispatch(eventsActions.toggleCategory({ category }));
  }

  nextDayPage(day: Date) {
    this.store.dispatch(eventsActions.nextDayPage({ day }));
  }

  previousDayPage(day: Date) {
    this.store.dispatch(eventsActions.previousDayPage({ day }));
  }

  next3DayPage() {
    this.store.dispatch(eventsActions.next3DayPage());
  }

  previous3DayPage() {
    this.store.dispatch(eventsActions.previous3DayPage());
  }

  hostname(link?: string): string {
    if (!link) {
      return '';
    }

    const url = new URL(link);
    return url.hostname;
  }
}
