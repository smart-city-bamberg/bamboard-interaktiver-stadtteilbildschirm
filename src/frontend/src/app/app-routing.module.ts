import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CultureComponent } from './sites/culture.component';
import { NewsComponent } from './sites/news.component';
import { PublicTransportComponent } from './sites/public-transport.component';
import { OurHouseComponent } from './sites/our-house.component';
import { MonitorSelectorComponent } from './sites/monitor-selector.component';
import { LandingPageComponent } from './sites/landing-page.component';
import { MainComponent } from './sites/main.component';
import { ScreensaverComponent } from './sites/screensaver.component';

export const routes: Routes = [
  { path: '', component: LandingPageComponent },
  { path: 'bildschirmauswahl', component: MonitorSelectorComponent },
  {
    path: 'seiten',
    component: MainComponent,
    children: [
      { path: 'unser-haus', component: OurHouseComponent },
      { path: 'oepnv', component: PublicTransportComponent },
      { path: 'nachrichten', component: NewsComponent },
      { path: 'kultur', component: CultureComponent },
      { path: 'screensaver', component: ScreensaverComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
