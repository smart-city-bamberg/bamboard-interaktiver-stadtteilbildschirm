import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class NinaService {
  private ninaBaseUrl = 'https://warnung.bund.de/api31';

  constructor(private http: HttpClient) {}

  getWarnungen(gebietscode: string): Observable<any> {
    const url = `${this.ninaBaseUrl}/dashboard/${gebietscode}.json`;
    return this.http.get(url);
  }

  getWarnungDetails(id: string): Observable<any> {
    const url = `${this.ninaBaseUrl}/warnings/${id}.json`;
    return this.http.get(url);
  }
}
