// https://github.com/jdemaeyer/brightsky/blob/master/docs/brightsky.yml

const weatherMapping: { [index: string]: { name: string; hasMoon: boolean } } =
  {
    dry: { name: '0-8', hasMoon: true },
    fog: { name: '40', hasMoon: false },
    rain: { name: '7', hasMoon: false },
    sleet: { name: '12', hasMoon: false },
    snow: { name: '16', hasMoon: false },
    hail: { name: '87', hasMoon: true },
    thunderstorm: { name: '83', hasMoon: true },
  };

export const weatherIcon = (name: string, isNight: boolean) => {
  const weather = weatherMapping[name];
  return weather.name + (isNight && weather.hasMoon ? '_moon' : '') + '.png';
};
