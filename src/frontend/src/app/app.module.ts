import { NgModule, isDevMode } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID } from '@angular/core';
import { AppRoutingModule, routes } from './app-routing.module';
import { AppComponent } from './app.component';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './stores';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { ApolloModule } from 'apollo-angular';
import { GraphQLModule } from 'src/graphql/graphql.module';
import { ComponentsModule } from './components/components.module';
import { WeatherModule } from './stores/weather/weather.module';
import { SitesModule } from './sites/sites.module';
import { RouterModule } from '@angular/router';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { OurHouseModule } from './stores/our-house/our-house.module';
import { MonitorModule } from './stores/monitor/monitor.module';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { NewsModule } from './stores/news/news.module';
import { EventsModule } from './stores/events/events.module';
import { PublicTransportModule } from './stores/public-transport/public-transport.module';
import { AuthModule } from './stores/auth/auth.module';
import { ScreenSaverModule } from './stores/screen-saver/screen-saver.module';
import { AnalyticsModule } from './analytics/analytics.module';
import { TracingEffects } from './analytics/tracing.effects';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/locale/', '.json');
}

@NgModule({
  declarations: [AppComponent],
  imports: [
    AnalyticsModule,
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(routes),
    ComponentsModule,
    HttpClientModule,
    SitesModule,
    StoreModule.forRoot(reducers, {
      metaReducers,
    }),
    EffectsModule.forRoot([]),
    StoreRouterConnectingModule.forRoot({
      stateKey: 'router',
    }),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: !isDevMode() , connectInZone: true}),
    ApolloModule,
    GraphQLModule,
    WeatherModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
    OurHouseModule,
    MonitorModule,
    NgxExtendedPdfViewerModule,
    NewsModule,
    EventsModule,
    PublicTransportModule,
    AuthModule,
    ScreenSaverModule,
    EffectsModule.forFeature([TracingEffects]),
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'de-DE' }],

  bootstrap: [AppComponent],
})
export class AppModule {}
