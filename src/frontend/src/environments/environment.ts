const oneSecond = 1000;
const oneMinute = 60 * oneSecond;

export const environment = {
  title: 'BamBoard',
  apiUrl:
    window.location.protocol +
    '//' +
    window.location.hostname +
    ':3000/api/graphql',

  screenSaver: {
    idleTimeout: 10 * oneSecond,
    pageDuration: 15 * oneSecond,
    entireDuration: 1 * oneMinute,
  },
  monitor: {
    pollingInterval: 1 * oneMinute,
  },
  menu: ['our-house', 'public-transport', 'news', 'culture'],
  openTelemetryUrl: 'http://localhost:4318/v1/traces',
  events: {
    hideEmptyDays: false,
    daysPerPage: 3,
    newsPerPage: 3,
    scrollPerPage: 1,
    maximumDays: 15,
  },
};
