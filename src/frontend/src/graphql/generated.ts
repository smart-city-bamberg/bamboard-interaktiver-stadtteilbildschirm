import { gql } from 'apollo-angular';
import { Injectable } from '@angular/core';
import * as Apollo from 'apollo-angular';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  CalendarDay: any;
  DateTime: any;
  /** The `JSON` scalar type represents JSON values as specified by [ECMA-404](http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf). */
  JSON: any;
  /** The `Upload` scalar type represents a file upload. */
  Upload: any;
};

export type Announcement = {
  content?: Maybe<Announcement_Content_Document>;
  content_arabic?: Maybe<Announcement_Content_Arabic_Document>;
  content_english?: Maybe<Announcement_Content_English_Document>;
  id: Scalars['ID'];
  monitorGroup?: Maybe<MonitorGroup>;
  organization?: Maybe<Organization>;
  timestamp?: Maybe<Scalars['DateTime']>;
  title?: Maybe<Scalars['String']>;
  title_arabic?: Maybe<Scalars['String']>;
  title_english?: Maybe<Scalars['String']>;
  visibleUntil?: Maybe<Scalars['DateTime']>;
};

export type AnnouncementCreateInput = {
  content?: InputMaybe<Scalars['JSON']>;
  content_arabic?: InputMaybe<Scalars['JSON']>;
  content_english?: InputMaybe<Scalars['JSON']>;
  monitorGroup?: InputMaybe<MonitorGroupRelateToOneForCreateInput>;
  organization?: InputMaybe<OrganizationRelateToOneForCreateInput>;
  timestamp?: InputMaybe<Scalars['DateTime']>;
  title?: InputMaybe<Scalars['String']>;
  title_arabic?: InputMaybe<Scalars['String']>;
  title_english?: InputMaybe<Scalars['String']>;
  visibleUntil?: InputMaybe<Scalars['DateTime']>;
};

export type AnnouncementManyRelationFilter = {
  every?: InputMaybe<AnnouncementWhereInput>;
  none?: InputMaybe<AnnouncementWhereInput>;
  some?: InputMaybe<AnnouncementWhereInput>;
};

export type AnnouncementOrderByInput = {
  id?: InputMaybe<OrderDirection>;
  timestamp?: InputMaybe<OrderDirection>;
  title?: InputMaybe<OrderDirection>;
  title_arabic?: InputMaybe<OrderDirection>;
  title_english?: InputMaybe<OrderDirection>;
  visibleUntil?: InputMaybe<OrderDirection>;
};

export type AnnouncementRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<AnnouncementWhereUniqueInput>>;
  create?: InputMaybe<Array<AnnouncementCreateInput>>;
};

export type AnnouncementRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<AnnouncementWhereUniqueInput>>;
  create?: InputMaybe<Array<AnnouncementCreateInput>>;
  disconnect?: InputMaybe<Array<AnnouncementWhereUniqueInput>>;
  set?: InputMaybe<Array<AnnouncementWhereUniqueInput>>;
};

export type AnnouncementUpdateArgs = {
  data: AnnouncementUpdateInput;
  where: AnnouncementWhereUniqueInput;
};

export type AnnouncementUpdateInput = {
  content?: InputMaybe<Scalars['JSON']>;
  content_arabic?: InputMaybe<Scalars['JSON']>;
  content_english?: InputMaybe<Scalars['JSON']>;
  monitorGroup?: InputMaybe<MonitorGroupRelateToOneForUpdateInput>;
  organization?: InputMaybe<OrganizationRelateToOneForUpdateInput>;
  timestamp?: InputMaybe<Scalars['DateTime']>;
  title?: InputMaybe<Scalars['String']>;
  title_arabic?: InputMaybe<Scalars['String']>;
  title_english?: InputMaybe<Scalars['String']>;
  visibleUntil?: InputMaybe<Scalars['DateTime']>;
};

export type AnnouncementWhereInput = {
  AND?: InputMaybe<Array<AnnouncementWhereInput>>;
  NOT?: InputMaybe<Array<AnnouncementWhereInput>>;
  OR?: InputMaybe<Array<AnnouncementWhereInput>>;
  id?: InputMaybe<IdFilter>;
  monitorGroup?: InputMaybe<MonitorGroupWhereInput>;
  organization?: InputMaybe<OrganizationWhereInput>;
  timestamp?: InputMaybe<DateTimeFilter>;
  title?: InputMaybe<StringFilter>;
  title_arabic?: InputMaybe<StringFilter>;
  title_english?: InputMaybe<StringFilter>;
  visibleUntil?: InputMaybe<DateTimeNullableFilter>;
};

export type AnnouncementWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']>;
};

export type Announcement_Content_Document = {
  document: Scalars['JSON'];
};


export type Announcement_Content_DocumentDocumentArgs = {
  hydrateRelationships?: Scalars['Boolean'];
};

export type Announcement_Content_Arabic_Document = {
  document: Scalars['JSON'];
};


export type Announcement_Content_Arabic_DocumentDocumentArgs = {
  hydrateRelationships?: Scalars['Boolean'];
};

export type Announcement_Content_English_Document = {
  document: Scalars['JSON'];
};


export type Announcement_Content_English_DocumentDocumentArgs = {
  hydrateRelationships?: Scalars['Boolean'];
};

export type AuthenticatedItem = User;

export type BooleanFilter = {
  equals?: InputMaybe<Scalars['Boolean']>;
  not?: InputMaybe<BooleanFilter>;
};

export type CalendarDayFilter = {
  equals?: InputMaybe<Scalars['CalendarDay']>;
  gt?: InputMaybe<Scalars['CalendarDay']>;
  gte?: InputMaybe<Scalars['CalendarDay']>;
  in?: InputMaybe<Array<Scalars['CalendarDay']>>;
  lt?: InputMaybe<Scalars['CalendarDay']>;
  lte?: InputMaybe<Scalars['CalendarDay']>;
  not?: InputMaybe<CalendarDayFilter>;
  notIn?: InputMaybe<Array<Scalars['CalendarDay']>>;
};

export type Contact = {
  content?: Maybe<Contact_Content_Document>;
  id: Scalars['ID'];
  monitorGroup?: Maybe<Array<MonitorGroup>>;
  monitorGroupCount?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  organization?: Maybe<Organization>;
};


export type ContactMonitorGroupArgs = {
  cursor?: InputMaybe<MonitorGroupWhereUniqueInput>;
  orderBy?: Array<MonitorGroupOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: MonitorGroupWhereInput;
};


export type ContactMonitorGroupCountArgs = {
  where?: MonitorGroupWhereInput;
};

export type ContactCreateInput = {
  content?: InputMaybe<Scalars['JSON']>;
  monitorGroup?: InputMaybe<MonitorGroupRelateToManyForCreateInput>;
  name?: InputMaybe<Scalars['String']>;
  organization?: InputMaybe<OrganizationRelateToOneForCreateInput>;
};

export type ContactOrderByInput = {
  id?: InputMaybe<OrderDirection>;
  name?: InputMaybe<OrderDirection>;
};

export type ContactRelateToOneForCreateInput = {
  connect?: InputMaybe<ContactWhereUniqueInput>;
  create?: InputMaybe<ContactCreateInput>;
};

export type ContactRelateToOneForUpdateInput = {
  connect?: InputMaybe<ContactWhereUniqueInput>;
  create?: InputMaybe<ContactCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']>;
};

export type ContactUpdateArgs = {
  data: ContactUpdateInput;
  where: ContactWhereUniqueInput;
};

export type ContactUpdateInput = {
  content?: InputMaybe<Scalars['JSON']>;
  monitorGroup?: InputMaybe<MonitorGroupRelateToManyForUpdateInput>;
  name?: InputMaybe<Scalars['String']>;
  organization?: InputMaybe<OrganizationRelateToOneForUpdateInput>;
};

export type ContactWhereInput = {
  AND?: InputMaybe<Array<ContactWhereInput>>;
  NOT?: InputMaybe<Array<ContactWhereInput>>;
  OR?: InputMaybe<Array<ContactWhereInput>>;
  id?: InputMaybe<IdFilter>;
  monitorGroup?: InputMaybe<MonitorGroupManyRelationFilter>;
  name?: InputMaybe<StringFilter>;
  organization?: InputMaybe<OrganizationWhereInput>;
};

export type ContactWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']>;
};

export type Contact_Content_Document = {
  document: Scalars['JSON'];
};


export type Contact_Content_DocumentDocumentArgs = {
  hydrateRelationships?: Scalars['Boolean'];
};

export type CreateInitialUserInput = {
  email?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  password?: InputMaybe<Scalars['String']>;
};

export type DateTimeFilter = {
  equals?: InputMaybe<Scalars['DateTime']>;
  gt?: InputMaybe<Scalars['DateTime']>;
  gte?: InputMaybe<Scalars['DateTime']>;
  in?: InputMaybe<Array<Scalars['DateTime']>>;
  lt?: InputMaybe<Scalars['DateTime']>;
  lte?: InputMaybe<Scalars['DateTime']>;
  not?: InputMaybe<DateTimeFilter>;
  notIn?: InputMaybe<Array<Scalars['DateTime']>>;
};

export type DateTimeNullableFilter = {
  equals?: InputMaybe<Scalars['DateTime']>;
  gt?: InputMaybe<Scalars['DateTime']>;
  gte?: InputMaybe<Scalars['DateTime']>;
  in?: InputMaybe<Array<Scalars['DateTime']>>;
  lt?: InputMaybe<Scalars['DateTime']>;
  lte?: InputMaybe<Scalars['DateTime']>;
  not?: InputMaybe<DateTimeNullableFilter>;
  notIn?: InputMaybe<Array<Scalars['DateTime']>>;
};

export type Document = {
  content?: Maybe<FileFieldOutput>;
  id: Scalars['ID'];
  monitorGroup?: Maybe<MonitorGroup>;
  name?: Maybe<Scalars['String']>;
  organization?: Maybe<Organization>;
};

export type DocumentCreateInput = {
  content?: InputMaybe<FileFieldInput>;
  monitorGroup?: InputMaybe<MonitorGroupRelateToOneForCreateInput>;
  name?: InputMaybe<Scalars['String']>;
  organization?: InputMaybe<OrganizationRelateToOneForCreateInput>;
};

export type DocumentManyRelationFilter = {
  every?: InputMaybe<DocumentWhereInput>;
  none?: InputMaybe<DocumentWhereInput>;
  some?: InputMaybe<DocumentWhereInput>;
};

export type DocumentOrderByInput = {
  id?: InputMaybe<OrderDirection>;
  name?: InputMaybe<OrderDirection>;
};

export type DocumentRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<DocumentWhereUniqueInput>>;
  create?: InputMaybe<Array<DocumentCreateInput>>;
};

export type DocumentRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<DocumentWhereUniqueInput>>;
  create?: InputMaybe<Array<DocumentCreateInput>>;
  disconnect?: InputMaybe<Array<DocumentWhereUniqueInput>>;
  set?: InputMaybe<Array<DocumentWhereUniqueInput>>;
};

export type DocumentUpdateArgs = {
  data: DocumentUpdateInput;
  where: DocumentWhereUniqueInput;
};

export type DocumentUpdateInput = {
  content?: InputMaybe<FileFieldInput>;
  monitorGroup?: InputMaybe<MonitorGroupRelateToOneForUpdateInput>;
  name?: InputMaybe<Scalars['String']>;
  organization?: InputMaybe<OrganizationRelateToOneForUpdateInput>;
};

export type DocumentWhereInput = {
  AND?: InputMaybe<Array<DocumentWhereInput>>;
  NOT?: InputMaybe<Array<DocumentWhereInput>>;
  OR?: InputMaybe<Array<DocumentWhereInput>>;
  id?: InputMaybe<IdFilter>;
  monitorGroup?: InputMaybe<MonitorGroupWhereInput>;
  name?: InputMaybe<StringFilter>;
  organization?: InputMaybe<OrganizationWhereInput>;
};

export type DocumentWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']>;
};

export type Event = {
  category?: Maybe<Scalars['String']>;
  date?: Maybe<Scalars['CalendarDay']>;
  eventProvider?: Maybe<EventProvider>;
  externalId?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  image?: Maybe<ImageFieldOutput>;
  link?: Maybe<Scalars['String']>;
  location?: Maybe<Scalars['String']>;
  monitorGroups?: Maybe<Array<MonitorGroup>>;
  monitorGroupsCount?: Maybe<Scalars['Int']>;
  organization?: Maybe<Organization>;
  time?: Maybe<Scalars['String']>;
  title?: Maybe<Scalars['String']>;
};


export type EventMonitorGroupsArgs = {
  cursor?: InputMaybe<MonitorGroupWhereUniqueInput>;
  orderBy?: Array<MonitorGroupOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: MonitorGroupWhereInput;
};


export type EventMonitorGroupsCountArgs = {
  where?: MonitorGroupWhereInput;
};

export type EventCreateInput = {
  category?: InputMaybe<Scalars['String']>;
  date?: InputMaybe<Scalars['CalendarDay']>;
  eventProvider?: InputMaybe<EventProviderRelateToOneForCreateInput>;
  externalId?: InputMaybe<Scalars['String']>;
  image?: InputMaybe<ImageFieldInput>;
  link?: InputMaybe<Scalars['String']>;
  location?: InputMaybe<Scalars['String']>;
  monitorGroups?: InputMaybe<MonitorGroupRelateToManyForCreateInput>;
  organization?: InputMaybe<OrganizationRelateToOneForCreateInput>;
  time?: InputMaybe<Scalars['String']>;
  title?: InputMaybe<Scalars['String']>;
};

export type EventManyRelationFilter = {
  every?: InputMaybe<EventWhereInput>;
  none?: InputMaybe<EventWhereInput>;
  some?: InputMaybe<EventWhereInput>;
};

export type EventOrderByInput = {
  category?: InputMaybe<OrderDirection>;
  date?: InputMaybe<OrderDirection>;
  externalId?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  link?: InputMaybe<OrderDirection>;
  location?: InputMaybe<OrderDirection>;
  time?: InputMaybe<OrderDirection>;
  title?: InputMaybe<OrderDirection>;
};

export type EventProvider = {
  configuration?: Maybe<Scalars['String']>;
  events?: Maybe<Array<Event>>;
  eventsCount?: Maybe<Scalars['Int']>;
  feedType?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  image?: Maybe<ImageFieldOutput>;
  monitorGroups?: Maybe<Array<MonitorGroup>>;
  monitorGroupsCount?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  organization?: Maybe<Organization>;
};


export type EventProviderEventsArgs = {
  cursor?: InputMaybe<EventWhereUniqueInput>;
  orderBy?: Array<EventOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: EventWhereInput;
};


export type EventProviderEventsCountArgs = {
  where?: EventWhereInput;
};


export type EventProviderMonitorGroupsArgs = {
  cursor?: InputMaybe<MonitorGroupWhereUniqueInput>;
  orderBy?: Array<MonitorGroupOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: MonitorGroupWhereInput;
};


export type EventProviderMonitorGroupsCountArgs = {
  where?: MonitorGroupWhereInput;
};

export type EventProviderCreateInput = {
  configuration?: InputMaybe<Scalars['String']>;
  events?: InputMaybe<EventRelateToManyForCreateInput>;
  feedType?: InputMaybe<Scalars['String']>;
  image?: InputMaybe<ImageFieldInput>;
  monitorGroups?: InputMaybe<MonitorGroupRelateToManyForCreateInput>;
  name?: InputMaybe<Scalars['String']>;
  organization?: InputMaybe<OrganizationRelateToOneForCreateInput>;
};

export type EventProviderManyRelationFilter = {
  every?: InputMaybe<EventProviderWhereInput>;
  none?: InputMaybe<EventProviderWhereInput>;
  some?: InputMaybe<EventProviderWhereInput>;
};

export type EventProviderOrderByInput = {
  configuration?: InputMaybe<OrderDirection>;
  feedType?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  name?: InputMaybe<OrderDirection>;
};

export type EventProviderRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<EventProviderWhereUniqueInput>>;
  create?: InputMaybe<Array<EventProviderCreateInput>>;
};

export type EventProviderRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<EventProviderWhereUniqueInput>>;
  create?: InputMaybe<Array<EventProviderCreateInput>>;
  disconnect?: InputMaybe<Array<EventProviderWhereUniqueInput>>;
  set?: InputMaybe<Array<EventProviderWhereUniqueInput>>;
};

export type EventProviderRelateToOneForCreateInput = {
  connect?: InputMaybe<EventProviderWhereUniqueInput>;
  create?: InputMaybe<EventProviderCreateInput>;
};

export type EventProviderRelateToOneForUpdateInput = {
  connect?: InputMaybe<EventProviderWhereUniqueInput>;
  create?: InputMaybe<EventProviderCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']>;
};

export type EventProviderUpdateArgs = {
  data: EventProviderUpdateInput;
  where: EventProviderWhereUniqueInput;
};

export type EventProviderUpdateInput = {
  configuration?: InputMaybe<Scalars['String']>;
  events?: InputMaybe<EventRelateToManyForUpdateInput>;
  feedType?: InputMaybe<Scalars['String']>;
  image?: InputMaybe<ImageFieldInput>;
  monitorGroups?: InputMaybe<MonitorGroupRelateToManyForUpdateInput>;
  name?: InputMaybe<Scalars['String']>;
  organization?: InputMaybe<OrganizationRelateToOneForUpdateInput>;
};

export type EventProviderWhereInput = {
  AND?: InputMaybe<Array<EventProviderWhereInput>>;
  NOT?: InputMaybe<Array<EventProviderWhereInput>>;
  OR?: InputMaybe<Array<EventProviderWhereInput>>;
  configuration?: InputMaybe<StringFilter>;
  events?: InputMaybe<EventManyRelationFilter>;
  feedType?: InputMaybe<StringNullableFilter>;
  id?: InputMaybe<IdFilter>;
  monitorGroups?: InputMaybe<MonitorGroupManyRelationFilter>;
  name?: InputMaybe<StringFilter>;
  organization?: InputMaybe<OrganizationWhereInput>;
};

export type EventProviderWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']>;
};

export type EventRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<EventWhereUniqueInput>>;
  create?: InputMaybe<Array<EventCreateInput>>;
};

export type EventRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<EventWhereUniqueInput>>;
  create?: InputMaybe<Array<EventCreateInput>>;
  disconnect?: InputMaybe<Array<EventWhereUniqueInput>>;
  set?: InputMaybe<Array<EventWhereUniqueInput>>;
};

export type EventUpdateArgs = {
  data: EventUpdateInput;
  where: EventWhereUniqueInput;
};

export type EventUpdateInput = {
  category?: InputMaybe<Scalars['String']>;
  date?: InputMaybe<Scalars['CalendarDay']>;
  eventProvider?: InputMaybe<EventProviderRelateToOneForUpdateInput>;
  externalId?: InputMaybe<Scalars['String']>;
  image?: InputMaybe<ImageFieldInput>;
  link?: InputMaybe<Scalars['String']>;
  location?: InputMaybe<Scalars['String']>;
  monitorGroups?: InputMaybe<MonitorGroupRelateToManyForUpdateInput>;
  organization?: InputMaybe<OrganizationRelateToOneForUpdateInput>;
  time?: InputMaybe<Scalars['String']>;
  title?: InputMaybe<Scalars['String']>;
};

export type EventWhereInput = {
  AND?: InputMaybe<Array<EventWhereInput>>;
  NOT?: InputMaybe<Array<EventWhereInput>>;
  OR?: InputMaybe<Array<EventWhereInput>>;
  category?: InputMaybe<StringFilter>;
  date?: InputMaybe<CalendarDayFilter>;
  eventProvider?: InputMaybe<EventProviderWhereInput>;
  externalId?: InputMaybe<StringFilter>;
  id?: InputMaybe<IdFilter>;
  link?: InputMaybe<StringFilter>;
  location?: InputMaybe<StringFilter>;
  monitorGroups?: InputMaybe<MonitorGroupManyRelationFilter>;
  organization?: InputMaybe<OrganizationWhereInput>;
  time?: InputMaybe<StringFilter>;
  title?: InputMaybe<StringFilter>;
};

export type EventWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']>;
};

export type FileFieldInput = {
  upload: Scalars['Upload'];
};

export type FileFieldOutput = {
  filename: Scalars['String'];
  filesize: Scalars['Int'];
  url: Scalars['String'];
};

export type FloatFilter = {
  equals?: InputMaybe<Scalars['Float']>;
  gt?: InputMaybe<Scalars['Float']>;
  gte?: InputMaybe<Scalars['Float']>;
  in?: InputMaybe<Array<Scalars['Float']>>;
  lt?: InputMaybe<Scalars['Float']>;
  lte?: InputMaybe<Scalars['Float']>;
  not?: InputMaybe<FloatFilter>;
  notIn?: InputMaybe<Array<Scalars['Float']>>;
};

export type GarbageCollection = {
  id: Scalars['ID'];
  monitor?: Maybe<Monitor>;
  organization?: Maybe<Organization>;
  source?: Maybe<Scalars['String']>;
  timestamp?: Maybe<Scalars['DateTime']>;
  type?: Maybe<Scalars['String']>;
};

export type GarbageCollectionAssignment = {
  biomuell?: Maybe<Scalars['String']>;
  gelber_sack?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  name?: Maybe<Scalars['String']>;
  organization?: Maybe<Organization>;
  papier?: Maybe<Scalars['String']>;
  restmuell?: Maybe<Scalars['String']>;
};

export type GarbageCollectionAssignmentCreateInput = {
  biomuell?: InputMaybe<Scalars['String']>;
  gelber_sack?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  organization?: InputMaybe<OrganizationRelateToOneForCreateInput>;
  papier?: InputMaybe<Scalars['String']>;
  restmuell?: InputMaybe<Scalars['String']>;
};

export type GarbageCollectionAssignmentOrderByInput = {
  biomuell?: InputMaybe<OrderDirection>;
  gelber_sack?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  name?: InputMaybe<OrderDirection>;
  papier?: InputMaybe<OrderDirection>;
  restmuell?: InputMaybe<OrderDirection>;
};

export type GarbageCollectionAssignmentRelateToOneForCreateInput = {
  connect?: InputMaybe<GarbageCollectionAssignmentWhereUniqueInput>;
  create?: InputMaybe<GarbageCollectionAssignmentCreateInput>;
};

export type GarbageCollectionAssignmentRelateToOneForUpdateInput = {
  connect?: InputMaybe<GarbageCollectionAssignmentWhereUniqueInput>;
  create?: InputMaybe<GarbageCollectionAssignmentCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']>;
};

export type GarbageCollectionAssignmentUpdateArgs = {
  data: GarbageCollectionAssignmentUpdateInput;
  where: GarbageCollectionAssignmentWhereUniqueInput;
};

export type GarbageCollectionAssignmentUpdateInput = {
  biomuell?: InputMaybe<Scalars['String']>;
  gelber_sack?: InputMaybe<Scalars['String']>;
  name?: InputMaybe<Scalars['String']>;
  organization?: InputMaybe<OrganizationRelateToOneForUpdateInput>;
  papier?: InputMaybe<Scalars['String']>;
  restmuell?: InputMaybe<Scalars['String']>;
};

export type GarbageCollectionAssignmentWhereInput = {
  AND?: InputMaybe<Array<GarbageCollectionAssignmentWhereInput>>;
  NOT?: InputMaybe<Array<GarbageCollectionAssignmentWhereInput>>;
  OR?: InputMaybe<Array<GarbageCollectionAssignmentWhereInput>>;
  biomuell?: InputMaybe<StringFilter>;
  gelber_sack?: InputMaybe<StringFilter>;
  id?: InputMaybe<IdFilter>;
  name?: InputMaybe<StringFilter>;
  organization?: InputMaybe<OrganizationWhereInput>;
  papier?: InputMaybe<StringFilter>;
  restmuell?: InputMaybe<StringFilter>;
};

export type GarbageCollectionAssignmentWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']>;
};

export type GarbageCollectionCreateInput = {
  monitor?: InputMaybe<MonitorRelateToOneForCreateInput>;
  organization?: InputMaybe<OrganizationRelateToOneForCreateInput>;
  source?: InputMaybe<Scalars['String']>;
  timestamp?: InputMaybe<Scalars['DateTime']>;
  type?: InputMaybe<Scalars['String']>;
};

export type GarbageCollectionManyRelationFilter = {
  every?: InputMaybe<GarbageCollectionWhereInput>;
  none?: InputMaybe<GarbageCollectionWhereInput>;
  some?: InputMaybe<GarbageCollectionWhereInput>;
};

export type GarbageCollectionOrderByInput = {
  id?: InputMaybe<OrderDirection>;
  source?: InputMaybe<OrderDirection>;
  timestamp?: InputMaybe<OrderDirection>;
  type?: InputMaybe<OrderDirection>;
};

export type GarbageCollectionRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<GarbageCollectionWhereUniqueInput>>;
  create?: InputMaybe<Array<GarbageCollectionCreateInput>>;
};

export type GarbageCollectionRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<GarbageCollectionWhereUniqueInput>>;
  create?: InputMaybe<Array<GarbageCollectionCreateInput>>;
  disconnect?: InputMaybe<Array<GarbageCollectionWhereUniqueInput>>;
  set?: InputMaybe<Array<GarbageCollectionWhereUniqueInput>>;
};

export type GarbageCollectionUpdateArgs = {
  data: GarbageCollectionUpdateInput;
  where: GarbageCollectionWhereUniqueInput;
};

export type GarbageCollectionUpdateInput = {
  monitor?: InputMaybe<MonitorRelateToOneForUpdateInput>;
  organization?: InputMaybe<OrganizationRelateToOneForUpdateInput>;
  source?: InputMaybe<Scalars['String']>;
  timestamp?: InputMaybe<Scalars['DateTime']>;
  type?: InputMaybe<Scalars['String']>;
};

export type GarbageCollectionWhereInput = {
  AND?: InputMaybe<Array<GarbageCollectionWhereInput>>;
  NOT?: InputMaybe<Array<GarbageCollectionWhereInput>>;
  OR?: InputMaybe<Array<GarbageCollectionWhereInput>>;
  id?: InputMaybe<IdFilter>;
  monitor?: InputMaybe<MonitorWhereInput>;
  organization?: InputMaybe<OrganizationWhereInput>;
  source?: InputMaybe<StringFilter>;
  timestamp?: InputMaybe<DateTimeFilter>;
  type?: InputMaybe<StringFilter>;
};

export type GarbageCollectionWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']>;
};

export type IdFilter = {
  equals?: InputMaybe<Scalars['ID']>;
  gt?: InputMaybe<Scalars['ID']>;
  gte?: InputMaybe<Scalars['ID']>;
  in?: InputMaybe<Array<Scalars['ID']>>;
  lt?: InputMaybe<Scalars['ID']>;
  lte?: InputMaybe<Scalars['ID']>;
  not?: InputMaybe<IdFilter>;
  notIn?: InputMaybe<Array<Scalars['ID']>>;
};

export enum ImageExtension {
  Gif = 'gif',
  Jpg = 'jpg',
  Png = 'png',
  Webp = 'webp'
}

export type ImageFieldInput = {
  upload: Scalars['Upload'];
};

export type ImageFieldOutput = {
  extension: ImageExtension;
  filesize: Scalars['Int'];
  height: Scalars['Int'];
  id: Scalars['ID'];
  url: Scalars['String'];
  width: Scalars['Int'];
};

export type KeystoneAdminMeta = {
  list?: Maybe<KeystoneAdminUiListMeta>;
  lists: Array<KeystoneAdminUiListMeta>;
};


export type KeystoneAdminMetaListArgs = {
  key: Scalars['String'];
};

export type KeystoneAdminUiFieldGroupMeta = {
  description?: Maybe<Scalars['String']>;
  fields: Array<KeystoneAdminUiFieldMeta>;
  label: Scalars['String'];
};

export type KeystoneAdminUiFieldMeta = {
  createView: KeystoneAdminUiFieldMetaCreateView;
  customViewsIndex?: Maybe<Scalars['Int']>;
  description?: Maybe<Scalars['String']>;
  fieldMeta?: Maybe<Scalars['JSON']>;
  isFilterable: Scalars['Boolean'];
  isNonNull?: Maybe<Array<KeystoneAdminUiFieldMetaIsNonNull>>;
  isOrderable: Scalars['Boolean'];
  itemView?: Maybe<KeystoneAdminUiFieldMetaItemView>;
  label: Scalars['String'];
  listView: KeystoneAdminUiFieldMetaListView;
  path: Scalars['String'];
  search?: Maybe<QueryMode>;
  viewsIndex: Scalars['Int'];
};


export type KeystoneAdminUiFieldMetaItemViewArgs = {
  id?: InputMaybe<Scalars['ID']>;
};

export type KeystoneAdminUiFieldMetaCreateView = {
  fieldMode: KeystoneAdminUiFieldMetaCreateViewFieldMode;
};

export enum KeystoneAdminUiFieldMetaCreateViewFieldMode {
  Edit = 'edit',
  Hidden = 'hidden'
}

export enum KeystoneAdminUiFieldMetaIsNonNull {
  Create = 'create',
  Read = 'read',
  Update = 'update'
}

export type KeystoneAdminUiFieldMetaItemView = {
  fieldMode?: Maybe<KeystoneAdminUiFieldMetaItemViewFieldMode>;
  fieldPosition?: Maybe<KeystoneAdminUiFieldMetaItemViewFieldPosition>;
};

export enum KeystoneAdminUiFieldMetaItemViewFieldMode {
  Edit = 'edit',
  Hidden = 'hidden',
  Read = 'read'
}

export enum KeystoneAdminUiFieldMetaItemViewFieldPosition {
  Form = 'form',
  Sidebar = 'sidebar'
}

export type KeystoneAdminUiFieldMetaListView = {
  fieldMode: KeystoneAdminUiFieldMetaListViewFieldMode;
};

export enum KeystoneAdminUiFieldMetaListViewFieldMode {
  Hidden = 'hidden',
  Read = 'read'
}

export type KeystoneAdminUiListMeta = {
  description?: Maybe<Scalars['String']>;
  fields: Array<KeystoneAdminUiFieldMeta>;
  groups: Array<KeystoneAdminUiFieldGroupMeta>;
  hideCreate: Scalars['Boolean'];
  hideDelete: Scalars['Boolean'];
  initialColumns: Array<Scalars['String']>;
  initialSort?: Maybe<KeystoneAdminUiSort>;
  isHidden: Scalars['Boolean'];
  isSingleton: Scalars['Boolean'];
  itemQueryName: Scalars['String'];
  key: Scalars['String'];
  label: Scalars['String'];
  labelField: Scalars['String'];
  listQueryName: Scalars['String'];
  pageSize: Scalars['Int'];
  path: Scalars['String'];
  plural: Scalars['String'];
  singular: Scalars['String'];
};

export type KeystoneAdminUiSort = {
  direction: KeystoneAdminUiSortDirection;
  field: Scalars['String'];
};

export enum KeystoneAdminUiSortDirection {
  Asc = 'ASC',
  Desc = 'DESC'
}

export type KeystoneMeta = {
  adminMeta: KeystoneAdminMeta;
};

export type Monitor = {
  boardLogo?: Maybe<ImageFieldOutput>;
  garbageCollectionAssignment?: Maybe<GarbageCollectionAssignment>;
  garbageCollectionCalendar?: Maybe<FileFieldOutput>;
  garbageCollections?: Maybe<Array<GarbageCollection>>;
  garbageCollectionsCount?: Maybe<Scalars['Int']>;
  group?: Maybe<MonitorGroup>;
  id: Scalars['ID'];
  name?: Maybe<Scalars['String']>;
  operatorLogo?: Maybe<ImageFieldOutput>;
  organization?: Maybe<Organization>;
  publicTransportDepartureLink?: Maybe<Scalars['String']>;
  publicTransportSettings?: Maybe<PublicTransport>;
};


export type MonitorGarbageCollectionsArgs = {
  cursor?: InputMaybe<GarbageCollectionWhereUniqueInput>;
  orderBy?: Array<GarbageCollectionOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: GarbageCollectionWhereInput;
};


export type MonitorGarbageCollectionsCountArgs = {
  where?: GarbageCollectionWhereInput;
};

export type MonitorCreateInput = {
  boardLogo?: InputMaybe<ImageFieldInput>;
  garbageCollectionAssignment?: InputMaybe<GarbageCollectionAssignmentRelateToOneForCreateInput>;
  garbageCollectionCalendar?: InputMaybe<FileFieldInput>;
  garbageCollections?: InputMaybe<GarbageCollectionRelateToManyForCreateInput>;
  group?: InputMaybe<MonitorGroupRelateToOneForCreateInput>;
  name?: InputMaybe<Scalars['String']>;
  operatorLogo?: InputMaybe<ImageFieldInput>;
  organization?: InputMaybe<OrganizationRelateToOneForCreateInput>;
  publicTransportDepartureLink?: InputMaybe<Scalars['String']>;
  publicTransportSettings?: InputMaybe<PublicTransportRelateToOneForCreateInput>;
};

export type MonitorGroup = {
  announcements?: Maybe<Array<Announcement>>;
  announcementsCount?: Maybe<Scalars['Int']>;
  contact?: Maybe<Contact>;
  defaultEventImage?: Maybe<ImageFieldOutput>;
  defaultNewsImage?: Maybe<ImageFieldOutput>;
  documents?: Maybe<Array<Document>>;
  documentsCount?: Maybe<Scalars['Int']>;
  eventProviders?: Maybe<Array<EventProvider>>;
  eventProvidersCount?: Maybe<Scalars['Int']>;
  events?: Maybe<Array<Event>>;
  eventsCount?: Maybe<Scalars['Int']>;
  id: Scalars['ID'];
  monitors?: Maybe<Array<Monitor>>;
  monitorsCount?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  newsProviders?: Maybe<Array<NewsProvider>>;
  newsProvidersCount?: Maybe<Scalars['Int']>;
  newsReports?: Maybe<Array<NewsReport>>;
  newsReportsCount?: Maybe<Scalars['Int']>;
  officialRegionalKey?: Maybe<Scalars['String']>;
  organization?: Maybe<Organization>;
  warnings?: Maybe<Array<Warning>>;
  warningsCount?: Maybe<Scalars['Int']>;
  weatherLocation?: Maybe<WeatherLocation>;
};


export type MonitorGroupAnnouncementsArgs = {
  cursor?: InputMaybe<AnnouncementWhereUniqueInput>;
  orderBy?: Array<AnnouncementOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: AnnouncementWhereInput;
};


export type MonitorGroupAnnouncementsCountArgs = {
  where?: AnnouncementWhereInput;
};


export type MonitorGroupDocumentsArgs = {
  cursor?: InputMaybe<DocumentWhereUniqueInput>;
  orderBy?: Array<DocumentOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: DocumentWhereInput;
};


export type MonitorGroupDocumentsCountArgs = {
  where?: DocumentWhereInput;
};


export type MonitorGroupEventProvidersArgs = {
  cursor?: InputMaybe<EventProviderWhereUniqueInput>;
  orderBy?: Array<EventProviderOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: EventProviderWhereInput;
};


export type MonitorGroupEventProvidersCountArgs = {
  where?: EventProviderWhereInput;
};


export type MonitorGroupEventsArgs = {
  cursor?: InputMaybe<EventWhereUniqueInput>;
  orderBy?: Array<EventOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: EventWhereInput;
};


export type MonitorGroupEventsCountArgs = {
  where?: EventWhereInput;
};


export type MonitorGroupMonitorsArgs = {
  cursor?: InputMaybe<MonitorWhereUniqueInput>;
  orderBy?: Array<MonitorOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: MonitorWhereInput;
};


export type MonitorGroupMonitorsCountArgs = {
  where?: MonitorWhereInput;
};


export type MonitorGroupNewsProvidersArgs = {
  cursor?: InputMaybe<NewsProviderWhereUniqueInput>;
  orderBy?: Array<NewsProviderOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: NewsProviderWhereInput;
};


export type MonitorGroupNewsProvidersCountArgs = {
  where?: NewsProviderWhereInput;
};


export type MonitorGroupNewsReportsArgs = {
  cursor?: InputMaybe<NewsReportWhereUniqueInput>;
  orderBy?: Array<NewsReportOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: NewsReportWhereInput;
};


export type MonitorGroupNewsReportsCountArgs = {
  where?: NewsReportWhereInput;
};


export type MonitorGroupWarningsArgs = {
  cursor?: InputMaybe<WarningWhereUniqueInput>;
  orderBy?: Array<WarningOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: WarningWhereInput;
};


export type MonitorGroupWarningsCountArgs = {
  where?: WarningWhereInput;
};

export type MonitorGroupCreateInput = {
  announcements?: InputMaybe<AnnouncementRelateToManyForCreateInput>;
  contact?: InputMaybe<ContactRelateToOneForCreateInput>;
  defaultEventImage?: InputMaybe<ImageFieldInput>;
  defaultNewsImage?: InputMaybe<ImageFieldInput>;
  documents?: InputMaybe<DocumentRelateToManyForCreateInput>;
  eventProviders?: InputMaybe<EventProviderRelateToManyForCreateInput>;
  events?: InputMaybe<EventRelateToManyForCreateInput>;
  monitors?: InputMaybe<MonitorRelateToManyForCreateInput>;
  name?: InputMaybe<Scalars['String']>;
  newsProviders?: InputMaybe<NewsProviderRelateToManyForCreateInput>;
  newsReports?: InputMaybe<NewsReportRelateToManyForCreateInput>;
  officialRegionalKey?: InputMaybe<Scalars['String']>;
  organization?: InputMaybe<OrganizationRelateToOneForCreateInput>;
  warnings?: InputMaybe<WarningRelateToManyForCreateInput>;
  weatherLocation?: InputMaybe<WeatherLocationRelateToOneForCreateInput>;
};

export type MonitorGroupManyRelationFilter = {
  every?: InputMaybe<MonitorGroupWhereInput>;
  none?: InputMaybe<MonitorGroupWhereInput>;
  some?: InputMaybe<MonitorGroupWhereInput>;
};

export type MonitorGroupOrderByInput = {
  id?: InputMaybe<OrderDirection>;
  name?: InputMaybe<OrderDirection>;
  officialRegionalKey?: InputMaybe<OrderDirection>;
};

export type MonitorGroupRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<MonitorGroupWhereUniqueInput>>;
  create?: InputMaybe<Array<MonitorGroupCreateInput>>;
};

export type MonitorGroupRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<MonitorGroupWhereUniqueInput>>;
  create?: InputMaybe<Array<MonitorGroupCreateInput>>;
  disconnect?: InputMaybe<Array<MonitorGroupWhereUniqueInput>>;
  set?: InputMaybe<Array<MonitorGroupWhereUniqueInput>>;
};

export type MonitorGroupRelateToOneForCreateInput = {
  connect?: InputMaybe<MonitorGroupWhereUniqueInput>;
  create?: InputMaybe<MonitorGroupCreateInput>;
};

export type MonitorGroupRelateToOneForUpdateInput = {
  connect?: InputMaybe<MonitorGroupWhereUniqueInput>;
  create?: InputMaybe<MonitorGroupCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']>;
};

export type MonitorGroupUpdateArgs = {
  data: MonitorGroupUpdateInput;
  where: MonitorGroupWhereUniqueInput;
};

export type MonitorGroupUpdateInput = {
  announcements?: InputMaybe<AnnouncementRelateToManyForUpdateInput>;
  contact?: InputMaybe<ContactRelateToOneForUpdateInput>;
  defaultEventImage?: InputMaybe<ImageFieldInput>;
  defaultNewsImage?: InputMaybe<ImageFieldInput>;
  documents?: InputMaybe<DocumentRelateToManyForUpdateInput>;
  eventProviders?: InputMaybe<EventProviderRelateToManyForUpdateInput>;
  events?: InputMaybe<EventRelateToManyForUpdateInput>;
  monitors?: InputMaybe<MonitorRelateToManyForUpdateInput>;
  name?: InputMaybe<Scalars['String']>;
  newsProviders?: InputMaybe<NewsProviderRelateToManyForUpdateInput>;
  newsReports?: InputMaybe<NewsReportRelateToManyForUpdateInput>;
  officialRegionalKey?: InputMaybe<Scalars['String']>;
  organization?: InputMaybe<OrganizationRelateToOneForUpdateInput>;
  warnings?: InputMaybe<WarningRelateToManyForUpdateInput>;
  weatherLocation?: InputMaybe<WeatherLocationRelateToOneForUpdateInput>;
};

export type MonitorGroupWhereInput = {
  AND?: InputMaybe<Array<MonitorGroupWhereInput>>;
  NOT?: InputMaybe<Array<MonitorGroupWhereInput>>;
  OR?: InputMaybe<Array<MonitorGroupWhereInput>>;
  announcements?: InputMaybe<AnnouncementManyRelationFilter>;
  contact?: InputMaybe<ContactWhereInput>;
  documents?: InputMaybe<DocumentManyRelationFilter>;
  eventProviders?: InputMaybe<EventProviderManyRelationFilter>;
  events?: InputMaybe<EventManyRelationFilter>;
  id?: InputMaybe<IdFilter>;
  monitors?: InputMaybe<MonitorManyRelationFilter>;
  name?: InputMaybe<StringFilter>;
  newsProviders?: InputMaybe<NewsProviderManyRelationFilter>;
  newsReports?: InputMaybe<NewsReportManyRelationFilter>;
  officialRegionalKey?: InputMaybe<StringFilter>;
  organization?: InputMaybe<OrganizationWhereInput>;
  warnings?: InputMaybe<WarningManyRelationFilter>;
  weatherLocation?: InputMaybe<WeatherLocationWhereInput>;
};

export type MonitorGroupWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']>;
};

export type MonitorManyRelationFilter = {
  every?: InputMaybe<MonitorWhereInput>;
  none?: InputMaybe<MonitorWhereInput>;
  some?: InputMaybe<MonitorWhereInput>;
};

export type MonitorOrderByInput = {
  id?: InputMaybe<OrderDirection>;
  name?: InputMaybe<OrderDirection>;
  publicTransportDepartureLink?: InputMaybe<OrderDirection>;
};

export type MonitorRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<MonitorWhereUniqueInput>>;
  create?: InputMaybe<Array<MonitorCreateInput>>;
};

export type MonitorRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<MonitorWhereUniqueInput>>;
  create?: InputMaybe<Array<MonitorCreateInput>>;
  disconnect?: InputMaybe<Array<MonitorWhereUniqueInput>>;
  set?: InputMaybe<Array<MonitorWhereUniqueInput>>;
};

export type MonitorRelateToOneForCreateInput = {
  connect?: InputMaybe<MonitorWhereUniqueInput>;
  create?: InputMaybe<MonitorCreateInput>;
};

export type MonitorRelateToOneForUpdateInput = {
  connect?: InputMaybe<MonitorWhereUniqueInput>;
  create?: InputMaybe<MonitorCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']>;
};

export type MonitorUpdateArgs = {
  data: MonitorUpdateInput;
  where: MonitorWhereUniqueInput;
};

export type MonitorUpdateInput = {
  boardLogo?: InputMaybe<ImageFieldInput>;
  garbageCollectionAssignment?: InputMaybe<GarbageCollectionAssignmentRelateToOneForUpdateInput>;
  garbageCollectionCalendar?: InputMaybe<FileFieldInput>;
  garbageCollections?: InputMaybe<GarbageCollectionRelateToManyForUpdateInput>;
  group?: InputMaybe<MonitorGroupRelateToOneForUpdateInput>;
  name?: InputMaybe<Scalars['String']>;
  operatorLogo?: InputMaybe<ImageFieldInput>;
  organization?: InputMaybe<OrganizationRelateToOneForUpdateInput>;
  publicTransportDepartureLink?: InputMaybe<Scalars['String']>;
  publicTransportSettings?: InputMaybe<PublicTransportRelateToOneForUpdateInput>;
};

export type MonitorWhereInput = {
  AND?: InputMaybe<Array<MonitorWhereInput>>;
  NOT?: InputMaybe<Array<MonitorWhereInput>>;
  OR?: InputMaybe<Array<MonitorWhereInput>>;
  garbageCollectionAssignment?: InputMaybe<GarbageCollectionAssignmentWhereInput>;
  garbageCollections?: InputMaybe<GarbageCollectionManyRelationFilter>;
  group?: InputMaybe<MonitorGroupWhereInput>;
  id?: InputMaybe<IdFilter>;
  name?: InputMaybe<StringFilter>;
  organization?: InputMaybe<OrganizationWhereInput>;
  publicTransportDepartureLink?: InputMaybe<StringFilter>;
  publicTransportSettings?: InputMaybe<PublicTransportWhereInput>;
};

export type MonitorWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']>;
};

export type Mutation = {
  authenticateUserWithPassword?: Maybe<UserAuthenticationWithPasswordResult>;
  createAnnouncement?: Maybe<Announcement>;
  createAnnouncements?: Maybe<Array<Maybe<Announcement>>>;
  createContact?: Maybe<Contact>;
  createContacts?: Maybe<Array<Maybe<Contact>>>;
  createDocument?: Maybe<Document>;
  createDocuments?: Maybe<Array<Maybe<Document>>>;
  createEvent?: Maybe<Event>;
  createEventProvider?: Maybe<EventProvider>;
  createEventProviders?: Maybe<Array<Maybe<EventProvider>>>;
  createEvents?: Maybe<Array<Maybe<Event>>>;
  createGarbageCollection?: Maybe<GarbageCollection>;
  createGarbageCollectionAssignment?: Maybe<GarbageCollectionAssignment>;
  createGarbageCollectionAssignments?: Maybe<Array<Maybe<GarbageCollectionAssignment>>>;
  createGarbageCollections?: Maybe<Array<Maybe<GarbageCollection>>>;
  createInitialUser: UserAuthenticationWithPasswordSuccess;
  createMonitor?: Maybe<Monitor>;
  createMonitorGroup?: Maybe<MonitorGroup>;
  createMonitorGroups?: Maybe<Array<Maybe<MonitorGroup>>>;
  createMonitors?: Maybe<Array<Maybe<Monitor>>>;
  createNewsProvider?: Maybe<NewsProvider>;
  createNewsProviders?: Maybe<Array<Maybe<NewsProvider>>>;
  createNewsReport?: Maybe<NewsReport>;
  createNewsReports?: Maybe<Array<Maybe<NewsReport>>>;
  createOrganization?: Maybe<Organization>;
  createOrganizations?: Maybe<Array<Maybe<Organization>>>;
  createPublicTransport?: Maybe<PublicTransport>;
  createPublicTransports?: Maybe<Array<Maybe<PublicTransport>>>;
  createUser?: Maybe<User>;
  createUsers?: Maybe<Array<Maybe<User>>>;
  createWarning?: Maybe<Warning>;
  createWarnings?: Maybe<Array<Maybe<Warning>>>;
  createWeatherForecast?: Maybe<WeatherForecast>;
  createWeatherForecasts?: Maybe<Array<Maybe<WeatherForecast>>>;
  createWeatherLocation?: Maybe<WeatherLocation>;
  createWeatherLocations?: Maybe<Array<Maybe<WeatherLocation>>>;
  createWeatherReport?: Maybe<WeatherReport>;
  createWeatherReports?: Maybe<Array<Maybe<WeatherReport>>>;
  deleteAnnouncement?: Maybe<Announcement>;
  deleteAnnouncements?: Maybe<Array<Maybe<Announcement>>>;
  deleteContact?: Maybe<Contact>;
  deleteContacts?: Maybe<Array<Maybe<Contact>>>;
  deleteDocument?: Maybe<Document>;
  deleteDocuments?: Maybe<Array<Maybe<Document>>>;
  deleteEvent?: Maybe<Event>;
  deleteEventProvider?: Maybe<EventProvider>;
  deleteEventProviders?: Maybe<Array<Maybe<EventProvider>>>;
  deleteEvents?: Maybe<Array<Maybe<Event>>>;
  deleteGarbageCollection?: Maybe<GarbageCollection>;
  deleteGarbageCollectionAssignment?: Maybe<GarbageCollectionAssignment>;
  deleteGarbageCollectionAssignments?: Maybe<Array<Maybe<GarbageCollectionAssignment>>>;
  deleteGarbageCollections?: Maybe<Array<Maybe<GarbageCollection>>>;
  deleteMonitor?: Maybe<Monitor>;
  deleteMonitorGroup?: Maybe<MonitorGroup>;
  deleteMonitorGroups?: Maybe<Array<Maybe<MonitorGroup>>>;
  deleteMonitors?: Maybe<Array<Maybe<Monitor>>>;
  deleteNewsProvider?: Maybe<NewsProvider>;
  deleteNewsProviders?: Maybe<Array<Maybe<NewsProvider>>>;
  deleteNewsReport?: Maybe<NewsReport>;
  deleteNewsReports?: Maybe<Array<Maybe<NewsReport>>>;
  deleteOrganization?: Maybe<Organization>;
  deleteOrganizations?: Maybe<Array<Maybe<Organization>>>;
  deletePublicTransport?: Maybe<PublicTransport>;
  deletePublicTransports?: Maybe<Array<Maybe<PublicTransport>>>;
  deleteUser?: Maybe<User>;
  deleteUsers?: Maybe<Array<Maybe<User>>>;
  deleteWarning?: Maybe<Warning>;
  deleteWarnings?: Maybe<Array<Maybe<Warning>>>;
  deleteWeatherForecast?: Maybe<WeatherForecast>;
  deleteWeatherForecasts?: Maybe<Array<Maybe<WeatherForecast>>>;
  deleteWeatherLocation?: Maybe<WeatherLocation>;
  deleteWeatherLocations?: Maybe<Array<Maybe<WeatherLocation>>>;
  deleteWeatherReport?: Maybe<WeatherReport>;
  deleteWeatherReports?: Maybe<Array<Maybe<WeatherReport>>>;
  endSession: Scalars['Boolean'];
  updateAnnouncement?: Maybe<Announcement>;
  updateAnnouncements?: Maybe<Array<Maybe<Announcement>>>;
  updateContact?: Maybe<Contact>;
  updateContacts?: Maybe<Array<Maybe<Contact>>>;
  updateDocument?: Maybe<Document>;
  updateDocuments?: Maybe<Array<Maybe<Document>>>;
  updateEvent?: Maybe<Event>;
  updateEventProvider?: Maybe<EventProvider>;
  updateEventProviders?: Maybe<Array<Maybe<EventProvider>>>;
  updateEvents?: Maybe<Array<Maybe<Event>>>;
  updateGarbageCollection?: Maybe<GarbageCollection>;
  updateGarbageCollectionAssignment?: Maybe<GarbageCollectionAssignment>;
  updateGarbageCollectionAssignments?: Maybe<Array<Maybe<GarbageCollectionAssignment>>>;
  updateGarbageCollections?: Maybe<Array<Maybe<GarbageCollection>>>;
  updateMonitor?: Maybe<Monitor>;
  updateMonitorGroup?: Maybe<MonitorGroup>;
  updateMonitorGroups?: Maybe<Array<Maybe<MonitorGroup>>>;
  updateMonitors?: Maybe<Array<Maybe<Monitor>>>;
  updateNewsProvider?: Maybe<NewsProvider>;
  updateNewsProviders?: Maybe<Array<Maybe<NewsProvider>>>;
  updateNewsReport?: Maybe<NewsReport>;
  updateNewsReports?: Maybe<Array<Maybe<NewsReport>>>;
  updateOrganization?: Maybe<Organization>;
  updateOrganizations?: Maybe<Array<Maybe<Organization>>>;
  updatePublicTransport?: Maybe<PublicTransport>;
  updatePublicTransports?: Maybe<Array<Maybe<PublicTransport>>>;
  updateUser?: Maybe<User>;
  updateUsers?: Maybe<Array<Maybe<User>>>;
  updateWarning?: Maybe<Warning>;
  updateWarnings?: Maybe<Array<Maybe<Warning>>>;
  updateWeatherForecast?: Maybe<WeatherForecast>;
  updateWeatherForecasts?: Maybe<Array<Maybe<WeatherForecast>>>;
  updateWeatherLocation?: Maybe<WeatherLocation>;
  updateWeatherLocations?: Maybe<Array<Maybe<WeatherLocation>>>;
  updateWeatherReport?: Maybe<WeatherReport>;
  updateWeatherReports?: Maybe<Array<Maybe<WeatherReport>>>;
};


export type MutationAuthenticateUserWithPasswordArgs = {
  email: Scalars['String'];
  password: Scalars['String'];
};


export type MutationCreateAnnouncementArgs = {
  data: AnnouncementCreateInput;
};


export type MutationCreateAnnouncementsArgs = {
  data: Array<AnnouncementCreateInput>;
};


export type MutationCreateContactArgs = {
  data: ContactCreateInput;
};


export type MutationCreateContactsArgs = {
  data: Array<ContactCreateInput>;
};


export type MutationCreateDocumentArgs = {
  data: DocumentCreateInput;
};


export type MutationCreateDocumentsArgs = {
  data: Array<DocumentCreateInput>;
};


export type MutationCreateEventArgs = {
  data: EventCreateInput;
};


export type MutationCreateEventProviderArgs = {
  data: EventProviderCreateInput;
};


export type MutationCreateEventProvidersArgs = {
  data: Array<EventProviderCreateInput>;
};


export type MutationCreateEventsArgs = {
  data: Array<EventCreateInput>;
};


export type MutationCreateGarbageCollectionArgs = {
  data: GarbageCollectionCreateInput;
};


export type MutationCreateGarbageCollectionAssignmentArgs = {
  data: GarbageCollectionAssignmentCreateInput;
};


export type MutationCreateGarbageCollectionAssignmentsArgs = {
  data: Array<GarbageCollectionAssignmentCreateInput>;
};


export type MutationCreateGarbageCollectionsArgs = {
  data: Array<GarbageCollectionCreateInput>;
};


export type MutationCreateInitialUserArgs = {
  data: CreateInitialUserInput;
};


export type MutationCreateMonitorArgs = {
  data: MonitorCreateInput;
};


export type MutationCreateMonitorGroupArgs = {
  data: MonitorGroupCreateInput;
};


export type MutationCreateMonitorGroupsArgs = {
  data: Array<MonitorGroupCreateInput>;
};


export type MutationCreateMonitorsArgs = {
  data: Array<MonitorCreateInput>;
};


export type MutationCreateNewsProviderArgs = {
  data: NewsProviderCreateInput;
};


export type MutationCreateNewsProvidersArgs = {
  data: Array<NewsProviderCreateInput>;
};


export type MutationCreateNewsReportArgs = {
  data: NewsReportCreateInput;
};


export type MutationCreateNewsReportsArgs = {
  data: Array<NewsReportCreateInput>;
};


export type MutationCreateOrganizationArgs = {
  data: OrganizationCreateInput;
};


export type MutationCreateOrganizationsArgs = {
  data: Array<OrganizationCreateInput>;
};


export type MutationCreatePublicTransportArgs = {
  data: PublicTransportCreateInput;
};


export type MutationCreatePublicTransportsArgs = {
  data: Array<PublicTransportCreateInput>;
};


export type MutationCreateUserArgs = {
  data: UserCreateInput;
};


export type MutationCreateUsersArgs = {
  data: Array<UserCreateInput>;
};


export type MutationCreateWarningArgs = {
  data: WarningCreateInput;
};


export type MutationCreateWarningsArgs = {
  data: Array<WarningCreateInput>;
};


export type MutationCreateWeatherForecastArgs = {
  data: WeatherForecastCreateInput;
};


export type MutationCreateWeatherForecastsArgs = {
  data: Array<WeatherForecastCreateInput>;
};


export type MutationCreateWeatherLocationArgs = {
  data: WeatherLocationCreateInput;
};


export type MutationCreateWeatherLocationsArgs = {
  data: Array<WeatherLocationCreateInput>;
};


export type MutationCreateWeatherReportArgs = {
  data: WeatherReportCreateInput;
};


export type MutationCreateWeatherReportsArgs = {
  data: Array<WeatherReportCreateInput>;
};


export type MutationDeleteAnnouncementArgs = {
  where: AnnouncementWhereUniqueInput;
};


export type MutationDeleteAnnouncementsArgs = {
  where: Array<AnnouncementWhereUniqueInput>;
};


export type MutationDeleteContactArgs = {
  where: ContactWhereUniqueInput;
};


export type MutationDeleteContactsArgs = {
  where: Array<ContactWhereUniqueInput>;
};


export type MutationDeleteDocumentArgs = {
  where: DocumentWhereUniqueInput;
};


export type MutationDeleteDocumentsArgs = {
  where: Array<DocumentWhereUniqueInput>;
};


export type MutationDeleteEventArgs = {
  where: EventWhereUniqueInput;
};


export type MutationDeleteEventProviderArgs = {
  where: EventProviderWhereUniqueInput;
};


export type MutationDeleteEventProvidersArgs = {
  where: Array<EventProviderWhereUniqueInput>;
};


export type MutationDeleteEventsArgs = {
  where: Array<EventWhereUniqueInput>;
};


export type MutationDeleteGarbageCollectionArgs = {
  where: GarbageCollectionWhereUniqueInput;
};


export type MutationDeleteGarbageCollectionAssignmentArgs = {
  where: GarbageCollectionAssignmentWhereUniqueInput;
};


export type MutationDeleteGarbageCollectionAssignmentsArgs = {
  where: Array<GarbageCollectionAssignmentWhereUniqueInput>;
};


export type MutationDeleteGarbageCollectionsArgs = {
  where: Array<GarbageCollectionWhereUniqueInput>;
};


export type MutationDeleteMonitorArgs = {
  where: MonitorWhereUniqueInput;
};


export type MutationDeleteMonitorGroupArgs = {
  where: MonitorGroupWhereUniqueInput;
};


export type MutationDeleteMonitorGroupsArgs = {
  where: Array<MonitorGroupWhereUniqueInput>;
};


export type MutationDeleteMonitorsArgs = {
  where: Array<MonitorWhereUniqueInput>;
};


export type MutationDeleteNewsProviderArgs = {
  where: NewsProviderWhereUniqueInput;
};


export type MutationDeleteNewsProvidersArgs = {
  where: Array<NewsProviderWhereUniqueInput>;
};


export type MutationDeleteNewsReportArgs = {
  where: NewsReportWhereUniqueInput;
};


export type MutationDeleteNewsReportsArgs = {
  where: Array<NewsReportWhereUniqueInput>;
};


export type MutationDeleteOrganizationArgs = {
  where: OrganizationWhereUniqueInput;
};


export type MutationDeleteOrganizationsArgs = {
  where: Array<OrganizationWhereUniqueInput>;
};


export type MutationDeletePublicTransportArgs = {
  where: PublicTransportWhereUniqueInput;
};


export type MutationDeletePublicTransportsArgs = {
  where: Array<PublicTransportWhereUniqueInput>;
};


export type MutationDeleteUserArgs = {
  where: UserWhereUniqueInput;
};


export type MutationDeleteUsersArgs = {
  where: Array<UserWhereUniqueInput>;
};


export type MutationDeleteWarningArgs = {
  where: WarningWhereUniqueInput;
};


export type MutationDeleteWarningsArgs = {
  where: Array<WarningWhereUniqueInput>;
};


export type MutationDeleteWeatherForecastArgs = {
  where: WeatherForecastWhereUniqueInput;
};


export type MutationDeleteWeatherForecastsArgs = {
  where: Array<WeatherForecastWhereUniqueInput>;
};


export type MutationDeleteWeatherLocationArgs = {
  where: WeatherLocationWhereUniqueInput;
};


export type MutationDeleteWeatherLocationsArgs = {
  where: Array<WeatherLocationWhereUniqueInput>;
};


export type MutationDeleteWeatherReportArgs = {
  where: WeatherReportWhereUniqueInput;
};


export type MutationDeleteWeatherReportsArgs = {
  where: Array<WeatherReportWhereUniqueInput>;
};


export type MutationUpdateAnnouncementArgs = {
  data: AnnouncementUpdateInput;
  where: AnnouncementWhereUniqueInput;
};


export type MutationUpdateAnnouncementsArgs = {
  data: Array<AnnouncementUpdateArgs>;
};


export type MutationUpdateContactArgs = {
  data: ContactUpdateInput;
  where: ContactWhereUniqueInput;
};


export type MutationUpdateContactsArgs = {
  data: Array<ContactUpdateArgs>;
};


export type MutationUpdateDocumentArgs = {
  data: DocumentUpdateInput;
  where: DocumentWhereUniqueInput;
};


export type MutationUpdateDocumentsArgs = {
  data: Array<DocumentUpdateArgs>;
};


export type MutationUpdateEventArgs = {
  data: EventUpdateInput;
  where: EventWhereUniqueInput;
};


export type MutationUpdateEventProviderArgs = {
  data: EventProviderUpdateInput;
  where: EventProviderWhereUniqueInput;
};


export type MutationUpdateEventProvidersArgs = {
  data: Array<EventProviderUpdateArgs>;
};


export type MutationUpdateEventsArgs = {
  data: Array<EventUpdateArgs>;
};


export type MutationUpdateGarbageCollectionArgs = {
  data: GarbageCollectionUpdateInput;
  where: GarbageCollectionWhereUniqueInput;
};


export type MutationUpdateGarbageCollectionAssignmentArgs = {
  data: GarbageCollectionAssignmentUpdateInput;
  where: GarbageCollectionAssignmentWhereUniqueInput;
};


export type MutationUpdateGarbageCollectionAssignmentsArgs = {
  data: Array<GarbageCollectionAssignmentUpdateArgs>;
};


export type MutationUpdateGarbageCollectionsArgs = {
  data: Array<GarbageCollectionUpdateArgs>;
};


export type MutationUpdateMonitorArgs = {
  data: MonitorUpdateInput;
  where: MonitorWhereUniqueInput;
};


export type MutationUpdateMonitorGroupArgs = {
  data: MonitorGroupUpdateInput;
  where: MonitorGroupWhereUniqueInput;
};


export type MutationUpdateMonitorGroupsArgs = {
  data: Array<MonitorGroupUpdateArgs>;
};


export type MutationUpdateMonitorsArgs = {
  data: Array<MonitorUpdateArgs>;
};


export type MutationUpdateNewsProviderArgs = {
  data: NewsProviderUpdateInput;
  where: NewsProviderWhereUniqueInput;
};


export type MutationUpdateNewsProvidersArgs = {
  data: Array<NewsProviderUpdateArgs>;
};


export type MutationUpdateNewsReportArgs = {
  data: NewsReportUpdateInput;
  where: NewsReportWhereUniqueInput;
};


export type MutationUpdateNewsReportsArgs = {
  data: Array<NewsReportUpdateArgs>;
};


export type MutationUpdateOrganizationArgs = {
  data: OrganizationUpdateInput;
  where: OrganizationWhereUniqueInput;
};


export type MutationUpdateOrganizationsArgs = {
  data: Array<OrganizationUpdateArgs>;
};


export type MutationUpdatePublicTransportArgs = {
  data: PublicTransportUpdateInput;
  where: PublicTransportWhereUniqueInput;
};


export type MutationUpdatePublicTransportsArgs = {
  data: Array<PublicTransportUpdateArgs>;
};


export type MutationUpdateUserArgs = {
  data: UserUpdateInput;
  where: UserWhereUniqueInput;
};


export type MutationUpdateUsersArgs = {
  data: Array<UserUpdateArgs>;
};


export type MutationUpdateWarningArgs = {
  data: WarningUpdateInput;
  where: WarningWhereUniqueInput;
};


export type MutationUpdateWarningsArgs = {
  data: Array<WarningUpdateArgs>;
};


export type MutationUpdateWeatherForecastArgs = {
  data: WeatherForecastUpdateInput;
  where: WeatherForecastWhereUniqueInput;
};


export type MutationUpdateWeatherForecastsArgs = {
  data: Array<WeatherForecastUpdateArgs>;
};


export type MutationUpdateWeatherLocationArgs = {
  data: WeatherLocationUpdateInput;
  where: WeatherLocationWhereUniqueInput;
};


export type MutationUpdateWeatherLocationsArgs = {
  data: Array<WeatherLocationUpdateArgs>;
};


export type MutationUpdateWeatherReportArgs = {
  data: WeatherReportUpdateInput;
  where: WeatherReportWhereUniqueInput;
};


export type MutationUpdateWeatherReportsArgs = {
  data: Array<WeatherReportUpdateArgs>;
};

export type NestedStringFilter = {
  contains?: InputMaybe<Scalars['String']>;
  endsWith?: InputMaybe<Scalars['String']>;
  equals?: InputMaybe<Scalars['String']>;
  gt?: InputMaybe<Scalars['String']>;
  gte?: InputMaybe<Scalars['String']>;
  in?: InputMaybe<Array<Scalars['String']>>;
  lt?: InputMaybe<Scalars['String']>;
  lte?: InputMaybe<Scalars['String']>;
  not?: InputMaybe<NestedStringFilter>;
  notIn?: InputMaybe<Array<Scalars['String']>>;
  startsWith?: InputMaybe<Scalars['String']>;
};

export type NestedStringNullableFilter = {
  contains?: InputMaybe<Scalars['String']>;
  endsWith?: InputMaybe<Scalars['String']>;
  equals?: InputMaybe<Scalars['String']>;
  gt?: InputMaybe<Scalars['String']>;
  gte?: InputMaybe<Scalars['String']>;
  in?: InputMaybe<Array<Scalars['String']>>;
  lt?: InputMaybe<Scalars['String']>;
  lte?: InputMaybe<Scalars['String']>;
  not?: InputMaybe<NestedStringNullableFilter>;
  notIn?: InputMaybe<Array<Scalars['String']>>;
  startsWith?: InputMaybe<Scalars['String']>;
};

export type NewsProvider = {
  configuration?: Maybe<Scalars['String']>;
  feedType?: Maybe<Scalars['String']>;
  feedUrl?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  image?: Maybe<ImageFieldOutput>;
  monitorGroups?: Maybe<Array<MonitorGroup>>;
  monitorGroupsCount?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  newsReports?: Maybe<Array<NewsReport>>;
  newsReportsCount?: Maybe<Scalars['Int']>;
  organization?: Maybe<Organization>;
};


export type NewsProviderMonitorGroupsArgs = {
  cursor?: InputMaybe<MonitorGroupWhereUniqueInput>;
  orderBy?: Array<MonitorGroupOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: MonitorGroupWhereInput;
};


export type NewsProviderMonitorGroupsCountArgs = {
  where?: MonitorGroupWhereInput;
};


export type NewsProviderNewsReportsArgs = {
  cursor?: InputMaybe<NewsReportWhereUniqueInput>;
  orderBy?: Array<NewsReportOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: NewsReportWhereInput;
};


export type NewsProviderNewsReportsCountArgs = {
  where?: NewsReportWhereInput;
};

export type NewsProviderCreateInput = {
  configuration?: InputMaybe<Scalars['String']>;
  feedType?: InputMaybe<Scalars['String']>;
  feedUrl?: InputMaybe<Scalars['String']>;
  image?: InputMaybe<ImageFieldInput>;
  monitorGroups?: InputMaybe<MonitorGroupRelateToManyForCreateInput>;
  name?: InputMaybe<Scalars['String']>;
  newsReports?: InputMaybe<NewsReportRelateToManyForCreateInput>;
  organization?: InputMaybe<OrganizationRelateToOneForCreateInput>;
};

export type NewsProviderManyRelationFilter = {
  every?: InputMaybe<NewsProviderWhereInput>;
  none?: InputMaybe<NewsProviderWhereInput>;
  some?: InputMaybe<NewsProviderWhereInput>;
};

export type NewsProviderOrderByInput = {
  configuration?: InputMaybe<OrderDirection>;
  feedType?: InputMaybe<OrderDirection>;
  feedUrl?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  name?: InputMaybe<OrderDirection>;
};

export type NewsProviderRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<NewsProviderWhereUniqueInput>>;
  create?: InputMaybe<Array<NewsProviderCreateInput>>;
};

export type NewsProviderRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<NewsProviderWhereUniqueInput>>;
  create?: InputMaybe<Array<NewsProviderCreateInput>>;
  disconnect?: InputMaybe<Array<NewsProviderWhereUniqueInput>>;
  set?: InputMaybe<Array<NewsProviderWhereUniqueInput>>;
};

export type NewsProviderRelateToOneForCreateInput = {
  connect?: InputMaybe<NewsProviderWhereUniqueInput>;
  create?: InputMaybe<NewsProviderCreateInput>;
};

export type NewsProviderRelateToOneForUpdateInput = {
  connect?: InputMaybe<NewsProviderWhereUniqueInput>;
  create?: InputMaybe<NewsProviderCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']>;
};

export type NewsProviderUpdateArgs = {
  data: NewsProviderUpdateInput;
  where: NewsProviderWhereUniqueInput;
};

export type NewsProviderUpdateInput = {
  configuration?: InputMaybe<Scalars['String']>;
  feedType?: InputMaybe<Scalars['String']>;
  feedUrl?: InputMaybe<Scalars['String']>;
  image?: InputMaybe<ImageFieldInput>;
  monitorGroups?: InputMaybe<MonitorGroupRelateToManyForUpdateInput>;
  name?: InputMaybe<Scalars['String']>;
  newsReports?: InputMaybe<NewsReportRelateToManyForUpdateInput>;
  organization?: InputMaybe<OrganizationRelateToOneForUpdateInput>;
};

export type NewsProviderWhereInput = {
  AND?: InputMaybe<Array<NewsProviderWhereInput>>;
  NOT?: InputMaybe<Array<NewsProviderWhereInput>>;
  OR?: InputMaybe<Array<NewsProviderWhereInput>>;
  configuration?: InputMaybe<StringFilter>;
  feedType?: InputMaybe<StringNullableFilter>;
  feedUrl?: InputMaybe<StringFilter>;
  id?: InputMaybe<IdFilter>;
  monitorGroups?: InputMaybe<MonitorGroupManyRelationFilter>;
  name?: InputMaybe<StringFilter>;
  newsReports?: InputMaybe<NewsReportManyRelationFilter>;
  organization?: InputMaybe<OrganizationWhereInput>;
};

export type NewsProviderWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']>;
};

export type NewsReport = {
  category?: Maybe<Scalars['String']>;
  content?: Maybe<NewsReport_Content_Document>;
  externalId?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  image?: Maybe<ImageFieldOutput>;
  link?: Maybe<Scalars['String']>;
  monitorGroups?: Maybe<Array<MonitorGroup>>;
  monitorGroupsCount?: Maybe<Scalars['Int']>;
  newsProvider?: Maybe<NewsProvider>;
  organization?: Maybe<Organization>;
  previewContent?: Maybe<Scalars['String']>;
  timestamp?: Maybe<Scalars['DateTime']>;
  title?: Maybe<Scalars['String']>;
};


export type NewsReportMonitorGroupsArgs = {
  cursor?: InputMaybe<MonitorGroupWhereUniqueInput>;
  orderBy?: Array<MonitorGroupOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: MonitorGroupWhereInput;
};


export type NewsReportMonitorGroupsCountArgs = {
  where?: MonitorGroupWhereInput;
};

export type NewsReportCreateInput = {
  category?: InputMaybe<Scalars['String']>;
  content?: InputMaybe<Scalars['JSON']>;
  externalId?: InputMaybe<Scalars['String']>;
  image?: InputMaybe<ImageFieldInput>;
  link?: InputMaybe<Scalars['String']>;
  monitorGroups?: InputMaybe<MonitorGroupRelateToManyForCreateInput>;
  newsProvider?: InputMaybe<NewsProviderRelateToOneForCreateInput>;
  organization?: InputMaybe<OrganizationRelateToOneForCreateInput>;
  previewContent?: InputMaybe<Scalars['String']>;
  timestamp?: InputMaybe<Scalars['DateTime']>;
  title?: InputMaybe<Scalars['String']>;
};

export type NewsReportManyRelationFilter = {
  every?: InputMaybe<NewsReportWhereInput>;
  none?: InputMaybe<NewsReportWhereInput>;
  some?: InputMaybe<NewsReportWhereInput>;
};

export type NewsReportOrderByInput = {
  category?: InputMaybe<OrderDirection>;
  externalId?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  link?: InputMaybe<OrderDirection>;
  previewContent?: InputMaybe<OrderDirection>;
  timestamp?: InputMaybe<OrderDirection>;
  title?: InputMaybe<OrderDirection>;
};

export type NewsReportRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<NewsReportWhereUniqueInput>>;
  create?: InputMaybe<Array<NewsReportCreateInput>>;
};

export type NewsReportRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<NewsReportWhereUniqueInput>>;
  create?: InputMaybe<Array<NewsReportCreateInput>>;
  disconnect?: InputMaybe<Array<NewsReportWhereUniqueInput>>;
  set?: InputMaybe<Array<NewsReportWhereUniqueInput>>;
};

export type NewsReportUpdateArgs = {
  data: NewsReportUpdateInput;
  where: NewsReportWhereUniqueInput;
};

export type NewsReportUpdateInput = {
  category?: InputMaybe<Scalars['String']>;
  content?: InputMaybe<Scalars['JSON']>;
  externalId?: InputMaybe<Scalars['String']>;
  image?: InputMaybe<ImageFieldInput>;
  link?: InputMaybe<Scalars['String']>;
  monitorGroups?: InputMaybe<MonitorGroupRelateToManyForUpdateInput>;
  newsProvider?: InputMaybe<NewsProviderRelateToOneForUpdateInput>;
  organization?: InputMaybe<OrganizationRelateToOneForUpdateInput>;
  previewContent?: InputMaybe<Scalars['String']>;
  timestamp?: InputMaybe<Scalars['DateTime']>;
  title?: InputMaybe<Scalars['String']>;
};

export type NewsReportWhereInput = {
  AND?: InputMaybe<Array<NewsReportWhereInput>>;
  NOT?: InputMaybe<Array<NewsReportWhereInput>>;
  OR?: InputMaybe<Array<NewsReportWhereInput>>;
  category?: InputMaybe<StringFilter>;
  externalId?: InputMaybe<StringFilter>;
  id?: InputMaybe<IdFilter>;
  link?: InputMaybe<StringFilter>;
  monitorGroups?: InputMaybe<MonitorGroupManyRelationFilter>;
  newsProvider?: InputMaybe<NewsProviderWhereInput>;
  organization?: InputMaybe<OrganizationWhereInput>;
  previewContent?: InputMaybe<StringFilter>;
  timestamp?: InputMaybe<DateTimeFilter>;
  title?: InputMaybe<StringFilter>;
};

export type NewsReportWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']>;
};

export type NewsReport_Content_Document = {
  document: Scalars['JSON'];
};


export type NewsReport_Content_DocumentDocumentArgs = {
  hydrateRelationships?: Scalars['Boolean'];
};

export enum OrderDirection {
  Asc = 'asc',
  Desc = 'desc'
}

export type Organization = {
  id: Scalars['ID'];
  name?: Maybe<Scalars['String']>;
};

export type OrganizationCreateInput = {
  name?: InputMaybe<Scalars['String']>;
};

export type OrganizationManyRelationFilter = {
  every?: InputMaybe<OrganizationWhereInput>;
  none?: InputMaybe<OrganizationWhereInput>;
  some?: InputMaybe<OrganizationWhereInput>;
};

export type OrganizationOrderByInput = {
  id?: InputMaybe<OrderDirection>;
  name?: InputMaybe<OrderDirection>;
};

export type OrganizationRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<OrganizationWhereUniqueInput>>;
  create?: InputMaybe<Array<OrganizationCreateInput>>;
};

export type OrganizationRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<OrganizationWhereUniqueInput>>;
  create?: InputMaybe<Array<OrganizationCreateInput>>;
  disconnect?: InputMaybe<Array<OrganizationWhereUniqueInput>>;
  set?: InputMaybe<Array<OrganizationWhereUniqueInput>>;
};

export type OrganizationRelateToOneForCreateInput = {
  connect?: InputMaybe<OrganizationWhereUniqueInput>;
  create?: InputMaybe<OrganizationCreateInput>;
};

export type OrganizationRelateToOneForUpdateInput = {
  connect?: InputMaybe<OrganizationWhereUniqueInput>;
  create?: InputMaybe<OrganizationCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']>;
};

export type OrganizationUpdateArgs = {
  data: OrganizationUpdateInput;
  where: OrganizationWhereUniqueInput;
};

export type OrganizationUpdateInput = {
  name?: InputMaybe<Scalars['String']>;
};

export type OrganizationWhereInput = {
  AND?: InputMaybe<Array<OrganizationWhereInput>>;
  NOT?: InputMaybe<Array<OrganizationWhereInput>>;
  OR?: InputMaybe<Array<OrganizationWhereInput>>;
  id?: InputMaybe<IdFilter>;
  name?: InputMaybe<StringFilter>;
};

export type OrganizationWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']>;
  name?: InputMaybe<Scalars['String']>;
};

export type PasswordState = {
  isSet: Scalars['Boolean'];
};

export type PublicTransport = {
  content?: Maybe<PublicTransport_Content_Document>;
  headline?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  image?: Maybe<ImageFieldOutput>;
  monitors?: Maybe<Array<Monitor>>;
  monitorsCount?: Maybe<Scalars['Int']>;
  organization?: Maybe<Organization>;
  service?: Maybe<Scalars['String']>;
  servicedata?: Maybe<PublicTransport_Servicedata_Document>;
};


export type PublicTransportMonitorsArgs = {
  cursor?: InputMaybe<MonitorWhereUniqueInput>;
  orderBy?: Array<MonitorOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: MonitorWhereInput;
};


export type PublicTransportMonitorsCountArgs = {
  where?: MonitorWhereInput;
};

export type PublicTransportCreateInput = {
  content?: InputMaybe<Scalars['JSON']>;
  headline?: InputMaybe<Scalars['String']>;
  image?: InputMaybe<ImageFieldInput>;
  monitors?: InputMaybe<MonitorRelateToManyForCreateInput>;
  organization?: InputMaybe<OrganizationRelateToOneForCreateInput>;
  service?: InputMaybe<Scalars['String']>;
  servicedata?: InputMaybe<Scalars['JSON']>;
};

export type PublicTransportOrderByInput = {
  headline?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  service?: InputMaybe<OrderDirection>;
};

export type PublicTransportRelateToOneForCreateInput = {
  connect?: InputMaybe<PublicTransportWhereUniqueInput>;
  create?: InputMaybe<PublicTransportCreateInput>;
};

export type PublicTransportRelateToOneForUpdateInput = {
  connect?: InputMaybe<PublicTransportWhereUniqueInput>;
  create?: InputMaybe<PublicTransportCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']>;
};

export type PublicTransportUpdateArgs = {
  data: PublicTransportUpdateInput;
  where: PublicTransportWhereUniqueInput;
};

export type PublicTransportUpdateInput = {
  content?: InputMaybe<Scalars['JSON']>;
  headline?: InputMaybe<Scalars['String']>;
  image?: InputMaybe<ImageFieldInput>;
  monitors?: InputMaybe<MonitorRelateToManyForUpdateInput>;
  organization?: InputMaybe<OrganizationRelateToOneForUpdateInput>;
  service?: InputMaybe<Scalars['String']>;
  servicedata?: InputMaybe<Scalars['JSON']>;
};

export type PublicTransportWhereInput = {
  AND?: InputMaybe<Array<PublicTransportWhereInput>>;
  NOT?: InputMaybe<Array<PublicTransportWhereInput>>;
  OR?: InputMaybe<Array<PublicTransportWhereInput>>;
  headline?: InputMaybe<StringFilter>;
  id?: InputMaybe<IdFilter>;
  monitors?: InputMaybe<MonitorManyRelationFilter>;
  organization?: InputMaybe<OrganizationWhereInput>;
  service?: InputMaybe<StringFilter>;
};

export type PublicTransportWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']>;
};

export type PublicTransport_Content_Document = {
  document: Scalars['JSON'];
};


export type PublicTransport_Content_DocumentDocumentArgs = {
  hydrateRelationships?: Scalars['Boolean'];
};

export type PublicTransport_Servicedata_Document = {
  document: Scalars['JSON'];
};


export type PublicTransport_Servicedata_DocumentDocumentArgs = {
  hydrateRelationships?: Scalars['Boolean'];
};

export type Query = {
  announcement?: Maybe<Announcement>;
  announcements?: Maybe<Array<Announcement>>;
  announcementsCount?: Maybe<Scalars['Int']>;
  authenticatedItem?: Maybe<AuthenticatedItem>;
  contact?: Maybe<Contact>;
  contacts?: Maybe<Array<Contact>>;
  contactsCount?: Maybe<Scalars['Int']>;
  document?: Maybe<Document>;
  documents?: Maybe<Array<Document>>;
  documentsCount?: Maybe<Scalars['Int']>;
  event?: Maybe<Event>;
  eventProvider?: Maybe<EventProvider>;
  eventProviders?: Maybe<Array<EventProvider>>;
  eventProvidersCount?: Maybe<Scalars['Int']>;
  events?: Maybe<Array<Event>>;
  eventsCount?: Maybe<Scalars['Int']>;
  garbageCollection?: Maybe<GarbageCollection>;
  garbageCollectionAssignment?: Maybe<GarbageCollectionAssignment>;
  garbageCollectionAssignments?: Maybe<Array<GarbageCollectionAssignment>>;
  garbageCollectionAssignmentsCount?: Maybe<Scalars['Int']>;
  garbageCollections?: Maybe<Array<GarbageCollection>>;
  garbageCollectionsCount?: Maybe<Scalars['Int']>;
  keystone: KeystoneMeta;
  monitor?: Maybe<Monitor>;
  monitorGroup?: Maybe<MonitorGroup>;
  monitorGroups?: Maybe<Array<MonitorGroup>>;
  monitorGroupsCount?: Maybe<Scalars['Int']>;
  monitors?: Maybe<Array<Monitor>>;
  monitorsCount?: Maybe<Scalars['Int']>;
  newsProvider?: Maybe<NewsProvider>;
  newsProviders?: Maybe<Array<NewsProvider>>;
  newsProvidersCount?: Maybe<Scalars['Int']>;
  newsReport?: Maybe<NewsReport>;
  newsReports?: Maybe<Array<NewsReport>>;
  newsReportsCount?: Maybe<Scalars['Int']>;
  organization?: Maybe<Organization>;
  organizations?: Maybe<Array<Organization>>;
  organizationsCount?: Maybe<Scalars['Int']>;
  publicTransport?: Maybe<PublicTransport>;
  publicTransports?: Maybe<Array<PublicTransport>>;
  publicTransportsCount?: Maybe<Scalars['Int']>;
  user?: Maybe<User>;
  users?: Maybe<Array<User>>;
  usersCount?: Maybe<Scalars['Int']>;
  warning?: Maybe<Warning>;
  warnings?: Maybe<Array<Warning>>;
  warningsCount?: Maybe<Scalars['Int']>;
  weatherForecast?: Maybe<WeatherForecast>;
  weatherForecasts?: Maybe<Array<WeatherForecast>>;
  weatherForecastsCount?: Maybe<Scalars['Int']>;
  weatherLocation?: Maybe<WeatherLocation>;
  weatherLocations?: Maybe<Array<WeatherLocation>>;
  weatherLocationsCount?: Maybe<Scalars['Int']>;
  weatherReport?: Maybe<WeatherReport>;
  weatherReports?: Maybe<Array<WeatherReport>>;
  weatherReportsCount?: Maybe<Scalars['Int']>;
};


export type QueryAnnouncementArgs = {
  where: AnnouncementWhereUniqueInput;
};


export type QueryAnnouncementsArgs = {
  cursor?: InputMaybe<AnnouncementWhereUniqueInput>;
  orderBy?: Array<AnnouncementOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: AnnouncementWhereInput;
};


export type QueryAnnouncementsCountArgs = {
  where?: AnnouncementWhereInput;
};


export type QueryContactArgs = {
  where: ContactWhereUniqueInput;
};


export type QueryContactsArgs = {
  cursor?: InputMaybe<ContactWhereUniqueInput>;
  orderBy?: Array<ContactOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: ContactWhereInput;
};


export type QueryContactsCountArgs = {
  where?: ContactWhereInput;
};


export type QueryDocumentArgs = {
  where: DocumentWhereUniqueInput;
};


export type QueryDocumentsArgs = {
  cursor?: InputMaybe<DocumentWhereUniqueInput>;
  orderBy?: Array<DocumentOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: DocumentWhereInput;
};


export type QueryDocumentsCountArgs = {
  where?: DocumentWhereInput;
};


export type QueryEventArgs = {
  where: EventWhereUniqueInput;
};


export type QueryEventProviderArgs = {
  where: EventProviderWhereUniqueInput;
};


export type QueryEventProvidersArgs = {
  cursor?: InputMaybe<EventProviderWhereUniqueInput>;
  orderBy?: Array<EventProviderOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: EventProviderWhereInput;
};


export type QueryEventProvidersCountArgs = {
  where?: EventProviderWhereInput;
};


export type QueryEventsArgs = {
  cursor?: InputMaybe<EventWhereUniqueInput>;
  orderBy?: Array<EventOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: EventWhereInput;
};


export type QueryEventsCountArgs = {
  where?: EventWhereInput;
};


export type QueryGarbageCollectionArgs = {
  where: GarbageCollectionWhereUniqueInput;
};


export type QueryGarbageCollectionAssignmentArgs = {
  where: GarbageCollectionAssignmentWhereUniqueInput;
};


export type QueryGarbageCollectionAssignmentsArgs = {
  cursor?: InputMaybe<GarbageCollectionAssignmentWhereUniqueInput>;
  orderBy?: Array<GarbageCollectionAssignmentOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: GarbageCollectionAssignmentWhereInput;
};


export type QueryGarbageCollectionAssignmentsCountArgs = {
  where?: GarbageCollectionAssignmentWhereInput;
};


export type QueryGarbageCollectionsArgs = {
  cursor?: InputMaybe<GarbageCollectionWhereUniqueInput>;
  orderBy?: Array<GarbageCollectionOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: GarbageCollectionWhereInput;
};


export type QueryGarbageCollectionsCountArgs = {
  where?: GarbageCollectionWhereInput;
};


export type QueryMonitorArgs = {
  where: MonitorWhereUniqueInput;
};


export type QueryMonitorGroupArgs = {
  where: MonitorGroupWhereUniqueInput;
};


export type QueryMonitorGroupsArgs = {
  cursor?: InputMaybe<MonitorGroupWhereUniqueInput>;
  orderBy?: Array<MonitorGroupOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: MonitorGroupWhereInput;
};


export type QueryMonitorGroupsCountArgs = {
  where?: MonitorGroupWhereInput;
};


export type QueryMonitorsArgs = {
  cursor?: InputMaybe<MonitorWhereUniqueInput>;
  orderBy?: Array<MonitorOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: MonitorWhereInput;
};


export type QueryMonitorsCountArgs = {
  where?: MonitorWhereInput;
};


export type QueryNewsProviderArgs = {
  where: NewsProviderWhereUniqueInput;
};


export type QueryNewsProvidersArgs = {
  cursor?: InputMaybe<NewsProviderWhereUniqueInput>;
  orderBy?: Array<NewsProviderOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: NewsProviderWhereInput;
};


export type QueryNewsProvidersCountArgs = {
  where?: NewsProviderWhereInput;
};


export type QueryNewsReportArgs = {
  where: NewsReportWhereUniqueInput;
};


export type QueryNewsReportsArgs = {
  cursor?: InputMaybe<NewsReportWhereUniqueInput>;
  orderBy?: Array<NewsReportOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: NewsReportWhereInput;
};


export type QueryNewsReportsCountArgs = {
  where?: NewsReportWhereInput;
};


export type QueryOrganizationArgs = {
  where: OrganizationWhereUniqueInput;
};


export type QueryOrganizationsArgs = {
  cursor?: InputMaybe<OrganizationWhereUniqueInput>;
  orderBy?: Array<OrganizationOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: OrganizationWhereInput;
};


export type QueryOrganizationsCountArgs = {
  where?: OrganizationWhereInput;
};


export type QueryPublicTransportArgs = {
  where: PublicTransportWhereUniqueInput;
};


export type QueryPublicTransportsArgs = {
  cursor?: InputMaybe<PublicTransportWhereUniqueInput>;
  orderBy?: Array<PublicTransportOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: PublicTransportWhereInput;
};


export type QueryPublicTransportsCountArgs = {
  where?: PublicTransportWhereInput;
};


export type QueryUserArgs = {
  where: UserWhereUniqueInput;
};


export type QueryUsersArgs = {
  cursor?: InputMaybe<UserWhereUniqueInput>;
  orderBy?: Array<UserOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: UserWhereInput;
};


export type QueryUsersCountArgs = {
  where?: UserWhereInput;
};


export type QueryWarningArgs = {
  where: WarningWhereUniqueInput;
};


export type QueryWarningsArgs = {
  cursor?: InputMaybe<WarningWhereUniqueInput>;
  orderBy?: Array<WarningOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: WarningWhereInput;
};


export type QueryWarningsCountArgs = {
  where?: WarningWhereInput;
};


export type QueryWeatherForecastArgs = {
  where: WeatherForecastWhereUniqueInput;
};


export type QueryWeatherForecastsArgs = {
  cursor?: InputMaybe<WeatherForecastWhereUniqueInput>;
  orderBy?: Array<WeatherForecastOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: WeatherForecastWhereInput;
};


export type QueryWeatherForecastsCountArgs = {
  where?: WeatherForecastWhereInput;
};


export type QueryWeatherLocationArgs = {
  where: WeatherLocationWhereUniqueInput;
};


export type QueryWeatherLocationsArgs = {
  cursor?: InputMaybe<WeatherLocationWhereUniqueInput>;
  orderBy?: Array<WeatherLocationOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: WeatherLocationWhereInput;
};


export type QueryWeatherLocationsCountArgs = {
  where?: WeatherLocationWhereInput;
};


export type QueryWeatherReportArgs = {
  where: WeatherReportWhereUniqueInput;
};


export type QueryWeatherReportsArgs = {
  cursor?: InputMaybe<WeatherReportWhereUniqueInput>;
  orderBy?: Array<WeatherReportOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: WeatherReportWhereInput;
};


export type QueryWeatherReportsCountArgs = {
  where?: WeatherReportWhereInput;
};

export enum QueryMode {
  Default = 'default',
  Insensitive = 'insensitive'
}

export type StringFilter = {
  contains?: InputMaybe<Scalars['String']>;
  endsWith?: InputMaybe<Scalars['String']>;
  equals?: InputMaybe<Scalars['String']>;
  gt?: InputMaybe<Scalars['String']>;
  gte?: InputMaybe<Scalars['String']>;
  in?: InputMaybe<Array<Scalars['String']>>;
  lt?: InputMaybe<Scalars['String']>;
  lte?: InputMaybe<Scalars['String']>;
  mode?: InputMaybe<QueryMode>;
  not?: InputMaybe<NestedStringFilter>;
  notIn?: InputMaybe<Array<Scalars['String']>>;
  startsWith?: InputMaybe<Scalars['String']>;
};

export type StringNullableFilter = {
  contains?: InputMaybe<Scalars['String']>;
  endsWith?: InputMaybe<Scalars['String']>;
  equals?: InputMaybe<Scalars['String']>;
  gt?: InputMaybe<Scalars['String']>;
  gte?: InputMaybe<Scalars['String']>;
  in?: InputMaybe<Array<Scalars['String']>>;
  lt?: InputMaybe<Scalars['String']>;
  lte?: InputMaybe<Scalars['String']>;
  mode?: InputMaybe<QueryMode>;
  not?: InputMaybe<NestedStringNullableFilter>;
  notIn?: InputMaybe<Array<Scalars['String']>>;
  startsWith?: InputMaybe<Scalars['String']>;
};

export type User = {
  createdAt?: Maybe<Scalars['DateTime']>;
  email?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  isAdmin?: Maybe<Scalars['Boolean']>;
  isMonitor?: Maybe<Scalars['Boolean']>;
  name?: Maybe<Scalars['String']>;
  organizations?: Maybe<Array<Organization>>;
  organizationsCount?: Maybe<Scalars['Int']>;
  password?: Maybe<PasswordState>;
};


export type UserOrganizationsArgs = {
  cursor?: InputMaybe<OrganizationWhereUniqueInput>;
  orderBy?: Array<OrganizationOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: OrganizationWhereInput;
};


export type UserOrganizationsCountArgs = {
  where?: OrganizationWhereInput;
};

export type UserAuthenticationWithPasswordFailure = {
  message: Scalars['String'];
};

export type UserAuthenticationWithPasswordResult = UserAuthenticationWithPasswordFailure | UserAuthenticationWithPasswordSuccess;

export type UserAuthenticationWithPasswordSuccess = {
  item: User;
  sessionToken: Scalars['String'];
};

export type UserCreateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  email?: InputMaybe<Scalars['String']>;
  isAdmin?: InputMaybe<Scalars['Boolean']>;
  isMonitor?: InputMaybe<Scalars['Boolean']>;
  name?: InputMaybe<Scalars['String']>;
  organizations?: InputMaybe<OrganizationRelateToManyForCreateInput>;
  password?: InputMaybe<Scalars['String']>;
};

export type UserOrderByInput = {
  createdAt?: InputMaybe<OrderDirection>;
  email?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  isAdmin?: InputMaybe<OrderDirection>;
  isMonitor?: InputMaybe<OrderDirection>;
  name?: InputMaybe<OrderDirection>;
};

export type UserUpdateArgs = {
  data: UserUpdateInput;
  where: UserWhereUniqueInput;
};

export type UserUpdateInput = {
  createdAt?: InputMaybe<Scalars['DateTime']>;
  email?: InputMaybe<Scalars['String']>;
  isAdmin?: InputMaybe<Scalars['Boolean']>;
  isMonitor?: InputMaybe<Scalars['Boolean']>;
  name?: InputMaybe<Scalars['String']>;
  organizations?: InputMaybe<OrganizationRelateToManyForUpdateInput>;
  password?: InputMaybe<Scalars['String']>;
};

export type UserWhereInput = {
  AND?: InputMaybe<Array<UserWhereInput>>;
  NOT?: InputMaybe<Array<UserWhereInput>>;
  OR?: InputMaybe<Array<UserWhereInput>>;
  createdAt?: InputMaybe<DateTimeNullableFilter>;
  email?: InputMaybe<StringFilter>;
  id?: InputMaybe<IdFilter>;
  isAdmin?: InputMaybe<BooleanFilter>;
  isMonitor?: InputMaybe<BooleanFilter>;
  name?: InputMaybe<StringFilter>;
  organizations?: InputMaybe<OrganizationManyRelationFilter>;
};

export type UserWhereUniqueInput = {
  email?: InputMaybe<Scalars['String']>;
  id?: InputMaybe<Scalars['ID']>;
};

export type Warning = {
  externalId?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  message?: Maybe<Scalars['String']>;
  monitorGroup?: Maybe<Array<MonitorGroup>>;
  monitorGroupCount?: Maybe<Scalars['Int']>;
  organization?: Maybe<Organization>;
  timestamp?: Maybe<Scalars['DateTime']>;
  type?: Maybe<Scalars['String']>;
};


export type WarningMonitorGroupArgs = {
  cursor?: InputMaybe<MonitorGroupWhereUniqueInput>;
  orderBy?: Array<MonitorGroupOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: MonitorGroupWhereInput;
};


export type WarningMonitorGroupCountArgs = {
  where?: MonitorGroupWhereInput;
};

export type WarningCreateInput = {
  externalId?: InputMaybe<Scalars['String']>;
  message?: InputMaybe<Scalars['String']>;
  monitorGroup?: InputMaybe<MonitorGroupRelateToManyForCreateInput>;
  organization?: InputMaybe<OrganizationRelateToOneForCreateInput>;
  timestamp?: InputMaybe<Scalars['DateTime']>;
  type?: InputMaybe<Scalars['String']>;
};

export type WarningManyRelationFilter = {
  every?: InputMaybe<WarningWhereInput>;
  none?: InputMaybe<WarningWhereInput>;
  some?: InputMaybe<WarningWhereInput>;
};

export type WarningOrderByInput = {
  externalId?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  message?: InputMaybe<OrderDirection>;
  timestamp?: InputMaybe<OrderDirection>;
  type?: InputMaybe<OrderDirection>;
};

export type WarningRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<WarningWhereUniqueInput>>;
  create?: InputMaybe<Array<WarningCreateInput>>;
};

export type WarningRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<WarningWhereUniqueInput>>;
  create?: InputMaybe<Array<WarningCreateInput>>;
  disconnect?: InputMaybe<Array<WarningWhereUniqueInput>>;
  set?: InputMaybe<Array<WarningWhereUniqueInput>>;
};

export type WarningUpdateArgs = {
  data: WarningUpdateInput;
  where: WarningWhereUniqueInput;
};

export type WarningUpdateInput = {
  externalId?: InputMaybe<Scalars['String']>;
  message?: InputMaybe<Scalars['String']>;
  monitorGroup?: InputMaybe<MonitorGroupRelateToManyForUpdateInput>;
  organization?: InputMaybe<OrganizationRelateToOneForUpdateInput>;
  timestamp?: InputMaybe<Scalars['DateTime']>;
  type?: InputMaybe<Scalars['String']>;
};

export type WarningWhereInput = {
  AND?: InputMaybe<Array<WarningWhereInput>>;
  NOT?: InputMaybe<Array<WarningWhereInput>>;
  OR?: InputMaybe<Array<WarningWhereInput>>;
  externalId?: InputMaybe<StringFilter>;
  id?: InputMaybe<IdFilter>;
  message?: InputMaybe<StringFilter>;
  monitorGroup?: InputMaybe<MonitorGroupManyRelationFilter>;
  organization?: InputMaybe<OrganizationWhereInput>;
  timestamp?: InputMaybe<DateTimeNullableFilter>;
  type?: InputMaybe<StringNullableFilter>;
};

export type WarningWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']>;
};

export type WeatherForecast = {
  icon?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  organization?: Maybe<Organization>;
  report?: Maybe<WeatherReport>;
  temperature?: Maybe<Scalars['Float']>;
  timestamp?: Maybe<Scalars['DateTime']>;
};

export type WeatherForecastCreateInput = {
  icon?: InputMaybe<Scalars['String']>;
  organization?: InputMaybe<OrganizationRelateToOneForCreateInput>;
  report?: InputMaybe<WeatherReportRelateToOneForCreateInput>;
  temperature?: InputMaybe<Scalars['Float']>;
  timestamp?: InputMaybe<Scalars['DateTime']>;
};

export type WeatherForecastManyRelationFilter = {
  every?: InputMaybe<WeatherForecastWhereInput>;
  none?: InputMaybe<WeatherForecastWhereInput>;
  some?: InputMaybe<WeatherForecastWhereInput>;
};

export type WeatherForecastOrderByInput = {
  icon?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  temperature?: InputMaybe<OrderDirection>;
  timestamp?: InputMaybe<OrderDirection>;
};

export type WeatherForecastRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<WeatherForecastWhereUniqueInput>>;
  create?: InputMaybe<Array<WeatherForecastCreateInput>>;
};

export type WeatherForecastRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<WeatherForecastWhereUniqueInput>>;
  create?: InputMaybe<Array<WeatherForecastCreateInput>>;
  disconnect?: InputMaybe<Array<WeatherForecastWhereUniqueInput>>;
  set?: InputMaybe<Array<WeatherForecastWhereUniqueInput>>;
};

export type WeatherForecastUpdateArgs = {
  data: WeatherForecastUpdateInput;
  where: WeatherForecastWhereUniqueInput;
};

export type WeatherForecastUpdateInput = {
  icon?: InputMaybe<Scalars['String']>;
  organization?: InputMaybe<OrganizationRelateToOneForUpdateInput>;
  report?: InputMaybe<WeatherReportRelateToOneForUpdateInput>;
  temperature?: InputMaybe<Scalars['Float']>;
  timestamp?: InputMaybe<Scalars['DateTime']>;
};

export type WeatherForecastWhereInput = {
  AND?: InputMaybe<Array<WeatherForecastWhereInput>>;
  NOT?: InputMaybe<Array<WeatherForecastWhereInput>>;
  OR?: InputMaybe<Array<WeatherForecastWhereInput>>;
  icon?: InputMaybe<StringFilter>;
  id?: InputMaybe<IdFilter>;
  organization?: InputMaybe<OrganizationWhereInput>;
  report?: InputMaybe<WeatherReportWhereInput>;
  temperature?: InputMaybe<FloatFilter>;
  timestamp?: InputMaybe<DateTimeFilter>;
};

export type WeatherForecastWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']>;
};

export type WeatherLocation = {
  id: Scalars['ID'];
  latitude?: Maybe<Scalars['Float']>;
  longitude?: Maybe<Scalars['Float']>;
  monitorGroups?: Maybe<Array<MonitorGroup>>;
  monitorGroupsCount?: Maybe<Scalars['Int']>;
  name?: Maybe<Scalars['String']>;
  organization?: Maybe<Organization>;
  reports?: Maybe<Array<WeatherReport>>;
  reportsCount?: Maybe<Scalars['Int']>;
};


export type WeatherLocationMonitorGroupsArgs = {
  cursor?: InputMaybe<MonitorGroupWhereUniqueInput>;
  orderBy?: Array<MonitorGroupOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: MonitorGroupWhereInput;
};


export type WeatherLocationMonitorGroupsCountArgs = {
  where?: MonitorGroupWhereInput;
};


export type WeatherLocationReportsArgs = {
  cursor?: InputMaybe<WeatherReportWhereUniqueInput>;
  orderBy?: Array<WeatherReportOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: WeatherReportWhereInput;
};


export type WeatherLocationReportsCountArgs = {
  where?: WeatherReportWhereInput;
};

export type WeatherLocationCreateInput = {
  latitude?: InputMaybe<Scalars['Float']>;
  longitude?: InputMaybe<Scalars['Float']>;
  monitorGroups?: InputMaybe<MonitorGroupRelateToManyForCreateInput>;
  name?: InputMaybe<Scalars['String']>;
  organization?: InputMaybe<OrganizationRelateToOneForCreateInput>;
  reports?: InputMaybe<WeatherReportRelateToManyForCreateInput>;
};

export type WeatherLocationOrderByInput = {
  id?: InputMaybe<OrderDirection>;
  latitude?: InputMaybe<OrderDirection>;
  longitude?: InputMaybe<OrderDirection>;
  name?: InputMaybe<OrderDirection>;
};

export type WeatherLocationRelateToOneForCreateInput = {
  connect?: InputMaybe<WeatherLocationWhereUniqueInput>;
  create?: InputMaybe<WeatherLocationCreateInput>;
};

export type WeatherLocationRelateToOneForUpdateInput = {
  connect?: InputMaybe<WeatherLocationWhereUniqueInput>;
  create?: InputMaybe<WeatherLocationCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']>;
};

export type WeatherLocationUpdateArgs = {
  data: WeatherLocationUpdateInput;
  where: WeatherLocationWhereUniqueInput;
};

export type WeatherLocationUpdateInput = {
  latitude?: InputMaybe<Scalars['Float']>;
  longitude?: InputMaybe<Scalars['Float']>;
  monitorGroups?: InputMaybe<MonitorGroupRelateToManyForUpdateInput>;
  name?: InputMaybe<Scalars['String']>;
  organization?: InputMaybe<OrganizationRelateToOneForUpdateInput>;
  reports?: InputMaybe<WeatherReportRelateToManyForUpdateInput>;
};

export type WeatherLocationWhereInput = {
  AND?: InputMaybe<Array<WeatherLocationWhereInput>>;
  NOT?: InputMaybe<Array<WeatherLocationWhereInput>>;
  OR?: InputMaybe<Array<WeatherLocationWhereInput>>;
  id?: InputMaybe<IdFilter>;
  latitude?: InputMaybe<FloatFilter>;
  longitude?: InputMaybe<FloatFilter>;
  monitorGroups?: InputMaybe<MonitorGroupManyRelationFilter>;
  name?: InputMaybe<StringFilter>;
  organization?: InputMaybe<OrganizationWhereInput>;
  reports?: InputMaybe<WeatherReportManyRelationFilter>;
};

export type WeatherLocationWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']>;
};

export type WeatherReport = {
  forecasts?: Maybe<Array<WeatherForecast>>;
  forecastsCount?: Maybe<Scalars['Int']>;
  icon?: Maybe<Scalars['String']>;
  id: Scalars['ID'];
  location?: Maybe<WeatherLocation>;
  organization?: Maybe<Organization>;
  temperature?: Maybe<Scalars['Float']>;
  timestamp?: Maybe<Scalars['DateTime']>;
};


export type WeatherReportForecastsArgs = {
  cursor?: InputMaybe<WeatherForecastWhereUniqueInput>;
  orderBy?: Array<WeatherForecastOrderByInput>;
  skip?: Scalars['Int'];
  take?: InputMaybe<Scalars['Int']>;
  where?: WeatherForecastWhereInput;
};


export type WeatherReportForecastsCountArgs = {
  where?: WeatherForecastWhereInput;
};

export type WeatherReportCreateInput = {
  forecasts?: InputMaybe<WeatherForecastRelateToManyForCreateInput>;
  icon?: InputMaybe<Scalars['String']>;
  location?: InputMaybe<WeatherLocationRelateToOneForCreateInput>;
  organization?: InputMaybe<OrganizationRelateToOneForCreateInput>;
  temperature?: InputMaybe<Scalars['Float']>;
  timestamp?: InputMaybe<Scalars['DateTime']>;
};

export type WeatherReportManyRelationFilter = {
  every?: InputMaybe<WeatherReportWhereInput>;
  none?: InputMaybe<WeatherReportWhereInput>;
  some?: InputMaybe<WeatherReportWhereInput>;
};

export type WeatherReportOrderByInput = {
  icon?: InputMaybe<OrderDirection>;
  id?: InputMaybe<OrderDirection>;
  temperature?: InputMaybe<OrderDirection>;
  timestamp?: InputMaybe<OrderDirection>;
};

export type WeatherReportRelateToManyForCreateInput = {
  connect?: InputMaybe<Array<WeatherReportWhereUniqueInput>>;
  create?: InputMaybe<Array<WeatherReportCreateInput>>;
};

export type WeatherReportRelateToManyForUpdateInput = {
  connect?: InputMaybe<Array<WeatherReportWhereUniqueInput>>;
  create?: InputMaybe<Array<WeatherReportCreateInput>>;
  disconnect?: InputMaybe<Array<WeatherReportWhereUniqueInput>>;
  set?: InputMaybe<Array<WeatherReportWhereUniqueInput>>;
};

export type WeatherReportRelateToOneForCreateInput = {
  connect?: InputMaybe<WeatherReportWhereUniqueInput>;
  create?: InputMaybe<WeatherReportCreateInput>;
};

export type WeatherReportRelateToOneForUpdateInput = {
  connect?: InputMaybe<WeatherReportWhereUniqueInput>;
  create?: InputMaybe<WeatherReportCreateInput>;
  disconnect?: InputMaybe<Scalars['Boolean']>;
};

export type WeatherReportUpdateArgs = {
  data: WeatherReportUpdateInput;
  where: WeatherReportWhereUniqueInput;
};

export type WeatherReportUpdateInput = {
  forecasts?: InputMaybe<WeatherForecastRelateToManyForUpdateInput>;
  icon?: InputMaybe<Scalars['String']>;
  location?: InputMaybe<WeatherLocationRelateToOneForUpdateInput>;
  organization?: InputMaybe<OrganizationRelateToOneForUpdateInput>;
  temperature?: InputMaybe<Scalars['Float']>;
  timestamp?: InputMaybe<Scalars['DateTime']>;
};

export type WeatherReportWhereInput = {
  AND?: InputMaybe<Array<WeatherReportWhereInput>>;
  NOT?: InputMaybe<Array<WeatherReportWhereInput>>;
  OR?: InputMaybe<Array<WeatherReportWhereInput>>;
  forecasts?: InputMaybe<WeatherForecastManyRelationFilter>;
  icon?: InputMaybe<StringFilter>;
  id?: InputMaybe<IdFilter>;
  location?: InputMaybe<WeatherLocationWhereInput>;
  organization?: InputMaybe<OrganizationWhereInput>;
  temperature?: InputMaybe<FloatFilter>;
  timestamp?: InputMaybe<DateTimeFilter>;
};

export type WeatherReportWhereUniqueInput = {
  id?: InputMaybe<Scalars['ID']>;
};

export type LastWeatherReportQueryVariables = Exact<{
  identity: Scalars['ID'];
}>;


export type LastWeatherReportQuery = { monitor?: { group?: { weatherLocation?: { reports?: Array<{ id: string, temperature?: number | null, icon?: string | null, timestamp?: any | null, forecasts?: Array<{ id: string, timestamp?: any | null, temperature?: number | null, icon?: string | null }> | null }> | null } | null } | null } | null };

export type OurHouseQueryVariables = Exact<{
  identity: Scalars['ID'];
  timestamp: Scalars['DateTime'];
}>;


export type OurHouseQuery = { monitor?: { group?: { contact?: { content?: { document: any } | null } | null, documents?: Array<{ name?: string | null, content?: { filename: string, filesize: number, url: string } | null }> | null, announcements?: Array<{ id: string, title?: string | null, title_english?: string | null, title_arabic?: string | null, timestamp?: any | null, content?: { document: any } | null, content_english?: { document: any } | null, content_arabic?: { document: any } | null }> | null } | null, garbageCollectionAssignment?: { id: string, name?: string | null } | null, biomeull?: Array<{ timestamp?: any | null, type?: string | null }> | null, restmuell?: Array<{ timestamp?: any | null, type?: string | null }> | null, gelber_sack?: Array<{ timestamp?: any | null, type?: string | null }> | null, papier?: Array<{ timestamp?: any | null, type?: string | null }> | null } | null };

export type MonitorQueryVariables = Exact<{
  identity: Scalars['ID'];
}>;


export type MonitorQuery = { monitor?: { id: string, name?: string | null, operatorLogo?: { url: string } | null, boardLogo?: { url: string } | null, organization?: { id: string, name?: string | null } | null } | null };

export type NewsReportsQueryVariables = Exact<{
  identity: Scalars['ID'];
}>;


export type NewsReportsQuery = { monitor?: { group?: { newsReportsCount?: number | null, defaultNewsImage?: { url: string } | null, newsProviders?: Array<{ newsReportsCount?: number | null, newsReports?: Array<{ id: string, timestamp?: any | null, title?: string | null, previewContent?: string | null, category?: string | null, link?: string | null, content?: { document: any } | null, image?: { url: string } | null, newsProvider?: { name?: string | null, image?: { url: string } | null } | null }> | null }> | null, newsReports?: Array<{ id: string, timestamp?: any | null, title?: string | null, previewContent?: string | null, category?: string | null, link?: string | null, content?: { document: any } | null, image?: { url: string } | null, newsProvider?: { name?: string | null, image?: { url: string } | null } | null }> | null } | null } | null };

export type EventsQueryVariables = Exact<{
  identity: Scalars['ID'];
  dateFrom: Scalars['CalendarDay'];
  dateTo: Scalars['CalendarDay'];
}>;


export type EventsQuery = { monitor?: { group?: { defaultEventImage?: { url: string } | null, eventProviders?: Array<{ events?: Array<{ id: string, title?: string | null, category?: string | null, date?: any | null, time?: string | null, location?: string | null, link?: string | null, image?: { id: string, url: string } | null, eventProvider?: { name?: string | null, image?: { url: string } | null } | null }> | null }> | null, events?: Array<{ id: string, title?: string | null, category?: string | null, date?: any | null, time?: string | null, location?: string | null, link?: string | null, image?: { id: string, url: string } | null, eventProvider?: { name?: string | null, image?: { url: string } | null } | null }> | null } | null } | null };

export type PublicTransportQueryVariables = Exact<{
  identity: Scalars['ID'];
}>;


export type PublicTransportQuery = { monitor?: { publicTransportDepartureLink?: string | null, publicTransportSettings?: { headline?: string | null, service?: string | null, content?: { document: any } | null, servicedata?: { document: any } | null, image?: { url: string } | null } | null } | null };

export type WarningsQueryVariables = Exact<{
  identity: Scalars['ID'];
}>;


export type WarningsQuery = { monitor?: { id: string, name?: string | null, group?: { warnings?: Array<{ id: string, message?: string | null, timestamp?: any | null, externalId?: string | null, type?: string | null }> | null } | null } | null };

export type MonitorsQueryVariables = Exact<{ [key: string]: never; }>;


export type MonitorsQuery = { monitors?: Array<{ id: string, name?: string | null, boardLogo?: { url: string } | null, operatorLogo?: { url: string } | null, organization?: { id: string, name?: string | null } | null }> | null };

export type AuthenticateUserWithPasswordMutationVariables = Exact<{
  email: Scalars['String'];
  password: Scalars['String'];
}>;


export type AuthenticateUserWithPasswordMutation = { authenticateUserWithPassword?: { message: string } | { sessionToken: string } | null };

export type LogoutMutationVariables = Exact<{ [key: string]: never; }>;


export type LogoutMutation = { endSession: boolean };

export const LastWeatherReportDocument = gql`
    query lastWeatherReport($identity: ID!) {
  monitor(where: {id: $identity}) {
    group {
      weatherLocation {
        reports(orderBy: {timestamp: desc}, take: 1) {
          id
          temperature
          icon
          timestamp
          forecasts(orderBy: {timestamp: asc}) {
            id
            timestamp
            temperature
            icon
          }
        }
      }
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class LastWeatherReportGQL extends Apollo.Query<LastWeatherReportQuery, LastWeatherReportQueryVariables> {
    override document = LastWeatherReportDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const OurHouseDocument = gql`
    query ourHouse($identity: ID!, $timestamp: DateTime!) {
  monitor(where: {id: $identity}) {
    group {
      contact {
        content {
          document
        }
      }
      documents {
        name
        content {
          filename
          filesize
          url
        }
      }
      announcements(
        where: {timestamp: {lte: $timestamp}, OR: [{visibleUntil: {equals: null}}, {visibleUntil: {gte: $timestamp}}]}
        orderBy: {timestamp: desc}
        take: 10
      ) {
        id
        title
        content {
          document
        }
        title_english
        content_english {
          document
        }
        title_arabic
        content_arabic {
          document
        }
        timestamp
      }
    }
    garbageCollectionAssignment {
      id
      name
    }
    biomeull: garbageCollections(
      where: {type: {equals: "biomuell"}, timestamp: {gte: $timestamp}}
      orderBy: {timestamp: asc}
      take: 1
    ) {
      timestamp
      type
    }
    restmuell: garbageCollections(
      where: {type: {equals: "restmuell"}, timestamp: {gte: $timestamp}}
      orderBy: {timestamp: asc}
      take: 1
    ) {
      timestamp
      type
    }
    gelber_sack: garbageCollections(
      where: {type: {equals: "gelber_sack"}, timestamp: {gte: $timestamp}}
      orderBy: {timestamp: asc}
      take: 1
    ) {
      timestamp
      type
    }
    papier: garbageCollections(
      where: {type: {equals: "papier"}, timestamp: {gte: $timestamp}}
      orderBy: {timestamp: asc}
      take: 1
    ) {
      timestamp
      type
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class OurHouseGQL extends Apollo.Query<OurHouseQuery, OurHouseQueryVariables> {
    override document = OurHouseDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const MonitorDocument = gql`
    query monitor($identity: ID!) {
  monitor(where: {id: $identity}) {
    id
    name
    operatorLogo {
      url
    }
    boardLogo {
      url
    }
    organization {
      id
      name
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class MonitorGQL extends Apollo.Query<MonitorQuery, MonitorQueryVariables> {
    override document = MonitorDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const NewsReportsDocument = gql`
    query newsReports($identity: ID!) {
  monitor(where: {id: $identity}) {
    group {
      defaultNewsImage {
        url
      }
      newsProviders {
        newsReportsCount
        newsReports(orderBy: {timestamp: desc}) {
          id
          timestamp
          title
          content {
            document
          }
          previewContent
          category
          image {
            url
          }
          link
          newsProvider {
            name
            image {
              url
            }
          }
        }
      }
      newsReportsCount
      newsReports(orderBy: {timestamp: desc}) {
        id
        timestamp
        title
        content {
          document
        }
        previewContent
        category
        image {
          url
        }
        link
        newsProvider {
          name
          image {
            url
          }
        }
      }
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class NewsReportsGQL extends Apollo.Query<NewsReportsQuery, NewsReportsQueryVariables> {
    override document = NewsReportsDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const EventsDocument = gql`
    query events($identity: ID!, $dateFrom: CalendarDay!, $dateTo: CalendarDay!) {
  monitor(where: {id: $identity}) {
    group {
      defaultEventImage {
        url
      }
      eventProviders {
        events(where: {date: {gte: $dateFrom, lte: $dateTo}}, orderBy: {date: asc}) {
          id
          title
          category
          date
          time
          location
          link
          image {
            id
            url
          }
          eventProvider {
            name
            image {
              url
            }
          }
        }
      }
      events(where: {date: {gte: $dateFrom, lte: $dateTo}}, orderBy: {date: asc}) {
        id
        title
        category
        date
        time
        location
        link
        image {
          id
          url
        }
        eventProvider {
          name
          image {
            url
          }
        }
      }
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class EventsGQL extends Apollo.Query<EventsQuery, EventsQueryVariables> {
    override document = EventsDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const PublicTransportDocument = gql`
    query publicTransport($identity: ID!) {
  monitor(where: {id: $identity}) {
    publicTransportDepartureLink
    publicTransportSettings {
      headline
      service
      content {
        document
      }
      servicedata {
        document
      }
      image {
        url
      }
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class PublicTransportGQL extends Apollo.Query<PublicTransportQuery, PublicTransportQueryVariables> {
    override document = PublicTransportDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const WarningsDocument = gql`
    query warnings($identity: ID!) {
  monitor(where: {id: $identity}) {
    id
    name
    group {
      warnings(orderBy: {timestamp: asc}) {
        id
        message
        timestamp
        externalId
        type
      }
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class WarningsGQL extends Apollo.Query<WarningsQuery, WarningsQueryVariables> {
    override document = WarningsDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const MonitorsDocument = gql`
    query monitors {
  monitors(orderBy: {name: asc}) {
    id
    name
    boardLogo {
      url
    }
    operatorLogo {
      url
    }
    organization {
      id
      name
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class MonitorsGQL extends Apollo.Query<MonitorsQuery, MonitorsQueryVariables> {
    override document = MonitorsDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const AuthenticateUserWithPasswordDocument = gql`
    mutation authenticateUserWithPassword($email: String!, $password: String!) {
  authenticateUserWithPassword(email: $email, password: $password) {
    ... on UserAuthenticationWithPasswordSuccess {
      sessionToken
    }
    ... on UserAuthenticationWithPasswordFailure {
      message
    }
  }
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class AuthenticateUserWithPasswordGQL extends Apollo.Mutation<AuthenticateUserWithPasswordMutation, AuthenticateUserWithPasswordMutationVariables> {
    override document = AuthenticateUserWithPasswordDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }
export const LogoutDocument = gql`
    mutation logout {
  endSession
}
    `;

  @Injectable({
    providedIn: 'root'
  })
  export class LogoutGQL extends Apollo.Mutation<LogoutMutation, LogoutMutationVariables> {
    override document = LogoutDocument;
    
    constructor(apollo: Apollo.Apollo) {
      super(apollo);
    }
  }