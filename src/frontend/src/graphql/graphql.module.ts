import { NgModule } from '@angular/core';
import { APOLLO_OPTIONS } from 'apollo-angular';
import { ApolloClientOptions, InMemoryCache } from '@apollo/client/core';
import { HttpLink } from 'apollo-angular/http';
import { DefaultOptions } from '@apollo/client/core/ApolloClient';
import { Store } from '@ngrx/store';
import { environment } from 'src/environments/environment';

const defaultOptions: DefaultOptions = {
  watchQuery: {
    fetchPolicy: 'no-cache',
    errorPolicy: 'none',
  },
  query: {
    fetchPolicy: 'no-cache',
    errorPolicy: 'none',
  },
  mutate: {
    fetchPolicy: 'no-cache',
    errorPolicy: 'none',
  },
};

const uri = environment.apiUrl;

export const createApollo = (
  httpLink: HttpLink,
  store: Store
): ApolloClientOptions<any> => {
  const link = httpLink.create({ uri, withCredentials: true });

  return {
    link,
    cache: new InMemoryCache({ resultCaching: false, addTypename: false }),
    defaultOptions,
  };
};

@NgModule({
  providers: [
    {
      provide: APOLLO_OPTIONS,
      useFactory: createApollo,
      deps: [HttpLink, Store],
    },
  ],
})
export class GraphQLModule {}
