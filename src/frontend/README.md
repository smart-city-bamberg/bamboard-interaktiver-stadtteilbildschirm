# BamBoard Frontend

Dies ist das Frontend-Projekt für einen interaktiven Stadtteilbildschirm auf Basis von Angular 15. Es lädt Daten über GraphQL aus dem CMS-Projekt.
![BamBoard Frontend](docs/Bildschirm.png)

---

## Lokale Entwicklung

Bevor du das Projekt lokal zu Entwicklungszwecken starten kannst, stelle sicher, dass du folgende Software installiert hast:

- Node.js: [https://nodejs.org](https://nodejs.org)
- Yarn: [https://yarnpkg.com](https://yarnpkg.com)

Folge diesen Schritten, um das Projekt lokal zu starten:

1. Führe den Befehl `yarn install` aus, um die Abhängigkeiten zu installieren.
2. Führe den Befehl `yarn run start` aus.
3. Das Frontend ist nun unter http://localhost:4200 erreichbar. Öffne einen Webbrowser und besuche diese Adresse, um das Projekt anzuzeigen.
---
## Aktualisierung des GraphQL-Schemas

Wenn sich das Schema im CMS-Projekt ändert, musst du das GraphQL-Schema im Frontend-Projekt aktualisieren. Folge diesen Schritten:

1. Führe den Befehl `yarn run generate --config codegen.online.ts` aus.
2. Das GraphQL-Schema wird aktualisiert, basierend auf den Änderungen im CMS-Projekt.

---

## Bereitstellung mit Docker

Folge diesen Schritten, um das Projekt mit Docker Compose und einer Override-Datei zu starten:

⚠️ Für den produktiven Einsatz muss die URL für das CMS in der Datei `environment.ts` angepasst werden.

1. Stelle sicher, dass Docker und Docker Compose auf deinem System installiert sind.
2. Führe den Befehl `docker-compose -f docker-compose.yml -f docker-compose.override.yml up -d` aus, um die Container zu starten.
3. Das Frontend ist nun unter http://localhost:4200 erreichbar. Öffne einen Webbrowser und besuche diese Adresse, um das Projekt anzuzeigen.

---

## Einrichten des Bildschirms

Um Inhalte für einen Bildschirm anzeigen zu können, muss dieser zunächst eingerichtet werden.

![Alt text](docs/bildschirmauswahl.jpg)

1. Öffne den Browser unter http://localhost:4200/bildschirmauswahl
2. Melde dich mit einem Benutzer aus dem CMS an
3. Klicke auf einen der Angezeigten Bildschirme. Die Zugangsdaten sowie die Auswahl wird im LocalStorage des Browsers gespeichert und wird beim nächsten Öffnen automatisch geladen.

![Alt text](docs/demo-bildschirm.jpg)
