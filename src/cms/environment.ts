import {
  BaseKeystoneTypeInfo,
  DatabaseConfig,
  ServerConfig,
  StorageConfig,
} from "@keystone-6/core/types";

const host = process.env.HOST_PUBLIC_NAME ?? "http://localhost:3000";
const hostPath = process.env.HOST_PATH ?? "";
const corsOrigin = process.env.CORS_ORIGIN ?? "http://localhost:4200";
const storagePath = process.env.STORAGE_PATH ?? "public";

const databaseUrl =
  process.env.DATABASE_URL ??
  "postgres://keystone:password@localhost:5432/keystone";

export const environment = {
  db: <DatabaseConfig<BaseKeystoneTypeInfo>>{
    provider: "postgresql",
    url: databaseUrl,
    useMigrations: true,
  },
  server: <ServerConfig<BaseKeystoneTypeInfo>>{
    options: {
      path: hostPath,
    },
    cors: {
      origin: [corsOrigin],
      credentials: true,
    },
  },
  storage: <Record<string, StorageConfig>>{
    document_store: {
      type: "file",
      kind: "local",
      storagePath: `${storagePath}/documents`,
      generateUrl: (path: string) => `${host}/files${path}`,
      serverRoute: {
        path: "/files",
      },
    },
    image_store: {
      type: "image",
      kind: "local",
      storagePath: `${storagePath}/images`,
      generateUrl: (path: string) => `${host}/images${path}`,
      serverRoute: {
        path: "/images",
      },
    },
    assets: {
      type: "file",
      kind: "local",
      storagePath: `./assets`,
      generateUrl: (path: string) => `${host}/assets${path}`,
      serverRoute: {
        path: "/assets",
      },
    },
  },
};
