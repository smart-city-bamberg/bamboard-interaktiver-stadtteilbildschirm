-- AlterTable
ALTER TABLE "Monitor" ADD COLUMN     "operatorLogo_extension" TEXT,
ADD COLUMN     "operatorLogo_filesize" INTEGER,
ADD COLUMN     "operatorLogo_height" INTEGER,
ADD COLUMN     "operatorLogo_id" TEXT,
ADD COLUMN     "operatorLogo_width" INTEGER;
