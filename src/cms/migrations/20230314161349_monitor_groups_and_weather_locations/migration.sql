/*
  Warnings:

  - The primary key for the `Contact` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - A unique constraint covering the columns `[monitorGroup]` on the table `Contact` will be added. If there are existing duplicate values, this will fail.

*/
-- AlterTable
ALTER TABLE "Announcement" ADD COLUMN     "monitorGroup" TEXT;

-- AlterTable
ALTER TABLE "Contact" DROP CONSTRAINT "Contact_pkey",
ADD COLUMN     "monitorGroup" TEXT,
ALTER COLUMN "id" SET DATA TYPE TEXT,
ADD CONSTRAINT "Contact_pkey" PRIMARY KEY ("id");

-- AlterTable
ALTER TABLE "Document" ADD COLUMN     "monitorGroup" TEXT;

-- AlterTable
ALTER TABLE "Monitor" ADD COLUMN     "group" TEXT;

-- AlterTable
ALTER TABLE "WeatherReport" ADD COLUMN     "location" TEXT;

-- CreateTable
CREATE TABLE "WeatherLocation" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL DEFAULT '',
    "longitude" DOUBLE PRECISION,
    "latitude" DOUBLE PRECISION,

    CONSTRAINT "WeatherLocation_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "MonitorGroup" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL DEFAULT '',
    "weatherLocation" TEXT,

    CONSTRAINT "MonitorGroup_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "MonitorGroup_weatherLocation_key" ON "MonitorGroup"("weatherLocation");

-- CreateIndex
CREATE INDEX "Announcement_monitorGroup_idx" ON "Announcement"("monitorGroup");

-- CreateIndex
CREATE UNIQUE INDEX "Contact_monitorGroup_key" ON "Contact"("monitorGroup");

-- CreateIndex
CREATE INDEX "Document_monitorGroup_idx" ON "Document"("monitorGroup");

-- CreateIndex
CREATE INDEX "Monitor_group_idx" ON "Monitor"("group");

-- CreateIndex
CREATE INDEX "WeatherReport_location_idx" ON "WeatherReport"("location");

-- AddForeignKey
ALTER TABLE "WeatherReport" ADD CONSTRAINT "WeatherReport_location_fkey" FOREIGN KEY ("location") REFERENCES "WeatherLocation"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Monitor" ADD CONSTRAINT "Monitor_group_fkey" FOREIGN KEY ("group") REFERENCES "MonitorGroup"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "MonitorGroup" ADD CONSTRAINT "MonitorGroup_weatherLocation_fkey" FOREIGN KEY ("weatherLocation") REFERENCES "WeatherLocation"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Contact" ADD CONSTRAINT "Contact_monitorGroup_fkey" FOREIGN KEY ("monitorGroup") REFERENCES "MonitorGroup"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Document" ADD CONSTRAINT "Document_monitorGroup_fkey" FOREIGN KEY ("monitorGroup") REFERENCES "MonitorGroup"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Announcement" ADD CONSTRAINT "Announcement_monitorGroup_fkey" FOREIGN KEY ("monitorGroup") REFERENCES "MonitorGroup"("id") ON DELETE SET NULL ON UPDATE CASCADE;
