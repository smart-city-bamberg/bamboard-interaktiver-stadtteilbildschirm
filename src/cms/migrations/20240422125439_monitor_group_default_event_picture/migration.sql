-- AlterTable
ALTER TABLE "MonitorGroup" ADD COLUMN     "defaultEventImage_extension" TEXT,
ADD COLUMN     "defaultEventImage_filesize" INTEGER,
ADD COLUMN     "defaultEventImage_height" INTEGER,
ADD COLUMN     "defaultEventImage_id" TEXT,
ADD COLUMN     "defaultEventImage_width" INTEGER;
