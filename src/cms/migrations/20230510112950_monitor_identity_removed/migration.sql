/*
  Warnings:

  - You are about to drop the column `identity` on the `Monitor` table. All the data in the column will be lost.

*/
-- DropIndex
DROP INDEX "Monitor_identity_key";

-- AlterTable
ALTER TABLE "Monitor" DROP COLUMN "identity";
