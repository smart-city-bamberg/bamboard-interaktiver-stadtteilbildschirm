-- AlterTable
ALTER TABLE "MonitorGroup" ADD COLUMN     "defaultNewsImage_extension" TEXT,
ADD COLUMN     "defaultNewsImage_filesize" INTEGER,
ADD COLUMN     "defaultNewsImage_height" INTEGER,
ADD COLUMN     "defaultNewsImage_id" TEXT,
ADD COLUMN     "defaultNewsImage_width" INTEGER;
