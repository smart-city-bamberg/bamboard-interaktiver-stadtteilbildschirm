-- AlterTable
ALTER TABLE "GarbageCollection" ADD COLUMN     "organization" TEXT;

-- AlterTable
ALTER TABLE "GarbageCollectionAssignment" ADD COLUMN     "organization" TEXT;

-- CreateIndex
CREATE INDEX "GarbageCollection_organization_idx" ON "GarbageCollection"("organization");

-- CreateIndex
CREATE INDEX "GarbageCollectionAssignment_organization_idx" ON "GarbageCollectionAssignment"("organization");

-- AddForeignKey
ALTER TABLE "GarbageCollection" ADD CONSTRAINT "GarbageCollection_organization_fkey" FOREIGN KEY ("organization") REFERENCES "Organization"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "GarbageCollectionAssignment" ADD CONSTRAINT "GarbageCollectionAssignment_organization_fkey" FOREIGN KEY ("organization") REFERENCES "Organization"("id") ON DELETE SET NULL ON UPDATE CASCADE;
