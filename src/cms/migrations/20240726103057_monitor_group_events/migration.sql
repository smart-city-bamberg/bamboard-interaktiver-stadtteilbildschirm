-- CreateTable
CREATE TABLE "_Event_monitorGroups" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "_MonitorGroup_eventProvider" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "_Event_monitorGroups_AB_unique" ON "_Event_monitorGroups"("A", "B");

-- CreateIndex
CREATE INDEX "_Event_monitorGroups_B_index" ON "_Event_monitorGroups"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_MonitorGroup_eventProvider_AB_unique" ON "_MonitorGroup_eventProvider"("A", "B");

-- CreateIndex
CREATE INDEX "_MonitorGroup_eventProvider_B_index" ON "_MonitorGroup_eventProvider"("B");

-- AddForeignKey
ALTER TABLE "_Event_monitorGroups" ADD CONSTRAINT "_Event_monitorGroups_A_fkey" FOREIGN KEY ("A") REFERENCES "Event"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_Event_monitorGroups" ADD CONSTRAINT "_Event_monitorGroups_B_fkey" FOREIGN KEY ("B") REFERENCES "MonitorGroup"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_MonitorGroup_eventProvider" ADD CONSTRAINT "_MonitorGroup_eventProvider_A_fkey" FOREIGN KEY ("A") REFERENCES "EventProvider"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_MonitorGroup_eventProvider" ADD CONSTRAINT "_MonitorGroup_eventProvider_B_fkey" FOREIGN KEY ("B") REFERENCES "MonitorGroup"("id") ON DELETE CASCADE ON UPDATE CASCADE;
