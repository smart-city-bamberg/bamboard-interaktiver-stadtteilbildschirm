/*
  Warnings:

  - Made the column `timestamp` on table `WeatherForecast` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE "WeatherForecast" ALTER COLUMN "timestamp" SET NOT NULL;
