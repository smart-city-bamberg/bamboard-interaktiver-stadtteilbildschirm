-- CreateTable
CREATE TABLE "WeatherReport" (
    "id" TEXT NOT NULL,
    "temperature" DOUBLE PRECISION NOT NULL,
    "icon" TEXT NOT NULL DEFAULT '',
    "timestamp" TIMESTAMP(3) DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "WeatherReport_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "WeatherForecast" (
    "id" TEXT NOT NULL,
    "report" TEXT,
    "timestamp" TIMESTAMP(3),
    "temperature" DOUBLE PRECISION NOT NULL,
    "icon" TEXT NOT NULL DEFAULT '',

    CONSTRAINT "WeatherForecast_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE INDEX "WeatherReport_timestamp_idx" ON "WeatherReport"("timestamp");

-- CreateIndex
CREATE INDEX "WeatherForecast_report_idx" ON "WeatherForecast"("report");

-- CreateIndex
CREATE INDEX "WeatherForecast_timestamp_idx" ON "WeatherForecast"("timestamp");

-- AddForeignKey
ALTER TABLE "WeatherForecast" ADD CONSTRAINT "WeatherForecast_report_fkey" FOREIGN KEY ("report") REFERENCES "WeatherReport"("id") ON DELETE SET NULL ON UPDATE CASCADE;
