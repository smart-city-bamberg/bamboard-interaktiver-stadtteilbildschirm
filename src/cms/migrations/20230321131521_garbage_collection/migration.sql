-- CreateTable
CREATE TABLE "GarbageCollection" (
    "id" TEXT NOT NULL,
    "timestamp" TIMESTAMP(3),
    "type" TEXT,

    CONSTRAINT "GarbageCollection_pkey" PRIMARY KEY ("id")
);
