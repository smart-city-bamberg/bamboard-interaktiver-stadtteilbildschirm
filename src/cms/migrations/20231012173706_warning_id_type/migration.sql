/*
  Warnings:

  - A unique constraint covering the columns `[externalId]` on the table `Warning` will be added. If there are existing duplicate values, this will fail.

*/
-- AlterTable
ALTER TABLE "Warning" ADD COLUMN     "externalId" TEXT NOT NULL DEFAULT '',
ADD COLUMN     "type" TEXT;

-- CreateIndex
CREATE UNIQUE INDEX "Warning_externalId_key" ON "Warning"("externalId");
