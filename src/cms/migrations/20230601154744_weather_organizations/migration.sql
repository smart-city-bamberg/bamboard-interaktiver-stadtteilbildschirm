-- AlterTable
ALTER TABLE "WeatherForecast" ADD COLUMN     "organization" TEXT;

-- AlterTable
ALTER TABLE "WeatherLocation" ADD COLUMN     "organization" TEXT;

-- AlterTable
ALTER TABLE "WeatherReport" ADD COLUMN     "organization" TEXT;

-- CreateIndex
CREATE INDEX "WeatherForecast_organization_idx" ON "WeatherForecast"("organization");

-- CreateIndex
CREATE INDEX "WeatherLocation_organization_idx" ON "WeatherLocation"("organization");

-- CreateIndex
CREATE INDEX "WeatherReport_organization_idx" ON "WeatherReport"("organization");

-- AddForeignKey
ALTER TABLE "WeatherReport" ADD CONSTRAINT "WeatherReport_organization_fkey" FOREIGN KEY ("organization") REFERENCES "Organization"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "WeatherForecast" ADD CONSTRAINT "WeatherForecast_organization_fkey" FOREIGN KEY ("organization") REFERENCES "Organization"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "WeatherLocation" ADD CONSTRAINT "WeatherLocation_organization_fkey" FOREIGN KEY ("organization") REFERENCES "Organization"("id") ON DELETE SET NULL ON UPDATE CASCADE;
