/*
  Warnings:

  - You are about to drop the column `feedTransformator` on the `NewsProvider` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "NewsProvider" DROP COLUMN "feedTransformator",
ADD COLUMN     "configuration" TEXT NOT NULL DEFAULT '';
