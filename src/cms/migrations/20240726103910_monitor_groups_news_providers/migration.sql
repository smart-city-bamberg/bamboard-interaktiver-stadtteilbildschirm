/*
  Warnings:

  - You are about to drop the `_MonitorGroup_eventProvider` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `_MonitorGroup_newsProvider` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "_MonitorGroup_eventProvider" DROP CONSTRAINT "_MonitorGroup_eventProvider_A_fkey";

-- DropForeignKey
ALTER TABLE "_MonitorGroup_eventProvider" DROP CONSTRAINT "_MonitorGroup_eventProvider_B_fkey";

-- DropForeignKey
ALTER TABLE "_MonitorGroup_newsProvider" DROP CONSTRAINT "_MonitorGroup_newsProvider_A_fkey";

-- DropForeignKey
ALTER TABLE "_MonitorGroup_newsProvider" DROP CONSTRAINT "_MonitorGroup_newsProvider_B_fkey";

-- DropTable
DROP TABLE "_MonitorGroup_eventProvider";

-- DropTable
DROP TABLE "_MonitorGroup_newsProvider";

-- CreateTable
CREATE TABLE "_MonitorGroup_newsProviders" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateTable
CREATE TABLE "_EventProvider_monitorGroups" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "_MonitorGroup_newsProviders_AB_unique" ON "_MonitorGroup_newsProviders"("A", "B");

-- CreateIndex
CREATE INDEX "_MonitorGroup_newsProviders_B_index" ON "_MonitorGroup_newsProviders"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_EventProvider_monitorGroups_AB_unique" ON "_EventProvider_monitorGroups"("A", "B");

-- CreateIndex
CREATE INDEX "_EventProvider_monitorGroups_B_index" ON "_EventProvider_monitorGroups"("B");

-- AddForeignKey
ALTER TABLE "_MonitorGroup_newsProviders" ADD CONSTRAINT "_MonitorGroup_newsProviders_A_fkey" FOREIGN KEY ("A") REFERENCES "MonitorGroup"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_MonitorGroup_newsProviders" ADD CONSTRAINT "_MonitorGroup_newsProviders_B_fkey" FOREIGN KEY ("B") REFERENCES "NewsProvider"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_EventProvider_monitorGroups" ADD CONSTRAINT "_EventProvider_monitorGroups_A_fkey" FOREIGN KEY ("A") REFERENCES "EventProvider"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_EventProvider_monitorGroups" ADD CONSTRAINT "_EventProvider_monitorGroups_B_fkey" FOREIGN KEY ("B") REFERENCES "MonitorGroup"("id") ON DELETE CASCADE ON UPDATE CASCADE;
