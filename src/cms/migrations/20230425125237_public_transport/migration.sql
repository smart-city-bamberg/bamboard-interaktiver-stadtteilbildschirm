-- AlterTable
ALTER TABLE "Monitor" ADD COLUMN     "publicTransportDepartureLink" TEXT NOT NULL DEFAULT '';

-- CreateTable
CREATE TABLE "PublicTransport" (
    "id" TEXT NOT NULL,
    "headline" TEXT NOT NULL DEFAULT '',
    "image_filesize" INTEGER,
    "image_extension" TEXT,
    "image_width" INTEGER,
    "image_height" INTEGER,
    "image_id" TEXT,
    "content" TEXT NOT NULL DEFAULT '',

    CONSTRAINT "PublicTransport_pkey" PRIMARY KEY ("id")
);
