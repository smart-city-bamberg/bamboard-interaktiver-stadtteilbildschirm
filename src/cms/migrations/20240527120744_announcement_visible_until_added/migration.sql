-- AlterTable
ALTER TABLE "Announcement" ADD COLUMN     "visibleUntil" TIMESTAMP(3);

-- CreateIndex
CREATE INDEX "Announcement_visibleUntil_idx" ON "Announcement"("visibleUntil");
