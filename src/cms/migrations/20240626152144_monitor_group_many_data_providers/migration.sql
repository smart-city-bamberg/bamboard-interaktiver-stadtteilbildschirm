/*
  Warnings:

  - You are about to drop the column `dataProvider` on the `MonitorGroup` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE "MonitorGroup" DROP CONSTRAINT "MonitorGroup_dataProvider_fkey";

-- DropIndex
DROP INDEX "MonitorGroup_dataProvider_idx";

-- AlterTable
ALTER TABLE "MonitorGroup" DROP COLUMN "dataProvider";

-- CreateTable
CREATE TABLE "_MonitorGroup_dataProvider" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "_MonitorGroup_dataProvider_AB_unique" ON "_MonitorGroup_dataProvider"("A", "B");

-- CreateIndex
CREATE INDEX "_MonitorGroup_dataProvider_B_index" ON "_MonitorGroup_dataProvider"("B");

-- AddForeignKey
ALTER TABLE "_MonitorGroup_dataProvider" ADD CONSTRAINT "_MonitorGroup_dataProvider_A_fkey" FOREIGN KEY ("A") REFERENCES "DataProvider"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_MonitorGroup_dataProvider" ADD CONSTRAINT "_MonitorGroup_dataProvider_B_fkey" FOREIGN KEY ("B") REFERENCES "MonitorGroup"("id") ON DELETE CASCADE ON UPDATE CASCADE;
