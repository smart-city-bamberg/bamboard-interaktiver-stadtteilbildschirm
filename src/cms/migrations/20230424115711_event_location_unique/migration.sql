/*
  Warnings:

  - A unique constraint covering the columns `[name]` on the table `EventLocation` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX "EventLocation_name_key" ON "EventLocation"("name");
