-- CreateTable
CREATE TABLE "Contact" (
    "id" INTEGER NOT NULL,
    "content" JSONB NOT NULL DEFAULT '[{"type":"paragraph","children":[{"text":""}]}]',

    CONSTRAINT "Contact_pkey" PRIMARY KEY ("id")
);
