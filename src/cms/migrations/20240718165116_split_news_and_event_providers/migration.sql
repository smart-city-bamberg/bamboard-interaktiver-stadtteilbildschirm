/*
  Warnings:

  - You are about to drop the column `dataProvider` on the `Event` table. All the data in the column will be lost.
  - You are about to drop the column `dataProvider` on the `NewsReport` table. All the data in the column will be lost.
  - You are about to drop the `DataProvider` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `_MonitorGroup_dataProvider` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE "DataProvider" DROP CONSTRAINT "DataProvider_organization_fkey";

-- DropForeignKey
ALTER TABLE "Event" DROP CONSTRAINT "Event_dataProvider_fkey";

-- DropForeignKey
ALTER TABLE "NewsReport" DROP CONSTRAINT "NewsReport_dataProvider_fkey";

-- DropForeignKey
ALTER TABLE "_MonitorGroup_dataProvider" DROP CONSTRAINT "_MonitorGroup_dataProvider_A_fkey";

-- DropForeignKey
ALTER TABLE "_MonitorGroup_dataProvider" DROP CONSTRAINT "_MonitorGroup_dataProvider_B_fkey";

-- DropIndex
DROP INDEX "Event_dataProvider_idx";

-- DropIndex
DROP INDEX "NewsReport_dataProvider_idx";

-- AlterTable
ALTER TABLE "Event" DROP COLUMN "dataProvider",
ADD COLUMN     "eventProvider" TEXT;

-- AlterTable
ALTER TABLE "NewsReport" DROP COLUMN "dataProvider",
ADD COLUMN     "newsProvider" TEXT;

-- DropTable
DROP TABLE "DataProvider";

-- DropTable
DROP TABLE "_MonitorGroup_dataProvider";

-- CreateTable
CREATE TABLE "NewsProvider" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL DEFAULT '',
    "image_filesize" INTEGER,
    "image_extension" TEXT,
    "image_width" INTEGER,
    "image_height" INTEGER,
    "image_id" TEXT,
    "feedUrl" TEXT NOT NULL DEFAULT '',
    "feedType" TEXT,
    "feedTransformator" TEXT NOT NULL DEFAULT '',
    "organization" TEXT,

    CONSTRAINT "NewsProvider_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "EventProvider" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL DEFAULT '',
    "image_filesize" INTEGER,
    "image_extension" TEXT,
    "image_width" INTEGER,
    "image_height" INTEGER,
    "image_id" TEXT,
    "feedType" TEXT,
    "organization" TEXT,

    CONSTRAINT "EventProvider_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "_MonitorGroup_newsProvider" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateIndex
CREATE INDEX "NewsProvider_organization_idx" ON "NewsProvider"("organization");

-- CreateIndex
CREATE INDEX "EventProvider_organization_idx" ON "EventProvider"("organization");

-- CreateIndex
CREATE UNIQUE INDEX "_MonitorGroup_newsProvider_AB_unique" ON "_MonitorGroup_newsProvider"("A", "B");

-- CreateIndex
CREATE INDEX "_MonitorGroup_newsProvider_B_index" ON "_MonitorGroup_newsProvider"("B");

-- CreateIndex
CREATE INDEX "Event_eventProvider_idx" ON "Event"("eventProvider");

-- CreateIndex
CREATE INDEX "NewsReport_newsProvider_idx" ON "NewsReport"("newsProvider");

-- AddForeignKey
ALTER TABLE "NewsReport" ADD CONSTRAINT "NewsReport_newsProvider_fkey" FOREIGN KEY ("newsProvider") REFERENCES "NewsProvider"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Event" ADD CONSTRAINT "Event_eventProvider_fkey" FOREIGN KEY ("eventProvider") REFERENCES "EventProvider"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "NewsProvider" ADD CONSTRAINT "NewsProvider_organization_fkey" FOREIGN KEY ("organization") REFERENCES "Organization"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "EventProvider" ADD CONSTRAINT "EventProvider_organization_fkey" FOREIGN KEY ("organization") REFERENCES "Organization"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_MonitorGroup_newsProvider" ADD CONSTRAINT "_MonitorGroup_newsProvider_A_fkey" FOREIGN KEY ("A") REFERENCES "MonitorGroup"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_MonitorGroup_newsProvider" ADD CONSTRAINT "_MonitorGroup_newsProvider_B_fkey" FOREIGN KEY ("B") REFERENCES "NewsProvider"("id") ON DELETE CASCADE ON UPDATE CASCADE;
