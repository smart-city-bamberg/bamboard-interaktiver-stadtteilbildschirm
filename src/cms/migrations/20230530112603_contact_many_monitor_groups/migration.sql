/*
  Warnings:

  - You are about to drop the column `monitorGroup` on the `Contact` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE "Contact" DROP CONSTRAINT "Contact_monitorGroup_fkey";

-- DropIndex
DROP INDEX "Contact_monitorGroup_key";

-- AlterTable
ALTER TABLE "Contact" DROP COLUMN "monitorGroup";

-- AlterTable
ALTER TABLE "MonitorGroup" ADD COLUMN     "contact" TEXT;

-- CreateIndex
CREATE INDEX "MonitorGroup_contact_idx" ON "MonitorGroup"("contact");

-- AddForeignKey
ALTER TABLE "MonitorGroup" ADD CONSTRAINT "MonitorGroup_contact_fkey" FOREIGN KEY ("contact") REFERENCES "Contact"("id") ON DELETE SET NULL ON UPDATE CASCADE;
