-- AlterTable
ALTER TABLE "Monitor" ADD COLUMN     "garbageCollectionAssignment" TEXT;

-- CreateTable
CREATE TABLE "GarbageCollectionAssignment" (
    "id" TEXT NOT NULL,
    "restmuell" TEXT NOT NULL DEFAULT '',
    "biomuell" TEXT NOT NULL DEFAULT '',
    "gelber_sack" TEXT NOT NULL DEFAULT '',
    "papier" TEXT NOT NULL DEFAULT '',

    CONSTRAINT "GarbageCollectionAssignment_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE INDEX "Monitor_garbageCollectionAssignment_idx" ON "Monitor"("garbageCollectionAssignment");

-- AddForeignKey
ALTER TABLE "Monitor" ADD CONSTRAINT "Monitor_garbageCollectionAssignment_fkey" FOREIGN KEY ("garbageCollectionAssignment") REFERENCES "GarbageCollectionAssignment"("id") ON DELETE SET NULL ON UPDATE CASCADE;
