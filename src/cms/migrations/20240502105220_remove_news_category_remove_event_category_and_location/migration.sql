/*
  Warnings:

  - You are about to drop the `EventCategory` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `EventLocation` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `NewsCategory` table. If the table is not empty, all the data it contains will be lost.
  - Made the column `category` on table `Event` required. This step will fail if there are existing NULL values in that column.
  - Made the column `location` on table `Event` required. This step will fail if there are existing NULL values in that column.
  - Made the column `category` on table `NewsReport` required. This step will fail if there are existing NULL values in that column.

*/
-- DropForeignKey
ALTER TABLE "Event" DROP CONSTRAINT "Event_category_fkey";

-- DropForeignKey
ALTER TABLE "Event" DROP CONSTRAINT "Event_location_fkey";

-- DropForeignKey
ALTER TABLE "EventCategory" DROP CONSTRAINT "EventCategory_organization_fkey";

-- DropForeignKey
ALTER TABLE "EventLocation" DROP CONSTRAINT "EventLocation_organization_fkey";

-- DropForeignKey
ALTER TABLE "NewsCategory" DROP CONSTRAINT "NewsCategory_organization_fkey";

-- DropForeignKey
ALTER TABLE "NewsReport" DROP CONSTRAINT "NewsReport_category_fkey";

-- DropIndex
DROP INDEX "Event_category_idx";

-- DropIndex
DROP INDEX "Event_location_idx";

-- DropIndex
DROP INDEX "NewsReport_category_idx";

-- AlterTable
ALTER TABLE "Event" ALTER COLUMN "category" SET NOT NULL,
ALTER COLUMN "category" SET DEFAULT '',
ALTER COLUMN "location" SET NOT NULL,
ALTER COLUMN "location" SET DEFAULT '';

-- AlterTable
ALTER TABLE "NewsReport" ALTER COLUMN "category" SET NOT NULL,
ALTER COLUMN "category" SET DEFAULT '';

-- DropTable
DROP TABLE "EventCategory";

-- DropTable
DROP TABLE "EventLocation";

-- DropTable
DROP TABLE "NewsCategory";
