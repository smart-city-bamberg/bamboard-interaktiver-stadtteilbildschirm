/*
  Warnings:

  - You are about to drop the column `link` on the `DataProvider` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "DataProvider" DROP COLUMN "link",
ADD COLUMN     "name" TEXT NOT NULL DEFAULT '';
