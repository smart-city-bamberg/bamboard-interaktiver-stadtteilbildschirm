/*
  Warnings:

  - Made the column `longitude` on table `WeatherLocation` required. This step will fail if there are existing NULL values in that column.
  - Made the column `latitude` on table `WeatherLocation` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE "WeatherLocation" ALTER COLUMN "longitude" SET NOT NULL,
ALTER COLUMN "latitude" SET NOT NULL;
