-- AlterTable
ALTER TABLE "Announcement" ADD COLUMN     "organization" TEXT;

-- AlterTable
ALTER TABLE "Contact" ADD COLUMN     "organization" TEXT;

-- AlterTable
ALTER TABLE "Document" ADD COLUMN     "organization" TEXT;

-- AlterTable
ALTER TABLE "Event" ADD COLUMN     "organization" TEXT;

-- AlterTable
ALTER TABLE "EventCategory" ADD COLUMN     "organization" TEXT;

-- AlterTable
ALTER TABLE "EventLocation" ADD COLUMN     "organization" TEXT;

-- AlterTable
ALTER TABLE "Monitor" ADD COLUMN     "organization" TEXT;

-- AlterTable
ALTER TABLE "MonitorGroup" ADD COLUMN     "organization" TEXT;

-- AlterTable
ALTER TABLE "NewsCategory" ADD COLUMN     "organization" TEXT;

-- AlterTable
ALTER TABLE "NewsReport" ADD COLUMN     "organization" TEXT;

-- AlterTable
ALTER TABLE "PublicTransport" ADD COLUMN     "organization" TEXT;

-- CreateIndex
CREATE INDEX "Announcement_organization_idx" ON "Announcement"("organization");

-- CreateIndex
CREATE INDEX "Contact_organization_idx" ON "Contact"("organization");

-- CreateIndex
CREATE INDEX "Document_organization_idx" ON "Document"("organization");

-- CreateIndex
CREATE INDEX "Event_organization_idx" ON "Event"("organization");

-- CreateIndex
CREATE INDEX "EventCategory_organization_idx" ON "EventCategory"("organization");

-- CreateIndex
CREATE INDEX "EventLocation_organization_idx" ON "EventLocation"("organization");

-- CreateIndex
CREATE INDEX "Monitor_organization_idx" ON "Monitor"("organization");

-- CreateIndex
CREATE INDEX "MonitorGroup_organization_idx" ON "MonitorGroup"("organization");

-- CreateIndex
CREATE INDEX "NewsCategory_organization_idx" ON "NewsCategory"("organization");

-- CreateIndex
CREATE INDEX "NewsReport_organization_idx" ON "NewsReport"("organization");

-- CreateIndex
CREATE INDEX "PublicTransport_organization_idx" ON "PublicTransport"("organization");

-- AddForeignKey
ALTER TABLE "Monitor" ADD CONSTRAINT "Monitor_organization_fkey" FOREIGN KEY ("organization") REFERENCES "Organization"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "MonitorGroup" ADD CONSTRAINT "MonitorGroup_organization_fkey" FOREIGN KEY ("organization") REFERENCES "Organization"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Contact" ADD CONSTRAINT "Contact_organization_fkey" FOREIGN KEY ("organization") REFERENCES "Organization"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Document" ADD CONSTRAINT "Document_organization_fkey" FOREIGN KEY ("organization") REFERENCES "Organization"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Announcement" ADD CONSTRAINT "Announcement_organization_fkey" FOREIGN KEY ("organization") REFERENCES "Organization"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "NewsReport" ADD CONSTRAINT "NewsReport_organization_fkey" FOREIGN KEY ("organization") REFERENCES "Organization"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "NewsCategory" ADD CONSTRAINT "NewsCategory_organization_fkey" FOREIGN KEY ("organization") REFERENCES "Organization"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Event" ADD CONSTRAINT "Event_organization_fkey" FOREIGN KEY ("organization") REFERENCES "Organization"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "EventLocation" ADD CONSTRAINT "EventLocation_organization_fkey" FOREIGN KEY ("organization") REFERENCES "Organization"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "EventCategory" ADD CONSTRAINT "EventCategory_organization_fkey" FOREIGN KEY ("organization") REFERENCES "Organization"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "PublicTransport" ADD CONSTRAINT "PublicTransport_organization_fkey" FOREIGN KEY ("organization") REFERENCES "Organization"("id") ON DELETE SET NULL ON UPDATE CASCADE;
