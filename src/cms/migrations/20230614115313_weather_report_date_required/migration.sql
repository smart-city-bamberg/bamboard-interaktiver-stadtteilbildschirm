/*
  Warnings:

  - Made the column `timestamp` on table `WeatherReport` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE "WeatherReport" ALTER COLUMN "timestamp" SET NOT NULL;
