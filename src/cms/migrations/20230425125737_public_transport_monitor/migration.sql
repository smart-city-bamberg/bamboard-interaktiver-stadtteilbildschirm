-- AlterTable
ALTER TABLE "Monitor" ADD COLUMN     "publicTransportSettings" TEXT;

-- CreateIndex
CREATE INDEX "Monitor_publicTransportSettings_idx" ON "Monitor"("publicTransportSettings");

-- AddForeignKey
ALTER TABLE "Monitor" ADD CONSTRAINT "Monitor_publicTransportSettings_fkey" FOREIGN KEY ("publicTransportSettings") REFERENCES "PublicTransport"("id") ON DELETE SET NULL ON UPDATE CASCADE;
