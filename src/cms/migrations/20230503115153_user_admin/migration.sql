-- AlterTable
ALTER TABLE "User" ADD COLUMN     "isAdmin" BOOLEAN NOT NULL DEFAULT false;

-- CreateTable
CREATE TABLE "_User_organizations" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "_User_organizations_AB_unique" ON "_User_organizations"("A", "B");

-- CreateIndex
CREATE INDEX "_User_organizations_B_index" ON "_User_organizations"("B");

-- AddForeignKey
ALTER TABLE "_User_organizations" ADD CONSTRAINT "_User_organizations_A_fkey" FOREIGN KEY ("A") REFERENCES "Organization"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_User_organizations" ADD CONSTRAINT "_User_organizations_B_fkey" FOREIGN KEY ("B") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;
