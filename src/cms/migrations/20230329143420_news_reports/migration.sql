-- CreateTable
CREATE TABLE "NewsReport" (
    "id" TEXT NOT NULL,
    "timestamp" TIMESTAMP(3) NOT NULL,
    "title" TEXT NOT NULL DEFAULT '',
    "content" JSONB NOT NULL DEFAULT '[{"type":"paragraph","children":[{"text":""}]}]',
    "previewContent" TEXT NOT NULL DEFAULT '',
    "category" TEXT,
    "image_filesize" INTEGER,
    "image_extension" TEXT,
    "image_width" INTEGER,
    "image_height" INTEGER,
    "image_id" TEXT,

    CONSTRAINT "NewsReport_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "NewsCategory" (
    "id" TEXT NOT NULL,
    "name" TEXT NOT NULL DEFAULT '',

    CONSTRAINT "NewsCategory_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE INDEX "NewsReport_category_idx" ON "NewsReport"("category");

-- AddForeignKey
ALTER TABLE "NewsReport" ADD CONSTRAINT "NewsReport_category_fkey" FOREIGN KEY ("category") REFERENCES "NewsCategory"("id") ON DELETE SET NULL ON UPDATE CASCADE;
