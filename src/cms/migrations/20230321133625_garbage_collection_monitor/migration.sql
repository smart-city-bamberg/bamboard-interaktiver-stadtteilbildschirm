-- AlterTable
ALTER TABLE "GarbageCollection" ADD COLUMN     "monitor" TEXT;

-- CreateIndex
CREATE INDEX "GarbageCollection_monitor_idx" ON "GarbageCollection"("monitor");

-- AddForeignKey
ALTER TABLE "GarbageCollection" ADD CONSTRAINT "GarbageCollection_monitor_fkey" FOREIGN KEY ("monitor") REFERENCES "Monitor"("id") ON DELETE SET NULL ON UPDATE CASCADE;

/*
  Warnings:

  - Made the column `timestamp` on table `GarbageCollection` required. This step will fail if there are existing NULL values in that column.
  - Made the column `type` on table `GarbageCollection` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE "GarbageCollection" ALTER COLUMN "timestamp" SET NOT NULL,
ALTER COLUMN "type" SET NOT NULL;