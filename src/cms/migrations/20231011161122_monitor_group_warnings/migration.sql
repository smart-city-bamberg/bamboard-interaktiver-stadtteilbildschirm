-- CreateTable
CREATE TABLE "Warning" (
    "id" TEXT NOT NULL,
    "message" TEXT NOT NULL DEFAULT '',
    "timestamp" TIMESTAMP(3),
    "organization" TEXT,

    CONSTRAINT "Warning_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "_MonitorGroup_warnings" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateIndex
CREATE INDEX "Warning_organization_idx" ON "Warning"("organization");

-- CreateIndex
CREATE UNIQUE INDEX "_MonitorGroup_warnings_AB_unique" ON "_MonitorGroup_warnings"("A", "B");

-- CreateIndex
CREATE INDEX "_MonitorGroup_warnings_B_index" ON "_MonitorGroup_warnings"("B");

-- AddForeignKey
ALTER TABLE "Warning" ADD CONSTRAINT "Warning_organization_fkey" FOREIGN KEY ("organization") REFERENCES "Organization"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_MonitorGroup_warnings" ADD CONSTRAINT "_MonitorGroup_warnings_A_fkey" FOREIGN KEY ("A") REFERENCES "MonitorGroup"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_MonitorGroup_warnings" ADD CONSTRAINT "_MonitorGroup_warnings_B_fkey" FOREIGN KEY ("B") REFERENCES "Warning"("id") ON DELETE CASCADE ON UPDATE CASCADE;
