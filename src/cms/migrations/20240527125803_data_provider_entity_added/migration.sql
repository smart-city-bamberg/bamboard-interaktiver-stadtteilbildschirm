-- CreateTable
CREATE TABLE "DataProvider" (
    "id" TEXT NOT NULL,
    "link" TEXT NOT NULL DEFAULT '',
    "image_filesize" INTEGER,
    "image_extension" TEXT,
    "image_width" INTEGER,
    "image_height" INTEGER,
    "image_id" TEXT,
    "identifier" TEXT NOT NULL DEFAULT '',
    "organization" TEXT,

    CONSTRAINT "DataProvider_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "DataProvider_identifier_key" ON "DataProvider"("identifier");

-- CreateIndex
CREATE INDEX "DataProvider_organization_idx" ON "DataProvider"("organization");

-- AddForeignKey
ALTER TABLE "DataProvider" ADD CONSTRAINT "DataProvider_organization_fkey" FOREIGN KEY ("organization") REFERENCES "Organization"("id") ON DELETE SET NULL ON UPDATE CASCADE;
