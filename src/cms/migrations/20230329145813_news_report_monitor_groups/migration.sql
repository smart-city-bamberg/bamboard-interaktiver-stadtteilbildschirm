-- CreateTable
CREATE TABLE "_MonitorGroup_newsReports" (
    "A" TEXT NOT NULL,
    "B" TEXT NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "_MonitorGroup_newsReports_AB_unique" ON "_MonitorGroup_newsReports"("A", "B");

-- CreateIndex
CREATE INDEX "_MonitorGroup_newsReports_B_index" ON "_MonitorGroup_newsReports"("B");

-- AddForeignKey
ALTER TABLE "_MonitorGroup_newsReports" ADD CONSTRAINT "_MonitorGroup_newsReports_A_fkey" FOREIGN KEY ("A") REFERENCES "MonitorGroup"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_MonitorGroup_newsReports" ADD CONSTRAINT "_MonitorGroup_newsReports_B_fkey" FOREIGN KEY ("B") REFERENCES "NewsReport"("id") ON DELETE CASCADE ON UPDATE CASCADE;
