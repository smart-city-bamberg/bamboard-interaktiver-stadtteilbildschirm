-- AlterTable
ALTER TABLE "Event" ADD COLUMN     "dataProvider" TEXT;

-- AlterTable
ALTER TABLE "NewsReport" ADD COLUMN     "dataProvider" TEXT;

-- CreateIndex
CREATE INDEX "Event_dataProvider_idx" ON "Event"("dataProvider");

-- CreateIndex
CREATE INDEX "NewsReport_dataProvider_idx" ON "NewsReport"("dataProvider");

-- AddForeignKey
ALTER TABLE "NewsReport" ADD CONSTRAINT "NewsReport_dataProvider_fkey" FOREIGN KEY ("dataProvider") REFERENCES "DataProvider"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Event" ADD CONSTRAINT "Event_dataProvider_fkey" FOREIGN KEY ("dataProvider") REFERENCES "DataProvider"("id") ON DELETE SET NULL ON UPDATE CASCADE;
