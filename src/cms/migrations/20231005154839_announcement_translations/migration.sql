-- AlterTable
ALTER TABLE "Announcement" ADD COLUMN     "content_arabic" JSONB NOT NULL DEFAULT '[{"type":"paragraph","children":[{"text":""}]}]',
ADD COLUMN     "content_english" JSONB NOT NULL DEFAULT '[{"type":"paragraph","children":[{"text":""}]}]',
ADD COLUMN     "title_arabic" TEXT NOT NULL DEFAULT '',
ADD COLUMN     "title_english" TEXT NOT NULL DEFAULT '';
