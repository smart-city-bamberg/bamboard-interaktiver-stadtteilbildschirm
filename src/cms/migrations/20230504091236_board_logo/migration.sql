-- AlterTable
ALTER TABLE "Monitor" ADD COLUMN     "boardLogo_extension" TEXT,
ADD COLUMN     "boardLogo_filesize" INTEGER,
ADD COLUMN     "boardLogo_height" INTEGER,
ADD COLUMN     "boardLogo_id" TEXT,
ADD COLUMN     "boardLogo_width" INTEGER;
