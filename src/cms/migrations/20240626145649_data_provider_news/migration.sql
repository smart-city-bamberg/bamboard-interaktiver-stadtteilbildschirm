-- AlterTable
ALTER TABLE "MonitorGroup" ADD COLUMN     "dataProvider" TEXT;

-- CreateIndex
CREATE INDEX "MonitorGroup_dataProvider_idx" ON "MonitorGroup"("dataProvider");

-- AddForeignKey
ALTER TABLE "MonitorGroup" ADD CONSTRAINT "MonitorGroup_dataProvider_fkey" FOREIGN KEY ("dataProvider") REFERENCES "DataProvider"("id") ON DELETE SET NULL ON UPDATE CASCADE;
