/*
  Warnings:

  - You are about to drop the column `identifier` on the `DataProvider` table. All the data in the column will be lost.

*/
-- DropIndex
DROP INDEX "DataProvider_identifier_key";

-- AlterTable
ALTER TABLE "DataProvider" DROP COLUMN "identifier";
