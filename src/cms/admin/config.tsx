// admin/config.tsx
/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from "@keystone-ui/core";

const CustomLogo = () => (
  <div
    css={{
      display: "flex",
      flexDirection: "row",
      maxHeight: "1rem",
      alignItems: "center",
    }}
  >
    <img
      css={{ maxHeight: "4rem", marginRight: "1rem" }}
      src="/assets/logo.png"
      alt="Logo"
    ></img>
  </div>
);

export const components = {
  Logo: CustomLogo,
};
