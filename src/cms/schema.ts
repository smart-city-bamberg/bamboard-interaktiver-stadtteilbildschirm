import { group, list } from "@keystone-6/core";
import { allowAll } from "@keystone-6/core/access";
import {
  text,
  relationship,
  password,
  timestamp,
  calendarDay,
  float,
  file,
  select,
  image,
  checkbox,
} from "@keystone-6/core/fields";

import { document } from "@keystone-6/fields-document";

import type { Lists } from ".keystone/types";

import { eventProviderOptions } from "./eventProviders";

type Session = {
  data: {
    id: string;
    isAdmin: boolean;
    organizations: { id: string; name: string }[];
  };
};

const isAdmin = ({ session }: { session?: Session }) =>
  session?.data.isAdmin === true;

const isAuthenticated = ({ session }: { session?: Session }) =>
  session?.data !== undefined;

const filterByOrganizations = ({ session }: { session?: Session }) => {
  if (session?.data.isAdmin) {
    return true;
  }

  if (!session?.data.organizations.length) {
    return false;
  }

  return {
    OR: session.data.organizations.map((organization) => ({
      id: { equals: organization.id },
    })),
  };
};

const filterByOrganizationsOrSameUser = ({
  session,
}: {
  session?: Session;
}) => {
  if (session?.data.isAdmin) {
    return true;
  }

  const organizationsById =
    session?.data.organizations?.map((organization) => ({
      organizations: {
        some: { id: { equals: organization.id } },
      },
    })) ?? [];

  return {
    OR: [
      ...organizationsById,
      {
        id: {
          equals: session?.data.id,
        },
      },
    ],
  };
};

const filterByAssignedOrganizationsOrAdmin = ({
  session,
}: {
  session?: Session;
}) => {
  if (session?.data.isAdmin) {
    return true;
  }

  const organizationsById =
    session?.data.organizations?.map((organization) => ({
      organization: {
        id: { equals: organization.id },
      },
    })) ?? [];

  return {
    OR: organizationsById,
  };
};

const isAdminOrSameUser = ({
  session,
  context,
  listKey,
  operation,
  inputData,
  item,
}: {
  session?: Session;
  context: any;
  listKey: any;
  operation: any;
  inputData: any;
  item: Lists.User.Item;
}) => {
  if (session?.data.isAdmin) {
    return true;
  }

  return item.id === session?.data.id;
};

const organizationalScopedAccess = {
  operation: {
    create: isAuthenticated,
    delete: isAuthenticated,
    update: isAuthenticated,
    query: isAuthenticated,
  },
  filter: {
    query: filterByAssignedOrganizationsOrAdmin,
  },
};

function validateHasRelation(args: any, name: string, message: string) {
  const idName = `${name}Id`;

  const hasConnection =
    args.item !== undefined &&
    (args.item?.[idName] !== null || args.item?.[idName] !== undefined);

  const hasNewConnect = args.inputData?.[name]?.connect?.id !== undefined;

  const hasDisconnect =
    (args.inputData?.[name] as any)?.disconnect !== undefined;

  if ((!hasConnection && !hasNewConnect) || (hasConnection && hasDisconnect)) {
    args.addValidationError(message);
  }
}

export const lists: Lists = {
  User: list({
    ui: {
      label: "👥 Benutzer",
      plural: "Benutzer",
      listView: {
        initialColumns: ["name", "email", "isAdmin", "isMonitor"],
        initialSort: {
          field: "name",
          direction: "ASC",
        },
      },
      hideCreate(args) {
        return !args.context.session.data.isAdmin;
      },
    },
    access: {
      operation: {
        create: isAdmin,
        delete: isAdmin,
        update: isAuthenticated,
        query: isAuthenticated,
      },
      item: {
        update: isAdminOrSameUser,
      },
      filter: {
        query: filterByOrganizationsOrSameUser,
      },
    },
    fields: {
      name: text({
        validation: { isRequired: true },
        ui: {
          itemView: {
            fieldMode(args) {
              const isSameUser = args.session.data.id === args.item.id;

              return args.session.data.isAdmin || isSameUser ? "edit" : "read";
            },
          },
        },
      }),
      email: text({
        validation: { isRequired: true },
        isIndexed: "unique",
        access: {
          update: isAdmin,
        },
        ui: {
          itemView: {
            fieldMode(args) {
              return args.session.data.isAdmin ? "edit" : "read";
            },
          },
        },
      }),
      password: password({
        validation: { isRequired: true },
        ui: {
          itemView: {
            fieldMode(args) {
              const isSameUser = args.session.data.id === args.item.id;

              return args.session.data.isAdmin || isSameUser
                ? "edit"
                : "hidden";
            },
          },
        },
      }),
      createdAt: timestamp({
        defaultValue: { kind: "now" },
        access: {
          update: isAdmin,
        },
        ui: {
          itemView: {
            fieldMode(args) {
              return args.session.data.isAdmin ? "edit" : "read";
            },
          },
        },
      }),
      organizations: relationship({
        ref: "Organization",
        label: "🌐 Organisationen",
        many: true,
        access: {
          update: isAdmin,
        },
        ui: {
          itemView: {
            fieldMode(args) {
              return args.session.data.isAdmin ? "edit" : "read";
            },
          },
        },
      }),
      isAdmin: checkbox({
        label: "👑 Administrator",
        access: {
          update: isAdmin,
        },
        ui: {
          itemView: {
            fieldMode(args) {
              return args.session.data.isAdmin ? "edit" : "read";
            },
          },
        },
      }),
      isMonitor: checkbox({
        label: "🖥️ Bildschirm",
        access: {
          update: isAdmin,
        },
        ui: {
          itemView: {
            fieldMode(args) {
              return args.session.data.isAdmin ? "edit" : "read";
            },
          },
        },
      }),
    },
  }),
  Organization: list({
    access: {
      operation: {
        create: isAdmin,
        delete: isAdmin,
        update: isAdmin,
        query: isAuthenticated,
      },
      filter: {
        query: filterByOrganizations,
      },
    },
    ui: {
      label: "🌐 Organisationen",
      hideCreate: (args) => !args.session?.data.isAdmin,
      hideDelete: (args) => !args.session?.data.isAdmin,
      listView: {
        initialColumns: ["name"],
        initialSort: {
          field: "name",
          direction: "ASC",
        },
      },
    },
    fields: {
      name: text({
        label: "🏷️ Name",
        isIndexed: "unique",
        validation: {
          isRequired: true,
        },
      }),
    },
  }),
  WeatherReport: list({
    access: allowAll,
    ui: {
      labelField: "timestamp",
      label: "🌤️ Wetterberichte",
      plural: "Wetterberichte",
      listView: {
        initialColumns: ["timestamp", "location", "temperature", "icon"],
        initialSort: {
          field: "timestamp",
          direction: "DESC",
        },
      },
    },
    fields: {
      timestamp: timestamp({
        label: "⏰ Zeitpunkt",
        defaultValue: { kind: "now" },
        isOrderable: true,
        isIndexed: true,
        isFilterable: true,
        validation: {
          isRequired: true,
        },
      }),
      location: relationship({
        ref: "WeatherLocation.reports",
        label: "📍 Wetterstandort",
        hooks: {
          validateInput(args) {
            validateHasRelation(
              args,
              "location",
              "Kein Wetterstandort ausgewählt."
            );
          },
        },
      }),
      temperature: float({
        label: "🌡️ Temperatur",
        validation: {
          isRequired: true,
        },
      }),
      icon: text({
        label: "🖼️ Bild",
        validation: {
          isRequired: true,
        },
      }),
      organization: relationship({
        ref: "Organization",
        label: "🌐 Organisation",
        hooks: {
          validateInput(args) {
            validateHasRelation(
              args,
              "organization",
              "Keine Organisation ausgewählt."
            );
          },
        },
      }),
      forecasts: relationship({
        label: "📈 Vorhersagen",
        ref: "WeatherForecast.report",
        many: true,
      }),
    },
  }),
  WeatherForecast: list({
    ui: {
      isHidden: true,
      label: "📈 Wettervorhersage",
      plural: "Wettervorhersagen",
      labelField: "timestamp",
      listView: {
        initialColumns: ["timestamp", "temperature", "icon"],
        initialSort: {
          field: "timestamp",
          direction: "DESC",
        },
      },
    },
    access: allowAll,
    fields: {
      timestamp: timestamp({
        label: "⏰ Zeitpunkt",
        isFilterable: true,
        isIndexed: true,
        isOrderable: true,
        validation: {
          isRequired: true,
        },
      }),
      temperature: float({
        label: "🌡️ Temperatur",
        validation: {
          isRequired: true,
        },
      }),
      icon: text({
        label: "🖼️ Bild",
        validation: {
          isRequired: true,
        },
      }),
      organization: relationship({
        ref: "Organization",
        label: "🌐 Organisation",
        hooks: {
          validateInput(args) {
            validateHasRelation(
              args,
              "organization",
              "Keine Organisation ausgewählt."
            );
          },
        },
      }),
      report: relationship({
        ref: "WeatherReport.forecasts",
        label: "🌤️ Bericht",
      }),
    },
  }),
  WeatherLocation: list({
    access: organizationalScopedAccess,
    ui: {
      label: "📍 Wetterstandorte",
      plural: "Wetterstandorte",
      singular: "Wetterstandort",
      listView: {
        initialColumns: ["name", "longitude", "latitude"],
        initialSort: {
          field: "name",
          direction: "ASC",
        },
      },
    },
    fields: {
      name: text({
        access: allowAll,
        label: "🏷️ Name",
        validation: {
          isRequired: true,
        },
      }),
      longitude: float({
        access: allowAll,
        label: "🌐 Längengrad",
        validation: {
          isRequired: true,
        },
      }),
      latitude: float({
        access: allowAll,
        label: "🌐 Breitengrad",
        validation: {
          isRequired: true,
        },
      }),
      monitorGroups: relationship({
        ref: "MonitorGroup.weatherLocation",
        label: "🔗 Bildschirmgruppen",
        many: true,
      }),
      organization: relationship({
        ref: "Organization",
        label: "🌐 Organisation",
        hooks: {
          validateInput(args) {
            validateHasRelation(
              args,
              "organization",
              "Keine Organisation ausgewählt."
            );
          },
        },
      }),
      reports: relationship({
        ref: "WeatherReport.location",
        many: true,
        label: "🌤️ Wetterberichte",
        ui: {
          createView: {
            fieldMode: "hidden",
          },
          linkToItem: true,
          hideCreate: true,
          itemView: {
            fieldMode: "read",
          },
        },
      }),
    },
  }),
  Monitor: list({
    access: organizationalScopedAccess,
    ui: {
      label: "🖥️ Bildschirme",
      plural: "Bildschirme",
      singular: "Bildschirm",
      listView: {
        initialColumns: ["name", "group"],
        initialSort: {
          field: "name",
          direction: "ASC",
        },
      },
    },
    fields: {
      name: text({
        isFilterable: true,
        label: "🏷️ Name",
        validation: {
          isRequired: true,
        },
      }),
      group: relationship({
        ref: "MonitorGroup.monitors",
        label: "🔗 Bildschirmgruppe",
      }),
      garbageCollections: relationship({
        ref: "GarbageCollection.monitor",
        many: true,
        ui: {
          displayMode: "count",
          hideCreate: true,
          createView: { fieldMode: "hidden" },
        },
        label: "📅 Entsorgungstermine",
      }),
      garbageCollectionAssignment: relationship({
        ref: "GarbageCollectionAssignment",
        label: "⚙️ Entsorgungstermin Einstellungen",
      }),
      garbageCollectionCalendar: file({
        storage: "document_store",
        label: "📄 iCal Entsorgungstermine",
      }),
      operatorLogo: image({
        label: "🖼️ Betreiber-Logo",
        storage: "image_store",
      }),
      boardLogo: image({
        label: "🖼️ Board-Logo",
        storage: "image_store",
      }),
      publicTransportDepartureLink: text({
        label: "🚏 Link ÖPNV Abfahrtstafel",
      }),
      publicTransportSettings: relationship({
        label: "🚌 ÖPNV Einstellungen",
        ref: "PublicTransport.monitors",
      }),
      organization: relationship({
        ref: "Organization",
        label: "🌐 Organisation",
        hooks: {
          validateInput(args) {
            validateHasRelation(
              args,
              "organization",
              "Keine Organisation ausgewählt."
            );
          },
        },
      }),
    },
  }),
  MonitorGroup: list({
    access: organizationalScopedAccess,
    ui: {
      label: "🔗 Bildschirmgruppen",
      plural: "Bildschirmgruppen",
      listView: {
        initialColumns: ["name", "weatherLocation", "monitors"],
        initialSort: {
          field: "name",
          direction: "ASC",
        },
      },
    },
    fields: {
      name: text({
        isFilterable: true,
        label: "🏷️ Name",
        validation: {
          isRequired: true,
        },
      }),
      monitors: relationship({
        ref: "Monitor.group",
        label: "🖥️ Bildschirme",
        many: true,
      }),
      organization: relationship({
        ref: "Organization",
        label: "🌐 Organisation",
        hooks: {
          validateInput(args) {
            validateHasRelation(
              args,
              "organization",
              "Keine Organisation ausgewählt."
            );
          },
        },
      }),
      contact: relationship({
        ref: "Contact.monitorGroup",
        label: "☎️ Kontakt",
      }),
      documents: relationship({
        ref: "Document.monitorGroup",
        label: "📄 Dokumente",
        many: true,
        ui: {
          displayMode: "count",
        },
      }),
      announcements: relationship({
        ref: "Announcement.monitorGroup",
        label: "📣 Mitteilungen",
        many: true,
        ui: {
          displayMode: "count",
        },
      }),
      weatherLocation: relationship({
        ref: "WeatherLocation.monitorGroups",
        label: "📍 Wetterstandort",
      }),
      defaultNewsImage: image({
        label: "🖼️ Standard Nachrichtenbild",
        storage: "image_store",
      }),
      defaultEventImage: image({
        label: "🖼️ Standard Veranstaltungsbild",
        storage: "image_store",
      }),
      officialRegionalKey: text({
        label: "🔑 Amtlicher Regionalschlüssel",
      }),
      warnings: relationship({
        ref: "Warning.monitorGroup",
        label: "🚨 Warnungen",
        many: true,
        ui: {
          displayMode: "count",
        },
      }),
      newsReports: relationship({
        ref: "NewsReport.monitorGroups",
        label: "📰 Nachrichten",
        many: true,
        ui: {
          displayMode: "count",
        },
      }),
      newsProviders: relationship({
        ref: "NewsProvider.monitorGroups",
        label: "🛡️ Nachrichtenanbieter",
        many: true,
      }),
      events: relationship({
        ref: "Event.monitorGroups",
        label: "📰 Nachrichten",
        many: true,
        ui: {
          displayMode: "count",
        },
      }),
      eventProviders: relationship({
        ref: "EventProvider.monitorGroups",
        label: "🛡️ Veranstaltungsanbieter",
        many: true,
      }),
    },
  }),
  Warning: list({
    access: organizationalScopedAccess,
    ui: {
      label: "🚨 Warnungen",
      labelField: "message",
      listView: {
        initialColumns: ["message"],
        initialSort: {
          field: "message",
          direction: "ASC",
        },
      },
    },
    fields: {
      message: text({
        label: "📄 Nachricht",
      }),
      timestamp: timestamp({
        label: "⏰ Zeitpunkt",
      }),
      externalId: text({
        label: "🏷️ Externe Kennung",
      }),
      type: select({
        label: "Typ",
        options: [
          {
            label: "Warnung",
            value: "alert",
          },
          {
            label: "Entwarnung",
            value: "clear",
          },
        ],
      }),
      monitorGroup: relationship({
        ref: "MonitorGroup.warnings",
        label: "🔗 Bildschirmgruppe",
        many: true,
      }),
      organization: relationship({
        ref: "Organization",
        label: "🌐 Organisation",
        hooks: {
          validateInput(args) {
            validateHasRelation(
              args,
              "organization",
              "Keine Organisation ausgewählt."
            );
          },
        },
      }),
    },
  }),
  Contact: list({
    access: organizationalScopedAccess,
    ui: {
      label: "☎️ Kontakt",
      labelField: "name",
      listView: {
        initialColumns: ["name", "monitorGroup"],
        initialSort: {
          field: "name",
          direction: "ASC",
        },
      },
    },
    fields: {
      name: text({
        label: "🏷️ Name",
        validation: {
          isRequired: true,
        },
      }),
      content: document({
        formatting: true,
        dividers: true,
        links: true,
        label: "📄 Inhalt",
      }),
      monitorGroup: relationship({
        ref: "MonitorGroup.contact",
        label: "🔗 Bildschirmgruppe",
        many: true,
      }),
      organization: relationship({
        ref: "Organization",
        label: "🌐 Organisation",
        hooks: {
          validateInput(args) {
            validateHasRelation(
              args,
              "organization",
              "Keine Organisation ausgewählt."
            );
          },
        },
      }),
    },
  }),
  Document: list({
    access: organizationalScopedAccess,
    ui: {
      label: "📄 Dokumente",
      listView: {
        initialColumns: ["name", "monitorGroup"],
        initialSort: {
          field: "name",
          direction: "ASC",
        },
      },
    },
    fields: {
      name: text({
        label: "🏷️ Name",
        validation: {
          isRequired: true,
        },
      }),
      content: file({
        label: "📃 Datei",
        storage: "document_store",
      }),
      monitorGroup: relationship({
        ref: "MonitorGroup.documents",
        label: "🔗 Bildschirmgruppe",
      }),
      organization: relationship({
        ref: "Organization",
        label: "🌐 Organisation",
        hooks: {
          validateInput(args) {
            validateHasRelation(
              args,
              "organization",
              "Keine Organisation ausgewählt."
            );
          },
        },
      }),
    },
  }),
  Announcement: list({
    access: organizationalScopedAccess,
    ui: {
      label: "📣 Mitteilungen",
      listView: {
        initialColumns: ["timestamp", "title", "monitorGroup"],
        initialSort: {
          field: "timestamp",
          direction: "DESC",
        },
      },
    },
    fields: {
      title: text({
        label: "🏷️ Titel",
        validation: {
          isRequired: true,
        },
      }),
      content: document({
        formatting: true,
        dividers: true,
        links: true,
        label: "📄 Inhalt",
      }),

      ...group({
        label: "Englisch",
        fields: {
          title_english: text({
            label: "🏷️ Titel",
          }),
          content_english: document({
            formatting: true,
            dividers: true,
            links: true,
            label: "📄 Inhalt",
          }),
        },
      }),

      ...group({
        label: "Arabisch",
        fields: {
          title_arabic: text({
            label: "🏷️ Titel",
          }),
          content_arabic: document({
            formatting: true,
            dividers: true,
            links: true,
            label: "📄 Inhalt",
          }),
        },
      }),

      timestamp: timestamp({
        label: "⏰ Zeitpunkt",
        isFilterable: true,
        isIndexed: true,
        isOrderable: true,
        validation: {
          isRequired: true,
        },
      }),

      visibleUntil: timestamp({
        label: "⏰ Sichtbar bis",
        isFilterable: true,
        isIndexed: true,
        isOrderable: true,
      }),

      monitorGroup: relationship({
        ref: "MonitorGroup.announcements",
        label: "🔗 Bildschirmgruppe",
      }),
      organization: relationship({
        ref: "Organization",
        label: "🌐 Organisation",
        hooks: {
          validateInput(args) {
            validateHasRelation(
              args,
              "organization",
              "Keine Organisation ausgewählt."
            );
          },
        },
      }),
    },
  }),
  GarbageCollection: list({
    access: organizationalScopedAccess,
    ui: {
      label: "♻️ Entsorgungstermine",
      labelField: "type",
      listView: {
        initialColumns: ["timestamp", "type", "monitor"],
        initialSort: {
          field: "timestamp",
          direction: "DESC",
        },
      },
    },
    fields: {
      organization: relationship({
        ref: "Organization",
        label: "🌐 Organisation",
        hooks: {
          validateInput(args) {
            validateHasRelation(
              args,
              "organization",
              "Keine Organisation ausgewählt."
            );
          },
        },
      }),
      timestamp: timestamp({
        label: "⏰ Zeitpunkt",
        validation: {
          isRequired: true,
        },
      }),
      monitor: relationship({
        ref: "Monitor.garbageCollections",
        label: "🖥️ Bildschirm",
        hooks: {
          validateInput(args) {
            validateHasRelation(args, "monitor", "Kein Monitor ausgewählt.");
          },
        },
      }),
      type: select({
        validation: {
          isRequired: true,
        },
        options: [
          {
            label: "⚫ Restmüll",
            value: "restmuell",
          },
          {
            label: "🔵 Papier",
            value: "papier",
          },
          {
            label: "🟡 Gelber Sack",
            value: "gelber_sack",
          },
          {
            label: "🟢 Biomüll",
            value: "biomuell",
          },
        ],
        label: "🏳️‍🌈 Art",
      }),
      source: text({
        label: "📃 Quelle",
      }),
    },
  }),
  GarbageCollectionAssignment: list({
    access: organizationalScopedAccess,
    ui: {
      label: "⚙️ Entsorgungseinstellungen",
      listView: {
        initialColumns: [
          "name",
          "restmuell",
          "papier",
          "gelber_sack",
          "biomuell",
        ],
        initialSort: {
          field: "name",
          direction: "ASC",
        },
      },
    },
    fields: {
      organization: relationship({
        ref: "Organization",
        label: "🌐 Organisation",
        hooks: {
          validateInput(args) {
            validateHasRelation(
              args,
              "organization",
              "Keine Organisation ausgewählt."
            );
          },
        },
      }),
      name: text({
        label: "🏷️ Name",
        validation: {
          isRequired: true,
        },
      }),
      restmuell: text({
        label: "⚫ Restmüll",
      }),
      biomuell: text({
        label: "🟤 Biomüll",
      }),
      gelber_sack: text({
        label: "🟡 Gelber Sack",
      }),
      papier: text({
        label: "🔵 Papier",
      }),
    },
  }),
  NewsReport: list({
    access: organizationalScopedAccess,
    ui: {
      label: "📰 Nachrichten",
      listView: {
        pageSize: 1000,
        initialColumns: ["timestamp", "title", "monitorGroups"],
        initialSort: {
          field: "timestamp",
          direction: "DESC",
        },
      },
    },
    fields: {
      timestamp: timestamp({
        label: "⏰ Datum",
        validation: {
          isRequired: true,
        },
      }),
      title: text({
        label: "🏷️ Titel",
        validation: {
          isRequired: true,
        },
      }),
      content: document({
        label: "📄 Text",
        formatting: true,
        dividers: true,
        links: true,
      }),
      previewContent: text({
        label: "📃 Vorschau-Text",
        validation: {
          isRequired: true,
        },
      }),
      link: text({
        label: "🔗 Link",
      }),
      category: text({
        label: "🏳️‍🌈 Kategorie",
      }),
      image: image({
        label: "🖼️ Bild",
        storage: "image_store",
      }),
      monitorGroups: relationship({
        ref: "MonitorGroup.newsReports",
        label: "🔗 Bildschirmgruppen",
        many: true,
      }),
      externalId: text({
        label: "🪧 Externe Kennung",
        ui: {
          createView: {
            fieldMode: "hidden",
          },
          itemView: {
            fieldMode: "read",
          },
        },
      }),
      newsProvider: relationship({
        ref: "NewsProvider.newsReports",
        label: "🛡️ Nachrichtenanbieter",
      }),
      organization: relationship({
        ref: "Organization",
        label: "🌐 Organisation",
        hooks: {
          validateInput(args) {
            validateHasRelation(
              args,
              "organization",
              "Keine Organisation ausgewählt."
            );
          },
        },
      }),
    },
  }),
  Event: list({
    access: organizationalScopedAccess,
    ui: {
      label: "🎫 Kultur",
      listView: {
        pageSize: 1000,
        initialColumns: ["date", "time", "title", "externalId"],
        initialSort: {
          field: "date",
          direction: "DESC",
        },
      },
    },
    fields: {
      title: text({
        label: "🏷️ Titel",
        validation: {
          isRequired: true,
        },
      }),
      category: text({
        label: "🏳️‍🌈 Kategorie",
      }),
      date: calendarDay({
        label: "📅 Tag",
        validation: {
          isRequired: true,
        },
      }),
      time: text({
        label: "⏰ Uhrzeit",
        validation: {
          isRequired: true,
        },
      }),
      location: text({
        label: "📍 Ort",
      }),
      link: text({
        label: "🔗 Link",
      }),
      image: image({
        storage: "image_store",
        label: "🖼️ Bild",
      }),
      monitorGroups: relationship({
        ref: "MonitorGroup.events",
        label: "🔗 Bildschirmgruppen",
        many: true,
      }),
      externalId: text({
        label: "🪧 Externe Kennung",
        ui: {
          createView: {
            fieldMode: "hidden",
          },
          itemView: {
            fieldMode: "read",
          },
        },
      }),
      eventProvider: relationship({
        ref: "EventProvider.events",
        label: "🛡️ Veranstaltungsanbieter",
      }),
      organization: relationship({
        ref: "Organization",
        label: "🌐 Organisation",
        hooks: {
          validateInput(args) {
            validateHasRelation(
              args,
              "organization",
              "Keine Organisation ausgewählt."
            );
          },
        },
      }),
    },
  }),
  PublicTransport: list({
    access: organizationalScopedAccess,
    ui: {
      label: "🚌 ÖPNV Einstellungen",
      labelField: "headline",
      listView: {
        initialColumns: ["headline"],
        initialSort: {
          field: "headline",
          direction: "ASC",
        },
      },
    },
    fields: {
      headline: text({
        label: "🏷️ Überschrift",
        validation: {
          isRequired: true,
        },
      }),
      image: image({
        label: "🖼️ Bild",
        storage: "image_store",
      }),
      content: document({
        label: "📄 Text",
        formatting: true,
        dividers: true,
        links: true,
      }),
      service: text({
        label: "☎️ Service-Überschrift",
        validation: {
          isRequired: true,
        },
      }),
      servicedata: document({
        label: "📄 Service-Daten",
        formatting: true,
        dividers: true,
        links: true,
      }),
      monitors: relationship({
        ref: "Monitor.publicTransportSettings",
        label: "🖥️ Bildschirme",
        many: true,
      }),
      organization: relationship({
        ref: "Organization",
        label: "🌐 Organisation",
        hooks: {
          validateInput(args) {
            validateHasRelation(
              args,
              "organization",
              "Keine Organisation ausgewählt."
            );
          },
        },
      }),
    },
  }),
  NewsProvider: list({
    access: organizationalScopedAccess,
    ui: {
      label: "🛡️ Nachrichtenanbieter",
      labelField: "name",
    },
    fields: {
      name: text({
        label: "🏷️ Name",
      }),
      image: image({
        storage: "image_store",
        label: "🖼️ Bild",
      }),
      feedUrl: text({
        label: "🔗 Feed URL",
      }),
      feedType: select({
        label: "🔗 Feed Typ",
        options: [
          { label: "🟠 RSS", value: "rss" },
          { label: "🔵 JSON", value: "json" },
        ],
      }),
      configuration: text({
        label: "⚙️ Einstellungen",
        ui: {
          displayMode: "textarea",
        },
      }),
      newsReports: relationship({
        ref: "NewsReport.newsProvider",
        label: "📰 Nachrichten",
        many: true,
        ui: {
          displayMode: "count",
        },
      }),
      monitorGroups: relationship({
        ref: "MonitorGroup.newsProviders",
        label: "🔗 Bildschirmgruppen",
        many: true,
      }),
      organization: relationship({
        ref: "Organization",
        label: "🌐 Organisation",
        hooks: {
          validateInput(args) {
            validateHasRelation(
              args,
              "organization",
              "Keine Organisation ausgewählt."
            );
          },
        },
      }),
    },
  }),
  EventProvider: list({
    access: organizationalScopedAccess,
    ui: {
      label: "🛡️ Veranstaltungsanbieter",
      labelField: "name",
    },
    fields: {
      name: text({
        label: "🏷️ Name",
      }),
      image: image({
        storage: "image_store",
        label: "🖼️ Bild",
      }),
      feedType: select({
        label: "🔗 Quelle",
        options: eventProviderOptions,
      }),
      configuration: text({
        label: "⚙️ Einstellungen",
        ui: {
          displayMode: "textarea",
        },
        access: {
          create: isAdmin,
          update: isAdmin,
          read: isAdmin,
        },
      }),
      events: relationship({
        ref: "Event.eventProvider",
        label: "🎫 Veranstaltungen",
        many: true,
        ui: {
          displayMode: "count",
        },
      }),
      monitorGroups: relationship({
        ref: "MonitorGroup.eventProviders",
        label: "🔗 Bildschirmgruppen",
        many: true,
      }),
      organization: relationship({
        ref: "Organization",
        label: "🌐 Organisation",
        hooks: {
          validateInput(args) {
            validateHasRelation(
              args,
              "organization",
              "Keine Organisation ausgewählt."
            );
          },
        },
      }),
    },
  }),
};
