# BamBoard CMS

Dies ist das Backend-CMS für den interaktiven Stadtteilbildschirm "BamBoard" auf Basis von Keystone 6 (https://github.com/keystonejs/keystone).

![Alt text](docs/cms-dashboard.jpg)

---

## Systemanforderungen

- PostgreSQL 
- Docker

---

## Lokale Entwicklung

Für Entwicklungszwecke muss die Datenbank selbst bereitgestellt werden.
Folgende Schritte sind erforderlich, um das CMS ohne Docker zu Entwicklungs\-zwecken zu starten:

1. Installiere alle Abhängigkeiten:

```bash
yarn install
```

1. In Postgres die Datenbank `keystone` mit dem Nutzer `keystone` und Passwort `password` anlegen

2. (optional) bei abweichender Konfiguration von Postgres eine neue Datei namens `process.env` anlegen und die angepasste Datenbank-URL als Umgebungsvariable einfügen:

```bash
process.env.DATABASE_URL=postgres://keystone:password@localhost:5432/keystone
```

3. Starte das CMS:

```bash
yarn run dev
```

4. Das CMS ist nun unter `http://localhost:3000` erreichbar.

---

## Docker-Bereitstellung

Das Projekt kann mithilfe von Docker Compose bereitgestellt werden. Dabei werden sowohl das CMS als auch die Datenbank in einem Docker-Container erstellt.

⚠️ Für den produktiven Einsatz sollten die Zugangsdaten für die Datenbank unbedingt geändert werden.

In der Datei `docker-compose.override.yml` können die Zugangsdaten und weitere Einstellungen wie der öffentliche Hostname angepasst werden:

```
version: '3.4'

services:
  keystone:    
    environment:      
      - HOST_PUBLIC_NAME=http://localhost:3000      
      - DATABASE_URL=postgres://keystone:password@db:5432/keystone
      - CORS_ORIGIN=http://localhost:4200      
    volumes:
      - ./public:/storage
    ports:
      - "3000:3000"

  db:    
    environment:
      - POSTGRES_USER=keystone
      - POSTGRES_PASSWORD=password
      - POSTGRES_DB=keystone
    volumes:
      - ./data:/var/lib/postgresql/data:rw
    ports:
      - "5432:5432"

```

Um Docker Compose aufzurufen, führe den folgenden Befehl aus:

```bash
docker-compose -f docker-compose.yml -f docker-compose.override.yml up -d
```

Das CMS ist nun unter `http://localhost:3000` erreichbar, Posgres unter `localhost:5432`

---

## Administrator-Nutzer erstellen

Um das CMS verwenden zu können, muss ein Administrator-Nutzer angelegt werden. Dafür sind folgende Schritte erforderlich:

1. Öffne das CMS in einem Webbrowser unter `http://localhost:3000`.
2. Registriere dich als neuer Benutzer.

Nachdem du den Administrator-Nutzer erstellt hast, kannst du dich mit den entsprechenden Zugangsdaten anmelden und das CMS vollständig nutzen.

---

## GraphQL Explorer

Unter http://localhost:3000/api/graphql kann der Apollo GraphQL Explorer geöffnet werden. Hier lässt sich die Schnittstelle erforschen und ausprobieren:

![Alt text](docs/cms-graphql-apollo.jpg)

---

## Entitäten

### Benutzer (User)
- Benutzer des Systems, die Inhalte verwalten können.
- Jeder Benutzer kann einer oder mehreren Organisationen angehören.
- Jeder Benutzer kann ein Admin-Flag haben und uneingeschränkten Zugriff haben.
- Es gibt "Monitor"-Benutzer, die nur zur Authentifizierung eines Bildschirms dienen.
- Jeder Benutzer kann nur die Inhalte sehen, die den Organisationen zugeordnet sind, denen er angehört.

### Organisationen (Organization)
- Bestimmen die Zugehörigkeit und Sichtbarkeit von Inhalten. Alle Inhalte sind jeweils einer Organisation zugeordnet.

### Wetterberichte (WeatherReport)
- Wetterbericht für einen bestimmten Zeitpunkt und Wetterstandort.
- Enthält Temperaturinformationen und einen Text mit einem Wort für die Wetterlage, z. B. "cloudy".
- Enthält mehrere Wettervorhersagen für die nächsten 48 Stunden.

### Wettervorhersagen (WeatherForecast)
- Wettervorhersage für einen bestimmten Zeitpunkt. Gehört zu einem Wetterbericht.
- Enthält Temperaturinformationen und einen Text mit einem Wort für die Wetterlage, z. B. "cloudy".

### Wetterstandorte (WeatherLocation)
- Hat Längen- und Breitengradinformationen.
- Dient dazu, den Wetterbericht und die Wettervorhersage für einen Ort von einem Webservice zu importieren.

### Bildschirme (Monitor)
- Dient zur Einstellung eines konkreten Bildschirms an einem Standort.
- Enthält eine iCal-Datei mit Terminen für die Abfallentsorgung.
- Enthält einen Link zur Abfahrstafel für den ÖPNV an diesem Standort.
- Hat ein Betreiber-Logo und ein Bildschirm-Logo als Bilder, die auf dem Bildschirm angezeigt werden.

### Bildschirmgruppen (MonitorGroup)
- Gruppiert mehrere Bildschirme.
- Hat einen Wetterstandort, um den Wetterbericht und die Vorhersage für den Standort auf den Bildschirmen anzuzeigen.
- Enthält Kontaktdaten, die auf dem Bildschirm angezeigt werden können.
- Kann mehrere Aushänge, Nachrichten und Dokumente enthalten, die auf dem Bildschirm angezeigt werden können.

### Kontakte (Contact)
- Enthält Kontaktinformationen, z. B. Hausmeister oder Hausverwaltung.

### Dokumente (Document)
- Enthält Dokumente wie beispielsweise eine Hausordnung.

### Aushänge (Announcement)
- Enthält Aushänge bzw. Bekanntmachungen wie Bauarbeiten oder andere terminbezogene Informationen.

### Entsorgungstermine (GarbageCollection)
- Datum der Abholung von Abfall.
- Typ des Abfalls: "Restmüll", "Papier", "Gelber Sack" oder "Biomüll".

### Entsorgungseinstellungen (GarbageCollectionAssignment)
- Zuordnung von Termin

namen aus der iCal-Datei zu Arten von Abfall.

### Nachrichten (NewsReport)
- Nachrichtenmeldungen mit Datum, Titel, Text, Vorschautext, Link, Kategorie und einem Bild.

### Nachrichtenkategorie (NewsCategory)
- Benannte Kategorien für Nachrichten.

### Veranstaltungen (Event)
- Kulturelle Veranstaltungen mit Datum, Uhrzeit, Ort, Link, Bild und einer externen Kennung.

### Veranstaltungsorte (EventLocation)
- Orte, an denen Veranstaltungen stattfinden.

### Veranstaltungskategorien (EventCategory)
- Benannte Kategorien für Veranstaltungen.

### ÖPNV-Einstellungen (PublicTransport)
- Informationen zum öffentlichen Nahverkehr (ÖPNV) für einen Standort.

