using BamBoard.Services.EventImport;
using BamBoard.Services.GraphQLClient;
using BamBoard.Services.Host;
using BamBoard.Services.NewsImport;
using BamBoard.Services.WarningImport;
using BamBoard.Services.WeatherImport;
using Quartz;
using Serilog;
using Importer = BamBoard.Services.WeatherImport.Importer;

var host = Host
    .CreateDefaultBuilder(args)
    .UseSerilog((context, configuration) => { configuration.ReadFrom.Configuration(context.Configuration); })
    .ConfigureServices(
        (context, services) =>
        {
            services.AddLogging();
            var bamBoardOptions = context.Configuration.GetRequiredSection("BamBoard").Get<BamBoardOptions>();
            if (bamBoardOptions == null) throw new("BamBoardOptions could not be loaded from configuration.");

            services.AddSingleton(bamBoardOptions);

            services.AddSingleton(
                new BamBoardClient(
                    new HttpClient
                    {
                        BaseAddress = new(bamBoardOptions.Url),
                        DefaultRequestHeaders =
                        {
                            { "Apollo-Require-Preflight", "true" }
                        }
                    }
                )
            );

            var cronJobOptions = context.Configuration.GetRequiredSection("CronJobs").Get<CronJobOptions>();
            if (cronJobOptions == null) throw new("CronJobOptions could not be loaded from configuration.");

            services.AddSingleton(cronJobOptions);
            var importersSection = context.Configuration.GetRequiredSection("Importers");
            if (importersSection == null) throw new("Importers section could not be loaded from configuration.");

            var eventImportOptions = importersSection.GetRequiredSection("Events").Get<EventImportOptions>();

            if (eventImportOptions == null)
                throw new("EventImportOptions could not be loaded from Importers section from configuration.");

            services.AddSingleton(eventImportOptions);
            var newsImportOptions = importersSection.GetRequiredSection("News").Get<NewsImportOptions>();

            if (newsImportOptions == null)
                throw new("NewsImportOptions could not be loaded from Importers section from configuration.");

            services.AddSingleton(newsImportOptions);
            var warningImportOptions = importersSection.GetRequiredSection("Warning").Get<WarningImportOptions>();

            if (warningImportOptions == null)
                throw new("WarningImportOptions could not be loaded from Importers section from configuration.");

            services.AddSingleton(warningImportOptions);
            var weatherImportOptions = importersSection.GetRequiredSection("Weather").Get<WeatherImportOptions>();

            if (weatherImportOptions == null)
                throw new("WeatherImportOptions could not be loaded from Importers section from configuration.");

            services.AddSingleton(weatherImportOptions);

            services
                .AddEventImporter()
                .AddNewsImporter()
                .AddScoped<BamBoard.Services.GarbageCollectionImport.Importer>()
                .AddScoped<BamBoard.Services.WarningImport.Importer>()
                .AddScoped<Importer>();

            services.AddQuartz(
                quartz =>
                {
                    quartz.UseInMemoryStore();

                    quartz
                        .AddAuthenticateJob(cronJobOptions)
                        .AddEventImportJob(cronJobOptions)
                        .AddNewsImportJob(cronJobOptions)
                        .AddWarningImportJob(cronJobOptions)
                        .AddWeatherImportJob(cronJobOptions)
                        .AddGarbageCollectionImportJob(cronJobOptions);
                }
            );

            services.AddQuartzHostedService(options => { options.WaitForJobsToComplete = true; });
        }
    )
    .Build();

host.Run();
