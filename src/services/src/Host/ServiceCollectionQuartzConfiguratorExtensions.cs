using BamBoard.Services.Host.Jobs;
using Quartz;

namespace BamBoard.Services.Host;

public static class ServiceCollectionQuartzConfiguratorExtensions
{
    private static String CronJobTrigger(String jobName)
    {
        return $"{jobName}-CronTrigger";
    }

    private static String CronJobImmediateTrigger(String jobName)
    {
        return $"{jobName}-ImmediateTrigger";
    }

    public static IServiceCollectionQuartzConfigurator AddAuthenticateJob(
        this IServiceCollectionQuartzConfigurator configurator,
        CronJobOptions cronJobOptions
    )
    {
        const String jobKey = "Authenticate";

        return configurator
            .AddJob<AuthenticateJob>(jobConfigurator => jobConfigurator.WithIdentity(jobKey))
            .AddTrigger(
                opts => opts
                    .ForJob(jobKey)
                    .WithIdentity(CronJobTrigger(jobKey))
                    .WithCronSchedule(cronJobOptions.Authenticate)
            )
            .AddTrigger(opts => opts.ForJob(jobKey).WithIdentity(CronJobImmediateTrigger(jobKey)).StartNow());
    }

    public static IServiceCollectionQuartzConfigurator AddEventImportJob(
        this IServiceCollectionQuartzConfigurator configurator,
        CronJobOptions cronJobOptions
    )
    {
        const String jobKey = "EventImport";

        return configurator
            .AddJob<EventImportJob>(jobConfigurator => jobConfigurator.WithIdentity(jobKey))
            .AddTrigger(
                opts => opts
                    .ForJob(jobKey)
                    .WithIdentity(CronJobTrigger(jobKey))
                    .WithCronSchedule(cronJobOptions.Events)
                    .StartAt(DateTimeOffset.Now.AddSeconds(10))
            )
            .AddTrigger(
                opts => opts
                    .ForJob(jobKey)
                    .WithIdentity(CronJobImmediateTrigger(jobKey))
                    .StartAt(DateTimeOffset.Now.AddSeconds(2))
            );
    }

    public static IServiceCollectionQuartzConfigurator AddNewsImportJob(
        this IServiceCollectionQuartzConfigurator configurator,
        CronJobOptions cronJobOptions
    )
    {
        const String jobKey = "NewsImport";

        return configurator
            .AddJob<NewsImportJob>(jobConfigurator => jobConfigurator.WithIdentity(jobKey))
            .AddTrigger(
                opts => opts
                    .ForJob(jobKey)
                    .WithIdentity(CronJobTrigger(jobKey))
                    .WithCronSchedule(cronJobOptions.News)
                    .StartAt(DateTimeOffset.Now.AddSeconds(10))
            )
            .AddTrigger(
                opts => opts
                    .ForJob(jobKey)
                    .WithIdentity(CronJobImmediateTrigger(jobKey))
                    .StartAt(DateTimeOffset.Now.AddSeconds(2))
            );
    }

    public static IServiceCollectionQuartzConfigurator AddWarningImportJob(
        this IServiceCollectionQuartzConfigurator configurator,
        CronJobOptions cronJobOptions
    )
    {
        const String jobKey = "WarningImport";

        return configurator
            .AddJob<WarningImportJob>(jobConfigurator => jobConfigurator.WithIdentity(jobKey))
            .AddTrigger(
                opts => opts
                    .ForJob(jobKey)
                    .WithIdentity(CronJobTrigger(jobKey))
                    .WithCronSchedule(cronJobOptions.Warning)
                    .StartAt(DateTimeOffset.Now.AddSeconds(10))
            )
            .AddTrigger(
                opts => opts
                    .ForJob(jobKey)
                    .WithIdentity(CronJobImmediateTrigger(jobKey))
                    .StartAt(DateTimeOffset.Now.AddSeconds(10))
            );
    }

    public static IServiceCollectionQuartzConfigurator AddWeatherImportJob(
        this IServiceCollectionQuartzConfigurator configurator,
        CronJobOptions cronJobOptions
    )
    {
        const String jobKey = "WeatherImport";

        return configurator
            .AddJob<WeatherImportJob>(jobConfigurator => jobConfigurator.WithIdentity(jobKey))
            .AddTrigger(
                opts => opts
                    .ForJob(jobKey)
                    .WithIdentity(CronJobTrigger(jobKey))
                    .WithCronSchedule(cronJobOptions.Weather)
                    .StartAt(DateTimeOffset.Now.AddSeconds(10))
            );
    }

    public static IServiceCollectionQuartzConfigurator AddGarbageCollectionImportJob(
        this IServiceCollectionQuartzConfigurator configurator,
        CronJobOptions cronJobOptions
    )
    {
        const String jobKey = "GarbageCollectionImport";

        return configurator
            .AddJob<GarbageCollectionImportJob>(jobConfigurator => jobConfigurator.WithIdentity(jobKey))
            .AddTrigger(
                opts => opts
                    .ForJob(jobKey)
                    .WithIdentity(CronJobTrigger(jobKey))
                    .WithCronSchedule(cronJobOptions.GarbageCollection)
                    .StartAt(DateTimeOffset.Now.AddSeconds(10))
            );
    }
}
