﻿namespace BamBoard.Services.Host;

public record CronJobOptions(
    String Authenticate,
    String Events,
    String News,
    String GarbageCollection,
    String Weather,
    String Warning
);
