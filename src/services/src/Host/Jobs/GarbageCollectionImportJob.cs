﻿using BamBoard.Services.GarbageCollectionImport;
using Quartz;

namespace BamBoard.Services.Host.Jobs;

internal class GarbageCollectionImportJob: IJob
{
    private readonly Importer importer;

    public GarbageCollectionImportJob( Importer importer )
    {
        this.importer = importer;
    }

    public Task Execute( IJobExecutionContext context )
    {
        return importer.RunAsync( context.CancellationToken );
    }
}
