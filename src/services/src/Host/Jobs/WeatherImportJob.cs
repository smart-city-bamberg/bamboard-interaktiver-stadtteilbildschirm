﻿using BamBoard.Services.WeatherImport;
using Quartz;

namespace BamBoard.Services.Host.Jobs;

internal class WeatherImportJob: IJob
{
    private readonly Importer importer;

    public WeatherImportJob( Importer importer )
    {
        this.importer = importer;
    }

    public Task Execute( IJobExecutionContext context )
    {
        return importer.RunAsync( context.CancellationToken );
    }
}
