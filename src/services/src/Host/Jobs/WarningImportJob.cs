﻿using BamBoard.Services.WarningImport;
using Quartz;

namespace BamBoard.Services.Host.Jobs;

internal class WarningImportJob : IJob
{
    private readonly Importer importer;

    public WarningImportJob(Importer importer)
    {
        this.importer = importer;
    }

    public Task Execute(IJobExecutionContext context)
    {
        return importer.RunAsync(context.CancellationToken);
    }
}
