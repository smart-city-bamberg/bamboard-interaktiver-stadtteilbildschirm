﻿using BamBoard.Services.EventImport;
using Quartz;

namespace BamBoard.Services.Host.Jobs;

internal class EventImportJob: IJob
{
    private readonly Importer importer;

    public EventImportJob( Importer importer )
    {
        this.importer = importer;
    }

    public Task Execute( IJobExecutionContext context )
    {       
        return importer.RunAsync( context.CancellationToken );
    }
}
