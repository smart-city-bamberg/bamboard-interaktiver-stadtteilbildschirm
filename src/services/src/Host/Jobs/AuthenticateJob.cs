﻿using BamBoard.Services.GraphQLClient;
using Quartz;
using ZeroQL;

internal class AuthenticateJob : IJob
{
    private readonly BamBoardClient bamboardClient;
    private readonly BamBoardOptions bamBoardOptions;

    public AuthenticateJob(BamBoardClient bamboardClient, BamBoardOptions bamBoardOptions)
    {
        this.bamboardClient = bamboardClient;
        this.bamBoardOptions = bamBoardOptions;
    }

    public Task Execute(IJobExecutionContext context)
    {
        return AuthenticateAsync(bamBoardOptions.User, bamBoardOptions.Password, bamboardClient);
    }

    private static async Task AuthenticateAsync(String userName, String password, BamBoardClient bamboardClient)
    {
        var authenticationResult = await bamboardClient.Mutation(
            mutation => mutation.AuthenticateUserWithPassword(
                userName,
                password,
                result => new
                {
                    Failure = result
                        .On<UserAuthenticationWithPasswordFailure>()
                        .Select(failure => new { failure.Message }),
                    Success = result
                        .On<UserAuthenticationWithPasswordSuccess>()
                        .Select(success => new { success.SessionToken })
                }
            )
        );

        if (authenticationResult.Data?.Failure != null)
            throw new($"Could not authenticate: {authenticationResult.Data.Failure.Message}");
    }
}
