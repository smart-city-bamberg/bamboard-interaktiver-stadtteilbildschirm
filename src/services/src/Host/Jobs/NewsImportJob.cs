﻿using BamBoard.Services.NewsImport;
using Quartz;

namespace BamBoard.Services.Host.Jobs;

internal class NewsImportJob : IJob
{
    private readonly Importer importer;

    public NewsImportJob(Importer importer)
    {
        this.importer = importer;
    }

    public Task Execute(IJobExecutionContext context)
    {
        return importer.RunAsync(context.CancellationToken);
    }
}
