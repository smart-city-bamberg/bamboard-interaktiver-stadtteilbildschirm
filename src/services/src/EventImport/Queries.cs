using System.Text.Json;
using BamBoard.Services.EventImport.Models;
using BamBoard.Services.GraphQLClient;
using Microsoft.Extensions.Logging;
using ZeroQL;

namespace BamBoard.Services.EventImport;

public class Queries(BamBoardClient bamboardClient, ILogger<Queries> logger)
{
    public async Task<GraphQLResult<EventProvider[]>> LoadEventProviders(
        CancellationToken cancellationToken,
        ID organizationId
    )
    {
        var where = new EventProviderWhereInput { Organization = new() { Id = new() { Equals = organizationId } } };
        var orderBy = Array.Empty<EventProviderOrderByInput>();
        logger.LogLoadingEventProviders(organizationId);

        var dataProvidersResult = await bamboardClient.Query(
            query => query.EventProviders(
                where,
                orderBy,
                null,
                0,
                null,
                provider => new EventProvider
                {
                    Id = provider.Id, Name = provider.Name, FeedType = provider.FeedType,
                    Configuration = provider.Configuration
                }
            ),
            cancellationToken
        );

        if (dataProvidersResult.Errors != null)
            logger.LogCouldNotReadEventProviders(JsonSerializer.Serialize(dataProvidersResult.Errors));
        else
            logger.LogLoadedDataProviders(organizationId, JsonSerializer.Serialize(dataProvidersResult.Data));

        return dataProvidersResult;
    }

    public async Task<GraphQLResult<ID[]>> LoadOrganizations(CancellationToken cancellationToken)
    {
        var filter = new OrganizationWhereInput();
        var orderBy = Array.Empty<OrganizationOrderByInput>();

        var graphQlResult = await bamboardClient.Query(
            query => query.Organizations(filter, orderBy, null, 0, null, organization => organization.Id),
            cancellationToken
        );

        if (graphQlResult.Errors != null)
            logger.LogCouldNotLoadOrganizations(JsonSerializer.Serialize(graphQlResult.Errors));
        else
            logger.LogReadOrganizations(graphQlResult.Data!.Length);

        return graphQlResult;
    }

    public async Task<GraphQLResult<EventIdPair[]>> LoadImportedEventIds(String organizationId, String eventProviderId)
    {
        var filter = new EventWhereInput
        {
            ExternalId = new()
            {
                Not = new()
                {
                    Equals = ""
                }
            },
            Organization = new()
            {
                Id = new()
                {
                    Equals = organizationId
                }
            },
            EventProvider = new()
            {
                Id = new()
                {
                    Equals = eventProviderId
                }
            }
        };

        var orderBy = Array.Empty<EventOrderByInput>();
        logger.LogImportingEvents(organizationId);

        var graphQlResult = await bamboardClient.Query(
            query => query.Events(
                filter,
                orderBy,
                null,
                0,
                null,
                @event => new EventIdPair(@event.Id, @event.ExternalId)
            )
        );

        if (graphQlResult.Errors != null)
            logger.LogCouldNotReadEvents(JsonSerializer.Serialize(graphQlResult.Errors));
        else
            logger.LogReadExternalEventIds(graphQlResult.Data.Length);

        return graphQlResult;
    }

    public async Task<GraphQLResult<EventIdPair[]>> LoadOldEventIds(String organizationId, DateTime olderThan)
    {
        var filter = new EventWhereInput
        {
            Organization = new()
            {
                Id = new() { Equals = organizationId }
            },
            Date = new()
            {
                Lt = new()
                {
                    Value = olderThan.ToString("yyyy-MM-dd")
                }
            }
        };

        var orderBy = new EventOrderByInput[]
        {
            new() { Date = OrderDirection.Asc }
        };

        var graphQlResult = await bamboardClient.Query(
            query => query.Events(
                filter,
                orderBy,
                null,
                0,
                null,
                @event => new EventIdPair(@event.Id, @event.ExternalId)
            )
        );

        if (graphQlResult.Errors != null)
            logger.LogCouldNotReadOldEvents(olderThan, JsonSerializer.Serialize(graphQlResult.Errors));
        else
            logger.LogReadOldEvents(graphQlResult.Data.Length, olderThan);

        return graphQlResult;
    }
}
