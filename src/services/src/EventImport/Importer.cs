using System.Globalization;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Xml.Linq;
using System.Xml.Serialization;
using BamBoard.Services.EventImport.Models;
using BamBoard.Services.GraphQLClient;
using Microsoft.Extensions.Logging;
using ZeroQL;

namespace BamBoard.Services.EventImport;

public class Importer(
    EventImportOptions eventImportOptions,
    Queries queries,
    Mutations mutations,
    HttpClient httpClient,
    ILogger<Importer> logger
)
{
    public async Task RunAsync(CancellationToken cancellationToken)
    {
        var organizationsResult = await queries.LoadOrganizations(cancellationToken);
        if (organizationsResult.Errors != null) return;

        foreach (var organizationId in organizationsResult.Data ?? [])
        {
            var eventProvidersResult = await queries.LoadEventProviders(cancellationToken, organizationId);
            if (eventProvidersResult.Errors != null) return;

            foreach (var eventProvider in eventProvidersResult.Data ?? [])
            {
                var existingEventIdsResult = await queries.LoadImportedEventIds(organizationId, eventProvider.Id);
                if (existingEventIdsResult.Errors != null) return;

                try
                {
                    var veranstaltungen = eventProvider.FeedType switch
                    {
                        "bamberg_kuba" => await LoadVeranstaltungenFromKulturBamberg(eventProvider),
                        "vibus" => await LoadVeranstaltungenFromVibus(eventProvider),
                        _ => []
                    };

                    await CreateEvents(veranstaltungen, existingEventIdsResult, eventProvider.Id, organizationId);
                    await DeleteAbandonedEvents(veranstaltungen, existingEventIdsResult);
                    await DeleteExpiredEvents(organizationId);
                }
                catch (Exception exception)
                {
                    logger.LogError(exception, "Error while loading events from provider {0}", eventProvider.Id);
                }
            }
        }
    }

    private async Task DeleteExpiredEvents(ID organizationId)
    {
        var olderThan = DateTime.Now.Subtract(eventImportOptions.DeleteThreshold);
        var oldEventsResult = await queries.LoadOldEventIds(organizationId, olderThan);
        if (oldEventsResult.Errors != null) return;

        var oldEventIdsToDelete = oldEventsResult.Data.Select(@event => @event.Id).ToArray();
        await mutations.DeleteEvents(oldEventIdsToDelete);
    }

    private async Task DeleteAbandonedEvents(
        Veranstaltung[] veranstaltungen,
        GraphQLResult<EventIdPair[]> existingEventIdsResult
    )
    {
        var eventIdsToDelete = existingEventIdsResult
            .Data.Where(
                pair => !String.IsNullOrWhiteSpace(pair.ExternalId) &&
                    veranstaltungen.All(
                        veranstaltung => pair.ExternalId != Convert.ToString(veranstaltung.VeranstaltungId)
                    )
            )
            .Select(pair => pair.Id)
            .ToArray();

        await mutations.DeleteEvents(eventIdsToDelete);
    }

    private async Task CreateEvents(
        Veranstaltung[] veranstaltungen,
        GraphQLResult<EventIdPair[]> existingEventIdsResult,
        String eventProviderId,
        ID organizationId
    )
    {
        var veranstaltungenToCreate = veranstaltungen
            .Where(veranstaltung => !ExternalEventExists(existingEventIdsResult.Data, veranstaltung))
            .ToArray();

        logger.LogCreatingEvents(veranstaltungenToCreate.Length);

        foreach (var veranstaltung in veranstaltungenToCreate)
            await CreateEventAsync(organizationId, eventProviderId, veranstaltung);
    }

    private static Boolean ExternalEventExists(EventIdPair[] existingEventIds, Veranstaltung veranstaltung)
    {
        return existingEventIds.Any(existing => existing.ExternalId == Convert.ToString(veranstaltung.VeranstaltungId));
    }

    public async Task CreateEventAsync(String organizationId, String eventProviderId, Veranstaltung veranstaltung)
    {
        Stream imageStream = default;
        var imageUrlValid = Uri.TryCreate(veranstaltung.Bild1, UriKind.Absolute, out var imageUri);

        if (imageUrlValid)
            try
            {
                var hasImageProxy = !String.IsNullOrEmpty(eventImportOptions.ImageProxy);
                var imageUrl = hasImageProxy ? $"{eventImportOptions.ImageProxy}/{imageUri}" : imageUri.AbsoluteUri;
                imageStream = await httpClient.GetStreamAsync(imageUrl);
                logger.LogDownloadedEventImage(imageUrl);
            }
            catch (Exception exception)
            {
                logger.LogEventImageNotDownloaded(exception.Message);
            }

        var date = veranstaltung.Datum.ToString("yyyy-MM-dd");

        var graphQlResult = await mutations.CreateEvent(
            organizationId,
            eventProviderId,
            imageStream,
            veranstaltung.Titel,
            veranstaltung.Kategorie,
            veranstaltung.Ort,
            date,
            veranstaltung.Uhrzeit != null ? veranstaltung.Uhrzeit?.ToString("HH:mm") : "n.v.",
            veranstaltung.VeranstaltungId,
            veranstaltung.Link
        );

        if (graphQlResult.Errors != null) logger.LogCouldNotCreateEvent(JsonSerializer.Serialize(graphQlResult.Errors));
        logger.LogCreatedEvent();
        if (imageStream != null) await imageStream.DisposeAsync();
        if (imageUrlValid) await Task.Delay(100);
    }

    private async Task<Veranstaltung[]> LoadVeranstaltungenFromKulturBamberg(EventProvider eventProvider)
    {
        var jsonConfiguration = JsonSerializer.Deserialize<KulturBambergConfig>(
            eventProvider.Configuration,
            new JsonSerializerOptions { PropertyNameCaseInsensitive = true }
        );

        using var stream = await httpClient.GetStreamAsync(jsonConfiguration.FeedUrl);
        var serializer = new XmlSerializer(typeof(KubaExport));
        var kubaExport = (KubaExport)serializer.Deserialize(stream);
        logger.LogReadEvents(kubaExport.Veranstaltungen.Count);

        return kubaExport
            .Veranstaltungen.Select(
                veranstaltung => new Veranstaltung
                {
                    VeranstaltungId = veranstaltung.VeranstaltungId,
                    Titel = veranstaltung.Titel,
                    Beschreibung = veranstaltung.Beschreibung,
                    Datum = DateOnly.ParseExact(veranstaltung.Datum, "dd.MM.yyyy", CultureInfo.InvariantCulture),
                    Uhrzeit = TimeOnly.ParseExact(veranstaltung.Uhrzeit, "HH:mm", CultureInfo.InvariantCulture),
                    Enddatum = DateOnly.ParseExact(veranstaltung.Enddatum, "dd.MM.yyyy", CultureInfo.InvariantCulture),
                    Ort = veranstaltung.Ort,
                    Veranstalter = veranstaltung.Veranstalter,
                    Link = $"https://kultur.bamberg.de/_plaza/kuba.cfm?veranstaltungid={veranstaltung.VeranstaltungId}",
                    Bild1 = veranstaltung.Bild1,
                    Bild2 = veranstaltung.Bild2,
                    Kategorie = veranstaltung.Kategorie
                }
            )
            .ToArray();
    }

    private async Task<Veranstaltung[]> LoadVeranstaltungenFromVibus(EventProvider eventProvider)
    {
        var vibusJsonConfiguration = JsonSerializer.Deserialize<VibusConfig>(
            eventProvider.Configuration,
            new JsonSerializerOptions { PropertyNameCaseInsensitive = true }
        );

        var parameters = Vibus.BuildParameters(
            vibusJsonConfiguration.UserId,
            vibusJsonConfiguration.Benutzer,
            vibusJsonConfiguration.Passwort
        );

        var sParamRendered = Vibus.BuildSParamXml(parameters);
        var encodedXmlString = Vibus.EncodeXml(sParamRendered);
        var xmlPayload = Vibus.BuildXmlPayload(encodedXmlString);

        var httpRequestMessage = new HttpRequestMessage
        {
            Method = HttpMethod.Post,
            Content = new StringContent(xmlPayload, new MediaTypeHeaderValue("text/xml", "utf-8")),
            RequestUri = new("http://xmlapi.vibus.de/center/xml.asmx"),
            Headers =
            {
                { "SOAPAction", "\"http://www.vibus.de/ticketportal/GetVeranstaltungsListeXml\"" }
            }
        };

        var httpResponseMessage = await httpClient.SendAsync(httpRequestMessage);
        var httpContent = await httpResponseMessage.Content.ReadAsStringAsync();
        var xmlDocument = XDocument.Parse(httpContent);
        var xmlNamespace = "http://www.vibus.de/ticketportal/";

        var events = xmlDocument
            .Descendants(XName.Get("GetVeranstaltungsListeXmlResult", xmlNamespace))
            .SelectMany(result => XDocument.Parse(result.Value).Descendants("EVENT"))
            .Select(
                e => new Veranstaltung
                {
                    VeranstaltungId = e.Element("ID")?.Value,
                    Titel = e.Element("TITLE")?.Value,
                    Beschreibung = e.Element("DESCRIPTION")?.Value,
                    Datum = DateOnly.ParseExact(
                        e.Element("PUBDATE")?.Value,
                        "dd.MM.yyyy HH:mm:ss",
                        CultureInfo.InvariantCulture
                    ),
                    Uhrzeit = TimeOnly.ParseExact(
                        e.Element("PUBDATE")?.Value,
                        "dd.MM.yyyy HH:mm:ss",
                        CultureInfo.InvariantCulture
                    ),
                    Enddatum = DateOnly.ParseExact(
                        e.Element("DATETO")?.Value,
                        "dd.MM.yyyy HH:mm:ss",
                        CultureInfo.InvariantCulture
                    ),
                    Ort = e.Element("LOCATION")?.Value,
                    Veranstalter = e.Element("ANBIETER")?.Value,
                    Link = e.Element("LINK")?.Value
                }
            )
            .ToArray();

        logger.LogReadEvents(events.Length);
        return events;
    }
}

internal class KulturBambergConfig
{
    public String FeedUrl { get; set; }
}

internal class VibusConfig
{
    public Int32 UserId { get; set; }
    public String Benutzer { get; set; }
    public String Passwort { get; set; }
}
