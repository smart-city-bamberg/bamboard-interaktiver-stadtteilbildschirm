using System.Text.Json;
using BamBoard.Services.GraphQLClient;
using Microsoft.Extensions.Logging;
using ZeroQL;

namespace BamBoard.Services.EventImport;

public class Mutations(BamBoardClient bamBoardClient, ILogger<Mutations> logger)
{
    public async Task<GraphQLResult<ID>> CreateEvent(
        String organizationId,
        String evnetProviderId,
        Stream imageStream,
        String title,
        String category,
        String location,
        String date,
        String time,
        String externalId,
        String link
    )
    {
        var eventCreateInput = new EventCreateInput
        {
            Title = title,
            Category = category,
            Location = location,
            Date = date,
            Time = time,
            ExternalId = externalId,
            Link = link,
            Organization = new() { Connect = new() { Id = organizationId } }
        };

        if (!String.IsNullOrEmpty(evnetProviderId))
            eventCreateInput.EventProvider = new() { Connect = new() { Id = evnetProviderId } };

        if (imageStream != null)
            eventCreateInput.Image = new()
            {
                Upload = new("image.jpg", imageStream)
            };

        var graphQlResult
            = await bamBoardClient.Mutation(mutation => mutation.CreateEvent(eventCreateInput, @event => @event.Id));

        return graphQlResult;
    }

    public async Task<GraphQLResult<ID[]>> DeleteEvents(String[] eventIds)
    {
        var filter = eventIds.Select(id => new EventWhereUniqueInput { Id = id }).ToArray();

        var graphQlResult
            = await bamBoardClient.Mutation(mutation => mutation.DeleteEvents(filter, @event => @event.Id));

        if (graphQlResult.Errors != null)
            logger.LogCouldNotDeleteEvents(JsonSerializer.Serialize(graphQlResult.Errors));
        else
            logger.LogDeletedEvents(graphQlResult.Data.Length);

        return graphQlResult;
    }
}
