﻿namespace BamBoard.Services.EventImport;

public record EventImportOptions(TimeSpan DeleteThreshold, String ImageProxy);
