﻿namespace BamBoard.Services.EventImport.Models;

public record EventIdPair(String Id, String ExternalId);
