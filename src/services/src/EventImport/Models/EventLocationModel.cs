﻿using ZeroQL;

internal record EventLocationModel(
    ID Id, String Name, Int32? EventCount = 0
);

