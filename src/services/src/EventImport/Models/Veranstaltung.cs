public class Veranstaltung
{
    public String VeranstaltungId { get; set; }

    public String Titel { get; set; }

    public String Untertitel { get; set; }

    public String Beschreibung { get; set; }

    public String Kategorie { get; set; }

    public String Rubrik { get; set; }

    public DateOnly Datum { get; set; }

    public TimeOnly? Uhrzeit { get; set; }

    public String Ort { get; set; }

    public String Anschrift { get; set; }

    public String PLZ { get; set; }

    public String Stadt { get; set; }

    public String Veranstalter { get; set; }

    public String Bild1 { get; set; }

    public String Bild2 { get; set; }

    public DateOnly Enddatum { get; set; }

    public String Link { get; set; }
}
