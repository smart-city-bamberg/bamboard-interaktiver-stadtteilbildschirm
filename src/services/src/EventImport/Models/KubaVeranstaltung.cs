﻿using System.Xml.Serialization;

public class KubaVeranstaltung
{
    [XmlElement("veranstaltung_id")]
    public String VeranstaltungId { get; set; }

    [XmlElement("titel")]
    public String Titel { get; set; }

    [XmlElement("untertitel")]
    public String Untertitel { get; set; }

    [XmlElement("beschreibung")]
    public String Beschreibung { get; set; }

    [XmlElement("kategorie")]
    public String Kategorie { get; set; }

    [XmlElement("rubrik")]
    public String Rubrik { get; set; }

    [XmlElement("datum")]
    public String Datum { get; set; }

    [XmlElement("uhrzeit")]
    public String Uhrzeit { get; set; }

    [XmlElement("ort")]
    public String Ort { get; set; }

    [XmlElement("anschrift")]
    public String Anschrift { get; set; }

    [XmlElement("plz")]
    public String PLZ { get; set; }

    [XmlElement("stadt")]
    public String Stadt { get; set; }

    [XmlElement("veranstalter")]
    public String Veranstalter { get; set; }

    [XmlElement("bild1")]
    public String Bild1 { get; set; }

    [XmlElement("bild2")]
    public String Bild2 { get; set; }

    [XmlElement("enddatum")]
    public String Enddatum { get; set; }
}
