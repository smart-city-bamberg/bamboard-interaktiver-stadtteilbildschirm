﻿using System.Xml.Serialization;

[XmlRoot("kubaexport")]
public class KubaExport
{
    [XmlElement("veranstaltung")]
    public List<KubaVeranstaltung> Veranstaltungen { get; set; }
}
