﻿using ZeroQL;

internal record EventCategoryModel(
    ID Id, String Name, Int32? EventCount = 0
);

