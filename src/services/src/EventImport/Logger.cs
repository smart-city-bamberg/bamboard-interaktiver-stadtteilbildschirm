using Microsoft.Extensions.Logging;

namespace BamBoard.Services.EventImport;

public static partial class Logger
{
    [LoggerMessage(Level = LogLevel.Information, Message = "Read {amount} Veranstaltungen from XML feed.")]
    public static partial void LogReadEvents(this ILogger logger, Int32 amount);

    [LoggerMessage(Level = LogLevel.Error, Message = "Could not load organizations: {errors}")]
    public static partial void LogCouldNotLoadOrganizations(this ILogger logger, String errors);

    [LoggerMessage(Level = LogLevel.Information, Message = "Read {amount} organizations.")]
    public static partial void LogReadOrganizations(this ILogger logger, Int32 amount);

    [LoggerMessage(Level = LogLevel.Information, Message = "Loading event providers for organization {organization}.")]
    public static partial void LogLoadingEventProviders(this ILogger logger, String organization);

    [LoggerMessage(Level = LogLevel.Error, Message = "Could not read event providers: {errors}")]
    public static partial void LogCouldNotReadEventProviders(this ILogger logger, String errors);

    [LoggerMessage(
        Level = LogLevel.Information,
        Message = "Loaded data providers for organization {organization}, providers: {providers}"
    )]
    public static partial void LogLoadedDataProviders(this ILogger logger, String organization, String providers);

    [LoggerMessage(Level = LogLevel.Information, Message = "Importing events for organization {organization}.")]
    public static partial void LogImportingEvents(this ILogger logger, String organization);

    [LoggerMessage(Level = LogLevel.Error, Message = "Could not read events: {errors}")]
    public static partial void LogCouldNotReadEvents(this ILogger logger, String errors);

    [LoggerMessage(Level = LogLevel.Information, Message = "Read {amount} external event ids.")]
    public static partial void LogReadExternalEventIds(this ILogger logger, Int32 amount);

    [LoggerMessage(Level = LogLevel.Information, Message = "Creating {eventCount} events")]
    public static partial void LogCreatingEvents(this ILogger logger, Int32 eventCount);

    [LoggerMessage(Level = LogLevel.Error, Message = "Could not create event: {errors}")]
    public static partial void LogCouldNotCreateEvent(this ILogger logger, String errors);

    [LoggerMessage(Level = LogLevel.Information, Message = "Created event.")]
    public static partial void LogCreatedEvent(this ILogger logger);

    [LoggerMessage(Level = LogLevel.Information, Message = "Deleted {amount} events.")]
    public static partial void LogDeletedEvents(this ILogger logger, Int32 amount);

    [LoggerMessage(Level = LogLevel.Error, Message = "Could not delete events: {errors}")]
    public static partial void LogCouldNotDeleteEvents(this ILogger logger, String errors);

    [LoggerMessage(Level = LogLevel.Information, Message = "Read {amount} events older than {olderThan}.")]
    public static partial void LogReadOldEvents(this ILogger logger, Int32 amount, DateTime olderThan);

    [LoggerMessage(Level = LogLevel.Error, Message = "Could not read events older than {olderThan}: {errors}")]
    public static partial void LogCouldNotReadOldEvents(this ILogger logger, DateTime olderThan, String errors);

    [LoggerMessage(Level = LogLevel.Information, Message = "Deleted {amount} events older than {olderThan}.")]
    public static partial void LogDeletedOldEvents(this ILogger logger, Int32 amount, DateTime olderThan);

    [LoggerMessage(Level = LogLevel.Information, Message = "Downloaded event image from {url}")]
    public static partial void LogDownloadedEventImage(this ILogger logger, String url);

    [LoggerMessage(Level = LogLevel.Warning, Message = "Event image could not be downloaded: {error}")]
    public static partial void LogEventImageNotDownloaded(this ILogger logger, String error);
}
