using Microsoft.Extensions.DependencyInjection;

namespace BamBoard.Services.EventImport;

public static class Configuration
{
    public static IServiceCollection AddEventImporter(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddHttpClient<Importer>("EventImporter");
        return serviceCollection.AddScoped<Queries>().AddScoped<Mutations>().AddScoped<Importer>();
    }
}
