using System.Text;

namespace BamBoard.Services.EventImport;

public static class Vibus
{
    public static String BuildSParamXml((String name, Object value)[] valueTuples)
    {
        var sParam = new StringBuilder();
        sParam.Append("<vtxml>");
        foreach (var parameter in valueTuples) sParam.Append(BuildParameter(parameter.name, parameter.value));
        sParam.Append("</vtxml>");
        var sParamRendered1 = sParam.ToString();
        return sParamRendered1;

        String BuildParameter(String name, Object value)
        {
            var encoded = value switch
            {
                String s => Convert.ToBase64String(Encoding.UTF8.GetBytes(s)),
                Int32 i => Convert.ToString(i),
                Boolean b => b ? "1" : "0",
                DateTime d => d.ToString("dd.MM.yyyy HH:mm:ss")
            };

            return $"<pa name=\"{name}\">{value.GetType().FullName};{encoded}</pa>";
        }
    }

    public static String EncodeXml(String input)
    {
        return input.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;");
    }

    public static (String name, Object value)[] BuildParameters(Int32 userId, String benutzer, String passwort)
    {
        var xmlTemplate = """
                          <TITLE><![CDATA[<Bezeichnung1>]]></TITLE>
                          <DESCRIPTION><![CDATA[<Info>]]></DESCRIPTION>
                          <PUBDATE><![CDATA[<Datum> <Beginn>:00]]></PUBDATE>
                          <DATETO><![CDATA[<Ende>:00]]></DATETO>
                          <LOCATION><![CDATA[<Ort> <Haus>]]></LOCATION>
                          <ANBIETER><![CDATA[<AnwenderName>]]></ANBIETER>
                          <ID><![CDATA[<VstKey>]]></ID>
                          <BILDER><bildliste></BILDER>
                          <LINK><![CDATA[<VstHref>]]></LINK>
                          """;

        (String name, Object value)[] valueTuples =
        [
            (name: "passwort", value: passwort),
            (name: "xmltemplate", value: xmlTemplate),
            (name: "xmlstarttag", value: "EVENTS"),
            (name: "userid", value: userId),
            (name: "xmllooptag", value: "EVENT"),
            (name: "benutzer", value: benutzer),
            (name: "utf8", value: 0),
            (name: "xmlroottag", value: "VSTLISTE"),
            (name: "templatekey", value: $"00{userId:D}000000")
        ];

        return valueTuples;
    }

    public static String BuildXmlPayload(String encodedXmlString1)
    {
        return $"""
                <?xml version="1.0" encoding="utf-8"?>
                <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
                    <soap:Body>
                        <GetVeranstaltungsListeXml xmlns="http://www.vibus.de/ticketportal/">
                            <sParam>{encodedXmlString1}</sParam>
                        </GetVeranstaltungsListeXml>
                    </soap:Body>
                </soap:Envelope>
                """;
    }
}
