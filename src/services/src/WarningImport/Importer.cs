﻿using System.Text.Json;
using BamBoard.Services.GraphQLClient;
using Microsoft.Extensions.Logging;
using WarningImport;
using ZeroQL;

namespace BamBoard.Services.WarningImport;

public class Importer
{
    private readonly BamBoardClient bamboardClient;
    private readonly ILogger<Importer> logger;
    private readonly WarningImportOptions warningImportOptions;

    public Importer(BamBoardClient bamboardClient, WarningImportOptions warningImportOptions, ILogger<Importer> logger)
    {
        this.bamboardClient = bamboardClient;
        this.warningImportOptions = warningImportOptions;
        this.logger = logger;
    }

    public async Task RunAsync(CancellationToken cancellationToken)
    {
        var date = DateTime.Now;
        var httpClient = new HttpClient();

        var warningClient = new NinaApi(httpClient)
        {
            BaseUrl = warningImportOptions.NinaServiceUrl
        };

        var monitorGroupsResult = await LoadMonitorGroupModelsAsync(bamboardClient);

        if (monitorGroupsResult.Errors != null)
        {
            logger.LogError(
                "Could not read monitor groups: {errors}",
                JsonSerializer.Serialize(monitorGroupsResult.Errors)
            );

            return;
        }

        logger.LogInformation("Read {amount} monitor groups.", monitorGroupsResult.Data.Length);

        foreach (var monitorGroupModel in monitorGroupsResult.Data)
        {
            var existingWarnings = await LoadExistingWarningsAsync(cancellationToken, monitorGroupModel);

            if (existingWarnings.Errors != null)
            {
                logger.LogError(
                    "Could not read existing warnings for monitor group {group}:, error: {errors}",
                    monitorGroupModel.GroupId,
                    JsonSerializer.Serialize(monitorGroupsResult.Errors)
                );

                continue;
            }

            logger.LogInformation(
                "Read {amount} existing warnings for monitor group {group}",
                existingWarnings.Data.Length,
                monitorGroupModel.GroupId
            );

            var ninaWarnings = await warningClient.GetDashboardAsync(
                monitorGroupModel.GroupOfficialRegionalKey,
                cancellationToken
            );

            logger.LogInformation(
                "Read {amount} new warnings for monitor group {group}",
                ninaWarnings.Count,
                monitorGroupModel.GroupId
            );

            var warningsToDelete = existingWarnings
                .Data.Where(warning => ninaWarnings.All(ninaWarning => ninaWarning.Id != warning.WarningExternalId))
                .ToArray();

            logger.LogInformation(
                "Found {amount} warnings to delete for monitor group {group}",
                warningsToDelete.Length,
                monitorGroupModel.GroupId
            );

            foreach (var ninaWarning in ninaWarnings)
            {
                var existingWarning
                    = existingWarnings.Data.FirstOrDefault(warning => warning.WarningExternalId == ninaWarning.Id);

                if (existingWarning == null)
                    await CreateWarningAsync(bamboardClient, ninaWarning, monitorGroupModel, cancellationToken);
                else
                    await UpdateWarningAsync(bamboardClient, ninaWarning, cancellationToken);
            }

            var deleteInput = warningsToDelete
                .Select(
                    warning => new WarningWhereUniqueInput
                    {
                        Id = warning.WarningId
                    }
                )
                .ToArray();

            var deleteWarningsResult = await bamboardClient.Mutation(
                mutation => mutation.DeleteWarnings(deleteInput, warning => warning.Id),
                cancellationToken
            );

            if (deleteWarningsResult.Errors != null)
            {
                logger.LogError(
                    "Could not delete warnings for monitor group: {group}, error: {errors}",
                    monitorGroupModel.GroupId,
                    JsonSerializer.Serialize(deleteWarningsResult.Errors)
                );

                continue;
            }

            logger.LogInformation(
                "Deleted {amount} warnings for monitor group {group}",
                deleteWarningsResult.Data.Length,
                monitorGroupModel.GroupId
            );
        }
    }

    private async Task<GraphQLResult<WarningModel[]>> LoadExistingWarningsAsync(
        CancellationToken cancellationToken,
        MonitorGroupModel monitorGroupModel
    )
    {
        var warningWhereInput = new WarningWhereInput
        {
            MonitorGroup = new()
            {
                Every = new()
                {
                    Id = new()
                    {
                        Equals = monitorGroupModel.GroupId
                    }
                }
            }
        };

        var warningOrderByInputs = Array.Empty<WarningOrderByInput>();

        var existingWarnings = await bamboardClient.Query(
            query => query.Warnings(
                warningWhereInput,
                warningOrderByInputs,
                null,
                0,
                null,
                warning => new WarningModel(warning.Id, warning.ExternalId)
            ),
            cancellationToken
        );

        return existingWarnings;
    }

    private static async Task<GraphQLResult<ID?[]?>> UpdateWarningAsync(
        BamBoardClient bamboardClient,
        Anonymous ninaWarning,
        CancellationToken cancellationToken
    )
    {
        var updateInput = new WarningUpdateInput
        {
            Message = ninaWarning.I18nTitle.De,
            Timestamp = ninaWarning.Sent
        };

        var where = new WarningUpdateArgs[]
        {
            new()
            {
                Data = updateInput,
                Where = new()
                {
                    Id = ninaWarning.Id
                }
            }
        };

        return await bamboardClient.Mutation(
            mutation => mutation.UpdateWarnings(where, warning => warning.Id),
            cancellationToken
        );
    }

    private static async Task<GraphQLResult<ID?>> CreateWarningAsync(
        BamBoardClient bamboardClient,
        Anonymous ninaWarning,
        MonitorGroupModel monitorGroupModel,
        CancellationToken cancellationToken
    )
    {
        var createInput = new WarningCreateInput
        {
            Message = ninaWarning.I18nTitle.De,
            Timestamp = ninaWarning.Sent,
            ExternalId = ninaWarning.Id,
            Organization = new() { Connect = new() { Id = monitorGroupModel.Organization } },
            MonitorGroup = new()
            {
                Connect = new[] { new MonitorGroupWhereUniqueInput { Id = monitorGroupModel.GroupId } }
            }
        };

        return await bamboardClient.Mutation(
            mutation => mutation.CreateWarning(createInput, warning => warning.Id),
            cancellationToken
        );
    }

    private static Task<GraphQLResult<MonitorGroupModel[]>> LoadMonitorGroupModelsAsync(BamBoardClient bamboardClient)
    {
        var monitorFilter = new MonitorGroupWhereInput
        {
            OfficialRegionalKey = new()
            {
                Not = new()
                {
                    Equals = ""
                }
            }
        };

        var orderBy = Array.Empty<MonitorGroupOrderByInput>();

        return bamboardClient.Query(
            query => query.MonitorGroups(
                monitorFilter,
                orderBy,
                null,
                0,
                null,
                group => new MonitorGroupModel(
                    group.Id,
                    group.Name,
                    group.Organization(organization => organization.Id),
                    group.OfficialRegionalKey
                )
            )
        );
    }
}

public record WarningModel(String WarningId, String? WarningExternalId);

internal record MonitorGroupModel(
    String? GroupId,
    String? GroupName,
    ID Organization,
    String? GroupOfficialRegionalKey
);
