﻿using System.Globalization;
using System.Net.Http.Json;
using System.Text.Json;
using BamBoard.Services.GraphQLClient;
using BamBoard.Services.WeatherImport.Models;
using Microsoft.Extensions.Logging;
using ZeroQL;
using WeatherForecast = BamBoard.Services.WeatherImport.Models.WeatherForecast;
using WeatherReport = BamBoard.Services.WeatherImport.Models.WeatherReport;

namespace BamBoard.Services.WeatherImport;

public class Importer
{
    private readonly BamBoardClient bamboardClient;
    private readonly ILogger<Importer> logger;
    private readonly WeatherImportOptions weatherImportOptions;

    public Importer(BamBoardClient bamboardClient, WeatherImportOptions weatherImportOptions, ILogger<Importer> logger)
    {
        this.bamboardClient = bamboardClient;
        this.weatherImportOptions = weatherImportOptions;
        this.logger = logger;
    }

    public async Task RunAsync(CancellationToken cancellationToken)
    {
        using var weatherClient = new HttpClient
        {
            BaseAddress = new(weatherImportOptions.BrightSkyUrl)
        };

        var date = DateTime.Now;
        var weatherLocationsResult = await LoadWeatherLocationsAsync(bamboardClient);

        if (weatherLocationsResult.Errors != null)
        {
            logger.LogError(
                "Could not read weather locations: {errors}",
                JsonSerializer.Serialize(weatherLocationsResult.Errors)
            );

            return;
        }

        logger.LogInformation("Read {amount} weather locations.", weatherLocationsResult.Data.Length);

        foreach (var weatherLocation in weatherLocationsResult.Data)
        {
            WeatherReport weatherReport = default;

            try
            {
                weatherReport = await LoadWeatherReportAsync(weatherClient, weatherLocation);
            }
            catch (Exception exception)
            {
                logger.LogError(
                    "Could not read BrightSky weather report for location {location}: {error}",
                    weatherLocation.Id,
                    exception
                );

                continue;
            }

            WeatherForecast weatherForecastToday = default;

            try
            {
                weatherForecastToday = await LoadWeatherForecastAsync(weatherClient, weatherLocation, date);
            }
            catch (Exception exception)
            {
                logger.LogError(
                    "Could not read BrightSky weather forecast for today for location {location}: {error}",
                    weatherLocation.Id,
                    exception
                );

                continue;
            }

            WeatherForecast weatherForecastTomorrow = default;

            try
            {
                weatherForecastTomorrow = await LoadWeatherForecastAsync(
                    weatherClient,
                    weatherLocation,
                    date.AddDays(1)
                );
            }
            catch (Exception exception)
            {
                logger.LogError(
                    "Could not read BrightSky weather forecast for tomorrow for location {location}: {error}",
                    weatherLocation.Id,
                    exception
                );

                continue;
            }

            var weatherForecastCombined
                = weatherForecastToday.Weather.Concat(weatherForecastTomorrow.Weather[1..]).ToArray();

            var weatherReportCreateInput = BuildWeatherReportAsync(
                weatherLocation.Id,
                date,
                weatherReport.Weather,
                weatherLocation.Organization,
                weatherForecastCombined
            );

            var createWeatherReportResult = await CreateWeatherReportAsync(weatherReportCreateInput, bamboardClient);

            if (createWeatherReportResult.Errors != null)
            {
                logger.LogError(
                    "Could not create weather report for location {location}: {errors}",
                    weatherLocation.Id,
                    JsonSerializer.Serialize(createWeatherReportResult.Errors)
                );

                continue;
            }

            logger.LogInformation("Created weather report for location {location}.", weatherLocation.Id);
            var existingWeatherReportsResult = await LoadExistingWeatherReportIdsAsync(bamboardClient, weatherLocation);

            if (existingWeatherReportsResult.Errors != null)
            {
                logger.LogError(
                    "Could not read weather reports for location {location}: {errors}",
                    weatherLocation.Id,
                    JsonSerializer.Serialize(existingWeatherReportsResult.Errors)
                );

                continue;
            }

            logger.LogInformation(
                "Read {amount} weather reports for location {location}.",
                existingWeatherReportsResult.Data.Length,
                weatherLocation.Id
            );

            foreach (var reportId in existingWeatherReportsResult.Data)
            {
                var filter = new WeatherForecastWhereInput { Report = new() { Id = new() { Equals = reportId } } };
                var orderBy = Array.Empty<WeatherForecastOrderByInput>();

                var existingForecastsResult = await bamboardClient.Query(
                    query => query.WeatherForecasts(filter, orderBy, null, 0, null, forecast => forecast.Id)
                );

                if (existingForecastsResult.Errors != null)
                {
                    logger.LogError(
                        "Could not read weather forecasts for report {report} for location {location}: {errors}",
                        reportId.Value,
                        weatherLocation.Id,
                        JsonSerializer.Serialize(existingForecastsResult.Errors)
                    );

                    continue;
                }

                logger.LogInformation(
                    "Read {amount} weather forecasts for report {report} for location {location}.",
                    existingForecastsResult.Data.Length,
                    reportId.Value,
                    weatherLocation.Id
                );

                var deleteForecastsFilter = existingForecastsResult
                    .Data.Select(forecastId => new WeatherForecastWhereUniqueInput { Id = forecastId })
                    .ToArray();

                var deletedForecastsResult = await bamboardClient.Mutation(
                    mutation => mutation.DeleteWeatherForecasts(deleteForecastsFilter, forecast => forecast.Id)
                );

                if (deletedForecastsResult.Errors != null)
                {
                    logger.LogError(
                        "Could not delete weather forecasts for report {report} for location {location}: {errors}",
                        reportId.Value,
                        weatherLocation.Id,
                        JsonSerializer.Serialize(deletedForecastsResult.Errors)
                    );

                    continue;
                }

                logger.LogInformation(
                    "Deleted {amount} weather forecasts for report {report} for location {location}.",
                    deletedForecastsResult.Data.Length,
                    reportId.Value,
                    weatherLocation.Id
                );

                var deleteReportFilter = new WeatherReportWhereUniqueInput { Id = reportId };

                var deletedReportResult = await bamboardClient.Mutation(
                    mutation => mutation.DeleteWeatherReport(deleteReportFilter, report => report.Id)
                );

                if (deletedReportResult.Errors != null)
                {
                    logger.LogError(
                        "Could not delete weather report {report} for location {location}: {errors}",
                        reportId.Value,
                        weatherLocation.Id,
                        JsonSerializer.Serialize(deletedReportResult.Errors)
                    );

                    continue;
                }

                logger.LogInformation(
                    "Deleted weather report {report} for location {location}.",
                    reportId.Value,
                    weatherLocation.Id
                );
            }
        }
    }

    private static Task<GraphQLResult<ID>> CreateWeatherReportAsync(
        WeatherReportCreateInput weatherReportCreateInput,
        BamBoardClient bamboardClient
    )
    {
        return bamboardClient.Mutation(
            mutation => mutation.CreateWeatherReport(weatherReportCreateInput, report => report.Id)
        );
    }

    private static WeatherReportCreateInput BuildWeatherReportAsync(
        String weatherLocationId,
        DateTime date,
        Weather weatherReport,
        String organization,
        Weather[] weatherForecasts
    )
    {
        var forecastData = weatherForecasts
            .Select(
                forecast => new WeatherForecastCreateInput
                {
                    Timestamp = forecast.Timestamp,
                    Temperature = forecast.Temperature,
                    Icon = forecast.Condition,
                    Organization = new() { Connect = new() { Id = organization } }
                }
            )
            .ToArray();

        return new()
        {
            Timestamp = date,
            Temperature = weatherReport.Temperature,
            Icon = weatherReport.Condition ?? "dry",
            Forecasts = new() { Create = forecastData, Connect = new WeatherForecastWhereUniqueInput[0] },
            Location = new() { Connect = new() { Id = weatherLocationId } },
            Organization = new() { Connect = new() { Id = organization } }
        };
    }

    private static async Task<WeatherReport> LoadWeatherReportAsync(
        HttpClient weatherClient,
        WeatherLocationModel weatherLocation
    )
    {
        var culture = CultureInfo.InvariantCulture;
        var formattedLatitude = Convert.ToString(weatherLocation.Latitude, culture);
        var formattedLongitude = Convert.ToString(weatherLocation.Longitude, culture);
        var url = $"/current_weather?lat={formattedLatitude}&lon={formattedLongitude}";
        return await weatherClient.GetFromJsonAsync<WeatherReport>(url);
    }

    private static async Task<WeatherForecast> LoadWeatherForecastAsync(
        HttpClient weatherClient,
        WeatherLocationModel weatherLocation,
        DateTime timestamp
    )
    {
        var culture = CultureInfo.InvariantCulture;
        var formattedLatitude = Convert.ToString(weatherLocation.Latitude, culture);
        var formattedLongitude = Convert.ToString(weatherLocation.Longitude, culture);
        var url = $"/weather?lat={formattedLatitude}&lon={formattedLongitude}&date={timestamp:yyyy-MM-dd}";
        return await weatherClient.GetFromJsonAsync<WeatherForecast>(url);
    }

    private static Task<GraphQLResult<WeatherLocationModel[]>> LoadWeatherLocationsAsync(BamBoardClient bamboardClient)
    {
        var filter = new WeatherLocationWhereInput();
        var orderBy = new WeatherLocationOrderByInput[0];

        return bamboardClient.Query(
            query => query.WeatherLocations(
                filter,
                orderBy,
                null,
                0,
                null,
                location => new WeatherLocationModel(
                    location.Id,
                    location.Name,
                    location.Longitude,
                    location.Latitude,
                    location.Organization(organization => organization.Id)
                )
            )
        );
    }

    private static Task<GraphQLResult<ID[]>> LoadExistingWeatherReportIdsAsync(
        BamBoardClient bamboardClient,
        WeatherLocationModel weatherLocation
    )
    {
        var filter = new WeatherReportWhereInput
        {
            Location = new()
            {
                Id = new() { Equals = weatherLocation.Id }
            }
        };

        var orderBy = new WeatherReportOrderByInput[] { new() { Timestamp = OrderDirection.Desc } };
        return bamboardClient.Query(query => query.WeatherReports(filter, orderBy, null, 1, null, report => report.Id));
    }
}
