﻿namespace BamBoard.Services.WeatherImport;

public record WeatherImportOptions(
    String BrightSkyUrl
);