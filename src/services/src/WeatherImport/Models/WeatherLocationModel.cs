﻿namespace BamBoard.Services.WeatherImport.Models;

internal record WeatherLocationModel(
    String Id,
    String Name,
    Double? Longitude,
    Double? Latitude,
    String Organization
);
