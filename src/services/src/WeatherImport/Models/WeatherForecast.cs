﻿namespace BamBoard.Services.WeatherImport.Models;

internal record WeatherForecast(
    Weather[] Weather
);