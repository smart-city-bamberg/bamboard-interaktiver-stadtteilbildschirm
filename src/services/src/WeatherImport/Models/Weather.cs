﻿namespace BamBoard.Services.WeatherImport.Models;

internal record Weather(
    String Condition,
    String Icon,
    Double Temperature,
    DateTime Timestamp
);
