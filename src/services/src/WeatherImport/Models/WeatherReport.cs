﻿namespace BamBoard.Services.WeatherImport.Models;

internal record WeatherReport(
    Weather Weather
);
