using System.Globalization;
using System.Text.Json;
using System.Xml.Linq;
using BamBoard.Services.GraphQLClient;
using BamBoard.Services.NewsImport.Models;
using CodeHollow.FeedReader;
using CodeHollow.FeedReader.Feeds;
using Microsoft.Extensions.Logging;
using ZeroQL;

namespace BamBoard.Services.NewsImport;

public class Importer(
    NewsImportOptions newsImportOptions,
    Queries queries,
    Mutations mutations,
    HttpClient httpClient,
    ILogger<Importer> logger
)
{
    public async Task RunAsync(CancellationToken cancellationToken)
    {
        var organizationsResult = await queries.LoadOrganizations(cancellationToken);
        if (organizationsResult.Errors != null) return;

        foreach (var organizationId in organizationsResult.Data ?? [])
        {
            var newsProvidersResult = await queries.LoadNewsProviders(organizationId, cancellationToken);
            if (newsProvidersResult.Errors != null) return;

            foreach (var newsProvider in newsProvidersResult.Data ?? [])
            {
                var existingNewsIdsResult = await queries.LoadExistingNewsIds(
                    organizationId,
                    newsProvider.Id,
                    cancellationToken
                );

                if (existingNewsIdsResult.Errors != null) return;

                try
                {
                    var nachrichten = newsProvider.FeedType switch
                    {
                        "rss" => await LoadNachrichtenRss(newsProvider, cancellationToken),
                        "json" => await LoadNachrichtenJson(newsProvider, cancellationToken),
                        _ => []
                    };

                    await CreateNewsReports(nachrichten, existingNewsIdsResult, organizationId, newsProvider.Id);
                    await DeleteAbandonedNewsReports(existingNewsIdsResult, nachrichten);
                    await DeleteExpiredNewsReports(organizationId);
                }
                catch (Exception exception)
                {
                    logger.LogError(exception, "Error while loading news from provider {0}", newsProvider.Id);
                }
            }
        }
    }

    private async Task DeleteExpiredNewsReports(ID organizationId)
    {
        var olderThan = DateTime.Now.Subtract(newsImportOptions.DeleteThreshold);
        var oldNewsResult = await queries.LoadOldNewsReportIds(organizationId, olderThan);
        if (oldNewsResult.Errors != null) return;

        var oldNewsIdsToDelete = oldNewsResult.Data.Select(news => news.Id).ToArray();
        await mutations.DeleteNewsReports(oldNewsIdsToDelete);
    }

    private async Task DeleteAbandonedNewsReports(
        GraphQLResult<NewsReportIdPair[]> existingNewsIdsResult,
        List<Nachricht> nachrichten
    )
    {
        var newsIdsToDelete = existingNewsIdsResult
            .Data.Where(
                pair => !String.IsNullOrWhiteSpace(pair.ExternalId) &&
                    nachrichten.All(news => pair.ExternalId != Convert.ToString(news.ID))
            )
            .Select(pair => pair.Id)
            .ToArray();

        await mutations.DeleteNewsReports(newsIdsToDelete);
    }

    private async Task CreateNewsReports(
        List<Nachricht> nachrichten,
        GraphQLResult<NewsReportIdPair[]> existingNewsIdsResult,
        ID organizationId,
        String newsProviderId
    )
    {
        var olderThan = DateTime.Now.Subtract(newsImportOptions.DeleteThreshold);

        var newsReportsToCreate = nachrichten
            .Where(
                news => DateTimeOffset.Parse(news.Datum) > olderThan &&
                    !ExternalNewsExists(existingNewsIdsResult.Data, news)
            )
            .ToArray();

        logger.LogCreatingNews(newsReportsToCreate.Length);

        foreach (var nachricht in newsReportsToCreate)
            await CreateNewsReport(organizationId, newsProviderId, nachricht);
    }

    private async Task CreateNewsReport(String organizationId, String newsProviderId, Nachricht nachricht)
    {
        Stream? imageStream = default;
        var imageUrlValid = Uri.TryCreate(nachricht.Bild, UriKind.Absolute, out var imageUri);

        if (imageUrlValid)
            try
            {
                var hasImageProxy = !String.IsNullOrEmpty(newsImportOptions.ImageProxy);
                var imageUrl = hasImageProxy ? $"{newsImportOptions.ImageProxy}/{imageUri}" : imageUri.AbsoluteUri;
                var httpResponseMessage = await httpClient.GetAsync(imageUrl);

                if (httpResponseMessage.IsSuccessStatusCode &&
                    httpResponseMessage.Content.Headers.ContentType.MediaType.StartsWith("image"))
                {
                    imageStream = await httpResponseMessage.Content.ReadAsStreamAsync();
                    logger.LogDownloadedNewsImage(imageUrl);
                }
            }
            catch (Exception exception)
            {
                logger.LogNewsImageNotDownloaded(exception.Message);
            }

        var graphQlResult = await mutations.CreateNewsReport(
            organizationId,
            newsProviderId,
            imageStream,
            nachricht.Titel,
            nachricht.Inhalt,
            nachricht.Vorschau,
            nachricht.Datum,
            nachricht.ID,
            nachricht.Link
        );

        if (graphQlResult.Errors != null) logger.LogCouldNotCreateNews(JsonSerializer.Serialize(graphQlResult.Errors));
        logger.LogCreatedNews();
        if (imageStream != null) await imageStream.DisposeAsync();
        if (imageUrlValid) await Task.Delay(100);
    }

    private Boolean ExternalNewsExists(IEnumerable<NewsReportIdPair> newsIds, Nachricht nachricht)
    {
        return newsIds.Any(existing => existing.ExternalId == nachricht.ID);
    }

    private async Task<List<Nachricht>> LoadNachrichtenRss(
        NewsProvider newsProvider,
        CancellationToken cancellationToken
    )
    {
        var feedResult = await FeedReader.ReadAsync(newsProvider.FeedUrl, cancellationToken);
        if (feedResult is null) return [];

        logger.LogReadNews(feedResult.Items.Count);

        var nachrichten = feedResult
            .Items.Select(
                item => new Nachricht(
                    item.Id ?? item.Link,
                    item.PublishingDateString,
                    item.Title,
                    item.Content,
                    item.SpecificItem switch
                    {
                        Rss20FeedItem rss20FeedItem => rss20FeedItem.Enclosure.Url, _ => String.Empty
                    },
                    "",
                    item.Link,
                    item.Description
                )
            )
            .ToList();

        return nachrichten;
    }

    private async Task<List<Nachricht>> LoadNachrichtenJson(
        NewsProvider newsProvider,
        CancellationToken cancellationToken
    )
    {
        var jsonContent = await httpClient.GetStringAsync(newsProvider.FeedUrl, cancellationToken);
        var jsonData = JsonDocument.Parse(jsonContent);
        var transformationRules = JsonDocument.Parse(newsProvider.Configuration!).RootElement;
        var rssNode = transformationRules.GetProperty("rss");

        // Erstellen des RSS-Feeds
        XNamespace contentNamespace = "http://purl.org/rss/1.0/modules/content/";

        var rssFeed = new XElement(
            "rss",
            new XAttribute(XNamespace.Xmlns + "content", contentNamespace),
            new XAttribute("version", "2.0"),
            new XElement(
                "channel",
                new XElement("title", rssNode.GetProperty("title").GetString()),
                new XElement("link", rssNode.GetProperty("link").GetString()),
                new XElement("description", rssNode.GetProperty("description").GetString()),
                new XElement("lastBuildDate", DateTime.Now.ToString("R"))
            )
        );

        foreach (var item in jsonData.RootElement.EnumerateArray())
        {
            var feedItem = new XElement("item");

            foreach (var rule in transformationRules.GetProperty("items").EnumerateArray())
            {
                var jsonPath = rule.GetProperty("json_path").GetString();
                var rssField = rule.GetProperty("rss_field").GetString();
                var value = item.GetProperty(jsonPath!).GetString();

                if (rssField == "pubDate" && rule.TryGetProperty("date_format", out var dateFormatProperty))
                {
                    var dateFormat = dateFormatProperty.GetString();

                    feedItem.Add(
                        new XElement(
                            rssField,
                            DateTime.ParseExact(value, dateFormat, CultureInfo.InvariantCulture).ToString("R")
                        )
                    );
                }
                else if (rssField == "content")
                {
                    feedItem.Add(new XElement(contentNamespace + "encoded", new XCData(value)));
                }
                else if (rssField == "image")
                {
                    feedItem.Add(
                        new XElement("enclosure", new XAttribute("url", value), new XAttribute("type", "image/jpeg"))
                    );
                }
                else
                {
                    feedItem.Add(new XElement(rssField, value));
                }
            }

            rssFeed.Element("channel").Add(feedItem);
        }

        using var xmlStream = new MemoryStream();
        await rssFeed.SaveAsync(xmlStream, SaveOptions.None, cancellationToken);
        xmlStream.Position = 0;
        var feedResult = FeedReader.ReadFromByteArray(xmlStream.ToArray());
        logger.LogReadNews(feedResult.Items.Count);

        var nachrichten = feedResult
            .Items.Select(
                item =>
                {
                    var imageUrl = "";
                    if (item.SpecificItem is Rss20FeedItem item20) imageUrl = item20.Enclosure.Url;

                    return new Nachricht(
                        item.Id,
                        item.PublishingDateString,
                        item.Title,
                        item.Content,
                        imageUrl,
                        "",
                        item.Link,
                        item.Description
                    );
                }
            )
            .ToList();

        return nachrichten;
    }
}
