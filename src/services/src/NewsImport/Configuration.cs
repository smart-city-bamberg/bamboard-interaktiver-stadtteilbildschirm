using Microsoft.Extensions.DependencyInjection;

namespace BamBoard.Services.NewsImport;

public static class Configuration
{
    public static IServiceCollection AddNewsImporter(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddHttpClient<Importer>("NewsImporter");
        return serviceCollection.AddScoped<Queries>().AddScoped<Mutations>().AddScoped<Importer>();
    }
}
