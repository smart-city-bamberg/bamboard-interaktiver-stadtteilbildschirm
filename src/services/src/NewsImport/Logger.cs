using Microsoft.Extensions.Logging;

namespace BamBoard.Services.NewsImport;

public static partial class Logger
{
    [LoggerMessage(Level = LogLevel.Information, Message = "Read {amount} Veranstaltungen from XML feed.")]
    public static partial void LogReadNews(this ILogger logger, Int32 amount);

    [LoggerMessage(Level = LogLevel.Error, Message = "Could not load organizations: {errors}")]
    public static partial void LogCouldNotLoadOrganizations(this ILogger logger, String errors);

    [LoggerMessage(Level = LogLevel.Information, Message = "Read {amount} organizations.")]
    public static partial void LogReadOrganizations(this ILogger logger, Int32 amount);

    [LoggerMessage(Level = LogLevel.Information, Message = "Loading news providers for organization {organization}.")]
    public static partial void LogLoadingNewsProviders(this ILogger logger, String organization);

    [LoggerMessage(Level = LogLevel.Error, Message = "Could not read news providers: {errors}")]
    public static partial void LogCouldNotReadNewsProviders(this ILogger logger, String errors);

    [LoggerMessage(
        Level = LogLevel.Information,
        Message = "Loaded data providers for organization {organization}, providers: {providers}"
    )]
    public static partial void LogLoadedNewsProviders(this ILogger logger, String organization, String providers);

    [LoggerMessage(Level = LogLevel.Information, Message = "Importing news for organization {organization}.")]
    public static partial void LogImportingNews(this ILogger logger, String organization);

    [LoggerMessage(Level = LogLevel.Error, Message = "Could not read news: {errors}")]
    public static partial void LogCouldNotReadNews(this ILogger logger, String errors);

    [LoggerMessage(Level = LogLevel.Information, Message = "Read {amount} external news ids.")]
    public static partial void LogReadExternalNewsIds(this ILogger logger, Int32 amount);

    [LoggerMessage(Level = LogLevel.Information, Message = "Creating {newsCount} news")]
    public static partial void LogCreatingNews(this ILogger logger, Int32 newsCount);

    [LoggerMessage(Level = LogLevel.Error, Message = "Could not create news: {errors}")]
    public static partial void LogCouldNotCreateNews(this ILogger logger, String errors);

    [LoggerMessage(Level = LogLevel.Information, Message = "Created news.")]
    public static partial void LogCreatedNews(this ILogger logger);

    [LoggerMessage(Level = LogLevel.Information, Message = "Deleted {amount} news.")]
    public static partial void LogDeletedNewsReports(this ILogger logger, Int32 amount);

    [LoggerMessage(Level = LogLevel.Error, Message = "Could not delete news: {errors}")]
    public static partial void LogCouldNotDeleteNewsReports(this ILogger logger, String errors);

    [LoggerMessage(Level = LogLevel.Information, Message = "Read {amount} news older than {olderThan}.")]
    public static partial void LogReadOldNewsReports(this ILogger logger, Int32 amount, DateTime olderThan);

    [LoggerMessage(Level = LogLevel.Error, Message = "Could not read news older than {olderThan}: {errors}")]
    public static partial void LogCouldNotReadOldNewsReports(this ILogger logger, DateTime olderThan, String errors);

    [LoggerMessage(Level = LogLevel.Information, Message = "Deleted {amount} news older than {olderThan}.")]
    public static partial void LogDeletedOldNews(this ILogger logger, Int32 amount, DateTime olderThan);

    [LoggerMessage(Level = LogLevel.Information, Message = "Downloaded news image from {url}")]
    public static partial void LogDownloadedNewsImage(this ILogger logger, String url);

    [LoggerMessage(Level = LogLevel.Warning, Message = "News image could not be downloaded: {error}")]
    public static partial void LogNewsImageNotDownloaded(this ILogger logger, String error);
}
