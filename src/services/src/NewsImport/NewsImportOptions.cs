﻿namespace BamBoard.Services.NewsImport;

public record NewsImportOptions(TimeSpan DeleteThreshold, String ImageProxy);
