﻿using System.Text.Json;

namespace BamBoard.Services.NewsImport.Models;

internal record Nachricht(
    String ID,
    String Datum,
    String Titel,
    String Inhalt,
    String Bild,
    String Bildrechte,
    String Link,
    String Vorschau
);
