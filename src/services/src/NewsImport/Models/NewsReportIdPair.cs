﻿namespace BamBoard.Services.NewsImport.Models;

public record NewsReportIdPair(String Id, String ExternalId);
