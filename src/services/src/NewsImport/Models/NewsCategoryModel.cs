﻿using ZeroQL;

namespace BamBoard.Services.NewsImport.Models;

internal record NewsCategoryModel(
    ID Id, String Name, Int32? NewsCount = 0
);