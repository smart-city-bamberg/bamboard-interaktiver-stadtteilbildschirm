﻿namespace BamBoard.Services.NewsImport.Models;

internal record NewsIdPair(
    String Id, String ExternalId
);