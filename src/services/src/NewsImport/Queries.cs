using System.Text.Json;
using BamBoard.Services.GraphQLClient;
using BamBoard.Services.NewsImport.Models;
using Microsoft.Extensions.Logging;
using ZeroQL;

namespace BamBoard.Services.NewsImport;

public class Queries(BamBoardClient bamboardClient, ILogger<Queries> logger)
{
    public async Task<GraphQLResult<NewsProvider[]?>> LoadNewsProviders(
        ID organizationId,
        CancellationToken cancellationToken
    )
    {
        var where = new NewsProviderWhereInput { Organization = new() { Id = new() { Equals = organizationId } } };
        var orderBy = Array.Empty<NewsProviderOrderByInput>();
        logger.LogLoadingNewsProviders(organizationId);

        var newsProvidersResult = await bamboardClient.Query(
            query => query.NewsProviders(
                where,
                orderBy,
                null,
                0,
                null,
                provider => new NewsProvider
                {
                    Id = provider.Id, Name = provider.Name, FeedType = provider.FeedType,
                    Configuration = provider.Configuration, FeedUrl = provider.FeedUrl
                }
            ),
            cancellationToken
        );

        if (newsProvidersResult.Errors != null)
            logger.LogCouldNotReadNewsProviders(JsonSerializer.Serialize(newsProvidersResult.Errors));
        else
            logger.LogLoadedNewsProviders(organizationId, JsonSerializer.Serialize(newsProvidersResult.Data));

        return newsProvidersResult;
    }

    public async Task<GraphQLResult<ID[]?>> LoadOrganizations(CancellationToken cancellationToken)
    {
        var filter = new OrganizationWhereInput();
        var orderBy = Array.Empty<OrganizationOrderByInput>();

        var graphQlResult = await bamboardClient.Query(
            query => query.Organizations(filter, orderBy, null, 0, null, organization => organization.Id),
            cancellationToken
        );

        if (graphQlResult.Errors != null)
            logger.LogCouldNotLoadOrganizations(JsonSerializer.Serialize(graphQlResult.Errors));
        else
            logger.LogReadOrganizations(graphQlResult.Data!.Length);

        return graphQlResult;
    }

    public async Task<GraphQLResult<NewsReportIdPair[]>> LoadExistingNewsIds(
        String organizationId,
        String newsProviderId,
        CancellationToken cancellationToken
    )
    {
        var filter = new NewsReportWhereInput
        {
            ExternalId = new()
            {
                Not = new()
                {
                    Equals = ""
                }
            },
            Organization = new()
            {
                Id = new()
                {
                    Equals = organizationId
                }
            },
            NewsProvider = new()
            {
                Id = new()
                {
                    Equals = newsProviderId
                }
            }
        };

        var orderBy = Array.Empty<NewsReportOrderByInput>();
        logger.LogImportingNews(organizationId);

        var graphQlResult = await bamboardClient.Query(
            query => query.NewsReports(
                filter,
                orderBy,
                null,
                0,
                null,
                @event => new NewsReportIdPair(@event.Id, @event.ExternalId)
            ),
            cancellationToken
        );

        if (graphQlResult.Errors != null)
            logger.LogCouldNotReadNews(JsonSerializer.Serialize(graphQlResult.Errors));
        else
            logger.LogReadExternalNewsIds(graphQlResult.Data.Length);

        return graphQlResult;
    }

    public async Task<GraphQLResult<NewsReportIdPair[]>> LoadOldNewsReportIds(String organizationId, DateTime olderThan)
    {
        var filter = new NewsReportWhereInput
        {
            Organization = new()
            {
                Id = new() { Equals = organizationId }
            },
            Timestamp = new()
            {
                Lt = new DateTimeOffset(olderThan)
            }
        };

        var orderBy = new NewsReportOrderByInput[]
        {
            new() { Timestamp = OrderDirection.Asc }
        };

        var graphQlResult = await bamboardClient.Query(
            query => query.NewsReports(
                filter,
                orderBy,
                null,
                0,
                null,
                @event => new NewsReportIdPair(@event.Id, @event.ExternalId)
            )
        );

        if (graphQlResult.Errors != null)
            logger.LogCouldNotReadOldNewsReports(olderThan, JsonSerializer.Serialize(graphQlResult.Errors));
        else
            logger.LogReadOldNewsReports(graphQlResult.Data.Length, olderThan);

        return graphQlResult;
    }
}
