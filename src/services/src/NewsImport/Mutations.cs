using System.Text.Json;
using BamBoard.Services.GraphQLClient;
using Microsoft.Extensions.Logging;
using ZeroQL;

namespace BamBoard.Services.NewsImport;

public class Mutations(BamBoardClient bamBoardClient, ILogger<Mutations> logger)
{
    public async Task<GraphQLResult<ID>> CreateNewsReport(
        String organizationId,
        String newsProviderId,
        Stream? imageStream,
        String title,
        String content,
        String preview,
        String date,
        String externalId,
        String link
    )
    {
        var newsReportCreateInput = new NewsReportCreateInput
        {
            Title = title,
            Category = "Nachrichten",
            Content = JsonSerializer.SerializeToElement(
                new[]
                {
                    new
                    {
                        type = "paragraph",
                        children = new[]
                        {
                            new
                            {
                                text = !String.IsNullOrWhiteSpace(content) ? content : preview
                            }
                        }
                    }
                }
            ),
            PreviewContent = String.IsNullOrWhiteSpace(preview) ? " " : preview,
            Timestamp = DateTimeOffset.Parse(date),
            ExternalId = !String.IsNullOrEmpty(externalId) ? Convert.ToString(externalId) : link,
            Link = link,
            Organization = new() { Connect = new() { Id = organizationId } },
            NewsProvider = new()
            {
                Connect = new()
                {
                    Id = newsProviderId
                }
            }
        };

        if (imageStream is not null)
            newsReportCreateInput.Image = new()
            {
                Upload = new("image.jpg", imageStream)
            };

        var graphQlResult = await bamBoardClient.Mutation(
            mutation => mutation.CreateNewsReport(newsReportCreateInput, @event => @event.Id)
        );

        return graphQlResult;
    }

    public async Task<GraphQLResult<ID[]>> DeleteNewsReports(String[] newsReportIds)
    {
        var filter = newsReportIds.Select(id => new NewsReportWhereUniqueInput { Id = id }).ToArray();

        var graphQlResult
            = await bamBoardClient.Mutation(mutation => mutation.DeleteNewsReports(filter, @event => @event.Id));

        if (graphQlResult.Errors != null)
            logger.LogCouldNotDeleteNewsReports(JsonSerializer.Serialize(graphQlResult.Errors));
        else
            logger.LogDeletedNewsReports(graphQlResult.Data.Length);

        return graphQlResult;
    }
}
