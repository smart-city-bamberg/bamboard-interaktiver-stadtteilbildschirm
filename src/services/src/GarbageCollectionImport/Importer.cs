﻿using System.Text.Json;
using BamBoard.Services.GraphQLClient;
using Ical.Net;
using Microsoft.Extensions.Logging;
using ZeroQL;

namespace BamBoard.Services.GarbageCollectionImport;

public class Importer
{
    private readonly BamBoardClient bamboardClient;
    private readonly ILogger<Importer> logger;

    public Importer(BamBoardClient bamboardClient, ILogger<Importer> logger)
    {
        this.bamboardClient = bamboardClient;
        this.logger = logger;
    }

    public async Task RunAsync(CancellationToken cancellationToken)
    {
        var monitorsResult = await LoadMonitorModelsAsync(bamboardClient);

        if (monitorsResult.Errors != null)
        {
            logger.LogError("Could not load monitors: {errors}", JsonSerializer.Serialize(monitorsResult.Errors));
            return;
        }

        logger.LogInformation("Read {amount} monitors.", monitorsResult.Data.Length);

        foreach (var monitor in monitorsResult.Data)
        {
            var existingGarbageCollectionsResult = await LoadGarbageCollections(monitor.Id, bamboardClient);

            if (existingGarbageCollectionsResult.Errors != null)
            {
                logger.LogError(
                    "Could not load garbage collections for monitor {id}: {errors}",
                    monitor.Id,
                    JsonSerializer.Serialize(existingGarbageCollectionsResult.Errors)
                );

                return;
            }

            logger.LogInformation(
                "Read {amount} garbage collections for monitor {id}.",
                existingGarbageCollectionsResult.Data.Length,
                monitor.Id
            );

            if (monitor.GarbageCollectionCalendar == null)
            {
                logger.LogInformation(
                    "Monitor {id} has no garbage collection calendar. Deleting abandoned garbage collections.",
                    monitor.Id
                );

                var abandonedGarbageCollectionIdsToDelete = existingGarbageCollectionsResult
                    .Data.Select(collection => collection.Id)
                    .ToArray();

                var abandonedGarbageCollectionsDeleteResult = await DeleteGarbageCollectionsAsync(
                    bamboardClient,
                    abandonedGarbageCollectionIdsToDelete
                );

                if (abandonedGarbageCollectionsDeleteResult.Errors != null)
                {
                    logger.LogError(
                        "Could not delete abandoned garbage collections for monitor {id}: {errors}",
                        monitor.Id,
                        JsonSerializer.Serialize(abandonedGarbageCollectionsDeleteResult.Errors)
                    );

                    return;
                }

                logger.LogInformation(
                    "Deleted {amount} abandoned garbage collections for monitor {id}.",
                    abandonedGarbageCollectionsDeleteResult.Data.Length,
                    monitor.Id
                );

                continue;
            }

            var iCalFileUrl = monitor.GarbageCollectionCalendar.Url;
            var fileName = monitor.GarbageCollectionCalendar.FileName;
            var organizationId = monitor.OrganizationId;

            var hasGarbageCollections
                = existingGarbageCollectionsResult.Data.Any(collection => collection.Source == fileName);

            if (hasGarbageCollections)
            {
                logger.LogInformation(
                    "Monitor {id} has {amount} garbage collections. Skipping import.",
                    monitor.Id,
                    existingGarbageCollectionsResult.Data.Length
                );

                continue;
            }

            var garbageCollectionIdsToDelete = existingGarbageCollectionsResult
                .Data.Where(collection => collection.Source != fileName)
                .Select(collection => collection.Id)
                .ToArray();

            var deleteGarbageCollectionsResult = await DeleteGarbageCollectionsAsync(
                bamboardClient,
                garbageCollectionIdsToDelete
            );

            if (deleteGarbageCollectionsResult.Errors != null)
            {
                logger.LogError(
                    "Could not delete garbage collections for monitor {id}: {errors}",
                    monitor.Id,
                    JsonSerializer.Serialize(deleteGarbageCollectionsResult.Errors)
                );

                continue;
            }

            logger.LogInformation(
                "Deleted {amount} garbage collections for monitor {id}.",
                deleteGarbageCollectionsResult.Data.Length,
                monitor.Id
            );

            HttpResponseMessage? calendarHttpResonse = default;

            try
            {
                calendarHttpResonse = await bamboardClient.HttpHandler.SendAsync(
                    new(HttpMethod.Get, iCalFileUrl),
                    cancellationToken
                );
            }
            catch (Exception exception)
            {
                logger.LogError("Could not load calendar file {file}: {error}", iCalFileUrl, exception);
                continue;
            }

            using var stream = await calendarHttpResonse.Content.ReadAsStreamAsync();
            using var fileReader = new StreamReader(stream);
            var calendars = CalendarCollection.Load(fileReader);
            var calendar = calendars.First();
            var assignment = monitor.GarbageCollectionAssignment;

            var keysValues = new Dictionary<String, String>
                {
                    { "restmuell", assignment.Restmuell },
                    { "gelber_sack", assignment.Gelber_Sack },
                    { "biomuell", assignment.Biomuell },
                    { "papier", assignment.Papier }
                }
                .Where(_ => !String.IsNullOrEmpty(_.Value))
                .ToDictionary(_ => _.Value, _ => _.Key);

            var garbageCollections = BuildGarbageCollections(
                monitor.Id,
                calendar,
                keysValues,
                fileName,
                organizationId
            );

            var createGarbageCollectionsResult
                = await CreateGarbageCollectionsAsync(bamboardClient, garbageCollections);

            if (createGarbageCollectionsResult.Errors != null)
            {
                logger.LogError(
                    "Could not create garbage collections for monitor {id}: {errors}",
                    monitor.Id,
                    JsonSerializer.Serialize(createGarbageCollectionsResult.Errors)
                );

                continue;
            }

            logger.LogInformation(
                "Created {amount} garbage collections for monitor {id}.",
                createGarbageCollectionsResult.Data.Length,
                monitor.Id
            );
        }
    }

    private static Task<GraphQLResult<MonitorModel[]>> LoadMonitorModelsAsync(BamBoardClient bamboardClient)
    {
        var monitorFilter = new MonitorWhereInput();
        var orderBy = Array.Empty<MonitorOrderByInput>();

        return bamboardClient.Query(
            query => query.Monitors(
                monitorFilter,
                orderBy,
                null,
                0,
                null,
                monitor => new MonitorModel(
                    monitor.Id,
                    monitor.Name,
                    monitor.Organization(organization => organization.Id),
                    monitor.GarbageCollectionAssignment(
                        assignment => new GarbageCollectionAssignmentModel(
                            assignment.Restmuell,
                            assignment.Biomuell,
                            assignment.Gelber_sack,
                            assignment.Papier
                        )
                    ),
                    monitor.GarbageCollectionCalendar(
                        calendar => new GarbageCollectionCalendarModel(calendar.Filename, calendar.Url)
                    )
                )
            )
        );
    }

    private static Task<GraphQLResult<GarbageCollectionModel[]>> LoadGarbageCollections(
        String monitorId,
        BamBoardClient bamboardClient
    )
    {
        var monitorFilter = new GarbageCollectionWhereInput { Monitor = new() { Id = new() { Equals = monitorId } } };
        var orderBy = new GarbageCollectionOrderByInput[0];

        return bamboardClient.Query(
            query => query.GarbageCollections(
                monitorFilter,
                orderBy,
                null,
                0,
                null,
                collection => new GarbageCollectionModel(
                    collection.Id,
                    collection.Type,
                    collection.Timestamp,
                    collection.Source
                )
            )
        );
    }

    private static Task<GraphQLResult<ID[]>> DeleteGarbageCollectionsAsync(
        BamBoardClient bamboardClient,
        String[] garbageCollectionIds
    )
    {
        var input = garbageCollectionIds
            .Select(collectionId => new GarbageCollectionWhereUniqueInput { Id = collectionId })
            .ToArray();

        return bamboardClient.Mutation(
            mutation => mutation.DeleteGarbageCollections(input, collection => collection.Id)
        );
    }

    private static GarbageCollectionCreateInput[] BuildGarbageCollections(
        String monitorId,
        Calendar calendar,
        Dictionary<String, String> garbageTypes,
        String fileName,
        String organizationId
    )
    {
        return calendar
            .Events.Where(_ => garbageTypes.Keys.Contains(_.Summary.Trim()))
            .Select(
                calendarEvent => new GarbageCollectionCreateInput
                {
                    Type = garbageTypes[calendarEvent.Summary.Trim()],
                    Timestamp = calendarEvent.DtStart.AsUtc,
                    Monitor = new()
                    {
                        Connect = new() { Id = monitorId }
                    },
                    Source = fileName,
                    Organization = new()
                    {
                        Connect = new()
                        {
                            Id = organizationId
                        }
                    }
                }
            )
            .ToArray();
    }

    private static Task<GraphQLResult<ID[]>> CreateGarbageCollectionsAsync(
        BamBoardClient bamboardClient,
        GarbageCollectionCreateInput[] garbageCollections
    )
    {
        return bamboardClient.Mutation(
            mutation => mutation.CreateGarbageCollections(garbageCollections, collection => collection.Id)
        );
    }
}
