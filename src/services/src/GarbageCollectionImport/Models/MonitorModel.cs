﻿internal record MonitorModel(
    String Id,
    String Name,
    String OrganizationId,
    GarbageCollectionAssignmentModel GarbageCollectionAssignment,
    GarbageCollectionCalendarModel GarbageCollectionCalendar
);
