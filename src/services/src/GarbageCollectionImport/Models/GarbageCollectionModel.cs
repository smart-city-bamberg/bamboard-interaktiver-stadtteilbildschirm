﻿internal record GarbageCollectionModel(
    String Id,
    String Type,    
    DateTimeOffset? TimeStamp,
    String Source
);