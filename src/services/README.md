# BamBoard Services

Dieses Projekt enthält die Hintergrunddienste zum Import von Wetterdaten, Nachrichten und Entsorgungsterminen für das BamBoard CMS.

Quelle der Wetterdaten:
- https://api.brightsky.dev (https://brightsky.dev/)

Quelle der Veranstaltungen:
  - http://kultur.bamberg.de/export/smartcity_xmlexport.xml (http://kultur.bamberg.de)
---

## Lokales Ausführen ohne Docker

Um das Projekt lokal auszuführen, werden das Dotnet SDK 7 und optional Visual Studio 2022 benötigt.

1. Geben Sie in der Datei `src/Host/appsettings.Development.json` im Abschnitt `BamBoard` die Zugangsdaten für den CMS-Benutzer an.

Für das Starten ohne Docker muss hier außerdem die URL für das CMS angepasst werden. Für eine lokale Entwicklungsumgebung ist das typischerweise `http://localhost:3000`

### Starten über Visual Studio

1. Öffnen Sie die Datei `Bamboard.Services.sln` in Visual Studio.
2. Wählen Sie den "Host" als Startprojekt aus.
3. Drücken Sie die F5-Taste, um das Projekt mit dem Debugger zu starten.

### Starten über das Terminal

1. Navigieren Sie zum Verzeichnis `src/Host`.
2. Geben Sie den Befehl `dotnet run` ein, um das Projekt zu starten.

---

## Starten über Docker

Um das Projekt mit Docker zu starten, können Sie Docker Compose und eine Override-Datei verwenden. Hier ist eine Anleitung:

⚠️ Für den produktiven Einsatz muss der Host Service im selben Docker-Netzwerk sein wie das CMS!

1. Stellen Sie sicher, dass Docker und Docker Compose auf Ihrem System installiert sind.
2. Geben Sie den Befehl `docker-compose -f docker-compose.yml -f docker-compose.override.yml up -d` ein, um das Projekt mit den in der Override-Datei angegebenen Konfigurationen zu starten.




---

## Cronjobs

In der Datei `src/Host/appsettings.Development.json` im Abschnitt `CronJobs` sind die Ausführungsintervalle aller Services als im crontab-Format definiert und können angepasst werden.

Der Standartwert ist jeweils *"einmal zu jeder vollen Minute"* bzw. `0 * * ? * *`

