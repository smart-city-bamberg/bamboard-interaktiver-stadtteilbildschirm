# "BamBoard" - Interaktiver Stadtteilbildschirm

Das vorliegende Projekt ist im Rahmen des [Smart City Bamberg](https://smartcity.bamberg.de/) Programms entstanden und stellt ein niederschwelliges lokales Informationsangebot für Quartiersbewohner bereit. Das Web-Frontend kann über einen handelsüblichen Touch-Bildschirm ausgespielt werden und ist auch für große Bildschirmdiagonalen optimiert. Zusätzlich im Repository enthalten ist ein Content-Management-System (CMS) zur redaktionellen Verwaltungen von Informationen und Bildschirmen.

<img src="docs/Bildschirm.png" alt="Bildschirmbeispiel" width="950"/>

Zu den Hauptfunktionen des Web-Frontend gehören

1. **Hausinformationen**: Bereitstellung von Nachrichten, Kontakten & Dokumenten rund um das Haus, in dem der Bildschirm installiert ist.
2. **ÖPNV**: Informationen zum öffentlichen Nachverkehr, wie Abfahrpläne zu nahegelegenen Bus- und Bahnhaltestelle.
3. **Nachrichten**: Lokale Nachrichten zu aktuellen Ereignissen und Geschehnissen.
4. **Veranstaltungen**: Kulturkalender zu Konzert-, Theater- und Ausstellungsveranstaltungen.

## Architektur

<img src="docs/Architektur.jpg" alt="Bildschirmbeispiel" width="510"/>

Das Projekt besteht aus folgenden 3 Unterprojekten
* [Frontend](src/frontend/README.md): Angular Web-App 
* [CMS](src/cms/README.md): Keystone 6 CMS
* [Services](src/services/README.md): .NET Services

Dokumentation ist in den einzelnen Unterprojekten im entsprechenden Verzeichnis hinterlegt.

## Förderung

Dieses Projekt wurde gefördert durch das  Bundesministerium des Innern, für Bau und Heimat and die Kreditanstalt für Wiederaufbau.

<img src="docs/Logo_BMI.jpg" alt="BMI" width="250"/>
<img src="docs/KFW.png" alt="KFW" width="200"/>
